<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class CompanyController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }


    public function UpdateCompanyDetails()
    {
    	$data['lang'] = $this->input->post('language', 'eng');
    	$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Company Details';
        $data['company'] = $this->company->getDetails($data['lang']);

        if(!empty($this->input->post('submit'))) {

        	$name = $this->input->post('name');
        	$address = $this->input->post('address');
        	$telephone = $this->input->post('telephone');
        	$email = $this->input->post('email');
        	$meta_key = $this->input->post('meta_key');
        	$meta_desc = $this->input->post('meta_desc');

        	$request = array(
        		'telephone' => $telephone,
        		'meta_key' => $meta_key,
        		'meta_description' => $meta_desc,
        		'email' => $email,
        		'name_'.$data['lang'] => $name,
        		'address_'.$data['lang'] => $address,
                'updated_at' => date('Y-m-d'),
        		);

        	$update_id = $this->company->update(1, $request);
        	if($update_id > 0) {
        		redirect(base_url().'inbound-admin/company-details');
        	}

        }

        $this->load->view('admin/manage-company', $data);
    }




} // end controller
?>