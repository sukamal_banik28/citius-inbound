<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

	public function login()
    {
        $data['pagetitle'] = 'Login';
        $data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';

        if (!empty($this->input->post('submit'))) {
            $this->form_validation->set_rules('loginID', 'Email or Phone or Registration Number', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {
                $loginID = $this->security->xss_clean($this->input->post('loginID'));
                $password = $this->security->xss_clean($this->input->post('password'));

                $input = array('loginID' => $loginID,
                    'password' => hash('sha256', $password)
                );

                $user = $this->users->doLogin($input);

                if (!empty($user)) {

                	// set session data
                    $sessionData = array(
                        'user_id' => $user['id'],
                        'role_id' => $user['role_id'],
                    	);

                    $this->session->set_userdata($sessionData);

                    switch ($user['role_id']) {

                        case '1':
                            redirect(base_url().'inbound-admin/dashboard');
                            break;

                        case '2':
                            redirect(base_url().'user/manage-profile?lang='.$data['lang']);
                            break;
                             
                        default:
                            redirect('/');
                            $this->session->sess_destroy();
                            break;
                    }

                } else {
                    $data['errorMsg'] = 'Invalid Login ID or Password or Your account is not Active. Please try forget password option or ask site administrator.';
                }
            }
        }
        $this->load->view('admin/login', $data);
    }

 	


 	public function logout()
 	{
        session_destroy ();
        redirect(base_url().'inbound-admin/login');
 	}


} // end controller
