<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class BannerController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }


    public function manageBanners()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Banners';
        $data['banners'] = $this->banner->getBanners();
        $this->load->view('admin/manage-banners', $data);
    }


    public function addNewBanners()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Add Banner';

        if(!empty($this->input->post('submit'))) {

            $lang = 'eng'; // Default language English

            $status = $this->security->xss_clean($this->input->post('status'));
            $order = $this->security->xss_clean($this->input->post('order'));
            $position = $this->security->xss_clean($this->input->post('position'));
            $slogan = $this->security->xss_clean($this->input->post('slogan'));
            $slogan_spanish = $this->security->xss_clean($this->input->post('slogan_spanish'));
            // $slogan_french = $this->security->xss_clean($this->input->post('slogan_french'));

            $input = array(
                'slogan_'.$lang => $slogan,
                'slogan_spanish' => $slogan_spanish,
                // 'slogan_french' => $slogan_french,
                'status' => $status,
                'order_no' => $order,
                'flag' => 'inbound',
                'position' => $position,
                );

            $banner_id = $this->banner->store($input);

            if($banner_id > 0) {

                if( !empty($_FILES['banner_image']['name']) && !empty($_FILES['banner_image']['name'][0]) )
                    {
                        $flag = 'banner_image';
                        $file_path = 'assets/uploads/banners/';
                        file_upload($_FILES['banner_image'], $banner_id, 'Banner', $file_path, $flag);
                    }


                if( !empty($_FILES['banner_video']['name']) && !empty($_FILES['banner_video']['name'][0]) )
                    {
                        $flag = 'banner_video';
                        $file_path = 'assets/uploads/banners/';
                        file_upload($_FILES['banner_video'], $banner_id, 'Banner', $file_path, $flag);
                    }

                redirect(base_url().'inbound-admin/manage-banners');
            } // end if
        } // end submit
        $this->load->view('admin/add-banner', $data);
    }


    public function updateBanners($banner_id)
    {
    	$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Banner Details';
        $append = " AND banner.`id` = ".$banner_id;
        $data['banner'] = $this->banner->getBanners($append)[0];
        // $data['banner'] = $this->banner->getBannerDetails( $banner_id );

        if(!empty($this->input->post('submit'))) { 

            $status = $this->security->xss_clean($this->input->post('status'));
            $order = $this->security->xss_clean($this->input->post('order'));
            $position = $this->security->xss_clean($this->input->post('position'));
            $slogan = $this->security->xss_clean($this->input->post('slogan'));
            $slogan_spanish = $this->security->xss_clean($this->input->post('slogan_spanish', NULL));

            $request = array(
                'slogan_eng' => $slogan,
                'slogan_spanish' => $slogan_spanish,
                'status' => $status,
                'order_no' => $order,
                'position' => $position,
                );

        	$update_id = $this->banner->update( $banner_id, $request );

        	    if($update_id > 0) {

                    if( !empty($_FILES['banner_image']['name']) && !empty($_FILES['banner_image']['name'][0]) ) 
                    {
                        $flag = 'banner_image';
                        $files = $this->files->getFilesByFileableId( $banner_id, 'Banner', $flag );
                        $path = FCPATH.$files['file_path'].$files['file_name'];
                        unlink($path);
                        $this->files->delete($files['id']);
                        $file_path = 'assets/uploads/banners/';

                        if($position == 'background') {
                            $file_name = 'background-home.jpg';
                            file_upload($_FILES['banner_image'], $banner_id, 'Banner', $file_path, $flag, $file_name);
                        } else {
                            file_upload($_FILES['banner_image'], $banner_id, 'Banner', $file_path, $flag);
                        }


                        // if( $position == 'header') {                            
                        //     $file_name = 'home_section_1.jpg';
                        //     file_upload($_FILES['banner_image'], $banner_id, 'Banner', $file_path, $flag, $file_name);
                        // } else {
                        // }




                        // unlink($path);
                        // $this->files->delete($files['id']);
                        // $file_path = 'assets/uploads/banners/';
                        // file_upload($_FILES['banner_image'], $banner_id, 'Banner', $file_path, $flag);
                        
                    }


                    if( !empty($_FILES['banner_video']['name']) && !empty($_FILES['banner_video']['name'][0]) )
                    {
                        $flag = 'banner_video';
                        $files = $this->files->getFilesByFileableId( $banner_id, 'Banner', $flag );
                        $path = FCPATH.$files['file_path'].$files['file_name'];
                        unlink($path);
                        $this->files->delete($files['id']);

                        $file_path = 'assets/uploads/banners/';
                        file_upload($_FILES['banner_video'], $banner_id, 'Banner', $file_path, $flag);
                    }


        		  redirect(base_url().'inbound-admin/manage-banners');
        	   }

            } // end submit
        $this->load->view('admin/edit-banner', $data);
    }



} // end controller
?>