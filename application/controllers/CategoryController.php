<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class CategoryController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }



    public function getSubCategoriesByAjax()
    {
    	$categories = $this->category->getCategoriesByFlag('package');
        $parent_ids = $_POST['category_ids'];

        if(!empty($categories)) {
            foreach ($categories as $cat_key => $cat_value) {
                foreach ($parent_ids as $key => $value) {
                    if($cat_value['parent_id'] == $value) {
                        $temp[$cat_key] = $cat_value;
                    }
                }
            }

            foreach ($temp as $cat_key => $cat_value) {
                echo '<option value='.$cat_value['id'].'>'.$cat_value['name_eng'].'</option>';
            }
        }
    }



    public function manageCategories()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Category';
        $data['package_seourls'] = $this->category->getCategoriesByFlag('package');
        $data['blog_seourls'] = $this->category->getCategoriesByFlag('blog');
        $this->load->view('admin/manage-categories', $data);
    }


    public function updateCategory( $category_id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Category';
        $data['category'] = $this->category->getCategoryDetailsByID( $category_id );

            if(!empty($this->input->post('submit')))
            {
                /* Set Validation rule  in the form */
                $this->form_validation->set_rules('seo_url', 'Seo URL', 'trim|callback_seourl_check[' . $category_id . ']');
                
                if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
                } else {
                    $name = $this->security->xss_clean($this->input->post('name'));
                    $name_spanish = $this->security->xss_clean($this->input->post('name_spanish'));
                    $name_french = $this->security->xss_clean($this->input->post('name_french'));
                    $seo_url = $this->security->xss_clean($this->input->post('seo_url'));
                    $order_by = $this->security->xss_clean($this->input->post('order_by'));

                    $short_desc_eng = $this->security->xss_clean($this->input->post('short_desc_eng'));
                    $short_desc_spanish = $this->security->xss_clean($this->input->post('short_desc_spanish'));

                    $updated_at = date('Y-m-d H:i:s');

                    $request = array( 'name_eng' => $name,
                        'name_spanish' => $name_spanish,
                        'name_french' => $name_french,
                        'short_desc_eng' => $short_desc_eng,
                        'short_desc_spanish' => $short_desc_spanish,
                        'seo_url' => $seo_url,
                        'order_by' => $order_by,
                        'updated_at' => $updated_at );

                    $updated_id = $this->category->update( $category_id, $request );



                    if($updated_id > 0) {
                        redirect( base_url().'inbound-admin/manage-categories' );
                    }
                }
            }


        $this->load->view('admin/edit-category', $data);
        }
    

    public function seourl_check( $seo_url, $category_id ) 
    {
        $data = $this->category->credentials_exists( $category_id );
        if ($seo_url == $data['seo_url']) {
            return true;
        } else {
            $distinct_seo = $this->category->distinct_credentials( 'seo_url' );
            foreach ($distinct_seo as $key => $value) {
                if ($value['seo_url'] == $seo_url) {
                    $this->form_validation->set_message('seourl_check', 'The SEO URL you have entered is already being used.');
                    return false;
                }
            }
        }
    }


} // end controller
