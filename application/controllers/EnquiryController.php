<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class EnquiryController extends MY_Controller 
{
	public function __construct()
    {
        parent::__construct();
		$this->user = checkRole();
    }

	public function manageEnquiry()
	{
		$data['user'] = $this->user;
        $data['pagetitle'] = 'Inbound Enquiry';
        $data['status'] = 'All';
        $enquiries = $this->enquiry->getEnquiries();

        if(!empty($this->input->post('status'))) {
        	$data['status'] = $this->input->post('status');
        }

        if(!empty($enquiries)) {
        	$temp = array();
        	foreach ($enquiries as $key => $value) {
        		if( $value['current_status'] == $data['status'] ) {
        			$temp[$key] = $value;
        		} elseif ($data['status'] == 'All') {
                    $temp[$key] = $value;
                }
                
        	}

        	$data['enquiries'] = $temp;
        }

		$this->load->view('admin/manage-enquiry', $data);
	}


	public function updateEnquiry($enquiry_id = null)
	{
		$data['user'] = $this->user;
        $data['pagetitle'] = 'Edit Enquiry';
        $data['enquiry'] = $this->enquiry->getEnquiryDetailsByID($enquiry_id);
        $data['package'] = $this->package->getPackageDetailsByID($data['enquiry']['enquire_id']);

        if( !empty($this->input->post('submit')) )
        {
        	$email = $this->input->post('email');
        	$status = $this->input->post('status');
        	$feedback = $this->input->post('feedback');

        	$request = array( 'email' => $email, 'current_status' => $status, 'updated_at' => date('Y-m-d') );
        	$updated_id = $this->enquiry->update( $enquiry_id, $request );
        	
        	if($updated_id > 0) {
        		$input = array( 'enquiry_id' => $enquiry_id, 'feedback' => $feedback );
        		$feedback_id = $this->enquiry->store_feedbacks( $input );

        		if(!empty($email)) {

        			$input = array('ticket_no' => $data['enquiry']['ticket_no'], 'name' => $data['enquiry']['name'], 'feedback' => $feedback);

        			$to = $email;
	                $subject = 'Citius Feedback - Enquiry Ticket No. '. $ticket_no;
	                $mesg = $this->load->view( 'admin/email-templates/enquiry_feedbacks', $input, true );
	                // sendEmailToClient($to, $subject, $mesg);
                    sendInboundEmailNotification($to, $subject, $mesg);


        		}

        		redirect(base_url().'inbound-admin/manage-enquire');
        	}

        }

        // echo '<pre>';
        // print_r($data);
        // exit();
        
        $this->load->view('admin/edit-enquiry', $data);
	}

} // controller end
