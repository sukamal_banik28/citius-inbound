<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class AdminController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function dashboard()
    {
    	$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Dashboard';
        $data['packages'] = $this->getPackages();
        $data['reviews'] = $this->review->getReviews();
        $data['customers'] = $this->users->getUsers();
        $data['enquiries'] = $this->enquiry->getEnquiries();
    	$this->load->view('admin/manage-dashboard', $data);
    }
 	


 	public function manageUsers()
 	{
 		$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Users';
        $data['customers'] = $this->users->getUsers();
    	$this->load->view('admin/manage-users', $data);
 	}


 	public function addUsers()
 	{
 		$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Add Users';

        if(!empty($this->input->post('submit')))
        {
        	/* Set Validation rule  in the form */
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'min_length[10]|required|is_unique[ct_users.phone]');
            $this->form_validation->set_rules('email', 'Email', 'valid_email|required|is_unique[ct_users.email]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {

                $name = $this->security->xss_clean($this->input->post('name'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $role_id = $this->security->xss_clean($this->input->post('role_id'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $explode_address = explode(' ', $address);
                $country = end($explode_address);

                $registration_number = 'CUST'.substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"), -5).mt_rand(1000,9999);

                $input = array( 'name' => ucwords($name),
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'country' => $country,
                    'password' => hash('sha256', $password),
                    'role_id' => $role_id,
                    'is_active' => $status,
                    'registration_number' => $registration_number
                  );


                // return current inserted ID
                $user_id = $this->users->store($input);

                if($user_id > 0) {

                	// profile photo
            		if( !empty($_FILES['profile_photo']['name']) && !empty($_FILES['profile_photo']['name'][0]) ) {

                        $file_path = 'assets/uploads/users/';
                        file_upload($_FILES['profile_photo'], $user_id, 'Users', $file_path, 'profile_photo');
                    	
                    	} else {  

                        $data = array(
                            'fileable_id' => $user_id,
                            'fileable_type' => 'User',
                            'file_name' => 'user-avatar.png',
                            'file_path' => 'assets/uploads/users/',
                            'flag' => 'profile_photo',
                        );
                        $this->files->store($data);
                    } // end else

                    if(!empty($email)) {

                        $mesg = $this->load->view( 'admin/email-templates/welcome', $input, true );

                        $to = $email;
                        $subject = 'Welcome User';
                        // sendEmailToClient($to, $subject, $mesg);
                        sendInboundEmailNotification($to, $subject, $mesg);

                        $input['original_password'] = $password;
                        $mesg = $this->load->view( 'admin/email-templates/account_credentials', $input, true );
                        $subject = 'Account Info';
                        // sendEmailToClient($to, $subject, $mesg);
                        sendInboundEmailNotification($to, $subject, $mesg);

                    }

                    redirect( base_url().'inbound-admin/manage-users' );

                }

            } // end else
        }
    	$this->load->view('admin/add-users', $data);
 	}


 	public function updateUser( $user_id )
 	{
 		$data['me'] = checkRole();
        $data['pagetitle'] = 'Inbound Edit Users';
        $data['user'] = $this->users->getUserByID( $user_id );

        if(!empty($this->input->post('submit')))
        {
        	$name = $this->input->post('name');
        	$email = $this->input->post('email');
        	$phone = $this->input->post('phone');
        	$role_id = $this->input->post('role_id');
        	$status = $this->input->post('status');

        	$request = array(
        		'name' => $name,
        		'phone' => $phone,
        		'email' => $email,
        		'role_id' => $role_id,
        		'is_active' => $status,
        		'updated_at' => date('Y-m-d'),
        	);

        	$updated_id = $this->users->update( $user_id, $request );

        		if($updated_id > 0) {
        			redirect( base_url().'inbound-admin/manage-users' );
        		}

        }
    	$this->load->view('admin/edit-user', $data);
 		
 	}


} // end controller
