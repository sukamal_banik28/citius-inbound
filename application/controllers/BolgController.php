<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'PackageController.php';

class BolgController extends PackageController {

	public function manageBlogs()
	{
		$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Blogs';
        $data['blogs'] = $this->blog->getBlogs();
		$this->load->view('admin/manage-blogs', $data);
	}


	public function addBlogs()
	{
		$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Add Blogs';
        $data['categories'] = getCategories('blog');
        $data['channels'] = $this->channel->getChannels();

	        if(!empty($this->input->post('submit'))) {

	            $language = 'eng'; // default is english
	            
	            /* Set Validation rule  in the form */
	            $this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[ct_blogs.title_'.$language.']');
	            $this->form_validation->set_rules('seo_url', 'Seo URL', 'trim|required|is_unique[ct_blogs.seo_url]');

	            if ($this->form_validation->run() == FALSE) {
	                //auto redirect to main page if any validation failed and show the error logs if any.
	            } else {

	            	$code = 'CITIBINB'.mt_rand(10000,99999);
	            	$author = $this->security->xss_clean($this->input->post('author'));
	            	$status = $this->security->xss_clean($this->input->post('status'));
	            	$tags = $this->security->xss_clean($this->input->post('tags'));
	                $seo_url = $this->security->xss_clean($this->input->post('seo_url'));
	                $meta_key = $this->security->xss_clean($this->input->post('meta_key'));
	                $meta_desc = $this->security->xss_clean($this->input->post('meta_desc'));

	            	$title = $this->security->xss_clean($this->input->post('title'));
	            	$description = $this->security->xss_clean($this->input->post('description'));

	                $input = array(
	                	'code' => $code,
	                	'author' => ucwords($author),
	                	'status' => $status,	                	
                    	'tags' => ucwords($tags),
	                    'seo_url' => str_replace(" ","-",$seo_url),
	                    'meta_key' => $meta_key,
	                    'meta_description' => $meta_desc,

	                    'title_'.$language => ucwords($title),
                    	'desc_'.$language => $description,
	                	);


	                $blog_id = $this->blog->store( $input );
	                $channels = $this->input->post('channels');
                	$categories = $this->input->post('categories');

                	if($blog_id > 0) {


                		if(!empty($channels)) {
	                        $input = array();
	                        foreach ($channels as $key => $value) {
	                           $input = array('object_id' => $blog_id, 
	                                'channel_id' => $value,
	                                'object_type' => 'Blog',
	                            );
                                $this->channel->store_channel_objects($input);
                        	}
                    	} // if channels is not empty



                		if(!empty($categories)) {
                        	$input = array();
                        	foreach ($categories as $key => $value) {
	                            $input = array('object_id' => $blog_id,
	                            'object_type' => 'Blog',
	                            'category_id' => $value,
	                            );
	                            $this->category->store_category_object($input);
	                        }
                    	} // if category is not empty



                    	// package image
                        if( !empty($_FILES['blog_image']['name']) && !empty($_FILES['blog_image']['name'][0]) ) {

                            $file_path = 'assets/uploads/blogs/';
                            file_upload($_FILES['blog_image'], $blog_id, 'Blog', $file_path, 'blog_logo');
                        } else {  

                            $data = array(
                                'fileable_id' => $blog_id,
                                'fileable_type' => 'Blog',
                                'file_name' => 'blog-default.png',
                                'file_path' => 'assets/uploads/blogs/',
                                'flag' => 'blog_logo',
                            );
                            $this->files->store($data);
                        } // end else


                		redirect( base_url().'inbound-admin/manage-blogs' );
                	}

	        }

	    } // end submit

		$this->load->view('admin/add-blogs', $data);
	}


	public function updateBlogs( $blog_id )
	{
		$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Edit Package';
        $data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';

        $data['blog'] = $this->blog->getBlogDetailsByID( $blog_id, $data['lang'] );

        $temp3 = $this->channel->getChannels();
        foreach ($temp3 as $key => $value) {
            $temp3[$key]['selected'] = $this->channel->checkChannelHasObject( $value['id'], $blog_id, 'Blog' ) ? $this->channel->checkChannelHasObject( $value['id'], $blog_id, 'Blog' ) : '';
        }
        $data['channels'] = $temp3;


        $temp = $this->getCategories( 'blog' );
        foreach ($temp as $key => $value) {
            $temp[$key]['selected'] = $this->category->checkObjectHasCategory( $blog_id, 'Blog', $value['id'] ) ? $this->category->checkObjectHasCategory( $blog_id, 'Blog', $value['id'] ) : '';
        }
        $data['categories'] = $temp;


        if($this->input->post('submit'))
        {
        	/* Set Validation rule  in the form */
            $this->form_validation->set_rules('title', 'Title', 'trim|callback_title_check[' . $blog_id . ']');
            $this->form_validation->set_rules('seo_url', 'Seo URL', 'trim|callback_seourl_check[' . $blog_id . ']');

            if ($this->form_validation->run() == FALSE) {
	                //auto redirect to main page if any validation failed and show the error logs if any.
	            } else {

	            	$author = $this->security->xss_clean($this->input->post('author'));
	            	$status = $this->security->xss_clean($this->input->post('status'));
	            	$tags = $this->security->xss_clean($this->input->post('tags'));
	                $seo_url = $this->security->xss_clean($this->input->post('seo_url'));
	                $meta_key = $this->security->xss_clean($this->input->post('meta_key'));
	                $meta_desc = $this->security->xss_clean($this->input->post('meta_desc'));

	            	$title = $this->security->xss_clean($this->input->post('title'));
	            	$description = $this->security->xss_clean($this->input->post('description'));

	                $request = array(
	                	'author' => ucwords($author),
	                	'status' => $status,	                	
                    	'tags' => ucwords($tags),
	                    'seo_url' => str_replace(" ","-",$seo_url),
	                    'meta_key' => $meta_key,
	                    'meta_description' => $meta_desc,

	                    'title_'.$data['lang'] => ucwords($title),
                    	'desc_'.$data['lang'] => $description,
	                	);


	                $updated_id = $this->blog->update($blog_id, $request);
	                $channels = $this->input->post('channels');
                	$categories = $this->input->post('categories');

	                if($updated_id > 0) {

	                	$this->channel->deleteExistingChannelsObjects($blog_id, 'Blog');
	                	$this->category->deleteExistingCategoryObjects( $blog_id, 'Blog' );

	                	if(!empty($channels)) {
	                        $input = array();
	                        foreach ($channels as $key => $value) {
	                           $input = array('object_id' => $blog_id, 
	                                'channel_id' => $value,
	                                'object_type' => 'Blog',
	                            );
                            $this->channel->store_channel_objects($input);
                        	}
                    	} // if channels is not empty



                		if(!empty($categories)) {
                        	$input = array();
                        	foreach ($categories as $key => $value) {
	                            $input = array('object_id' => $blog_id,
	                            'object_type' => 'Blog',
	                            'category_id' => $value,
	                            );
	                            $this->category->store_category_object($input);
	                        }
                    	} // if category is not empty


                    	if( !empty($_FILES['blog_image']['name']) && !empty($_FILES['blog_image']['name'][0]) ) {

                    		$flag = 'blog_logo';
                            $files = $this->files->getFilesByFileableId( $blog_id, 'Blog', $flag );
                            $this->files->delete($files['id']);


                            $file_path = 'assets/uploads/blogs/';
                            file_upload($_FILES['blog_image'], $blog_id, 'Blog', $file_path, $flag );
                        }


                        redirect(base_url().'inbound-admin/manage-blogs');


	                } // end if
	            } // end else
        	} // end submit

		$this->load->view('admin/edit-blogs', $data);
	}



    public function manageBlogComments()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Reviews';
        $data['reviews'] = $this->review->getReviews( 'Blog' );
        if(!empty($data['reviews'])) {
            foreach ($data['reviews'] as $key => $value) {
                $data['reviews'][$key]['package'] = $this->package->getPackageDetailsByID( $value['reviewable_id'] );
                $data['reviews'][$key]['user'] = $this->users->getUserByID( $value['user_id'] );
            }
        }
        $this->load->view('admin/manage-reviews', $data);
    }





	public function seourl_check( $seo_url, $blog_id ) 
    {
        $data = $this->home->credentials_exists( $blog_id );
        if ($seo_url == $data['seo_url']) {
            return true;
        } else {
            $distinct_seo = $this->home->distinct_credentials( 'seo_url' );
            foreach ($distinct_seo as $key => $value) {
                if ($value['seo_url'] == $seo_url) {
                    $this->form_validation->set_message('seourl_check', 'The SEO URL you have entered is already being used. Please try different.');
                    return false;
                }
            }
        }
    }



    public function title_check( $title_eng, $blog_id ) 
    {
        $data = $this->home->credentials_exists( $blog_id );
        if ($title_eng == $data['title_eng']) {
            return true;
        } else {
            $distinct_title = $this->home->distinct_credentials( 'title_eng' );
            foreach ($distinct_title as $key => $value) {
                if ($value['title_eng'] == $title_eng) {
                    $this->form_validation->set_message('title_check', 'The Title you have entered is already being used.');
                    return false;
                }
            }
        }
    }





} // end controller
