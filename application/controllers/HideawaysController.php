<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class HideawaysController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }




    public function manageHideaways()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Hideaways';
        $data['hideaways'] = $this->hideaways->getHideaways();
        $this->load->view('admin/manage-hideaways', $data);
    }



    public function addHideaways()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Hideaways';

        if(!empty($this->input->post('submit'))) {

            $title = $this->security->xss_clean($this->input->post('title'));
            $web_link = $this->security->xss_clean($this->input->post('web_link'));
            $status = $this->security->xss_clean($this->input->post('status'));
            $order_by = $this->security->xss_clean($this->input->post('order_by'));
            $file_type = $this->security->xss_clean($this->input->post('file_type'));


            $input = array('title' => $title, 
                'web_link' => $web_link,
                'order_by' => $order_by,
                'is_active' => $status,
                'flag' => 'inbound',
                'file_type' => $file_type,
            );

            $hideaways_id = $this->hideaways->store($input);

            if($hideaways_id > 0) {

                // package image
                if( !empty($_FILES['hideaway_image']['name']) && !empty($_FILES['hideaway_image']['name'][0]) ) {

                    $file_path = 'assets/uploads/hideaways/';
                    file_upload($_FILES['hideaway_image'], $hideaways_id, 'Hideaway', $file_path, 'hideaway_logo');
                } 

                redirect( base_url().'/inbound-admin/manage-hideaways' );
            }


        }


        $this->load->view('admin/add-hideaways', $data);
    }


    public function updateHideaways( $hideaway_id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Hideaways';

        $append = " AND hideaways.id = ".$hideaway_id;
        $data['hideaway'] = $this->hideaways->getHideaways( $append )[0];

            if(!empty($this->input->post('submit')))
            {   

            $title = $this->security->xss_clean($this->input->post('title'));
            $web_link = $this->security->xss_clean($this->input->post('web_link'));
            $status = $this->security->xss_clean($this->input->post('status'));
            $order_by = $this->security->xss_clean($this->input->post('order_by'));
            $file_type = $this->security->xss_clean($this->input->post('file_type'));
            $updated_at = date('Y-m-d H:i:s');

            $input = array('title' => $title, 
                'web_link' => $web_link,
                'order_by' => $order_by,
                'is_active' => $status,
                'updated_at' => $updated_at,
                'file_type' => $file_type,
            );


                $updated_id = $this->hideaways->update( $hideaway_id, $input );

                if($updated_id > 0) {


                    // package banner
                    if( !empty($_FILES['hideaway_image']['name']) && !empty($_FILES['hideaway_image']['name'][0]) ) {

                            $flag = 'hideaway_logo';
                            $files = $this->files->getFilesByFileableId( $hideaway_id, 'Hideaway', $flag );
                            unlinkFile($files);
                            $this->files->delete($files['id']);

                            $file_path = 'assets/uploads/hideaways/';
                            file_upload($_FILES['hideaway_image'], $hideaway_id, 'Hideaway', $file_path, $flag);
                        }

                        
                    redirect( base_url().'/inbound-admin/manage-hideaways' );
                }
            }


        $this->load->view('admin/edit-hideaways', $data);
        }
    




} // end controller
