<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class MasterController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }


    public function manageAddon()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Addons';
        $data['addons'] = $this->product->getAddOns();
        $this->load->view('admin/manage-addon', $data);
    }



    public function addAddon()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Addons';

        if(!empty($this->input->post('submit'))) {

            $type_name = $this->security->xss_clean($this->input->post('type_name'));                
            $flag = $this->security->xss_clean($this->input->post('flag'));

            $data = array( 'type_name_eng' => $type_name, 'flag' => $flag );
            $inserted_id = $this->product->store_addons($data);

            if(!empty($inserted_id)) {
                redirect( base_url().'inbound-admin/manage-addon' );
            }
        }

        $this->load->view('admin/add-addon', $data);
    }



    public function updateAddon($id)
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Addons';
        $append = 'WHERE `id` = '.$id;
        $data['addons'] = $this->product->getAddOns($append)[0];

        if(!empty($this->input->post('submit'))) {

            $type_name = $this->security->xss_clean($this->input->post('type_name'));                
            $flag = $this->security->xss_clean($this->input->post('flag'));
            $is_active = $this->security->xss_clean($this->input->post('is_active'));

            $data = array(
                'type_name_eng' => $type_name,
                'flag' => $flag,
                'is_active' => $is_active
                );

            $updated_id = $this->product->update_addons($id, $data);
            if(!empty($updated_id)) {
                redirect( base_url().'inbound-admin/manage-addon' );
            }

        }

        $this->load->view('admin/edit-addon', $data);
    }





    public function manageEbrochure()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Ebrochures';
        $ebrochure = $this->ebrochure->getEbrochures();

        if(!empty($ebrochure)) {
            foreach ($ebrochure as $key => $value) {
                $ebrochure[$key]['brochure_file'] = $this->files->getFilesByFileableId( $value['id'], 'Brochure', 'brochure_file' );
            }
        }

        $data['ebrochure'] = $ebrochure;
        $this->load->view('admin/manage-ebrochure', $data);
    }

    public function addEbrochure()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Ebrochures';


        if(!empty($this->input->post('submit'))) {

            $title = $this->security->xss_clean($this->input->post('title'));
            $author = $this->security->xss_clean($this->input->post('author'));
            $order_by = $this->security->xss_clean($this->input->post('order_by'));

            $input = array('title' => $title, 
                'author_name' => $author,
                'order_by' => $order_by,
                'flag' => 'inbound',
                'is_active' => 1
            );

            $inserted_id = $this->ebrochure->store($input);

            if( !empty($inserted_id) ) {
                // ebrochure image
                if( !empty($_FILES['brochure_image']['name']) && !empty($_FILES['brochure_image']['name'][0]) ) {
                    $file_path = 'assets/uploads/brochures/';
                    file_upload($_FILES['brochure_image'], $inserted_id, 'Brochure', $file_path, 'brochure_image');
                } 


                // ebrochure file
                if( !empty($_FILES['brochure_file']['name']) && !empty($_FILES['brochure_file']['name'][0]) ) {
                    $file_path = 'assets/uploads/brochures/';
                    file_upload($_FILES['brochure_file'], $inserted_id, 'Brochure', $file_path, 'brochure_file');
                } 

                redirect( base_url().'inbound-admin/manage-ebrochure' );
            }


        }


        $this->load->view('admin/add-ebrochure', $data);
    }


    public function updateEbrochure( $id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Ebrochures';
        $append = " AND ebrochure.`id` = ".$id;
        $data['ebrochure'] = $this->ebrochure->getEbrochures( $append )[0];

        if(!empty($this->input->post('submit'))) 
        {
            $title = $this->security->xss_clean($this->input->post('title'));
            $author = $this->security->xss_clean($this->input->post('author'));
            $order_by = $this->security->xss_clean($this->input->post('order_by'));
            $status = $this->security->xss_clean($this->input->post('status'));

            $input = array('title' => $title, 
                'author_name' => $author,
                'order_by' => $order_by,
                'flag' => 'inbound',
                'is_active' => $status
            );

            $updated_id = $this->ebrochure->update( $id, $input );

            if( !empty($updated_id) ) {


                    if( !empty($_FILES['brochure_image']['name']) && !empty($_FILES['brochure_image']['name'][0]) ) {

                        $flag = 'brochure_image';
                        $files = $this->files->getFilesByFileableId( $updated_id, 'Brochure', $flag );
                        
                        unlinkFile($files);
                        $this->files->delete($files['id']);

                        $file_path = 'assets/uploads/brochures/';
                        file_upload($_FILES['brochure_image'], $updated_id, 'Brochure', $file_path, $flag);
                    }




                    if( !empty($_FILES['brochure_file']['name']) && !empty($_FILES['brochure_file']['name'][0]) ) {

                        $flag = 'brochure_file';
                        $files = $this->files->getFilesByFileableId( $updated_id, 'Brochure', $flag );

                        unlinkFile($files);
                        $this->files->delete($files['id']);

                        $file_path = 'assets/uploads/brochures/';
                        file_upload($_FILES['brochure_file'], $updated_id, 'Brochure', $file_path, $flag);
                    }
                        
                    redirect( base_url().'inbound-admin/manage-ebrochure' );
                }



        }

        // echo '<pre>';
        // print_r($data);
        // exit();

        $this->load->view('admin/edit-ebrochure', $data);
    }


    public function bookingHistory()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Booking';

        $append = " WHERE `flag` = 'inbound'";
        $bookings = $this->booking->getBookingPackages();

        if(!empty($bookings)) {
            $data['bookings'] = $bookings;
        }

        $this->load->view( 'admin/manage-bookings', $data );

        // echo '<pre>';
        // print_r($bookings);
        // exit();
    }




    public function updateBookingHistory( $booking_id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Booking';

        $data['booking_info'] = $this->booking->getBookingInfoByID( $booking_id );
        $data['booking_details'] = $this->booking->getBookingDetailsByBookingID( $booking_id );

        $this->load->view( 'admin/edit-booking', $data );

        // echo '<pre>';
        // print_r($data);
        // exit();

    }




    public function manageCities()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Cities';

        $data['cities'] = $this->city->getCities();

        $this->load->view( 'admin/manage-cities', $data );
    }



    public function addCities()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Cities';
        $data['states'] = $this->city->getStates();

        if(!empty($this->input->post('submit'))) 
            {

                $city = $this->security->xss_clean($this->input->post('city'));
                $state_id = $this->security->xss_clean($this->input->post('state_id'));
                $is_active = $this->security->xss_clean($this->input->post('is_active'));

                $input = array(
                    'city' => $city,
                    'state_id' => $state_id,
                    'is_active' => $is_active
                    );


                $inserted_id = $this->city->store( $input );

                if($inserted_id) {
                    redirect( base_url().'inbound-admin/manage-cities' );
                }

            }

        $this->load->view( 'admin/add-city', $data );
    }



    public function updateCities( $city_id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Cities';
        $data['states'] = $this->city->getStates();

        $append = " WHERE `id` = ".$city_id;
        $data['city'] = $this->city->getCities($append)[0];


            if(!empty($this->input->post('submit'))) 
            {

                $city = $this->security->xss_clean($this->input->post('city'));
                $state_id = $this->security->xss_clean($this->input->post('state_id'));
                $is_active = $this->security->xss_clean($this->input->post('is_active'));

                $input = array(
                    'city' => $city,
                    'state_id' => $state_id,
                    'is_active' => $is_active
                    );


                $updated_id = $this->city->update( $city_id, $input );

                if($updated_id) {
                    redirect( base_url().'inbound-admin/manage-cities' );
                }

            }

        $this->load->view( 'admin/edit-city', $data );
    }





    public function manageAmenities()
    {
        $data = common_elements('Inbound Payments');
        $append = " WHERE `parent_id` != 0";
        $data['child_amenities'] = $this->product->getAmenities($append);
        $this->load->view( 'admin/manage-amenities', $data );
    }



    public function addAmenities()
    {
        $data = common_elements('Inbound Payments');
        $append = " WHERE `parent_id` = 0";
        $data['parent_amenities'] = $this->product->getAmenities($append);

        if(!empty($this->input->post('submit'))) 
        {

            $name_eng = $this->security->xss_clean($this->input->post('name_eng'));
            $name_spanish = $this->security->xss_clean($this->input->post('name_spanish'));
            $parent_id = $this->security->xss_clean($this->input->post('parent_id'));

            $inputs = array(
                'parent_id' => $parent_id,
                'name_eng' => ucfirst($name_eng),
                'name_spanish' => ucfirst($name_spanish),
                );

        $inserted_id = $this->product->store_amenities( $inputs );

        if(!empty($inserted_id)) {
            redirect( base_url('inbound-admin/manage-amenities') );
        }

        }

        $this->load->view( 'admin/add-amenity', $data );
    }




    public function updateAmenities( $ameniti_id )
    {
        $data = common_elements('Inbound Payments');

        $append = " WHERE `id` = ".$ameniti_id;
        $data['amenities'] = $this->product->getAmenities($append)[0];

        $append = " WHERE `parent_id` = 0";
        $data['parent_amenities'] = $this->product->getAmenities($append);

        if(!empty($this->input->post('submit'))) 
        {

            $name_eng = $this->security->xss_clean($this->input->post('name_eng'));
            $name_spanish = $this->security->xss_clean($this->input->post('name_spanish'));
            $parent_id = $this->security->xss_clean($this->input->post('parent_id'));

            $inputs = array(
                'parent_id' => $parent_id,
                'name_eng' => ucfirst($name_eng),
                'name_spanish' => ucfirst($name_spanish),
                );

        $updated_id = $this->product->update_amenities( $ameniti_id, $inputs );

        if(!empty($updated_id)) {
            redirect( base_url('inbound-admin/manage-amenities') );
        }

        }

        $this->load->view( 'admin/edit-amenity', $data );
    }




    public function manageSocialLinks()
    {
        $data = common_elements('Inbound Social Links');
        $data['links'] = $this->home->getSocialLinks();
        $this->load->view('admin/manage-sociallinks', $data);
    }


    public function addSocialLinks()
    {
        $data = common_elements('Inbound Social Links');

        if(!empty($this->input->post('submit'))) 
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $link = $this->security->xss_clean($this->input->post('link'));
            $is_active = $this->security->xss_clean($this->input->post('is_active'));

            $temp_icon = $this->security->xss_clean($this->input->post('fa_icon'));
            $fa_icon = empty($temp_icon) ? '<i class="fa fa-link" aria-hidden="true"></i>' : $temp_icon;

            $input = array(
                'name' => $name,
                'link' => $link,
                'fa_icon' => $fa_icon,
                'is_active' => $is_active,
                );

            $inserted_id = $this->home->store( $input );

            if(!empty($inserted_id)) {
                redirect( base_url('inbound-admin/manage-sociallinks') );
            }

        }
        $this->load->view('admin/add-sociallinks', $data);
    }



    public function updateSocialLinks($link_id)
    {
        $data = common_elements('Inbound Social Links');
        $append = " WHERE `id` = ".$link_id;
        $data['link'] = $this->home->getSocialLinks($append)[0];


        if(!empty($this->input->post('submit'))) 
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $link = $this->security->xss_clean($this->input->post('link'));
            $is_active = $this->security->xss_clean($this->input->post('is_active'));

            $temp_icon = $this->security->xss_clean($this->input->post('fa_icon'));
            $fa_icon = empty($temp_icon) ? $data['link']['fa_icon'] : $temp_icon;

            $input = array(
                'name' => $name,
                'link' => $link,
                'fa_icon' => $fa_icon,
                'is_active' => $is_active,
                'updated_at' => date('Y-m-d'),
                );

            $inserted_id = $this->home->update( $link_id, $input );

            if(!empty($inserted_id)) {
                redirect( base_url('inbound-admin/manage-sociallinks') );
            }
            
        }

        $this->load->view('admin/edit-sociallinks', $data);
    }





    public function manageProvince() 
    {
        $data = common_elements('Inbound Provinces');
        $data['provinces'] = $this->province->getProvinces();
        $this->load->view('admin/manage-provinces', $data);
    }



    public function addProvince()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Add Provinces';


        $append1 = " AND province.`parent_id` = 0";
        $data['countries'] = $this->province->getProvinces($append1);


        if(!empty($this->input->post('submit'))) {

            $lang = 'eng'; // Default language English

            $country = $this->security->xss_clean($this->input->post('country'));
            $state = $this->security->xss_clean($this->input->post('state'));


            $is_active = $this->security->xss_clean($this->input->post('is_active'));
            $order_by = $this->security->xss_clean($this->input->post('order_by'));
            
            $name_eng = $this->security->xss_clean($this->input->post('name_eng'));
            $name_spanish = $this->security->xss_clean($this->input->post('name_spanish'));

            $description_eng = $this->security->xss_clean($this->input->post('description_eng'));
            $description_spanish = $this->security->xss_clean($this->input->post('description_spanish'));

            $font_icon_tag = $this->security->xss_clean($this->input->post('font_icon_tag'));

            $parent_id = empty($country) && empty($state) ? 0 : ( !empty($country) && empty($state) ? $country : $state );

            $input = array(
                'parent_id' => $parent_id,
                'name_eng' => $name_eng,
                'name_spanish' => $name_spanish,
                'description_eng' => $description_eng,
                'description_spanish' => $description_spanish,
                'is_active' => $is_active,
                'order_by' => $order_by,
                'flag' => 'inbound',
                'font_icon_tag' => strtolower($font_icon_tag),
            );


            // echo '<pre>';
            // print_r($input);
            // exit();


            $province_id = $this->province->store($input);

            if($province_id > 0) {

                if( !empty($_FILES['province_image']['name']) && !empty($_FILES['province_image']['name'][0]) )
                    {
                        $flag = 'province_image';
                        $file_path = 'assets/uploads/provinces/';
                        file_upload($_FILES['province_image'], $province_id, 'Province', $file_path, $flag);
                    }


                redirect(base_url().'inbound-admin/manage-province');
            } // end if
        } // end submit
        $this->load->view('admin/add-province', $data);
    }




    public function updateProvince($province_id)
    {
        checkRole();
        $data['pagetitle'] = 'Inbound Update Provinces';

        $append = " AND province.`id` = ".$province_id;
        $data['province'] = $this->province->getProvinces($append)[0];


        if(!empty($this->input->post('submit'))) {

            $lang = 'eng'; // Default language English

            $is_active = $this->security->xss_clean($this->input->post('is_active'));
            $order_by = $this->security->xss_clean($this->input->post('order_by'));
            
            $name_eng = $this->security->xss_clean($this->input->post('name_eng'));
            $name_spanish = $this->security->xss_clean($this->input->post('name_spanish'));

            $description_eng = $this->security->xss_clean($this->input->post('description_eng'));
            $description_spanish = $this->security->xss_clean($this->input->post('description_spanish'));

            $font_icon_tag = $this->security->xss_clean($this->input->post('font_icon_tag'));

            $input = array(
                'name_eng' => $name_eng,
                'name_spanish' => $name_spanish,
                'description_eng' => $description_eng,
                'description_spanish' => $description_spanish,
                'is_active' => $is_active,
                'order_by' => $order_by,
                'font_icon_tag' => strtolower($font_icon_tag),
            );


            $province_id = $this->province->update($province_id, $input);

            if( !empty($province_id) ) {

                if( !empty($_FILES['province_image']['name']) && !empty($_FILES['province_image']['name'][0]) )
                    {
                        $flag = 'province_image';
                        $files = $this->files->getFilesByFileableId( $province_id, 'Province', $flag );
                        $path = FCPATH.$files['file_path'].$files['file_name'];
                        unlink($path);
                        $this->files->delete($files['id']);

                        $file_path = 'assets/uploads/provinces/';
                        file_upload($_FILES['province_image'], $province_id, 'Province', $file_path, $flag);
                    }


                redirect(base_url().'inbound-admin/manage-province');
            } // end if
        } // end submit



        $this->load->view('admin/edit-province', $data);
        // echo '<pre>';
        // print_r($data);
        // exit();

    }






    public function getStates()
    {
        $country = $_POST['country'];
        $append1 = " AND province.`parent_id` = ".$country;
        $states = $this->province->getProvinces($append1);

        if(!empty($states)) {
                echo '<option value="0">NULL</option>';
            foreach ($states as $key => $value) {
                echo '<option value='.$value['id'].'>'.$value['name_eng'].'</option>';
            }
        } else {
            echo '<option value="0">NULL</option>';
        }
    }



} // end controller
?>