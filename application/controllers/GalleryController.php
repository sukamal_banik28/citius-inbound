<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class GalleryController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }


    public function manageGallery()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Gallery';
        $data['gallery'] = $this->gallery->getGallery();
        $this->load->view('admin/manage-gallery', $data);
    }



    public function addGallery()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Gallery';

        if(!empty($this->input->post('submit'))) {

            $title = $this->security->xss_clean($this->input->post('title'));
            $desc = $this->security->xss_clean($this->input->post('desc'));
            $status = $this->security->xss_clean($this->input->post('status'));
            $order_by = $this->security->xss_clean($this->input->post('order_by'));

            $input = array('title' => $title, 
                'description' => $desc,
                'order_by' => $order_by,
                'is_active' => $status,
                'flag' => 'inbound'
            );

            $gallery_id = $this->gallery->store($input);

            if($gallery_id > 0) {

                // package image
                if( !empty($_FILES['gallery_image']['name']) && !empty($_FILES['gallery_image']['name'][0]) ) {

                    $file_path = 'assets/uploads/gallery/';
                    file_upload($_FILES['gallery_image'], $gallery_id, 'Gallery', $file_path, 'gallery_image');
                } 

                redirect( base_url().'inbound-admin/manage-gallery' );
            }


        }


        $this->load->view('admin/add-gallery', $data);
    }








    public function updateGallery( $gallery_id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Gallery';

        $append = " AND gallery.id = ".$gallery_id;
        $data['gallery'] = $this->gallery->getGallery( $append )[0];

            if(!empty($this->input->post('submit')))
            {   

            $title = $this->security->xss_clean($this->input->post('title'));
            $desc = $this->security->xss_clean($this->input->post('desc'));
            $status = $this->security->xss_clean($this->input->post('status'));
            $order_by = $this->security->xss_clean($this->input->post('order_by'));
            $updated_at = date('Y-m-d H:i:s');

            $input = array('title' => $title, 
                'description' => $desc,
                'order_by' => $order_by,
                'is_active' => $status,
                'updated_at' => $updated_at
            );


                $updated_id = $this->gallery->update( $gallery_id, $input );

                if($updated_id > 0) 
                {
                    // package banner
                    if( !empty($_FILES['gallery_image']['name']) && !empty($_FILES['gallery_image']['name'][0]) ) 
                        {

                            $flag = 'gallery_image';
                            $files = $this->files->getFilesByFileableId( $gallery_id, 'Gallery', $flag );
                            unlinkFile($files);
                            $this->files->delete($files['id']);

                            $file_path = 'assets/uploads/gallery/';
                            file_upload($_FILES['gallery_image'], $gallery_id, 'Gallery', $file_path, $flag);
                        }

                        
                    redirect( base_url().'inbound-admin/manage-gallery' );
                }
            }


        $this->load->view('admin/edit-gallery', $data);
    }





    public function deleteGallery($id)
    {

        $append = " AND gallery.`id` = ".$id;        
        $gallery = $this->gallery->getGallery( $append )[0];

        // echo '<pre>';
        // print_r($gallery);
        // exit();

        $files = $this->files->getFileById( $gallery['file_id'] );
        $path = FCPATH.$files['file_path'].$files['file_name'];

        unlink($path);
        $this->files->delete($files['id']);
        $this->gallery->delete($id);

        redirect(base_url().'inbound-admin/manage-gallery');
    }





} // end controller
?>