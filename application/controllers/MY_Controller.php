<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// display error on local server : turn OFF when live
// error_reporting( E_ALL );
// ini_set( "display_errors", 1 );
// ini_set('upload_max_filesize', '200M');

class MY_Controller extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        checkSession();
        date_default_timezone_set('Asia/Kolkata');
    }


    public function file_upload($file_data, $fileable_id, $fileable_type, $file_path, $flag = 'package_logo') {

				$filesCount = count($file_data['name']);
				$files = $file_data;
				$data = array(); 

				for($i = 0; $i < $filesCount; $i++) {

					$_FILES['files']['name']     = $files['name'][$i];
					$_FILES['files']['type']     = $files['type'][$i];
					$_FILES['files']['tmp_name'] 	= $files['tmp_name'][$i];
					$_FILES['files']['error']     = $files['error'][$i];
					$_FILES['files']['size']     = $files['size'][$i];

					$extension = pathinfo($_FILES['files']['name'], PATHINFO_EXTENSION);
					$newFileName = microtime(true) * 10000 . '.'. $extension;

					$config['upload_path'] = $file_path;
					$config['allowed_types'] = '*';
					$config['overwrite'] = TRUE;
					$config['file_name'] = $newFileName;
					$this->load->library('upload', $config);
					$this->upload->initialize($config); 

					if(!$this->upload->do_upload('files')) {
						echo $this->upload->display_errors(); 
						exit();
					} else {
						$config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
						$config['maintain_ratio'] = FALSE;	

						$data = array(
							'fileable_id' => $fileable_id,
							'fileable_type' => $fileable_type,
							'file_name' => $this->upload->data('file_name'),
							'file_type' => $this->upload->data('file_type'),
							'file_path' => $file_path,
							'file_ext' => $this->upload->data('file_ext'),
							'file_size' => $this->upload->data('file_size'),
							'original_name' => $this->upload->data('client_name'),
							'flag' => $flag,
						);
						
							$this->files->store($data);

				    }
	            } // end loop
			} // end file upload function
 



	public function getPackages()
    {   
        $temp = array();
        $packages = $this->package->getPackages();

        if(!empty($packages)) {
            foreach ($packages as $key => $value) {
                if($value['flag'] == 'package_logo') {
                    $temp[$key] = $value;
                }
            }

            return $temp;
        }
    }


    public function getDeletedPackages()
    {   
        $temp = array();
        $packages = $this->package->getDeletedPackages();

        if(!empty($packages)) {
            foreach ($packages as $key => $value) {
                if($value['flag'] == 'package_logo') {
                    $temp[$key] = $value;
                }
            }

            return $temp;
        }
    }




    public function getProducts()
    {
    	$temp = array();
        $products = $this->product->getProducts();

        if(!empty($products)) {
            foreach ($products as $key => $value) {
                if($value['flag'] == 'product_logo') {
                    $temp[$key] = $value;
                }
            }

            return $temp;
        }
    }


} // end controller
