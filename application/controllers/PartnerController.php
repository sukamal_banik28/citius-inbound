<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class PartnerController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }


    public function managePartners()
    {
        $data['pagetitle'] = 'Inbound Partners';
        $data['partners'] = $this->partner->getPartners();
        $this->load->view('admin/manage-partners', $data);
    }


    public function addNewPartners()
    {
        $data['pagetitle'] = 'Inbound Add Partner';

        if(!empty($this->input->post('submit'))) {

            $lang = 'eng'; // Default language English

            $status = $this->security->xss_clean($this->input->post('status'));
            $order = $this->security->xss_clean($this->input->post('order'));
            $position = $this->security->xss_clean($this->input->post('position'));
            // $slogan = $this->security->xss_clean($this->input->post('slogan'));
            // $slogan_spanish = $this->security->xss_clean($this->input->post('slogan_spanish'));
            // $slogan_french = $this->security->xss_clean($this->input->post('slogan_french'));

            $input = array(
                // 'slogan_'.$lang => $slogan,
                // 'slogan_spanish' => $slogan_spanish,
                // 'slogan_french' => $slogan_french,
                'status' => $status,
                'order_no' => $order,
                'flag' => 'inbound',
                'position' => $position,
                );


            $partner_id = $this->partner->store($input);

            if($partner_id > 0) {

                if( !empty($_FILES['banner_image']['name']) && !empty($_FILES['banner_image']['name'][0]) )
                    {
                        $flag = 'partner_image';
                        $file_path = 'assets/uploads/partners/';
                        file_upload($_FILES['banner_image'], $partner_id, 'Partner', $file_path, $flag);
                    }

                redirect(base_url().'inbound-admin/manage-partners');
            } // end if
        } // end submit
        $this->load->view('admin/add-partner', $data);
    }


    public function updatePartners($partner_id)
    {
        $data['pagetitle'] = 'Inbound Partner Details';
        $data['banner'] = $this->partner->getPartnerDetails( $partner_id );

        if(!empty($this->input->post('submit'))) { 

            $status = $this->security->xss_clean($this->input->post('status'));
            $order = $this->security->xss_clean($this->input->post('order'));
            $position = $this->security->xss_clean($this->input->post('position'));
            // $slogan = $this->security->xss_clean($this->input->post('slogan'));
            // $slogan_spanish = $this->security->xss_clean($this->input->post('slogan_spanish', NULL));

            $request = array(
                // 'slogan_eng' => $slogan,
                // 'slogan_spanish' => $slogan_spanish,
                'status' => $status,
                'order_no' => $order,
                'position' => $position,
                'updated_at' => date('Y-m-d'),
                );

        	$update_id = $this->partner->update( $partner_id, $request );

        	    if($update_id > 0) {

                    if( !empty($_FILES['banner_image']['name']) && !empty($_FILES['banner_image']['name'][0]) ) 
                    {
                        $flag = 'partner_image';
                        $files = $this->files->getFilesByFileableId( $partner_id, 'Partner', $flag );
                        $path = FCPATH.$files['file_path'].$files['file_name'];
                        unlink($path);
                        $this->files->delete($files['id']);
                        $file_path = 'assets/uploads/partners/';

                        
                        file_upload($_FILES['banner_image'], $partner_id, 'Partner', $file_path, $flag);

                        
                    }

        		  redirect(base_url().'inbound-admin/manage-partners');
        	   }

            } // end submit
        $this->load->view('admin/edit-partner', $data);
    }




} // end controller
?>