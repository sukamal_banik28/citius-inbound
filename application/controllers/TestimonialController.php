<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class TestimonialController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }
 	

 	public function manageTestimonial()
	{
		$data['user'] = checkRole();
		$data['pagetitle'] = 'Inbound Testimonial';
		$data['testimonials'] = $this->testimonial->getTestimonials();
		$this->load->view('admin/manage-testimonial', $data);
	}

 	public function addTestimonial()
	{
		$data['user'] = checkRole();
		$data['pagetitle'] = 'Inbound Testimonial';

		if(!empty($this->input->post('submit'))) {
			
			$language = 'eng';

			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('created_by', 'Created By', 'required');				 
	
		if ($this->form_validation->run() == FALSE) {
				//auto redirect to main page if any validation failed and show the error logs if any.
            } else {

            	$title = $this->security->xss_clean($this->input->post('title'));
            	$created_by = $this->security->xss_clean($this->input->post('created_by'));
                $short_desc = $this->security->xss_clean($this->input->post('short_desc'));
                $long_desc = $this->security->xss_clean($this->input->post('long_description'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $input = array(
                	'status' => $status,
                	'created_by' => $created_by,
                	'title_'.$language => $title,
                	'short_desc_'.$language => $short_desc,
                	'long_desc_'.$language => $long_desc,
                	);

				$testimonial_id = $this->testimonial->store($input);

				if(!empty( $testimonial_id )) {

						if( !empty($_FILES['testimonial_image']['name']) && !empty($_FILES['testimonial_image']['name'][0]) ) {

                            $flag = 'testimonial_logo';
                            $file_path = 'assets/uploads/testimonials/';
                            file_upload($_FILES['testimonial_image'], $testimonial_id, 'Testimonial', $file_path, $flag);
                        } else {  

                            $data = array(
                                'fileable_id' => $testimonial_id,
                                'fileable_type' => 'Testimonial',
                                'file_name' => 'testimonial-avater.png',
                                'file_path' => 'assets/uploads/testimonials/',
                                'flag' => $flag,
                            );
                            $this->files->store($data);
                        } // end else

					redirect( base_url().'inbound-admin/manage-testimonials' );
				}

			} // end else
		} // end submit	

		$this->load->view('admin/add-testimonial', $data);
	}



 	public function updateTestimonial( $testimonial_id )  
 	{
 		$data['user'] = checkRole();
		$data['pagetitle'] = 'Inbound Edit Testimonial';
		$data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';
		$data['testimonial'] = $this->testimonial->getTestimonialByID( $testimonial_id, $data['lang'] );
 		
		if(!empty($this->input->post('submit'))) 
			{
				$this->form_validation->set_rules('title', 'Title', 'required');
				$this->form_validation->set_rules('created_by', 'Created By', 'required');
					
				if ($this->form_validation->run() == FALSE) {
	                   //auto redirect to main page if any validation failed and show the error logs if any.
                } else {

                $title = $this->security->xss_clean($this->input->post('title'));
            	$created_by = $this->security->xss_clean($this->input->post('created_by'));
                $short_desc = $this->security->xss_clean($this->input->post('short_desc'));
                $long_desc = $this->security->xss_clean($this->input->post('long_description'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $request = array(
                	'status' => $status,
                	'created_by' => $created_by,
                	'title_'.$data['lang'] => $title,
                	'short_desc_'.$data['lang'] => $short_desc,
                	'long_desc_'.$data['lang'] => $long_desc,
                    'updated_at' => date('Y-m-d'),
                	);

                $updated_id = $this->testimonial->update($testimonial_id, $request);

                if(!empty($updated_id)) {

                		if( !empty($_FILES['testimonial_image']['name']) && !empty($_FILES['testimonial_image']['name'][0]) )   {

                            $flag = 'testimonial_logo';
                            $files = $this->files->getFilesByFileableId( $testimonial_id, 'Testimonial', $flag );
                            
                            $this->files->delete($files['id']);

                            $file_path = 'assets/uploads/testimonials/';
                            file_upload($_FILES['testimonial_image'], $testimonial_id, 'Testimonial', $file_path, $flag);
                        }


                	redirect( base_url().'inbound-admin/manage-testimonials' );
                	
                	} // end if
				} // end else
			} 	// end submit	
		$this->load->view('admin/edit-testimonial', $data); 	
  	}		



  	public function delete_Testimonial_info()
  	{
  		echo "Welcome for Next";
  	}




}



 
