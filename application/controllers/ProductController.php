<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class ProductController extends MY_Controller 
{

	public function __construct()
    {
        parent::__construct();
    }

    public function manageProduct()
    {
    	$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Manage Product';
        $data['products'] = $this->getProducts();
        $this->load->view('admin/manage-products', $data);
    }


    public function addNewProduct()
    {
    	$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Add Product';
        $append = " WHERE `parent_id` = 0";
        $data['amenities'] = $this->product->getAmenities($append);
        $data['packages'] = $this->getPackages();
        $data['channels'] = $this->channel->getChannels();
        $data['addons'] = $this->product->getAddOns();

        $append = ' WHERE `is_active` = 1';
        $data['locations'] = $this->city->getCities($append);
       	
        if(!empty($this->input->post('submit'))) {

            $language = 'eng'; // default is english
            
            /* Set Validation rule  in the form */
            $this->form_validation->set_rules('crm_id', 'CRM ID', 'trim|required|is_unique[ct_packages.crm_id]');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[ct_packages.title_'.$language.']');
            $this->form_validation->set_rules('seo_url', 'Seo URL', 'trim|required|is_unique[ct_products.seo_url]');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {


                $crm_id = $this->security->xss_clean($this->input->post('crm_id'));
            	$title = $this->security->xss_clean($this->input->post('title'));
                $short_desc = $this->security->xss_clean($this->input->post('short_desc'));
                $long_description = $this->input->post('long_description');
                $product_type = $this->security->xss_clean($this->input->post('product_type'));
                $validity_date_from = $this->security->xss_clean($this->input->post('validity_date_from'));
                $validity_date_to = $this->security->xss_clean($this->input->post('validity_date_to'));                
                $dflag = $this->security->xss_clean($this->input->post('dflag'));
                // $rprice = $this->security->xss_clean($this->input->post('rprice'));
                $seo_url = $this->security->xss_clean($this->input->post('seo_url'));
                $meta_key = $this->security->xss_clean($this->input->post('meta_key'));
                $meta_desc = $this->security->xss_clean($this->input->post('meta_desc'));
                $location = $this->security->xss_clean($this->input->post('location'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $is_default_addon = $this->security->xss_clean($this->input->post('is_default_addon'));
                $code = 'CITIPINB'.mt_rand(10000,99999);

                $input = array(
                    'crm_id' => strtoupper($crm_id),
                    'code' => $code,
                    'deletion_flag' => $dflag,
                    // 'regular_price' => $rprice,
                    'validity_date_from' => $validity_date_from,
                    'validity_date_to' => $validity_date_to,
                    'type' => $product_type,
                    'seo_url' => $seo_url,
                    'meta_key' => $meta_key,
                    'meta_description' => $meta_desc,

                    'title_'.$language => ucfirst($title),
                    'short_desc_'.$language => ucfirst($short_desc),
                    'long_desc_'.$language => ucfirst($long_description),
                );


                switch ($product_type) {
                    case '7':
                        $input['rating'] = $this->input->post('product_rating');
                        break;
                    default:
                        break;
                }


                // return current inserted product ID
                $product_id = $this->product->store($input);




                $channels = $this->input->post('channels');
                $packages = $this->input->post('packages');
                $amenities = $this->input->post('amenities');

                if( $product_id > 0 ) {


                    if(!empty($location)) {
                        foreach ($location as $key => $value) {

                            $temp = array(
                                'object_id' => $product_id,
                                'object_type' => 'product',
                                'city_id' => $value,
                                'flag' => 'hotel',
                            );
                            $this->product->storeObjectLocation( $temp );

                            $temp = array();
                        }
                    }
                    


                    // incase if product type is hotel / meal / extension
                    if($product_type == 7 || $product_type == 8 || $product_type == 13) 
                    {
                        $rates = $this->security->xss_clean($this->input->post('rates'));

                        if(!empty($rates)) {
                            foreach ($rates as $r_key => $r_value) {

                                $is_addon = ($r_key == $is_default_addon) ? 1 : 0;

                                foreach ($currency[$r_key] as $key => $value) {
                                    
                                    $temp = array();

                                    if(!empty($value)) {

                                        $temp = array(
                                            'currency_id' => $key,
                                            'object_id' => $product_id,
                                            'objectable_type' => 'product',
                                            'sub_object_id' => $r_key,
                                            'sub_objectable_type' => 'addon',
                                            'value' => $value,
                                            'flag' => 'inbound',
                                            'is_default_addon' => $is_addon,
                                        );

                                        $this->currency->store_currency_objects($temp);

                                    }
                                }
                            }
                        }
                    }




                        // store package products
	                    if(!empty($packages)) {
                            $input = array();
	                        foreach ($packages as $key => $value) {
	                            $input = array('package_id' => $value, 
	                            'product_id' => $product_id,
	                            );
	                            $this->product->store_package_product($input);
	                        }
	                    } // if package is not empty


                        // store product amenities
                        if(!empty($amenities)) {
                            $input = array();
                            foreach ($amenities as $key => $value) {
                                $input = array( 'product_id' => $product_id, 
                                'amenity_id' => $value );
                                $this->product->store_product_amenities($input);
                            }
                        }


                        if(!empty($channels)) {
                        $input = array();
                        foreach ($channels as $key => $value) {
                           $input = array('object_id' => $product_id, 
                                'channel_id' => $value,
                                'object_type' => 'Product',
                            );
                            $this->channel->store_channel_objects($input);
                        }
                    } // if channels is not empty


                        // product logo
                    	if( !empty($_FILES['product_image']['name']) && !empty($_FILES['product_image']['name'][0]) )
                    	{
                            $flag = 'product_logo';
                            $file_path = 'assets/uploads/products/';
                            file_upload($_FILES['product_image'], $product_id, 'Product', $file_path, $flag);

                        } else {

                            $data = array(
                                'fileable_id' => $product_id,
                                'fileable_type' => 'Product',
                                'file_name' => 'product-avater.png',
                                'file_path' => 'assets/uploads/products/',
                                'flag' => 'product_logo',
                            );
                            $this->files->store($data);
                    	} // end else



                        // product banner
                        if( !empty($_FILES['product_banner']['name']) && !empty($_FILES['product_banner']['name'][0]) )
                        {
                            $flag = 'product_banner';
                            $file_path = 'assets/uploads/banners/';
                            file_upload($_FILES['product_banner'], $product_id, 'Product', $file_path, $flag);

                        } else {

                            $data = array(
                                'fileable_id' => $product_id,
                                'fileable_type' => 'Product',
                                'file_name' => 'default_banner.jpg',
                                'file_path' => 'assets/uploads/banners/',
                                'flag' => 'product_banner',
                            );
                            $this->files->store($data);
                        } // end else




                        // product gallery
                        if( !empty($_FILES['product_gallery']['name']) && !empty($_FILES['product_gallery']['name'][0]) ){
                            $file_path = 'assets/uploads/gallery/';
                            $flag = 'product_gallery';
                            file_upload($_FILES['product_gallery'], $product_id, 'Product', $file_path, $flag);
                        }



                    redirect(base_url().'inbound-admin/manage-products');
            	}
            }
        } // end submit    

        $this->load->view('admin/add-product', $data);
    }




    public function updateProduct($product_id)
    {
        $append_amenities = null;
    	$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Edit Product';
        $data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';
        $data['product'] = $this->product->getProductDetailsByID( $product_id, $data['lang'] );
        $data['currencies'] = $this->currency->getCurrencies();

        $data['locations'] = $this->city->getCities();
        foreach ($data['locations'] as $key => $value) {
            $data['locations'][$key]['selected'] = $this->product->checkObjectHasCity( $product_id, $value['id'] );
        }

        if($data['product']['type'] == 7) {
            $append = " WHERE `flag` = 'hotel' AND `is_active` = 1 ";
            $data['addons'] = $this->product->getAddOns($append);
            $append_amenities = " WHERE `parent_id` = 7";

        } elseif ($data['product']['type'] == 8) {
            $append = " WHERE `flag` = 'meal' AND `is_active` = 1 ";
            $data['addons'] = $this->product->getAddOns($append);
            $append_amenities = " WHERE `parent_id` = 8";

        } elseif ($data['product']['type'] == 14) {
            $append = " WHERE `flag` = 'special_interests' AND `is_active` = 1 ";
            $data['addons'] = $this->product->getAddOns($append);
            $append_amenities = " WHERE `parent_id` = 14";
            
        } else {
            $append = " WHERE `flag` = 'extension' AND `is_active` = 1 ";
            $data['addons'] = $this->product->getAddOns($append);
            $append_amenities = " WHERE `parent_id` = 13";
        }


        $append = " WHERE `parent_id` = 0";
        $data['parent_amenities'] = $this->product->getAmenities($append);
        
        $data['packages'] = $this->getPackages();
        foreach ($data['packages'] as $key => $value) {
        	$data['packages'][$key]['selected'] = $this->product->checkPackageHasProduct( $value['id'], $product_id );
        }

        $data['amenities'] = $this->product->getAmenities( $append_amenities );
        foreach ($data['amenities'] as $key => $value) {
            $data['amenities'][$key]['selected'] = $this->product->checkProductHasAmenities( $value['id'], $product_id );
        }


        $temp3 = $this->channel->getChannels();
        foreach ($temp3 as $key => $value) {
            $temp3[$key]['selected'] = $this->channel->checkChannelHasObject( $value['id'], $product_id, 'Product' ) ? $this->channel->checkChannelHasObject( $value['id'], $product_id, 'Product' ) : '';
        }
        $data['channels'] = $temp3;


        if(!empty($this->input->post('submit')))
        {

                // $is_default_addon = $this->security->xss_clean($this->input->post('is_default_addon'));
                // $rates = $this->security->xss_clean($this->input->post('rates'));
                // echo '<pre>';
                // echo $is_default_addon;
                // print_r($rates);
                // exit();

                /* Set Validation rule  in the form */
                $this->form_validation->set_rules('crm_id', 'CRM ID', 'trim|callback_crm_check[' . $product_id . ']');
                $this->form_validation->set_rules('title', 'Title', 'trim|callback_title_check[' . $product_id . ']');
                $this->form_validation->set_rules('seo_url', 'Seo URL', 'trim|callback_seourl_check[' . $product_id . ']');

                if ($this->form_validation->run() == FALSE) {
                    //auto redirect to main page if any validation failed and show the error logs if any.
                } else {


        		$crm_id = $this->security->xss_clean($this->input->post('crm_id'));
            	$title = $this->security->xss_clean($this->input->post('title'));
                $short_desc = $this->security->xss_clean($this->input->post('short_desc'));
                $long_description = $this->input->post('long_description');
                $product_type = $this->security->xss_clean($this->input->post('product_type'));
                $validity_date_from = $this->security->xss_clean($this->input->post('validity_date_from'));
                $validity_date_to = $this->security->xss_clean($this->input->post('validity_date_to'));                
                $dflag = $this->security->xss_clean($this->input->post('dflag'));
                // $rprice = $this->security->xss_clean($this->input->post('rprice'));
                $seo_url = $this->security->xss_clean($this->input->post('seo_url'));
                $meta_key = $this->security->xss_clean($this->input->post('meta_key'));
                $meta_desc = $this->security->xss_clean($this->input->post('meta_desc'));
                $location = $this->security->xss_clean($this->input->post('location'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $deleted_at = $this->security->xss_clean($this->input->post('status'));
                $is_default_addon = $this->security->xss_clean($this->input->post('is_default_addon'));

                $product_rating = $this->input->post('product_rating');


                $request = array( 
                    'crm_id' => strtoupper($crm_id),
                    'deletion_flag' => $dflag,
                    // 'regular_price' => $rprice,
                    'validity_date_from' => $validity_date_from,
                    'validity_date_to' => $validity_date_to,
                    'type' => $product_type,
                    'seo_url' => $seo_url,
                    'meta_key' => $meta_key,
                    'meta_description' => $meta_desc,
                    'rating' => $product_rating,

                    'title_'.$data['lang'] => ucfirst($title),
                    'short_desc_'.$data['lang'] => ucfirst($short_desc),
                    'long_desc_'.$data['lang'] => ucfirst($long_description),

                    'deleted_at' => $deleted_at == 'publish' ? NULL : date('Y-m-d'),
                );


                $updated_id = $this->product->update($product_id, $request);

                $channels = $this->input->post('channels');
                $packages = $this->input->post('packages');
                $amenities = $this->input->post('amenities');

                if($updated_id > 0) {




                    if(!empty($location)) {

                        $this->product->deleteExistingObjectCity($updated_id);

                        foreach ($location as $key => $value) {

                            $temp = array(
                                'object_id' => $updated_id,
                                'object_type' => 'product',
                                'city_id' => $value,
                                'flag' => 'hotel',
                            );
                            $this->product->storeObjectLocation( $temp );

                            $temp = array();
                        }
                    }





                    // incase if product type is hotel / meal / extension
                    if($product_type == 7 || $product_type == 8 || $product_type == 13) 
                    {

                        $this->currency->deleteExistingObjectCurrency($product_id);

                        $rates = $this->security->xss_clean($this->input->post('rates'));

                        if(!empty($rates)) {
                            foreach ($rates as $r_key => $r_value) {

                                $is_addon = ($r_key == $is_default_addon) ? 1 : 0;

                                foreach ($currency[$r_key] as $key => $value) {
                                    
                                    $temp = array();

                                    if(!empty($value)) {

                                        $temp = array(
                                            'currency_id' => $key,
                                            'object_id' => $product_id,
                                            'objectable_type' => 'product',
                                            'sub_object_id' => $r_key,
                                            'sub_objectable_type' => 'addon',
                                            'value' => $value,
                                            'flag' => 'inbound',
                                            'is_default_addon' => $is_addon,
                                        );

                                        $this->currency->store_currency_objects($temp);

                                    }
                                }
                            }
                        }
                    }






                    // store new packages
                	$this->product->deleteExistingPackageProduct($product_id);
                    if(!empty($packages)) {
                        $input = array();
                        foreach ($packages as $key => $value) {
                            $input = array('package_id' => $value, 
                            'product_id' => $product_id,
                            );
                            $this->product->store_package_product($input);
                        }
                    } // if package is not empty


                    // store new amenities
                    $this->product->deleteExistingProductAmenities($product_id);
                    if(!empty($amenities)) {
                            $input = array();
                            foreach ($amenities as $key => $value) {
                                $input = array( 'product_id' => $product_id, 
                                'amenity_id' => $value );
                                $this->product->store_product_amenities($input);
                            }
                        }


                    $this->channel->deleteExistingChannelsObjects($product_id, 'Product');
                    if(!empty($channels)) {
                        $input = array();
                        foreach ($channels as $key => $value) {
                           $input = array('object_id' => $product_id, 
                                'channel_id' => $value,
                                'object_type' => 'Product',
                            );
                            $this->channel->store_channel_objects($input);
                        }
                    } // if channels is not empty


                    // product logo
                    if( !empty($_FILES['product_image']['name']) && !empty($_FILES['product_image']['name'][0]) ) 
                    {
                        $flag = 'product_logo';
	                    $files = $this->files->getFilesByFileableId( $product_id, 'Product', $flag );

	                    $this->files->delete($files['id']);

	                    $file_path = 'assets/uploads/products/';
	                    file_upload($_FILES['product_image'], $product_id, 'Product', $file_path, $flag);
	                }



                    // product banner
                    if( !empty($_FILES['product_banner']['name']) && !empty($_FILES['product_banner']['name'][0]) ) 
                    {

                        $flag = 'product_banner';
                        $files = $this->files->getFilesByFileableId( $product_id, 'Product', $flag );
                        $this->files->delete($files['id']);

                        $file_path = 'assets/uploads/banners/';
                        file_upload($_FILES['product_banner'], $product_id, 'Product', $file_path, $flag);
                    }



                    // product gallery
                    if( !empty($_FILES['product_gallery']['name']) && !empty($_FILES['product_gallery']['name'][0]) ){
                        $file_path = 'assets/uploads/gallery/';
                        $flag = 'product_gallery';
                        file_upload($_FILES['product_gallery'], $product_id, 'Product', $file_path, $flag);
                    }


                	redirect(base_url().'inbound-admin/manage-products');
                }
            }

        } // end submit

        // echo '<pre>';
        // print_r($data);
        // exit();
        
    	$this->load->view('admin/edit-product', $data);
    }





    public function crm_check( $crm_id, $product_id )
    {
        $data = $this->product->credentials_exists( $product_id );
        if ($crm_id == $data['crm_id']) {
            return true;
        } else {
            $distinct_crm = $this->product->distinct_credentials( 'crm_id' );
            foreach ($distinct_crm as $key => $value) {
                if ($value['crm_id'] == $crm_id) {
                    $this->form_validation->set_message('crm_check', 'The CRM number you have entered is already being used.');
                    return false;
                }
            }
        }
    }


    public function seourl_check( $seo_url, $product_id ) 
    {
        $data = $this->product->credentials_exists( $product_id );
        if ($seo_url == $data['seo_url']) {
            return true;
        } else {
            $distinct_seo = $this->product->distinct_credentials( 'seo_url' );
            foreach ($distinct_seo as $key => $value) {
                if ($value['seo_url'] == $seo_url) {
                    $this->form_validation->set_message('seourl_check', 'The SEO URL you have entered is already being used.');
                    return false;
                }
            }
        }
    }



    public function title_check( $title_eng, $product_id ) 
    {
        $data = $this->product->credentials_exists( $product_id );
        if ($title_eng == $data['title_eng']) {
            return true;
        } else {
            $distinct_title = $this->product->distinct_credentials( 'title_eng' );
            foreach ($distinct_title as $key => $value) {
                if ($value['title_eng'] == $title_eng) {
                    $this->form_validation->set_message('title_check', 'The Title you have entered is already being used.');
                    return false;
                }
            }
        }
    }




    public function getProductSubTypes( ) 
    {
        $product_parent_id = $_POST['parent_id'];
        $append = " WHERE `parent_id` = ".$product_parent_id;
        $amenities = $this->product->getAmenities($append);

        if(!empty($amenities)) {
            foreach ($amenities as $key => $value) {
                echo '<option value="'.$value['id'].'">'. $value['name_eng']. '</option>';
            }
        }

    }



    public function getProductVariation()
    {
        $flag = $_POST['flag'];
        $currencies = $this->currency->getCurrencies();
        $append = " WHERE `is_active` = 1 AND `flag` = '".$flag."'";
        $addons = $this->product->getAddOns( $append );

        if(!empty($addons)) {

            foreach ($addons as $key => $value) {
                
                echo '<div class="input-group">';
                echo '<span class="input-group-addon">'.$value['type_name_eng'].'</span>';
                echo '<input type="hidden" min="0" name=rates['.$value['id'].'] >';

                foreach ($currencies as $cur_key => $cur_value) {
                    
                    echo '<input type="number" min="0" name=currency['.$value['id'].']['.$cur_value['id'].'] placeholder="'.$cur_value['cur_name'].'" class="form-control" >';

                }

                echo '<span class="input-group-addon"><input type="radio" name="is_default_addon" value='.$value['id'].' > is Default</span>';

                echo '</div><br>';
            }
        } else {
            echo 'No variation found.';
        }

        // echo '<pre>';
        // print_r($currencies);
        // exit();
    }




} // end controller

?>