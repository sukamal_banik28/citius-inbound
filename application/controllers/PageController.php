<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class PageController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }


    public function managePages()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Pages';
        $data['pages'] = $this->page->getPages();
        $this->load->view('admin/manage-pages', $data);
    }


    public function addPages()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Add Pages';

        if(!empty($this->input->post('submit'))) {

            $lang = 'eng'; // Default language English

            /* Set Validation rule  in the form */
            $this->form_validation->set_rules('seo_url', 'SEO Url', 'trim|required|is_unique[ct_pages.seo_url]');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {

                $title = $this->security->xss_clean($this->input->post('title'));
                $short_desc = $this->security->xss_clean($this->input->post('short_desc'));
                $long_desc = $this->input->post('long_desc');
                $seo_url = $this->security->xss_clean($this->input->post('seo_url'));
                $meta_key = $this->security->xss_clean($this->input->post('meta_key'));
                $meta_desc = $this->security->xss_clean($this->input->post('meta_desc'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $input = array(
                    'title_'.$lang => $title,
                    'short_desc_'.$lang => $short_desc,
                    'long_desc_'.$lang => trim($long_desc),
                    'seo_url' => $seo_url,
                    'meta_key' => $meta_key,
                    'meta_description' => $meta_desc,
                    'status' => $status
                    );


                $page_id = $this->page->store($input);

                if($page_id > 0) {
                    redirect(base_url().'inbound-admin/manage-pages');
                }

            }

        }
        $this->load->view('admin/add-pages', $data);
    }


    public function updatePages($page_id)
    {
    	$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Pages Details';
        $data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';
        $data['page'] = $this->page->getPageDetails( $page_id, $data['lang'] );

        if(!empty($this->input->post('submit'))) { 

        	$title = $this->security->xss_clean($this->input->post('title'));
            $status = $this->security->xss_clean($this->input->post('status'));
            
            $long_desc = $this->input->post('long_desc');
            $description_spanish = $this->input->post('description_spanish');


            $request = array(
                'title' => $title,
                'description' => $long_desc,
                'description_spanish' => $description_spanish,
                'is_active' => $status,
                'updated_at' => date('Y-m-d'),
                );

        	$update_id = $this->page->update( $page_id, $request );
        	    
            if($update_id > 0) {
    		  redirect(base_url().'inbound-admin/manage-pages');
    	   }

        } // end submit

        $this->load->view('admin/edit-pages', $data);
    }



    public function seourl_check( $seo_url, $page_id ) 
    {
        $data = $this->page->credentials_exists( $page_id );
        if ($seo_url == $data['seo_url']) { 
            return true;
        } else {
            $distinct_seo = $this->page->distinct_credentials( 'seo_url' );
            foreach ($distinct_seo as $key => $value) {
                if ($value['seo_url'] == $seo_url) {
                    $this->form_validation->set_message('seourl_check', 'The SEO URL you have entered is already being used.');
                    return false;
                }
            }
        }
    }


} // end controller
?>