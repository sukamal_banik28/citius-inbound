<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class ReviewController extends MY_Controller 
{

	public function __construct()
    {
        parent::__construct();
    }

    public function manageReviews()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Reviews';
        $data['reviews'] = $this->review->getReviews();
        if(!empty($data['reviews'])) {
            foreach ($data['reviews'] as $key => $value) {
                $data['reviews'][$key]['package'] = $this->package->getPackageDetailsByID( $value['reviewable_id'] );
                $data['reviews'][$key]['user'] = $this->users->getUserByID( $value['user_id'] );
            }
        }
        $this->load->view('admin/manage-reviews', $data);
    }


    public function updateReview( $review_id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Reviews';
        $data['review'] = $this->review->getReviewDetailsByID( $review_id );

        if(!empty($this->input->post('submit'))) 
        {
            $status = $this->input->post('status');
            $updated_at = date('Y-m-d H:i:s');
            $deleted_at = $status == 'unpublished' ? date('Y-m-d H:i:s') : NULL;

            $request = array( 'status' => $status,
                'updated_at' => $updated_at,
                'deleted_at' => $deleted_at );

            $updated_id = $this->review->update( $review_id, $request );
            if($updated_id > 0) {
                redirect( base_url().'inbound-admin/manage-packages/reviews' );
            }
        }

        $this->load->view('admin/edit-review', $data);
    }


} // end controller

?>