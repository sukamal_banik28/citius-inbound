<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

	protected $user;

	public function __construct()
    {
        parent::__construct();
        $this->user = checkSession();
        $this->parent_categories = getCategoriesByFlag('Package');
        date_default_timezone_set('Asia/Kolkata');
    }

	public function manageProfile()
	{
		$data = $this->userDetails();
        $pagetitle = 'Profile';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );
		$this->load->view( 'user/manage-profile', $data );
	}


	public function updateProfile($user_id = null)
	{
		$data = $this->userDetails();
		$pagetitle = 'Update';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

		if(!empty($this->input->post('submit'))) {

			/* Set Validation rule  in the form */
            $this->form_validation->set_rules('phone', 'Phone Number', 'trim|callback_phone_check[' . $user_id . ']');
            $this->form_validation->set_rules('email', 'Email', 'trim|callback_email_check[' . $user_id . ']');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {

            	$name = $this->security->xss_clean($this->input->post('name'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $address = $this->security->xss_clean($this->input->post('address'));
               
                $explode_address = explode(' ', $address);
                $country = end($explode_address);

                $input = array( 'name' => ucwords($name),
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'country' => $country,
                  );


                $updated_id = $this->users->update( $user_id, $input );

                if($updated_id > 0) {


                	// package banner
                    if( !empty($_FILES['profile_photo']['name']) && !empty($_FILES['profile_photo']['name'][0]) ) {

                            $flag = 'profile_photo';
                            $files = $this->files->getFilesByFileableId( $user_id, 'User', $flag );
                            $this->files->delete($files['id']);

                            $file_path = 'assets/uploads/users/';
                            file_upload($_FILES['profile_photo'], $user_id, 'User', $file_path, $flag);
                        }


                	redirect( base_url().'user/manage-profile?lang='.$data['lang']);
                }

            }

		}

		$this->load->view( 'user/edit-profile', $data );
	}


    public function manageEnquire()
    {
        $data = $this->userDetails();
        $pagetitle = 'Enquire';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        $enquiries = $this->enquiry->getEnquiries();

        if(!empty($enquiries)) {
            $temp = array();
            foreach ($enquiries as $key => $value) {
                if( $value['user_id'] == $data['user']['id'] ) {
                    $temp[$key] = $value;
                }
            }

            $data['enquiries'] = $temp;
        }

        $this->load->view( 'user/manage-enquire', $data );
    }



    public function managePassword()
    {
        $data = $this->userDetails();
        $data['pagetitle'] = 'Change';

        // echo '<pre>';
        // print_r($data); exit();

        if( !empty($this->input->post('submit')) )
        {
            $this->form_validation->set_rules('old_password', 'Password', 'trim|required|min_length[6]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {

                $old_password = hash( 'sha256', $this->security->xss_clean($this->input->post('old_password')) );
                $new_password = hash( 'sha256', $this->security->xss_clean($this->input->post('password')) );

                $user_id = $data['user']['id'];
                $flag = true;
                

                if( $data['user']['password'] != $old_password ) { 
                    $flag = false;
                    $data['errorMsg'] = 'Your current password is not matching with your existing password.';
                }

                if($flag) { 
                    if( $old_password == $new_password ) { 
                        $data['errorMsg'] = 'Current Password and New Password can not be same. Please try a different one.';
                    } else {

                        $request = array( 'password' => $new_password );
                        $updated_id = $this->users->update( $user_id, $request );
                        
                        if($updated_id > 0) {
                            $data['errorMsg'] = 'Password changed successfully.';

                            if(!empty($data['user']['email'])) {
                                $mesg = $this->load->view( 'admin/email-templates/change_password_success', $data, true );
                                $to = $data['user']['email'];
                                $subject = 'Password Changed Succesfully.';
                                // sendEmailToClient($to, $subject, $mesg);
                                sendInboundEmailNotification($to, $subject, $mesg);
                            }

                        }
                    }
                }

                

            } // end else
        }

        $this->load->view( 'user/manage-password', $data );
    }



	public function userDetails()
	{
		$data = array();
		$data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';
		$data['user'] = $this->user;
		$data['profile_photo'] = $this->files->getFilesByFileableId( $data['user']['id'], 'User', 'profile_photo' );
		return $data;
	}


	public function phone_check( $phone, $user_id )
    {
        $data = $this->users->credentials_exists( $user_id );
        if ($phone == $data['phone']) {
            return true;
        } else {
            $distinct_crm = $this->users->distinct_credentials( 'phone' );
            foreach ($distinct_crm as $key => $value) {
                if ( $value['phone'] == $phone ) {
                    $this->form_validation->set_message('phone_check', 'The Phone number you have entered is already being used. Please use another one.');
                    return false;
                }
            }
        }
    }


    public function email_check( $email, $user_id )
    {
        $data = $this->users->credentials_exists( $user_id );
        if ($email == $data['email']) {
            return true;
        } else {
            $distinct_crm = $this->users->distinct_credentials( 'email' );
            foreach ($distinct_crm as $key => $value) {
                if ( $value['email'] == $email ) {
                    $this->form_validation->set_message('email_check', 'The Email you have entered is already being used. Please use another one.');
                    return false;
                }
            }
        }
    }


} // end controller
