<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class PackageController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }



    public function managePackage()
    {
    	$data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Packages';
        $data['packages'] = $this->getPackages();
    	$this->load->view('admin/manage-packages', $data);
    }


    public function manageDeletedPackage()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Packages';
        $data['packages'] = $this->getDeletedPackages();
        $this->load->view('admin/manage-packages', $data);
    }


    public function addNewPackage()
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound New Package';
        $data['categories'] = $this->getCategories();
        $data['channels'] = $this->channel->getChannels();
        $data['currencies'] = $this->currency->getCurrencies();
        $data['travel_pax'] = $this->currency->getTravelPax();

        if(!empty($this->input->post('submit'))) {

            $language = 'eng'; // default is english
            
            /* Set Validation rule  in the form */
            $this->form_validation->set_rules('crm_id', 'CRM ID', 'trim|required|is_unique[ct_packages.crm_id]');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[ct_packages.title_'.$language.']');
            $this->form_validation->set_rules('seo_url', 'Seo URL', 'trim|required|is_unique[ct_packages.seo_url]');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {

                $crm_id = $this->security->xss_clean($this->input->post('crm_id'));                
                $seat = $this->security->xss_clean($this->input->post('seat'));
                $days = $this->security->xss_clean($this->input->post('days'));
                $nights = $this->security->xss_clean($this->input->post('nights'));
                $inventory = $this->security->xss_clean($this->input->post('inventory'));                
                $validity_date_from = $this->security->xss_clean($this->input->post('validity_date_from'));
                $validity_date_to = $this->security->xss_clean($this->input->post('validity_date_to'));                
                $dflag = $this->security->xss_clean($this->input->post('dflag'));

                $price = $this->security->xss_clean($this->input->post('price'));                
                // $rprice = $this->security->xss_clean($this->input->post('rprice'));
                // $sprice = $this->security->xss_clean($this->input->post('sprice'));
                // $gst = $this->security->xss_clean($this->input->post('gst'));

                $citius_touch = $this->security->xss_clean($this->input->post('citius_touch'));
                $tags = $this->security->xss_clean($this->input->post('tags'));
                $seo_url = $this->security->xss_clean($this->input->post('seo_url'));
                $meta_key = $this->security->xss_clean($this->input->post('meta_key'));
                $meta_desc = $this->security->xss_clean($this->input->post('meta_desc'));

                $seo_url = removeAccents($seo_url);

                $title = $this->security->xss_clean($this->input->post('title'));
                $short_desc = $this->security->xss_clean($this->input->post('short_desc'));
                $long_description = $this->input->post('long_description');
                $city_depart = $this->security->xss_clean($this->input->post('city_depart'));
                $city_arrival = $this->security->xss_clean($this->input->post('city_arrival'));
                $cpolicy = $this->security->xss_clean($this->input->post('cpolicy'));

                $tpax = $this->security->xss_clean($this->input->post('tpax'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                
                if($citius_touch == 'Customised') {
                    $code = 'CITIPINB'.mt_rand(10000,99999);
                } else {
                    $code = 'FDGT'.mt_rand(10000,99999);
                }

                $input = array( 
                    'crm_id' => strtoupper($crm_id),
                    'seat' => $seat,
                    'code' => $code,
                    'days' => $days,
                    'nights' => $nights,
                    'inventory' => $inventory,
                    'deletion_flag' => $dflag,
                    'validity_date_from' => $validity_date_from,
                    'validity_date_to' => $validity_date_to,
                    // 'regular_price' => $rprice,
                    // 'sales_price' => $sprice,
                    // 'gst' => $gst,
                    'citius_touch' => $citius_touch,
                    'tags' => ucwords($tags),
                    'seo_url' => $seo_url,
                    'meta_key' => $meta_key,
                    'meta_description' => $meta_desc,
                    'is_new' => 1,

                    'title_'.$language => $title,
                    'short_desc_'.$language => $short_desc,
                    'long_desc_'.$language => $long_description,
                    'depart_city_'.$language => ucwords($city_depart),
                    'arrival_city_'.$language => ucwords($city_arrival),
                    'cancellation_policy_'.$language => $cpolicy
                );


                // return current inserted package ID
                $package_id = $this->package->store($input);

                $channels = $this->input->post('channels');
                $categories = $this->input->post('categories');
                $sub_categories = $this->input->post('sub_categories');

                // if package id is created
                if($package_id > 0) {



                    // insert value for package variation
                    if(!empty($tpax)) {
                    foreach ($tpax as $r_key => $r_value) {

                        foreach ($currency[$r_key] as $key => $value) {
                            
                            $temp = array();

                            if(!empty($value)) {

                                $temp = array(
                                    'currency_id' => $key,
                                    'object_id' => $package_id,
                                    'objectable_type' => 'package',
                                    'travel_pax_id' => $r_key,
                                    'value' => $value,
                                    'flag' => 'inbound'
                                );

                                    $this->currency->store_currency_objects($temp);
                                }
                            }
                        }
                    }




                    if(!empty($price)) {
                    foreach ($price as $key => $value) {
                        $temp = array(
                            'currency_id' => $key, 
                            'value' => $value,
                            'object_id' => $package_id,
                            'objectable_type' => 'package',
                            'flag' => 'inbound'
                        );

                            $this->currency->store_currency_objects($temp);
                        }
                    }



                    if(!empty($categories)) {
                        $input = array();
                        foreach ($categories as $key => $value) {

                            $input = array('object_id' => $package_id,
                                'object_type' => 'Package',
                                'category_id' => $value,
                                );
                            $this->category->store_category_object($input);

                        }
                    } // if category is not empty


                    if(!empty($sub_categories)) {
                        $input = array();
                        foreach ($sub_categories as $key => $value) {

                            $input = array('object_id' => $package_id,
                                'object_type' => 'Package',
                                'category_id' => $value,
                                );
                            $this->category->store_category_object($input);

                        }
                    } // if sub-category is not empty



                    if(!empty($channels)) {
                        $input = array();
                        foreach ($channels as $key => $value) {
                           $input = array('object_id' => $package_id, 
                                'channel_id' => $value,
                                'object_type' => 'Package',
                            );
                            $this->channel->store_channel_objects($input);
                        }
                    } // if channels is not empty


                        // package image
                        if( !empty($_FILES['package_image']['name']) && !empty($_FILES['package_image']['name'][0]) ) {

                            $file_path = 'assets/uploads/packages/';
                            file_upload($_FILES['package_image'], $package_id, 'Package', $file_path, 'package_logo');
                        } else {  

                            $data = array(
                                'fileable_id' => $package_id,
                                'fileable_type' => 'Package',
                                'file_name' => 'package-avater.png',
                                'file_path' => 'assets/uploads/packages/',
                                'flag' => 'package_logo',
                            );
                            $this->files->store($data);
                        } // end else



                        // package banner
                        if( !empty($_FILES['package_banner']['name']) && !empty($_FILES['package_banner']['name'][0]) ) {

                            $file_path = 'assets/uploads/banners/';
                            file_upload($_FILES['package_banner'], $package_id, 'Package', $file_path, 'package_banner');
                        } else {  

                            $data = array(
                                'fileable_id' => $package_id,
                                'fileable_type' => 'Package',
                                'file_name' => 'default_banner.jpg',
                                'file_path' => 'assets/uploads/banners/',
                                'flag' => 'package_banner',
                            );
                            $this->files->store($data);
                        } // end else




                        // package gallery
                        if( !empty($_FILES['package_gallery']['name']) && !empty($_FILES['package_gallery']['name'][0]) ){
                            $file_path = 'assets/uploads/gallery/';
                            $flag = 'package_gallery';
                            file_upload($_FILES['package_gallery'], $package_id, 'Package', $file_path, $flag);
                        }






                } // end if
                
                redirect(base_url().'inbound-admin/manage-packages');

                } // end else
            } // end submit
            $this->load->view('admin/add-package', $data);
        }
    



    public function updatePackage($package_id)
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Edit Package';
        $data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';
        $data['records'] = $this->currency->getCurrenciesObejcts($package_id, 'package');
        $data['package'] = $this->package->getPackageDetailsByID($package_id, $data['lang']);

        $temp = $this->getCategories();
        foreach ($temp as $key => $value) {
            $temp[$key]['selected'] = $this->category->checkObjectHasCategory( $package_id, 'Package', $value['id'] ) ? $this->category->checkObjectHasCategory( $package_id, 'Package', $value['id'] ) : '';
        }
        $data['categories'] = $temp;

        $temp2 = $this->getSubCategories( 'package' );
        foreach ($temp2 as $key => $value) {
            $temp2[$key]['selected'] = $this->category->checkObjectHasCategory( $package_id, 'Package', $value['id'] ) ? $this->category->checkObjectHasCategory( $package_id, 'Package', $value['id'] ) : '';
        }
        $data['sub_categories'] = $temp2;


        $temp3 = $this->channel->getChannels();
        foreach ($temp3 as $key => $value) {
            $temp3[$key]['selected'] = $this->channel->checkChannelHasObject( $value['id'], $package_id, 'Package' ) ? $this->channel->checkChannelHasObject( $value['id'], $package_id, 'Package' ) : '';
        }
        $data['channels'] = $temp3;


        $data['currencies'] = $this->currency->getCurrencies();

        $append1 = " WHERE `type` = 'others'";
        $data['travel_pax'] = $this->currency->getTravelPax($append1);

        $append2 = " WHERE `type` = 'adult' OR `type` = 'child'";
        $data['per_persons'] = $this->currency->getTravelPax($append2);

        // if(!empty($currencies)) {
        //     foreach ($currencies as $key => $value) {
        //         $currencies[$key]['package'] = $this->currency->getObjectValuesForCurrency($value['id'], $package_id, 'package');
        //     }
        //     $data['currencies'] = $currencies;
        // }



        if(!empty($this->input->post('submit'))) {

            /* Set Validation rule  in the form */
            $this->form_validation->set_rules('crm_id', 'CRM ID', 'trim|callback_crm_check[' . $package_id . ']');
            $this->form_validation->set_rules('title', 'Title', 'trim|callback_title_check[' . $package_id . ']');
            $this->form_validation->set_rules('seo_url', 'Seo URL', 'trim|callback_seourl_check[' . $package_id . ']');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {

                $crm_id = $this->security->xss_clean($this->input->post('crm_id'));                
                $seat = $this->security->xss_clean($this->input->post('seat'));
                $days = $this->security->xss_clean($this->input->post('days'));
                $nights = $this->security->xss_clean($this->input->post('nights'));
                $inventory = $this->security->xss_clean($this->input->post('inventory'));                
                $validity_date_from = $this->security->xss_clean($this->input->post('validity_date_from'));
                $validity_date_to = $this->security->xss_clean($this->input->post('validity_date_to'));                
                $dflag = $this->security->xss_clean($this->input->post('dflag'));

                // $price = $this->security->xss_clean($this->input->post('price')); 
                // $rprice = $this->security->xss_clean($this->input->post('rprice'));
                // $sprice = $this->security->xss_clean($this->input->post('sprice'));
                // $gst = $this->security->xss_clean($this->input->post('gst'));

                $citius_touch = $this->security->xss_clean($this->input->post('citius_touch'));
                $tags = $this->security->xss_clean($this->input->post('tags'));
                $seo_url = $this->security->xss_clean($this->input->post('seo_url'));
                $meta_key = $this->security->xss_clean($this->input->post('meta_key'));
                $meta_desc = $this->security->xss_clean($this->input->post('meta_desc'));
                $deleted_at = $this->security->xss_clean($this->input->post('status'));
                $is_new = $this->security->xss_clean($this->input->post('is_new'));

                $seo_url = removeAccents($seo_url);

                $title = $this->security->xss_clean($this->input->post('title'));
                $short_desc = $this->security->xss_clean($this->input->post('short_desc'));
                $long_description = $this->input->post('long_description');
                $city_depart = $this->security->xss_clean($this->input->post('city_depart'));
                $city_arrival = $this->security->xss_clean($this->input->post('city_arrival'));
                $cpolicy = $this->security->xss_clean($this->input->post('cpolicy'));
                // $code = 'CITIPINB'.mt_rand(10000,99999);

                $request = array( 
                    'crm_id' => strtoupper($crm_id),
                    // 'code' => $code,
                    'seat' => $seat,
                    'days' => $days,
                    'nights' => $nights,
                    'inventory' => $inventory,
                    'deletion_flag' => $dflag,
                    'validity_date_from' => $validity_date_from,
                    'validity_date_to' => $validity_date_to,
                    
                    // 'regular_price' => $rprice,
                    // 'sales_price' => $sprice,
                    // 'gst' => $gst,

                    'citius_touch' => $citius_touch,
                    'tags' => ucwords($tags),
                    'seo_url' => $seo_url,
                    'meta_key' => $meta_key,
                    'meta_description' => $meta_desc,
                    'is_new' => $is_new,

                    'title_'.$data['lang'] => $title,
                    'short_desc_'.$data['lang'] => $short_desc,
                    'long_desc_'.$data['lang'] => $long_description,
                    'depart_city_'.$data['lang'] => ucwords($city_depart),
                    'arrival_city_'.$data['lang'] => ucwords($city_arrival),
                    'cancellation_policy_'.$data['lang'] => $cpolicy,
                    'updated_at' => date('Y-m-d'),
                    'deleted_at' => $deleted_at == 'publish' ? NULL : date('Y-m-d'),
                );


                $updated_id = $this->package->update($package_id, $request);

                $channels = $this->input->post('channels');
                $categories = $this->input->post('categories');
                $sub_categories = $this->input->post('sub_categories');
                
                if($updated_id > 0) {

                    $this->category->deleteExistingCategoryObjects( $package_id, 'Package' );
                    $this->channel->deleteExistingChannelsObjects( $package_id, 'Package' );

                    

                    // $this->currency->deleteExistingObjectCurrency( $package_id, 'package' );
                    // if(!empty($price)) {
                    // foreach ($price as $key => $value) {
                    //     $temp = array(
                    //         'currency_id' => $key, 
                    //         'value' => $value,
                    //         'object_id' => $updated_id,
                    //         'objectable_type' => 'package',
                    //         'flag' => 'inbound'
                    //     );

                    //         $this->currency->store_currency_objects($temp);
                    //     }
                    // }



                    $input = array();
                    if(!empty($categories)) {
                        foreach ($categories as $key => $value) {

                            $input = array('object_id' => $package_id,
                                'object_type' => 'Package',
                                'category_id' => $value,
                                );
                            $this->category->store_category_object($input);


                        }
                    } // if category is not empty


                    if(!empty($sub_categories)) {
                        foreach ($sub_categories as $key => $value) {

                            $input = array('object_id' => $package_id,
                                'object_type' => 'Package',
                                'category_id' => $value,
                                );
                            $this->category->store_category_object($input);


                        }
                    } // if sub-category is not empty


                    if(!empty($channels)) {
                        $input = array();
                        foreach ($channels as $key => $value) {
                           $input = array('object_id' => $package_id, 
                                'channel_id' => $value,
                                'object_type' => 'Package',
                            );
                            $this->channel->store_channel_objects($input);
                        }
                    } // if channels is not empty



                    // package banner
                    if( !empty($_FILES['package_image']['name']) && !empty($_FILES['package_image']['name'][0]) ) {

                            $flag = 'package_logo';
                            $files = $this->files->getFilesByFileableId( $package_id, 'Package', $flag );
                            $this->files->delete($files['id']);

                            $file_path = 'assets/uploads/packages/';
                            file_upload($_FILES['package_image'], $package_id, 'Package', $file_path, $flag);
                        }


                    // package banner
                    if( !empty($_FILES['package_banner']['name']) && !empty($_FILES['package_banner']['name'][0]) ) {

                            $flag = 'package_banner';
                            $files = $this->files->getFilesByFileableId( $package_id, 'Package', $flag );
                            $this->files->delete($files['id']);

                            $file_path = 'assets/uploads/banners/';
                            file_upload($_FILES['package_banner'], $package_id, 'Package', $file_path, $flag);
                        }


                    // package gallery
                    if( !empty($_FILES['package_gallery']['name']) && !empty($_FILES['package_gallery']['name'][0]) ) {

                        $flag = 'package_gallery';
                        $file_path = 'assets/uploads/gallery/';
                        file_upload($_FILES['package_gallery'], $package_id, 'Package', $file_path, $flag);
                    }


                    redirect(base_url().'inbound-admin/manage-packages');
                }

            }

        }

        $this->load->view('admin/edit-package', $data);
    }




    public function storePackagePrices() 
    {

        $package_id  = $_POST['package_id'];
        $currency_id  = $_POST['currency_type'];
        $travel_pax  = $_POST['travel_pax'];
        $price  = $_POST['price'];
        $isDefault  = $_POST['isDefault'] == 'true' ? 1 : 0;

        $data = array(
            'currency_id' => $currency_id, 
            'object_id' => $package_id,
            'objectable_type' => 'package',
            'travel_pax_id' => $travel_pax,
            'value' => $price,
            'is_default' => $isDefault,
            'flag' => 'inbound'
        );


        // echo $isDefault;
        // print_r($data); 
        // exit();  

        if($isDefault) {
            $prevCurrency = $this->currency->getObjectValuesForCurrency($currency_id, $package_id, 'package', 'array');

            if(empty($prevCurrency)) {
                $new_currency_id = $this->currency->store_currency_objects($data);
                $this->currency->updateCurrencyObjectsOtherThanNewlyCreated($currency_id, $package_id, 'package');
                $this->currency->updateCurrencyObjectsDefaultValues($currency_id, $package_id, 'package');
            }
        }

        // echo '<pre>';
        // print_r($prevCurrency);
        // exit();

        if(empty($new_currency_id)) {
        
            $this->currency->deletePreviousRecord($currency_id, $package_id, 'package', $travel_pax);
            $last_inserted_id = $this->currency->store_currency_objects($data);

            if($isDefault == 1) {
                $this->currency->updateCurrencyObjectsOtherThanNewlyCreated($currency_id, $package_id, 'package');
                $this->currency->updateCurrencyObjectsDefaultValues($currency_id, $package_id, 'package');
            }
        }

        $records = $this->currency->getCurrenciesObejcts($package_id, 'package');

        // echo '<pre>';
        // print_r($records);
        // exit();

        if(!empty($records)) {
            foreach ($records as $key => $value) {

                $currency = getCurrencyByID($value['currency_id']);
                $pax = getTravelPax($value['travel_pax_id']);

                echo '<tr id="rec'.$value['id'].'">';
                echo '<td>'.$currency[0]['cur_name'].'</td>';
                echo '<td>'.$pax[0]['pax_range'].' '.$pax[0]['type'].'</td>';
                echo '<td>'.$value['value'].'</td>';
                echo '<td>'.$value['is_default'].'</td>';
                echo '<td><input type="button" value="Edit" class="edit" data-currency_id="'.$value['currency_id'].'" data-travel_pax_id="'.$value['travel_pax_id'].'" data-value="'.$value['value'].'" data-is_default="'.$value['is_default'].'" ></td>';
                
                echo '<td><input type="button" class="delete" value="delete" data-record_id="'.$value['id'].'" ></td>';

            }
        }
    }





    public function deleteCurrencyRecord()
    {
        $id = $_POST['record_id'];
        $this->currency->deleteExistingObjectCurrencyByID($id);
        echo true;
    }



    public function packageItinerary( $package_id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Package Itinerary';
        $data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';
        $data['package'] = $this->package->getPackageDetailsByID($package_id, $data['lang']);
        $data['itinerary'] = $this->itinerary->getItinerariesByPackageID( $package_id );
        $this->load->view('admin/manage-itineraries', $data);
    }


    public function addPackageItinerary( $package_id )
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Package Itinerary';
        $data['package'] = $this->package->getPackageDetailsByID($package_id, 'eng');

        if(!empty($this->input->post('submit'))) {
            $language = 'eng'; // default is English

            $day_wise_plan = $this->security->xss_clean($this->input->post('day_wise_plan'));
            $short_desc = $this->input->post('short_desc');
            $long_desc = $this->input->post('long_desc');

            $input = array(
                'package_id' => $package_id,
                'day_wise_plan' => $day_wise_plan, 
                'short_desc_'.$language => $short_desc,
                'long_desc_'.$language => $long_desc,
            );

            // return last inserted ID
            $itinerary_id = $this->itinerary->store($input);

            if( $itinerary_id > 0 ) {

                $request = array(
                    'package_id' => $package_id, 
                    'itinerary_id' => $itinerary_id,
                );
                $this->itinerary->store_package_itinerary($request);

                if( !empty($_FILES['itinerary_image']['name']) && !empty($_FILES['itinerary_image']['name'][0]) ) {

                            $flag = 'itinerary_logo';
                            $file_path = 'assets/uploads/itineraries/';
                            file_upload($_FILES['itinerary_image'], $itinerary_id, 'Itinerary', $file_path, $flag);

                        } else {  
                            
                            $data = array(
                                'fileable_id' => $itinerary_id,
                                'fileable_type' => 'Itinerary',
                                'file_name' => 'itinerary-avater.png',
                                'file_path' => 'assets/uploads/itineraries/',
                                'flag' => 'itinerary_logo',
                            );

                            $this->files->store($data);
                    }

                redirect(base_url().'inbound-admin/manage-packages/package-id/'.$package_id.'/manage-itinerary');
            }

        }

        $this->load->view('admin/add-packages-itinerary', $data);
    }



    public function updateItinerary($itinerary_id)
    {
        $data['user'] = checkRole();
        $data['pagetitle'] = 'Inbound Edit Itinerary';
        $data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';
        $data['itinerary'] = $this->itinerary->getItineraryByID( $itinerary_id, $data['lang'] );

        if(!empty($this->input->post('submit')))
        {
            $day_wise_plan = $this->security->xss_clean($this->input->post('day_wise_plan'));
            $short_desc = $this->input->post('short_desc');
            $long_desc = $this->input->post('long_desc');

            $request = array(
                'day_wise_plan' => $day_wise_plan, 
                'short_desc_'.$data['lang'] => $short_desc,
                'long_desc_'.$data['lang'] => $long_desc,
                'updated_at' => date('Y-m-d'),
            );

            $updated_id = $this->itinerary->update( $itinerary_id, $request );

            if($updated_id > 0) {

                if( !empty($_FILES['itinerary_image']['name']) && !empty($_FILES['itinerary_image']['name'][0]) )   {

                    $flag = 'itinerary_logo';
                    $files = $this->files->getFilesByFileableId( $itinerary_id, 'Itinerary', $flag );
                    $this->files->delete($files['id']);

                    $file_path = 'assets/uploads/itineraries/';
                    file_upload($_FILES['itinerary_image'], $itinerary_id, 'Itinerary', $file_path, $flag);
                }

                redirect(base_url().'inbound-admin/manage-packages/package-id/'.$data['itinerary']['package_id'].'/manage-itinerary');
            }
        } // end submit

        $this->load->view('admin/edit-packages-itinerary', $data);
    }


    public function deleteItinerary()
    {
        $data['user'] = checkRole();
        $itinerary_id = $_POST['itinerary_id'];
        $this->itinerary->delete($itinerary_id);
        echo true;
    }



    public function getCategories( $flag = 'package' )
    {
        $temp = array();
        $categories = $this->category->getCategoriesByFlag( $flag );       

        if(!empty($categories)){
            foreach ($categories as $key => $value) {
                if($value['parent_id'] == 0) {
                    $temp[$key] = $value;
                }
            }
            return $temp;
        }
    }



    public function getSubCategories( $flag = 'package' )
    {
        $temp = array();
        $categories = $this->category->getCategoriesByFlag( $flag );       

        if(!empty($categories)){
            foreach ($categories as $key => $value) {
                if($value['parent_id'] != 0) {
                    $temp[$key] = $value;
                }
            }
            return $temp;
        }
    }


    public function crm_check( $crm_id, $package_id )
    {
        $data = $this->package->credentials_exists( $package_id );
        if ($crm_id == $data['crm_id']) {
            return true;
        } else {
            $distinct_crm = $this->package->distinct_credentials( 'crm_id' );
            foreach ($distinct_crm as $key => $value) {
                if ($value['crm_id'] == $crm_id) {
                    $this->form_validation->set_message('crm_check', 'The CRM number you have entered is already being used.');
                    return false;
                }
            }
        }
    }


    public function seourl_check( $seo_url, $package_id ) 
    {
        $data = $this->package->credentials_exists( $package_id );
        if ($seo_url == $data['seo_url']) {
            return true;
        } else {
            $distinct_seo = $this->package->distinct_credentials( 'seo_url' );
            foreach ($distinct_seo as $key => $value) {
                if ($value['seo_url'] == $seo_url) {
                    $this->form_validation->set_message('seourl_check', 'The SEO URL you have entered is already being used.');
                    return false;
                }
            }
        }
    }



    public function title_check( $title_eng, $package_id ) 
    {
        $data = $this->package->credentials_exists( $package_id );
        if ($title_eng == $data['title_eng']) {
            return true;
        } else {
            $distinct_title = $this->package->distinct_credentials( 'title_eng' );
            foreach ($distinct_title as $key => $value) {
                if ($value['title_eng'] == $title_eng) {
                    $this->form_validation->set_message('title_check', 'The Title you have entered is already being used.');
                    return false;
                }
            }
        }
    }




    public function getPackageVariations()
    {

        $flag = $_POST['flag'];
        $currencies = $this->currency->getCurrencies();

        if( $flag == 'Fixed' ) {

            $append1 = " WHERE `type` = 'others'";
            $travel_pax = $this->currency->getTravelPax($append1);
        } else {
            $append2 = " WHERE `type` = 'adult' OR `type` = 'child'";
            $travel_pax = $this->currency->getTravelPax($append2);
        }

        if(!empty($travel_pax)) {
            foreach ($travel_pax as $key => $value) {

                echo '<div class="input-group">';
                echo '<span class="input-group-addon">'.$value['pax_range'].' '.ucwords($value['type']).'</span>';
                echo '<input type="hidden" min="0" name=tpax['.$value['id'].'] >';

                foreach ($currencies as $cur_key => $cur_value) {
                    
                    echo '<input type="number" min="0" name=currency['.$value['id'].']['.$cur_value['id'].'] placeholder="'.$cur_value['cur_name'].'" class="form-control" >';

                }

                echo '</div><br>';

            }
        }

        

        
    }



} // end controller
