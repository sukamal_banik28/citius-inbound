<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'MY_Controller.php';

class PaymentController extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }


    public function manageOfflinePayments()
    {
    	$data = common_elements('Inbound Payments');
        $data['payments'] = $this->payment->getPayments();
    	$this->load->view('admin/manage-payments', $data);
    }


    public function addOfflinePayment()
    {
    	$data = common_elements('Inbound Payments');
    	$data['currencies'] = $this->currency->getCurrencies();

    	if(!empty($this->input->post('submit'))) {

    		$currency_id = $this->security->xss_clean($this->input->post('currency_id'));      
    		$invoice_no = $this->security->xss_clean($this->input->post('invoice_no'));      
    		$invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));      
    		$client_name = $this->security->xss_clean($this->input->post('client_name'));      
    		$client_phone = $this->security->xss_clean($this->input->post('client_phone'));      
    		$client_email = $this->security->xss_clean($this->input->post('client_email'));      
    		$client_address = $this->security->xss_clean($this->input->post('client_address'));      

    		$inputs = array(
    			'currency_id' => $currency_id, 
    			'invoice_no' => $invoice_no,
    			'invoice_amount' => $invoice_amount,
    			'client_name' => $client_name,
    			'client_phone' => $client_phone,
    			'client_email' => $client_email,
    			'client_address' => $client_address,
    			'flag' => 'inbound',
    			'author_id' => $this->session->userdata('user_id')
    		);


    		$inserted_id = $this->payment->store( $inputs );

    		if(!empty($inserted_id)) {
    			redirect( base_url('/inbound-admin/manage-offline-payments') );
    		}

    	}

    	$this->load->view('admin/add-offline-payment', $data);
    }



    public function updatePayment( $payment_id )
    {
    	$data = common_elements('Inbound Payments');
    	$data['currencies'] = $this->currency->getCurrencies();

    	$append = ' WHERE `id` = '.$payment_id;
    	$data['payment'] = $this->payment->getPayments($append)[0];


    	if(!empty($this->input->post('submit'))) {

    		$currency_id = $this->security->xss_clean($this->input->post('currency_id'));      
    		$invoice_no = $this->security->xss_clean($this->input->post('invoice_no'));      
    		$invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));      
    		$client_name = $this->security->xss_clean($this->input->post('client_name'));      
    		$client_phone = $this->security->xss_clean($this->input->post('client_phone'));      
    		$client_email = $this->security->xss_clean($this->input->post('client_email'));      
    		$client_address = $this->security->xss_clean($this->input->post('client_address'));   
    		$status = $this->security->xss_clean($this->input->post('status'));   
    		$is_active = $this->security->xss_clean($this->input->post('is_active'));         
    		      

    		$inputs = array(
    			'currency_id' => $currency_id, 
    			'invoice_no' => $invoice_no,
    			'invoice_amount' => $invoice_amount,
    			'client_name' => $client_name,
    			'client_phone' => $client_phone,
    			'client_email' => $client_email,
    			'client_address' => $client_address,
    			'status' => $status,
    			'is_active' => $is_active,
    			'updated_at' => date('Y-m-d'),
    		);


    		$updated_id = $this->payment->update( $payment_id, $inputs );

    		if(!empty($updated_id)) {
    			redirect( base_url('/inbound-admin/manage-offline-payments') );
    		}

    	}


		$this->load->view('admin/edit-offline-payment', $data);
    }




} // end controller
?>