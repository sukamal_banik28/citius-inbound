<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// error_reporting( E_ALL );
// ini_set( "display_errors", 1 );

class HomeController extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->parent_categories = getCategoriesByFlag('Package');
    }


    public function home()
    {
        $temp = $this->category->getCategoriesByFlag('Package');

        $categories = array();
        // extract all parent categories
        if(!empty($temp)){
            foreach ($temp as $key => $value) {
                if($value['parent_id'] == 0) {
                    $categories[$key] = $value;
                }
            }
        }


        // get all packages according to categories AND channel type ( In this case it is inbound )
        if(!empty($categories)) {
        	foreach ($categories as $key => $value) {
        		$categories[$key]['packages'] = $this->category->getPackagesAccordingToCategories($value['id']);
        	}


            foreach ( $categories as $key => $value ) {
                if(!empty( $value['packages'] )) {
                    foreach ( $value['packages'] as $p_key => $p_value ) {
                        $categories[$key]['packages'][$p_key] = $this->home->CheckIfObjectExistsInChannelType( 'inbound', $p_value['id'], 'Package' ) ? $p_value : NULL;
                    }
                }
            }

        }


        // echo '<pre>';
        // print_r($categories);
        // exit();

        // how-to-sort-an-array-of-associative-arrays-by-value-of-a-given-key-in-php
        array_multisort(array_column($categories, 'order_by'), SORT_ASC, $categories);

        $data['categories'] = $categories;

        $temp = getCommonHeaderAttributes();
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        $data['package_list'] = getPackages();
        $data['banners'] = getBanners('header'); 

        if($this->input->post('search')) 
        {
            $temp['lang'] = isset($temp['lang']) ? $temp['lang'] : 'eng';
            $search_string = $this->input->post('search_string');
            redirect( base_url().'manage-tour/search-result/'.$search_string.'?lang='.$temp['lang'] );
        }

        $this->load->view('user/home', $data);
    }



    public function packageDetails($seo_url)
    {
    	$data['package'] = getDetailsBySEO($seo_url, 'home');
        $data['packageItinerary'] = $this->itinerary->getItinerariesByPackageID($data['package']['id']);
        $data['gallery'] = $this->files->getFilesByFileableId( $data['package']['id'], 'Package', 'package_gallery' );
        $data['banner'] = $this->files->getFilesByFileableId( $data['package']['id'], 'Package', 'package_banner' );

        $data['package_list'] = getPackages();

        $products = $this->product->getProductsByPackageID($data['package']['id']);
        if( !empty($products) ) {
            foreach ($products as $key => $value) {
                $products[$key] = $this->home->CheckIfObjectExistsInChannelType( 'inbound', $value['id'], 'Product' ) ? $value : NULL;
            }

            $data['products'] = $products;
        }

        $pagetitle = 'Package Details';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );
        
        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }      

        $data['reviews'] = $this->review->getReviewsForObject($data['package']['id'], 'Package');
        
        // social link
        $data['facebook_share_link'] = base_url().'packages/'.$seo_url;

        $currency_id = $data['lang'] == 'eng' ? 2 : 1;

        if(!empty($data['products'])) {
            foreach ($data['products'] as $key => $value) {
                $data['products'][$key]['location'] = $this->city->getCitiDetailsByObjectID($value['id'])[0];
                $data['products'][$key]['addon'] = $this->currency->getObjectValuesForCurrency($currency_id, $value['id'], 'product', 'array');
                
                // $data['products'][$key]['addon'] = $this->product->getAddOnsUsingObjectID($value['id']);
            }

            foreach ($data['products'] as $key => $value) {
                if($value['type'] == $value['type']) {
                    $temp2[$value['type']][$key] = $value;
                }
            }
            $data['products'] = $temp2;

        }

        // echo '<pre>';
        // print_r($data);
        // exit();

        storeHits( $data['package']['id'], 'Package', 'ct_packages' );

        $this->load->view('user/package-details', $data);
    }



    public function productDetails($seo_url)
    {
        $data['product'] = getDetailsBySEO($seo_url, 'home', 'ct_products');
        $data['banner'] = $this->files->getFilesByFileableId( $data['product']['id'], 'Product', 'product_banner' );
        $data['gallery'] = $this->files->getFilesByFileableId( $data['product']['id'], 'Product', 'product_gallery' );
        $data['amenities'] = $this->product->getAmenitiesByProductID( $data['product']['id'] );
        
        $data['package_list'] = getPackages();

        $pagetitle = 'Product Details';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        } 

        storeHits( $data['product']['id'], 'Product', 'ct_products' );

        // social link
        $data['facebook_share_link'] = base_url().'products/'.$seo_url;

        $this->load->view( 'user/product-details', $data );
    }



    public function signup()
    {
        $data['pagetitle'] = 'Citius Signup';
        $data['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';

        if(!empty($this->input->post('submit'))) {

            /* Set Validation rule  in the form */
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'min_length[10]|required|is_unique[ct_users.phone]');
            $this->form_validation->set_rules('email', 'Email', 'valid_email|required|is_unique[ct_users.email]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

            if ($this->form_validation->run() == FALSE) {
                //auto redirect to main page if any validation failed and show the error logs if any.
            } else {

                $name = $this->security->xss_clean($this->input->post('name'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $password = $this->security->xss_clean($this->input->post('password'));

                $explode_address = explode(' ', $address);
                $country = end($explode_address);

                $registration_number = 'CUST'.substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"), -5).mt_rand(1000,9999);

                $input = array( 'name' => ucwords($name),
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'country' => $country,
                    'password' => hash('sha256', $password),
                    'role_id' => 2,
                    'registration_number' => $registration_number
                  );


                // return current inserted ID
                $user_id = $this->users->store($input);

                if($user_id > 0) {

                    $data = array(
                        'fileable_id' => $user_id,
                        'fileable_type' => 'User',
                        'file_name' => 'user-avatar.png',
                        'file_path' => 'assets/uploads/users/',
                        'flag' => 'profile_photo',
                    );
                    $this->files->store($data);


                    if(!empty($email)) {

                        $mesg = $this->load->view( 'admin/email-templates/welcome', $input, true );

                        $to = $email;
                        $subject = 'Welcome User';
                        // sendEmailToClient($to, $subject, $mesg);
                        sendInboundEmailNotification($to, $subject, $mesg);
                    }

                    redirect( base_url().'user-signin?lang='.$data['lang'] );

                }
            }


        }

        $this->load->view( 'user/signup', $data );
    }



    public function saveEnquiryByAjax()
    {
        $ticket_no = 'ENQ'.mt_rand(10000, 99999);
        $input = array(
            'name' => ucwords($_POST['name']),
            'email' => $this->input->post('email', NULL),
            'phone' => $_POST['phone'],
            'message' => ucfirst($_POST['message']),
            'enquire_id' => $_POST['package_id'],
            'enquire_type' => 'Package',
            'ticket_no' => $ticket_no,
            'flag' => 'inbound'
            );

        $user = $this->users->getUserByCredentials('phone', $_POST['phone']);
        $input['user_id'] = !empty($user) ? $user['id'] : NULL;
        $enquiry_id = $this->enquiry->store( $input );

        if( !empty($enquiry_id) ) 
        {
            if( !empty($_POST['email']) ) {

                $company = getCompanyDetails();

                $to = $_POST['email'];
                $subject = 'Enquiry Reference No. '. $ticket_no;
                $mesg = $this->load->view( 'admin/email-templates/enquiry_requested', $input, true );
                sendInboundEmailNotification($to, $subject, $mesg);

                // $to = "inbound@citius.in";
                $to = $company['email'];
                $mesgAdmin = $this->load->view( 'admin/email-templates/enquiry_requested_admin', $input, true );
                sendInboundEmailNotification($to, $subject, $mesgAdmin);
                
                // sendEmailToAdmin($to, $subject, $mesgAdmin);
                // sendEmailToClient($to, $subject, $mesg);
            }

            echo json_encode($input);
        }
    }



     public function saveReviewByAjax()
    {
        $input = array(
            'name' => ucwords($_POST['name']),
            'email' => $this->input->post('email', NULL),
            'review' => ucfirst($_POST['review']),
            'reviewable_id' => $_POST['reviewable_id'],
            'reviewable_type' => $_POST['reviewable_type'],
            );

        $user = $this->users->getUserByCredentials('email', $_POST['email']);
        $input['user_id'] = !empty($user) ? $user['id'] : NULL;
        $enquiry_id = $this->review->store( $input );

        if($enquiry_id > 0) 
        {
            echo json_encode($input);
        }
    }



    public function manageTour( $seo_url, $page = 1 )
    {
        $category = getDetailsBySEO( $seo_url, 'home', 'ct_categories');
        $viewd_flag = isset($_GET['flag']) ? $_GET['flag'] : 'all';


// if(!empty($packages)) 
// {
//     foreach ($packages as $key => $value) {
//         $packages[$key] = $this->home->CheckIfObjectExistsInChannelType( 'inbound', $value['id'], 'Package' ) ? $value : NULL;
//     }
// }

        $data['viewd_flag'] = $viewd_flag;

        $pagetitle = 'Citius Tours';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        } 


        /* pagination */
        $total_rows_gallery = $this->category->getPackagesAccordingToCategories( $category['id'], $viewd_flag );

        $config['base_url'] = base_url().'manage-tour/'.$seo_url;
        $config['total_rows'] = count($total_rows_gallery);
        
        // merge the other common configs to the array
        $config = array_merge($config, getPaginationConfig());
        

        if (empty($_GET['page']))
        {
            $_GET['page'] = 1;
        }
        $offset = ($_GET['page'] - 1) * $config['per_page'];

        $table = 'ct_category_object';
        $object_type = 'Package';
        $flag = 'package_logo';
        $packages = $this->category->getPackagesAccordingToCategories( $category['id'], $viewd_flag, $table, $object_type, $flag, $config['per_page'], $offset );
        $data['packages'] = $packages;

        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        /* pagination */

        $this->load->view('user/manage-tour', $data);
    }



    public function aboutUs()
    {
        $data = array();

        $pagetitle = 'Citius All About';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        $data['content'] = $this->page->getPageByKeyWord('about');

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        // echo '<pre>';
        // print_r($data['content']);
        // exit();

        $this->load->view('user/manage-aboutus', $data);
    }



    public function contactUs()
    {
        $data = array();

        $pagetitle = 'Citius Contact Us';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }


        if(!empty($this->input->post('submit')))
        {
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');
            $ticket_no = 'ENQ'.mt_rand(10000, 99999);

            $input = array(
                'name' => ucwords($name),
                'email' => $email,
                'phone' => $phone,
                'ticket_no' => $ticket_no,
                'flag' => 'contact',
                'message' => $message,
            );

            $user = $this->users->getUserByCredentials('phone', $phone);
            $input['user_id'] = !empty($user) ? $user['id'] : NULL;
            $enquiry_id = $this->enquiry->store( $input );

            if( !empty($enquiry_id) )
            {
                if( !empty($email) ) {

                    $company = getCompanyDetails();

                    $to = $email;
                    // $adminEmail = 'inbound@citius.in'; // previously used
                    $adminEmail = $company['email'];
                    $subject = 'Reference Ticket No. '. $ticket_no;
                    $mesg = $this->load->view( 'admin/email-templates/enquiry_requested', $input, true );
                    sendInboundEmailNotification($to, $subject, $mesg);

                    $mesgAdmin = $this->load->view( 'admin/email-templates/enquiry_requested_admin', $input, true );
                    sendInboundEmailNotification($adminEmail, $subject, $mesgAdmin);
                    
                    // sendEmailToAdmin($to, $subject, $mesgAdmin);
                    // sendEmailToClient($to, $subject, $mesg);
                }

                redirect(base_url());
            }
        } // end submit

        $data['company'] = $this->company->getDetails( $data['lang']);

        // echo '<pre>';
        // print_r($data);
        // exit();

        $this->load->view('user/manage-contactus', $data);
    }




    public function manageBlogs()
    {
        $data['blogs'] = $this->getBlogs();

        if(!empty($data['blogs'])) 
        {
            $data['tags'] = $this->getTags( 'tags', 'ct_blogs' );
        }

        $data['categories'] = $this->getCategoryWiseBlog();

        $pagetitle = 'Citius Blogs';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        if(!empty($data['blogs'])) {
            foreach ($data['blogs'] as $key => $value) {
                $data['blogs'][$key]['comments'] = $this->review->getReviewsForObject($value['id'], 'Blog');
            }
        }

        $this->load->view('user/manage-blogs', $data);
    }


    public function manageCategoryBlogs( $seo_url )
    { 

        $category = getDetailsBySEO($seo_url, 'home', 'ct_categories');

        $data['blogs'] = $this->category->getObjectsAccordingToCategories( $category['id'], 'Blog', 'ct_blogs', 'publish' );

        // check if that blog exists in inbound channel
        if(!empty($data['blogs'])) {

            foreach ($data['blogs'] as $key => $value) {
                $data['blogs'][$key] = $this->home->CheckIfObjectExistsInChannelType( 'inbound', $value['id'], 'Blog' ) ? $value : NULL;
            }


            foreach ($data['blogs'] as $key => $value) {
                if(!empty($value)) {
                    $data['blogs'][$key]['file'] = $this->files->getFilesByFileableId( $value['id'], 'Blog', 'blog_logo' );
                }
            }

            foreach ($data['blogs'] as $key => $value) {
                $data['blogs'][$key]['comments'] = $this->review->getReviewsForObject($value['id'], 'Blog');
            }

            $data['tags'] = $this->getTags( 'tags', 'ct_blogs' );
        }
        

        $data['flag'] = 'category';
        
        $data['categories'] = $this->getCategoryWiseBlog();

        $pagetitle = 'Citius Blogs';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        $this->load->view('user/manage-blogs', $data);
    }



    public function manageTagsBlogs( $tag_string )
    {
        $data = array();
        $data['flag'] = 'tags';
        $data['tags'] = $this->getTags( 'tags', 'ct_blogs' );
        $data['categories'] = $this->getCategoryWiseBlog();

        $pagetitle = 'Citius Blogs';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        $tag_string = urldecode($tag_string);
        $data['blogs'] = $this->home->getObjectsAccordingToTags( $tag_string, 'ct_blogs' );


        if(!empty($data['blogs'])) {

            foreach ($data['blogs'] as $key => $value) {
                $data['blogs'][$key] = $this->home->CheckIfObjectExistsInChannelType( 'inbound', $value['id'], 'Blog' ) ? $value : NULL;
            }


            foreach ($data['blogs'] as $key => $value) {
                if(!empty($value)) {
                    $data['blogs'][$key]['file'] = $this->files->getFilesByFileableId( $value['id'], 'Blog', 'blog_logo' );
                }
            }

            foreach ($data['blogs'] as $key => $value) {
                $data['blogs'][$key]['comments'] = $this->review->getReviewsForObject($value['id'], 'Blog');
            }

        }


        $this->load->view('user/manage-blogs', $data);

    }
    


    public function blogDetails( $seo_url )
    {

        $data['blogs'] = $this->getBlogs();
        $data['categories'] = $this->getCategoryWiseBlog();
        $data['tags'] = $this->getTags( 'tags', 'ct_blogs' );

        $data['blog'] = getDetailsBySEO($seo_url, 'home', 'ct_blogs');
        $data['blog_logo'] = $this->files->getFilesByFileableId( $data['blog']['id'], 'Blog', 'blog_logo' );

        $data['comments'] = $this->review->getReviewsForObject($data['blog']['id'], 'Blog');


        $pagetitle = 'Citius Blogs';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );


        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        storeHits( $data['blog']['id'], 'Blog', 'ct_blogs' );

        $this->load->view('user/blog-details', $data);
    }


    //  main mail function belong to common helper method: this is a test mail function for aws
    public function emailTest()
    {

        // $hostname = gethostname();
        // echo $hostname;
        // echo php_uname('n');
        // exit();

        $company = getCompanyDetails();

        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');     
        $mail = new PHPMailer(true);
        
        // $mail->isSMTP();                                      // Set mailer to use SMTP
        // $mail->SMTPDebug = 3;
        // $mail->Host = 'mail.citius.in';              // Specify main and backup SMTP servers
        // $mail->Host = 'ssl://email-smtp.us-east-1.amazonaws.com';
        // $mail->Host = 'mail9.mithiskyconnect.com';

        $mail->Host = 'smtp.rediffmailpro.com';

        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'inbound@citiusinfo.com';                 // SMTP username
        $mail->Password = 'Inbound@123';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable tls encryption, `ssl` also accepted
        $mail->Port = 587; //443,465, 587, 1125
        $mail->AddCustomHeader("X-MSMail-Priority: High");

        $input = array('name' => 'Sukamal Banik - OpaTech', 'registration_number' => 'HJ4578636HJG');
        $mesg = $this->load->view( 'admin/email-templates/welcome', $input, true );

        $mail->setFrom('inbound@citiusinfo.com', 'CITIUS Inbound', 0);
        // $mail->addAddress($company['email']);

        // $mail->addAddress('inbound@citius.in');
        $mail->addAddress('baniksukamal129@yahoo.com');
        // $mail->addAddress('manasij@citius.in');        
        $mail->addAddress('inbound@citiusinfo.com');
        // $mail->addAddress('manasijkanjilal94@gmail.com');


        $mail->Subject  = 'PHPMailer test - pls Ignore - rediffmailpro';
        // $mail->Body     = 'Hi! This is my first e-mail sent through PHPMailer.';

        $mail->Body = $mesg;
        $mail->msgHTML($mesg);

        $mail->send();

        // if(!$mail->send()) {
        //   echo 'Message was not sent.';
        //   echo 'Mailer error: ' . $mail->ErrorInfo;
        // } else {
        //   echo 'Message has been sent.';
        // }



        try {
          //Email information comes here
          $mail->Send();
          echo "Message Sent OK\n";
        } catch (phpmailerException $e) {
          echo $e->errorMessage(); //Pretty error messages from PHPMailer
        }



        // $to = 'inbound@citius.in';
        // $to = 'manasij@citius.in';
        // $subject = 'Test - pls ignore';
        // $messages = 'Test - pls ignore';

        // $header = "From: noreply@example.com\r\n"; 
        // $header.= "MIME-Version: 1.0\r\n"; 
        // $header.= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 
        // $header.= "X-Priority: 1\r\n"; 

        // $status = mail($to, $subject, $message, $header);

        // if($status)
        // { 
        //     echo '<p>Your mail has been sent!</p>';
        // } else { 
        //     echo '<p>Something went wrong, Please try again!</p>'; 
        // }


    }



    public function getBlogs()
    {
        $blogs = $this->blog->getBlogs( 'DESC', 'publish' );

        if(!empty($blogs)) {
            foreach ($blogs as $key => $value) {
                $blogs[$key] = $this->home->CheckIfObjectExistsInChannelType( 'inbound', $value['id'], 'Blog' ) ? $value : NULL;
            }
        }

        return $blogs;
    }


    public function getTags( $column = 'tags', $table = 'ct_blogs' )
    {
        $temp = array();
        $tags = $this->home->distinct_credentials( $column, $table );

        if(!empty($tags)) {
            foreach ($tags as $key => $value) {
                $explode = explode(',', $value['tags']);
                foreach ($explode as $ex_key => $ex_value) {
                    if(!empty($ex_value)) {
                        $temp[] = $ex_value;
                    }
                }
            }
            return array_unique($temp);
        }
        return $temp;
    }


    public function getCategoryWiseBlog()
    {
        $categories = getCategories('blog');

        if(!empty($categories)) {
            foreach ($categories as $key => $value) {
                $categories[$key]['blogs'] = $this->category->getObjectsAccordingToCategories( $value['id'], 'Blog', 'ct_blogs', 'publish' );
            }
        }

        return $categories;
    }



    public function searchResult($search_string, $page = 1)
    {

        $viewd_flag = isset($_GET['flag']) ? $_GET['flag'] : 'all';

        $packages = $this->home->getPackagesAccordingToSearchedResult( $search_string, $viewd_flag );

        if(!empty($packages)) {
            foreach ($packages as $key => $value) {
                $packages[$key] = $this->home->CheckIfObjectExistsInChannelType( 'inbound', $value['id'], 'Package' ) ? $value : NULL;
            }
        }

        $data['viewd_flag'] = $viewd_flag;
        $data['packages'] = $packages;

        $pagetitle = 'Citius Tours';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        $this->load->view('user/manage-tour', $data);
    }





    public function gallery()
    {
        $data = array();

        $pagetitle = 'Citius Gallery';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        $append = 'AND gallery.is_active = 1 ORDER BY gallery.order_by ASC';
        $total_rows_gallery = $this->gallery->getGallery( $append );

        $config['base_url'] = base_url().'citius-gallery';
        $config['total_rows'] = count($total_rows_gallery);
        
        // merge the other common configs to the array
        $config = array_merge($config, getPaginationConfig());
        

        if (empty($_GET['page']))
        {
            $_GET['page'] = 1;
        }
        $offset = ($_GET['page'] - 1) * $config['per_page'];


        $append = 'AND gallery.is_active = 1 ORDER BY gallery.order_by ASC LIMIT '.$config['per_page'].' OFFSET '.$offset;
        $data['gallery'] = $this->gallery->getGallery( $append );

        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();


        // echo '<pre>';
        // print_r($config);
        // print_r($data['links']);
        // echo '<br>'.count($data['gallery']);
        // exit();

        $this->load->view('user/manage-gallery', $data);
    }



    public function ebrochure()
    {
        $data = array();

        $pagetitle = 'Citius Brochures';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        $append = ' AND ebrochure.is_active = 1 ORDER BY ebrochure.order_by ASC';
        $data['ebrochure'] = $this->ebrochure->getEbrochures( $append );


        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        $this->load->view('user/manage-brochure', $data);
    }




    public function getFixedPricesForPackage()
    {
        $travel_pax_id = $_POST['pax_id'];
        $package_id = $_POST['package_id'];

        $lang = $_POST['lang'];
        // 2 for INR, 1 for EUR
        $currency_id = $lang == 'eng' ? 2 : 1;

        $data = $this->currency->getObjectValuesForCurrencyPax( $currency_id, $package_id, 'package', $travel_pax_id );

        echo json_encode($data);
    }


    public function getFITPricesForPackage()
    {
        $pax_no = $_POST['pax'];
        $package_id = $_POST['package_id'];
        $flag = $_POST['flag'];

        $lang = $_POST['lang'];
        // 2 for INR, 1 for EUR
        $currency_id = $lang == 'eng' ? 2 : 1;

        // 5 for child, 4 for adult
        $travel_pax_id = $flag == 'child' ? 5 : 4;

        $data = $this->currency->getObjectValuesForCurrencyPax( $currency_id, $package_id, 'package', $travel_pax_id );

        echo json_encode($data);
    }



    public function savePrimaryBookingInfo()
    {
        // echo '<pre>';
        // print_r($_POST);
        // exit();

        $client_name = $_POST['client_name'];
        $client_telephone = $_POST['client_telephone'];
        $client_email = $_POST['client_email'];
        $client_address = $_POST['client_address'];
        $client_message = $_POST['client_message'];
        $total = $_POST['package_price'];

        $input = array(
            'client_name' => $client_name,
            'client_telephone' => $client_telephone,
            'client_email' => $client_email,
            'client_address' => $client_address,
            'client_message' => $client_message,
            'total' => $total,
            'flag' => 'inbound'
            );

        if(isset($_POST['fixed_pax_range'])) {
            $input['fixed_pax_range_id'] = $_POST['fixed_pax_range'];
        }

        if(isset($_POST['fit_adult'])) {
            $input['fit_adult'] = $_POST['fit_adult'];
        }

        if(isset($_POST['fit_child'])) {
            $input['fit_child'] = $_POST['fit_child'];
        }



        $booking_id = $this->booking->store($input);

        if(!empty($booking_id)) 
        {

            if(isset($_POST['hotels'])) 
            {
                $hotels = $_POST['hotels'];
                store_booking_details($booking_id, $hotels, $_POST['currency_id']);
            } /* end hotels */



            if(isset($_POST['meals']))
            {
                $meals = $_POST['meals'];
                store_booking_details($booking_id, $meals, $_POST['currency_id']);
            } /* end meals */


            if(isset($_POST['extensions'])) 
            {
                $extensions = $_POST['extensions'];
                store_booking_details($booking_id, $extensions, $_POST['currency_id']);
            } /* end extensions */

            if(isset($_POST['other_package']))
            {
                $other_package = $_POST['other_package'];
                if(!empty($other_package)) 
                {
                    $data = array(

                        'booking_id' => $booking_id,
                        'currency_id' => $_POST['currency_id'],
                        'objectable_id' => $other_package,
                        'objectable_type' => 'package',
                        );

                    $this->booking->store_booking_details($data);
                }
            } /* end other_package */



            redirect( base_url().'citius-package-summary/booking-id/'.$booking_id.'/check-out?lang=eng' );


        } // end booking_id

    }



    public function paymentCheckout( $booking_id ) 
    {
        // echo $booking_id;

        $data = array();

        $pagetitle = 'Citius Brochures';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        $data['booking_info'] = $this->booking->getBookingInfoByID( $booking_id );
        $data['booking_details'] = $this->booking->getBookingDetailsByBookingID( $booking_id );

        // echo '<pre>';
        // print_r($data);
        // exit();

        $this->load->view('user/check-out', $data);
    }




    public function AddDifferentiationAmount()
    {

        // echo '<pre>';

        $addonid = $_POST['addonid'];
        $object_id = $_POST['object_id'];
        $lang = $_POST['lang'];
        $citius_touch = $_POST['citius_touch'];

        // 2 for INR, 1 for EUR
        $currency_id = $lang == 'eng' ? 2 : 1;

        $defaultAddon = $this->currency->getProductDefaultAddonValueByCurrency( $currency_id, $object_id, 'product' );
        $selectedAddon = $this->currency->getProductValuesForCurrency( $object_id, 'product', $addonid, 'addon', $currency_id);

        if($defaultAddon['sub_object_id'] != $addonid) {

            // print_r($selectedAddon);

            if( !empty($defaultAddon) && $citius_touch == 'Fixed' ) {
                $differentiateValue = $selectedAddon['value'] - $defaultAddon['value'];
                echo $differentiateValue;
                // print_r($differentiateValue);
            }
        }


        if ( $citius_touch == 'Customised' ) 
        {
           echo $selectedAddon['value'];
        }

        // print_r($defaultAddon);
        // exit();
    }




    public function destinations()
    {

        $data = array();

        $pagetitle = 'Citius Destinations';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if ( !empty($this->session->userdata("user_id")) ) {

            $user_id = $this->session->userdata("user_id");
            $temp = getUserProfilePhotoIfLoggedIn( $user_id );
            $data = array_merge( $data, $temp );
            
        }

        $append = " AND province.parent_id = 0 AND province.is_active = 1 ORDER BY province.order_by ASC";
        $data['countries'] = $this->province->getProvinces($append);

        $this->load->view('user/manage-destinations', $data);

        // echo '<pre>';
        // print_r($countries);
        // exit();
    }




    public function getProvinceDetails()
    {
        $cityid = $_POST['cityid'];
        $append = " AND province.id = ".$cityid;
        $data = $this->province->getProvinces($append)[0];
        echo json_encode($data);
    }




    public function quickPayment()
    {
        $data = array();
        $pagetitle = 'Citius Quick Payment';
        $temp = getCommonHeaderAttributes( $pagetitle );
        $data = array_merge( $data, $temp );

        if(!empty($this->input->post('submit'))) {

            /* check invoice number */
            $currency = $this->security->xss_clean($this->input->post('currency'));    
            $invoice_number = $this->security->xss_clean($this->input->post('invoice_number'));    

            $append = "  WHERE `currency_id` = ".$currency." AND `invoice_no` = '".$invoice_number."'";
            $checkInfo = $this->payment->getPayments($append);  

            if(empty($checkInfo)) {
                $data['msg'] = "Invoice Number or Currency is not matched. Please check.";
            }


        }

        $this->load->view('user/quick-payment', $data);
    }


    }// end controller
?>