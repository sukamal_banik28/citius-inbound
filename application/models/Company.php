<?php 

class Company extends CI_Model {
	public $table = 'ct_company_details';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}

	public function getDetails($lang = 'eng')
	{
		$sql = "SELECT `id`, `email`, `telephone`, `meta_key`, `meta_description`, `name_".$lang."`, `address_".$lang."` FROM `ct_company_details` WHERE `id` = 1";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}


	public function update($company_id, $data) {
		$this->db->where('id', $company_id);
		$this->db->update($this->table, $data);
		return $company_id;
	}




	} // end model

?>