<?php

class Ebrochure extends CI_Model {

	public $table = 'ct_ebrochures';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($id, $data) {
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}


	public function getEbrochures($append = null)
	{
		$sql = "SELECT ebrochure.*, files.`file_name`, files.`file_path` FROM `ct_ebrochures` ebrochure, `ct_files` files WHERE ebrochure.flag = 'inbound' AND ebrochure.id = files.fileable_id AND files.flag = 'brochure_image' AND files.fileable_type = 'Brochure' ".$append;

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}



	


} // end of model class



?>