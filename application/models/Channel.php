<?php

class Channel extends CI_Model {

	public $table = 'ct_channels';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store( $data )
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update( $channel_id, $data ) {
		$this->db->where('id', $page_id);
		$this->db->update($this->table, $data);
		return $page_id;
	}


	public function getChannels()
	{
		$sql = "SELECT * FROM `ct_channels`";
		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function store_channel_objects( $data, $model = 'ct_channel_object' )
	{
		$model = $this->db->insert( $model, $data );
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function checkChannelHasObject( $channel_id, $object_id, $object_type = 'Package' )
	{
		$sql = "SELECT * FROM `ct_channel_object` WHERE `channel_id` = $channel_id AND `object_id` = $object_id AND `object_type` = '".$object_type."' ";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0) { 
        		return $model->result_array();
        	}
        return false;
	}


	public function deleteExistingChannelsObjects( $object_id, $object_type = 'Package', $table = 'ct_channel_object')
	{
		$sql = "DELETE FROM `".$table."` WHERE `object_id` = $object_id AND `object_type` = '".$object_type."'";
		$model = $this->db->query($sql);
	}



} // end of model class



?>