<?php

class Payment extends CI_Model {

	public $table = 'ct_offline_invoice';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($payment_id, $data) {
		$this->db->where('id', $payment_id);
		$this->db->update($this->table, $data);
		return $payment_id;
	}


	public function getPayments( $append = null )
	{

		$sql = "SELECT * FROM `ct_offline_invoice`".$append;

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}



} // end of model class



?>