<?php

class Files extends CI_Model
{

    public $table = 'ct_files';

    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }


    public function store($data)
    {
        $model = $this->db->insert($this->table, $data);
        if (!empty($model) && $model === true) {
            return $this->db->insert_id();
        }
        return false;
    }


    public function getFilesByFileableId( $fileable_id, $fileable_type = 'Package', $flag = 'package_logo', $model = 'ct_files' )
    {
        $sql = "SELECT * FROM `".$model."` WHERE `fileable_id` = $fileable_id AND `fileable_type` = '".$fileable_type."' AND `flag` = '".$flag."'";

        $model = $this->db->query($sql);
        if (!empty($model) && $model->num_rows() > 0) {

            switch ($flag) {

                case 'package_gallery':
                    return $model->result_array();
                    break;

                case 'product_gallery':
                    return $model->result_array();
                    break;

                default:
                    return $model->row_array();
                    break;

            }
           
        }
        return false;
    }


    public function delete($file_id)
    {
        $this->db->query("DELETE FROM `ct_files` WHERE `id` = " . $file_id);
    }



    public function getFileById($file_id) 
    {
        $sql = "SELECT * FROM `ct_files` WHERE `id` = $file_id";
        $model = $this->db->query($sql);
        if (!empty($model) && $model->num_rows() > 0) {
            return $model->row_array();
        }
        return false;
    }




    } // end of model class


?>