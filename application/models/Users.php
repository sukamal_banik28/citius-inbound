<?php

class Users extends CI_Model {

	public $table = 'ct_users';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}



	public function store($data) 
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}



	public function doLogin($data) 
	{
		$sql = "SELECT `id`, `name`, `email`, `phone`, `registration_number`, `role_id`, `is_active` FROM `ct_users` WHERE (`email` = '".$data['loginID']."' OR `phone` = '".$data['loginID']."' OR `registration_number` = '".$data['loginID']."' ) AND `password` = '".$data['password']."' AND is_active = 1";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}



	public function getUserByID($id) 
	{
		$sql = "SELECT * FROM `ct_users` WHERE `id` = ".$id."";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}



	public function update( $user_id, $data ) 
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table, $data);
		return $user_id;
	}



	public function credentials_exists( $user_id )
	{
		$sql = "SELECT * FROM `ct_users` WHERE `id` = $user_id";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}


	public function distinct_credentials( $credentials ) 
	{
		$sql = "SELECT DISTINCT(`".$credentials."`) FROM `ct_users`";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function getUserByCredentials( $type = 'phone', $value ) 
	{
		$sql = "SELECT * FROM `ct_users` WHERE `".$type."` = '".$value."'";
		
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}


	public function getUsers()
	{
		$sql = "SELECT * FROM `ct_users` WHERE role_id = 2 AND `id` NOT IN(1)";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}




} // end of model class



?>