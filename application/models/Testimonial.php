<?php 

class Testimonial extends CI_Model {

	public $table = 'ct_testimonials';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
 	{
 		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
 	}



	public function getTestimonials()
	{
		$sql = "SELECT testimonial.*, files.`id` as file_id, files.`file_name`, files.`file_path` FROM `ct_testimonials` testimonial, `ct_files` files WHERE testimonial.id = files.fileable_id AND files.fileable_type = 'Testimonial'";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function getTestimonialByID($testimonial_id, $lang = 'eng')
	{
		$sql = "SELECT `id`, `status`, `created_by`, `title_".$lang."`, `short_desc_".$lang."`, `long_desc_".$lang."` FROM `ct_testimonials` WHERE `id` = $testimonial_id";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}


	public function update($testimonial_id, $data) {
		$this->db->where('id', $testimonial_id);
		$this->db->update($this->table, $data);
		return $testimonial_id;
	}












	} // end model

?>