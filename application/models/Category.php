<?php

class Category extends CI_Model {

	public $table = 'ct_categories';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function getCategoriesByFlag($flag)
	{
		$sql = "SELECT * FROM `ct_categories` WHERE `flag` = '".$flag."'";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function getCategoryDetailsByID($category_id)
	{
		$sql = "SELECT * FROM `ct_categories` WHERE `id` = $category_id";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}


	public function getSubCategoriesByParentID($category_id)
	{
		$sql = "SELECT * FROM `ct_categories` WHERE `parent_id` = $category_id";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}



	public function store_category_object( $data, $table = 'ct_category_object' )
	{
		$model = $this->db->insert( $table, $data );
		if(!empty($model) && $model === true) 
		{
			return $this->db->insert_id();
		}
		return false;
	}



	public function checkObjectHasCategory($object_id, $object_type = 'Blog', $category_id, $table = 'ct_category_object')
	{
		$sql = "SELECT `id`, `object_id`, `category_id` FROM ".$table." WHERE object_id = $object_id AND object_type = '".$object_type."' AND category_id = $category_id";
		
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0)
			{
        		return $model->result_array();
        	}
        return false;
	}


	public function deleteExistingCategoryObjects( $object_id, $object_type = 'Blog', $table = 'ct_category_object')
	{
		$sql = "DELETE FROM `".$table."` WHERE `object_id` = $object_id AND object_type = '".$object_type."'";
		$model = $this->db->query($sql);
	}


	public function getObjectsAccordingToCategories( $category_id, $object_type = 'Blog', $target_table = 'ct_blogs', $status = 'publish' )
	{
		if($status == 'publish') 
		{
			$append = " status = 'publish' AND ";
		} else {
			$append = NULL;
		}

		$sql = "SELECT * FROM `". $target_table ."` WHERE ".$append." `id` IN (SELECT `object_id` FROM `ct_category_object` WHERE `category_id` = $category_id AND `object_type` = '".$object_type."')";
		$model = $this->db->query($sql);

		if(!empty($model) && $model->num_rows() > 0)
			{
        		return $model->result_array();
        	}
        return false;
	}



	public function getPackagesAccordingToCategories( $category_id, $viewd_flag = 'all', $table = 'ct_category_object', $object_type = 'Package', $flag = 'package_logo', $limit = 1000000, $offset = 0 ) 
	{

		$append_string = ( $viewd_flag == 'popular' ) ? ' GROUP BY packages.`viewd` DESC ' : ( ( $viewd_flag == 'latest' ) ? ' ORDER BY packages.`created_at` DESC ' : 'ORDER BY packages.`title_eng` ASC, packages.`created_at` ASC' );

		$sql = "SELECT packages.*, files.`id` as file_id, files.`file_name`, files.`file_path`, files.`flag` FROM `ct_packages` packages, `ct_files` files, ". $table ." category WHERE category.category_id = ". $category_id ." AND category.object_id = packages.id AND category.`object_type` = '". $object_type ."' AND packages.id = files.fileable_id AND files.fileable_type = '". $object_type ."' AND files.flag = 'package_logo' AND packages.deleted_at IS NULL ".$append_string." LIMIT ".$offset.", ".$limit."";
		
		$model = $this->db->query($sql);		
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}



	public function credentials_exists( $category_id ) 
	{
		$sql = "SELECT `id`, `seo_url`, `name_eng`, `name_spanish` FROM `ct_categories` WHERE `id` = $category_id";
		$model = $this->db->query($sql);		
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}


	public function distinct_credentials( $credentials ) 
	{
		$sql = "SELECT DISTINCT(`".$credentials."`) FROM `ct_categories`";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function update($category_id, $data) {
		$this->db->where('id', $category_id);
		$this->db->update($this->table, $data);
		return $category_id;
	}
	


} // end of model class



?>