<?php

class Gallery extends CI_Model {

	public $table = 'ct_gallery';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($page_id, $data) {
		$this->db->where('id', $page_id);
		$this->db->update($this->table, $data);
		return $page_id;
	}


	public function getGallery( $append = null )
	{

		$sql = "SELECT gallery.*, files.`id` as file_id, files.`file_name`, files.`file_path` FROM `ct_gallery` gallery, `ct_files` files  WHERE gallery.flag = 'inbound' AND gallery.id = files.fileable_id AND files.fileable_type = 'Gallery' ".$append;

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function delete($id)
    {
        $this->db->query("DELETE FROM `ct_gallery` WHERE `id` = " . $id);
    }


} // end of model class



?>