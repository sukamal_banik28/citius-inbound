<?php

class Review extends CI_Model
{

    public $table = 'ct_reviews';

    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }


    public function store($data)
    {
        $model = $this->db->insert($this->table, $data);
        if (!empty($model) && $model === true) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function getReviews( $reviewable_type='Package' )
    {
        $sql = "SELECT * FROM `ct_reviews`";
        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0){
                return $model->result_array();
            }
        return false;
    }


    public function getReviewDetailsByID( $review_id )
    {
        $sql = "SELECT * FROM `ct_reviews` WHERE `id` = $review_id";
        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0){
                return $model->row_array();
            }
        return false;
    }


    public function update($review_id, $data) 
    {
        $this->db->where('id', $review_id);
        $this->db->update($this->table, $data);
        return $review_id;
    }


    public function getReviewsForObject( $reviewable_id, $reviewable_type = 'Package' )
    {
        $sql = "SELECT * FROM `ct_reviews` WHERE `reviewable_id` = $reviewable_id AND `reviewable_type` = '".$reviewable_type."'";
        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0){
                return $model->result_array();
            }
        return false;
    }





    } // end of model class


?>