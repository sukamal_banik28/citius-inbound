<?php

class Currency extends CI_Model {

	public $table = 'ct_currency';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data) 
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update( $id, $data ) 
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}




	public function getCurrencies( $append = null)
	{
		$sql = "SELECT * FROM `ct_currency`".$append;
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}



	public function store_currency_objects($data)
	{
		$model = $this->db->insert('ct_currency_object', $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}



	public function getCurrenciesObejcts($object_id, $objectable_type = 'package')
	{
		$sql = "SELECT * FROM `ct_currency_object` WHERE `object_id` = $object_id AND `objectable_type` = '".$objectable_type."'";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function getObjectValuesForCurrency($currency_id, $object_id, $objectable_type = 'package', $return_type = null)
	{
		$sql = "SELECT * FROM `ct_currency_object` WHERE `currency_id` = $currency_id AND `object_id` = $object_id AND `objectable_type` = '".$objectable_type."'";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){

				if(empty($return_type)) {
        			return $model->row_array();
				} else {
					return $model->result_array();
				}
        	}
        return false;
	}


	public function deleteExistingObjectCurrency($object_id, $objectable_type = 'product', $flag = 'inbound')
	{
		$sql = "DELETE FROM `ct_currency_object` WHERE `object_id` = $object_id AND `objectable_type` = '".$objectable_type."' AND `flag` = '".$flag."'";
		$model = $this->db->query($sql);
	}



	public function getTravelPax($append = null)
	{
		$sql = "SELECT * FROM `ct_travel_pax` ".$append;
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0) {
        		return $model->result_array();
        	}
        return false;
	}


	public function deletePreviousRecord($currency_id, $object_id, $objectable_type = 'package', $travel_pax)
	{
		$sql = "DELETE FROM `ct_currency_object` WHERE `currency_id` = $currency_id AND `object_id` = $object_id AND `objectable_type` = '".$objectable_type."' AND `travel_pax_id` = $travel_pax";
		$model = $this->db->query($sql);
	}



	public function updateCurrencyObjectsOtherThanNewlyCreated($currency_id, $object_id, $objectable_type = 'package')
	{
		$sql = "UPDATE `ct_currency_object` SET `is_default`= 0 WHERE `currency_id` != $currency_id AND `objectable_type` = '".$objectable_type."'";
		$model = $this->db->query($sql);
	}


	public function updateCurrencyObjectsDefaultValues($currency_id, $object_id, $objectable_type = 'package')
	{
		$sql = "UPDATE `ct_currency_object` SET `is_default`= 1 WHERE `currency_id` = $currency_id AND `objectable_type` = '".$objectable_type."'";
		$model = $this->db->query($sql);
	}


	public function deleteExistingObjectCurrencyByID($id)
	{
		$this->db->delete('ct_currency_object', array('id' => $id));
	}




	public function getProductValuesForCurrency($object_id, $objectable_type = 'product', $sub_object_id, $sub_objectable_type = 'addon', $cur_id)
	{
		$sql = "SELECT * FROM `ct_currency_object` WHERE `object_id` = $object_id AND `objectable_type` = '".$objectable_type."' AND `sub_object_id` = $sub_object_id AND `sub_objectable_type` = '".$sub_objectable_type."' AND `currency_id` = $cur_id";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
				return $model->row_array();
        	}
        return false;
	}


	public function getPackageMinValueForCurrency( $currency_id, $object_id, $objectable_type = 'package', $travel_pax_id = 1 )
	{
		$sql = "SELECT * FROM `ct_currency_object` WHERE `currency_id` = $currency_id AND `travel_pax_id` IN ($travel_pax_id,4) AND `object_id` = $object_id AND `objectable_type` = '".$objectable_type."'";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        	return $model->row_array();
		}
        return false;
	}



	public function getObjectValuesForCurrencyPax($currency_id, $object_id, $objectable_type = 'package', $travel_pax_id, $return_type = null)
	{
		$sql = "SELECT * FROM `ct_currency_object` WHERE `currency_id` = $currency_id AND `object_id` = $object_id AND `travel_pax_id` = $travel_pax_id AND `objectable_type` = '".$objectable_type."'";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){

				if(empty($return_type)) {
        			return $model->row_array();
				} else {
					return $model->result_array();
				}
        	}
        return false;
	}



	public function getProductDefaultAddonValueByCurrency($currency_id, $object_id, $objectable_type = 'product', $is_default_addon = 1 )
	{
		$sql = "SELECT * FROM `ct_currency_object` WHERE `currency_id` = $currency_id AND `object_id` = $object_id AND `objectable_type` = '".$objectable_type."' AND `is_default_addon` = $is_default_addon";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0)
		{
        	return $model->row_array();
    	}
        return false;
	}



} // end of model class



?>