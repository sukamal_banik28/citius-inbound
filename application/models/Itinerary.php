<?php

class Itinerary extends CI_Model {

	public $table = 'ct_itineraries';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($itinerary_id, $data) {
		$this->db->where('id', $itinerary_id);
		$this->db->update($this->table, $data);
		return $itinerary_id;
	}


	public function store_package_itinerary($data)
	{
		$model = $this->db->insert('ct_package_itinerary', $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function getItinerariesByPackageID( $package_id )
	{
		$sql = "SELECT itinerary.*, files.`id` as file_id, files.`file_name`, files.`file_path` FROM `ct_itineraries` itinerary, `ct_files` files WHERE itinerary.package_id = $package_id AND itinerary.id = files.fileable_id AND files.fileable_type = 'Itinerary' ORDER BY day_wise_plan ASC";
		
		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function getItineraryByID( $itinerary_id, $lang = 'eng' )
	{
		$sql = "SELECT `id`, `package_id`, `day_wise_plan`, `short_desc_".$lang."`, `long_desc_".$lang."` FROM `ct_itineraries` WHERE `id` = $itinerary_id";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}


	public function delete( $itinerary_id )
	{
		$this->db->query("DELETE FROM `ct_itineraries` WHERE `id` = " . $itinerary_id);
		$this->db->query("DELETE FROM `ct_files` WHERE `fileable_id` = $itinerary_id AND `fileable_type` = 'Itinerary'");
		$this->db->query("DELETE FROM `ct_package_itinerary` WHERE `itinerary_id` = " . $itinerary_id);
	}





} // end of model class



?>