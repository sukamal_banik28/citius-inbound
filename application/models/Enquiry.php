<?php

class Enquiry extends CI_Model
{

    public $table = 'ct_enquiries';

    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }


    public function store($data)
    {
        $model = $this->db->insert($this->table, $data);
        if (!empty($model) && $model === true) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function getEnquiries()
    {
        $sql = "SELECT * FROM `ct_enquiries` WHERE (`flag` = 'inbound' || `flag` = 'contact') ";
        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0){
                return $model->result_array();
            }
        return false;
    }


    public function getEnquiryDetailsByID( $enquiry_id )
    {
        $sql = "SELECT * FROM `ct_enquiries` WHERE `id` = $enquiry_id";
        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0){
                return $model->row_array();
            }
        return false;
    }


    public function update($enquire_id, $data) 
    {
        $this->db->where('id', $enquire_id);
        $this->db->update($this->table, $data);
        return $enquire_id;
    }


    public function store_feedbacks($data)
    {
        $model = $this->db->insert( 'ct_feedbacks', $data);
        if (!empty($model) && $model === true) {
            return $this->db->insert_id();
        }
        return false;
    }




    } // end of model class


?>