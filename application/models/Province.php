<?php

class Province extends CI_Model {

	public $table = 'ct_provinces';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($province_id, $data) {
		$this->db->where('id', $province_id);
		$this->db->update($this->table, $data);
		return $province_id;
	}


	public function getProvinces($append = null)
	{
		$sql = "SELECT province.*, files.`id` as file_id, files.`file_name`, files.`file_path`, files.`original_name` FROM `ct_provinces` province, `ct_files` files WHERE province.flag = 'inbound' AND province.id = files.fileable_id AND files.fileable_type = 'Province' AND files.flag = 'province_image' ".$append;

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}





} // end of model class



?>