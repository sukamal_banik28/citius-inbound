<?php

class Booking extends CI_Model {

	public $table = 'ct_inbound_bookings';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert( $this->table, $data );
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($id, $data) {
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}


	public function store_booking_details($data)
	{
		$model = $this->db->insert( 'ct_inbound_booking_details', $data );
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function getBookingInfoByID( $booking_id )
	{
		$sql = "SELECT * FROM `ct_inbound_bookings` WHERE `id` = $booking_id";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}
	

	public function getBookingDetailsByBookingID( $booking_id )
	{
		$sql = "SELECT * FROM `ct_inbound_booking_details` WHERE `booking_id` = $booking_id";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}



	public function getBookingPackages( $append = null )
	{
		$sql = "SELECT * FROM `ct_inbound_bookings` ".$append;
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


} // end of model class



?>