<?php

class Blog extends CI_Model {

	public $table = 'ct_blogs';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($blog_id, $data) 
	{
		$this->db->where('id', $blog_id);
		$this->db->update($this->table, $data);
		return $blog_id;
	}


	public function getBlogs( $order_by = 'ASC', $status = 'unpublish' )
	{

		if( $status == 'publish') 
		{
			$append = " AND blogs.status = 'publish'";
		} else {
			$append = NULL;
		}

		$sql = "SELECT blogs.*, files.`id` as file_id, files.`file_name`, files.`file_path`, files.`flag` FROM `ct_blogs` blogs, `ct_files` files WHERE blogs.id = files.fileable_id AND files.fileable_type = 'Blog' AND files.flag = 'blog_logo' ".$append." AND blogs.deleted_at IS NULL ORDER BY blogs.created_at ".$order_by."";


		// echo $sql;
		// exit();

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function getBlogDetailsByID( $blog_id, $lang = 'eng' )
	{
		$sql = "SELECT `id`, `code`, `author`, `status`, `tags`, `flag`, `seo_url`, `meta_key`, `meta_description`, `title_".$lang."`, `title_".$lang."`, `title_".$lang."`, `desc_".$lang."`, `desc_".$lang."`, `desc_".$lang."` FROM `ct_blogs` WHERE `id` = $blog_id";
		
		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}




} // end of model class



?>