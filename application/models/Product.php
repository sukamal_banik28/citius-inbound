<?php

class Product extends CI_Model {

	public $table = 'ct_products';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($product_id, $data) {
		$this->db->where('id', $product_id);
		$this->db->update($this->table, $data);
		return $product_id;
	}


	public function store_package_product($data)
	{
		
		$model = $this->db->insert('ct_package_product', $data);		
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function getProducts()
	{
		$sql = "SELECT products.*, files.`id` as file_id, files.`file_name`, files.`file_path`, files.`flag` FROM `ct_products` products, ct_files files WHERE products.id = files.fileable_id AND files.fileable_type = 'Product'";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function getProductDetailsByID($product_id, $lang = 'eng')
	{
		$sql = "SELECT `id`, `crm_id`, `type`, `validity_date_from`, `validity_date_to`, `deletion_flag`, `regular_price`, `title_".$lang."`, `short_desc_".$lang."`, `long_desc_".$lang."`, `seo_url`, `meta_key`, `meta_description`, `deleted_at`, `rating` FROM `ct_products` WHERE `id` = $product_id";
		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}


	public function checkPackageHasProduct($package_id, $product_id)
	{
		$sql = "SELECT * FROM `ct_package_product` WHERE `package_id` = $package_id AND `product_id` = $product_id";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function deleteExistingPackageProduct( $product_id )
	{
		$sql = "DELETE FROM `ct_package_product` WHERE `product_id` = " . $product_id;
		$model = $this->db->query($sql);
	}


	public function getProductsByPackageID( $package_id = null, $flag = 'product_logo' )
	{
		$sql = "SELECT products.*, files.`id` as file_id, files.`file_name`, files.`file_path` FROM `ct_products` products, ct_files files WHERE products.id = files.fileable_id AND files.fileable_type = 'Product' AND files.flag = '".$flag."' AND products.deleted_at IS NULL AND products.id IN (SELECT `product_id` FROM `ct_package_product` WHERE `package_id` = $package_id)";
		
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}




	public function credentials_exists( $product_id ) 
	{
		$sql = "SELECT `id`, `crm_id`, `seo_url`, `title_eng` FROM `ct_products` WHERE `id` = $product_id";
		$model = $this->db->query($sql);		
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}

	public function distinct_credentials( $credentials ) 
	{
		$sql = "SELECT DISTINCT(`".$credentials."`) FROM `ct_products`";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function getAmenities( $append = null ) 
	{
		$sql = "SELECT * FROM `ct_amenities`".$append;
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function store_product_amenities( $data )
	{
		$model = $this->db->insert( 'ct_product_amenity', $data );
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function checkProductHasAmenities( $amenity_id, $product_id )
	{
		$sql = "SELECT * FROM `ct_product_amenity` WHERE `amenity_id` = $amenity_id AND `product_id` = $product_id";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function deleteExistingProductAmenities( $product_id )
	{
		$sql = "DELETE FROM `ct_product_amenity` WHERE `product_id` = " . $product_id;
		$model = $this->db->query($sql);
	}



	public function getAmenitiesByProductID( $product_id )
	{
		$sql = "SELECT * FROM `ct_amenities` WHERE id IN ( SELECT `amenity_id` FROM `ct_product_amenity` WHERE `product_id` = $product_id )";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0) {
        		return $model->result_array();
        	}
        return false;
	}


	public function getAddOns( $append = null )
	{
		$sql = "SELECT * FROM `ct_addon_type` ".$append; 
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function storeObjectAddons($data)
	{
		$model = $this->db->insert('ct_addon_object', $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function checkProductHasAddons( $object_id, $addon_id, $object_type = 'product' ) 
	{
		$sql = "SELECT * FROM `ct_addon_object` WHERE `object_id` = $object_id AND `object_type` = '".$object_type."' AND `addon_id` = $addon_id";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}



	public function deleteExistingObjectAddons( $object_id, $object_type = 'product', $flag = 'hotel' )
	{
		$sql = "DELETE FROM `ct_addon_object` WHERE `object_id` = $object_id AND `object_type` = '".$object_type."' AND `flag` = '".$flag."'";
		$model = $this->db->query($sql);
	}


	public function storeObjectLocation($data)
	{
		$model = $this->db->insert('ct_object_city', $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}



	public function checkObjectHasCity( $object_id, $city_id, $object_type = 'product' ) 
	{
		$sql = "SELECT * FROM `ct_object_city` WHERE `object_id` = $object_id AND `object_type` = '".$object_type."' AND `city_id` = $city_id";

		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function deleteExistingObjectCity( $object_id, $object_type = 'product', $flag = 'hotel' )
	{
		$sql = "DELETE FROM `ct_object_city` WHERE `object_id` = $object_id AND `object_type` = '".$object_type."' AND `flag` = '".$flag."'";
		$model = $this->db->query($sql);
	}


	public function getAddOnsUsingObjectID( $object_id, $object_type = 'product', $flag = 'hotel') 
    {
        // $sql = "SELECT * FROM `ct_addon_object` WHERE `object_id` = $object_id AND `object_type` = '".$object_type."'";
        
        $sql = "SELECT addonobj.*, addontype.type_name_eng FROM `ct_addon_object` addonobj, `ct_addon_type` addontype WHERE addonobj.`object_id` = $object_id AND addonobj.`object_type` = '".$object_type."' AND addonobj.addon_id = addontype.id";

        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0) {
                return $model->result_array();
            }
        return false;
    }



    public function store_addons($data)
	{
		$model = $this->db->insert('ct_addon_type', $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}



	public function update_addons($id, $data) 
	{
		$this->db->where('id', $id);
		$this->db->update('ct_addon_type', $data);
		return $id;
	}



	public function store_amenities($data)
	{
		$model = $this->db->insert( 'ct_amenities', $data );
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}




	public function update_amenities($id, $data) 
	{
		$this->db->where('id', $id);
		$this->db->update('ct_amenities', $data);
		return $id;
	}





} // end of model class



?>