<?php

class Banner extends CI_Model {

	public $table = 'ct_banners';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($banner_id, $data) {
		$this->db->where('id', $banner_id);
		$this->db->update($this->table, $data);
		return $banner_id;
	}


	public function getBanners($append = null)
	{
		$sql = "SELECT banner.*, files.`id` as file_id, files.`file_name`, files.`file_path`, files.`original_name`, files.`flag` FROM `ct_banners` banner, `ct_files` files WHERE banner.flag = 'inbound' AND banner.id = files.fileable_id AND files.fileable_type = 'Banner' AND (files.flag = 'banner_image' || files.flag = 'banner_video' ) ".$append;

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function getBannerDetails( $banner_id )
	{
		$sql = "SELECT * FROM `ct_banners` WHERE `id` = $banner_id";
		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}





} // end of model class



?>