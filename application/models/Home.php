<?php

class Home extends CI_Model
{

    public $table = 'ct_files';

    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }


    public function getDetailsBySEO($seo_url, $table = 'ct_packages')
    {
        $sql = "SELECT * FROM `".$table."` WHERE `seo_url` = '".$seo_url."'";
        $model = $this->db->query($sql);        
        if(!empty($model) && $model->num_rows() > 0){
                return $model->row_array();
            }
        return false;
    }



    public function CheckIfObjectExistsInChannelType( $channel_name = 'inbound', $object_id = null, $object_type = 'Package' )
    {
        $sql = "SELECT * FROM `ct_channel_object` WHERE `object_id` = $object_id AND `object_type` = '".$object_type."' AND `channel_id` IN (SELECT `id` FROM `ct_channels` WHERE `name` = '".$channel_name."')";
        
        $model = $this->db->query($sql);        
        if(!empty($model) && $model->num_rows() > 0){
                return $model->row_array();
            }
        return false;
    }


    public function credentials_exists( $object_id, $table = 'ct_blogs' ) 
    {
        $sql = "SELECT `id`, `seo_url`, `title_eng` FROM `".$table."` WHERE `id` = $object_id";
        $model = $this->db->query($sql);        
        if(!empty($model) && $model->num_rows() > 0) {
                return $model->row_array();
            }
        return false;
    }

    public function distinct_credentials( $credentials, $table = 'ct_blogs' ) 
    {
        $sql = "SELECT DISTINCT(`".$credentials."`) FROM `".$table."`"; 
        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0) {
                return $model->result_array();
            }
        return false;
    }


    public function getObjectsAccordingToTags( $tag_string, $table = 'ct_blogs' )
    {
        $sql = "SELECT * FROM `". $table ."` WHERE `tags` LIKE '%". $tag_string ."%'";

        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0) {
                return $model->result_array();
            }
        return false;
    }


    public function getPackagesAccordingToSearchedResult( $search_string, $viewd_flag = 'all' )
    {
        $append_string = ( $viewd_flag == 'popular' ) ? ' GROUP BY packages.`viewd` DESC ' : ( ( $viewd_flag == 'latest' ) ? ' ORDER BY packages.`created_at` DESC ' : 'ORDER BY packages.`created_at` DESC' );

        $sql = "SELECT packages.*, files.`id` as file_id, files.`file_name`, files.`file_path`, files.`flag` FROM `ct_packages` packages, `ct_files` files WHERE packages.`title_eng` LIKE '%". $search_string ."%' AND packages.id = files.fileable_id AND files.fileable_type = 'Package' AND files.flag = 'package_logo' ". $append_string ." LIMIT 0, 1000000";

        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0) {
                return $model->result_array();
            }
        return false;

    }


    public function getTravelPaxDef( $id )
    {
        $sql = "SELECT * FROM `ct_travel_pax` WHERE `id` = $id";

        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0) {
                return $model->row_array();
            }
        return false;
    }



    public function getAddOnDefByID( $id )
    {
        $sql = "SELECT * FROM `ct_addon_type` WHERE `id` = $id";

        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0) {
                return $model->row_array();
            }
        return false;
    }



    public function getSocialLinks( $append = null ) 
    {
        $sql = "SELECT * FROM `ct_social_links` ".$append;
        $model = $this->db->query($sql);
        if(!empty($model) && $model->num_rows() > 0) {
                return $model->result_array();
            }
        return false;
    }


    public function store($data)
    {
        $model = $this->db->insert( 'ct_social_links', $data);
        if(!empty($model) && $model === true) {
            return $this->db->insert_id();
        }
        return false;
    }


    public function update($_id, $data) 
    {
        $this->db->where('id', $_id);
        $this->db->update( 'ct_social_links', $data);
        return $_id;
    }
    


    } // end of model class


?>