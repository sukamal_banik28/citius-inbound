<?php

class Page extends CI_Model {

	public $table = 'ct_visa_pages';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($page_id, $data) {
		$this->db->where('id', $page_id);
		$this->db->update($this->table, $data);
		return $page_id;
	}


	public function getPages()
	{
		$sql = "SELECT * FROM `ct_visa_pages` WHERE flag = 'inbound'";
		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function getPageDetails($page_id, $lang = 'eng')
	{
		//$sql = "SELECT `id`, `meta_key`, `meta_description`, `status`, `seo_url`, `title_".$lang."`, `short_desc_".$lang."`, `long_desc_".$lang."` FROM `ct_pages` WHERE `id` = $page_id";

		$sql = "SELECT * FROM `ct_visa_pages` WHERE flag = 'inbound' AND id = $page_id";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}



	public function credentials_exists( $page_id ) 
	{
		$sql = "SELECT `id`, `seo_url` FROM `ct_pages` WHERE `id` = $page_id";
		$model = $this->db->query($sql);		
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}



	public function distinct_credentials( $credentials ) 
	{
		$sql = "SELECT DISTINCT(`".$credentials."`) FROM `ct_pages`";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}


	public function getPageByKeyWord($page_keyword, $lang = 'eng')
	{
		//$sql = "SELECT `id`, `meta_key`, `meta_description`, `status`, `seo_url`, `title_".$lang."`, `short_desc_".$lang."`, `long_desc_".$lang."` FROM `ct_pages` WHERE `key_word` = '".$page_keyword."'";

		$sql = "SELECT * FROM `ct_visa_pages` WHERE `key_word` = '".$page_keyword."' AND `flag` = 'inbound'";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;
	}


} // end of model class



?>