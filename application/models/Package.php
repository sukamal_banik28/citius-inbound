<?php

class Package extends CI_Model {

	public $table = 'ct_packages';

	public function __construct() {
		parent::__construct();
		$this->db->db_debug = FALSE;
	}


	public function store($data)
	{
		$model = $this->db->insert($this->table, $data);
		if(!empty($model) && $model === true) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function update($package_id, $data) {
		$this->db->where('id', $package_id);
		$this->db->update($this->table, $data);
		return $package_id;
	}


	public function getPackages($append = null)
	{
		$sql = "SELECT packages.*, files.`id` as file_id, files.`file_name`, files.`file_path`, files.`flag` FROM `ct_packages` packages, `ct_files` files WHERE packages.id = files.fileable_id AND files.fileable_type = 'Package' AND packages.deleted_at IS NULL".$append;

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}


	public function getPackageDetailsByID($package_id, $lang = 'eng')
	{
		$sql = "SELECT `id`, `crm_id`, `code`, `seat`, `days`, `nights`, `inventory`, `deletion_flag`, `regular_price`, `validity_date_from`, `validity_date_to`, `sales_price`, `gst`, `citius_touch`, `tags`, `seo_url`, `meta_key`, `meta_description`, `is_new`, `deleted_at`, `title_".$lang."`, `short_desc_".$lang."`, `long_desc_".$lang."`, `depart_city_".$lang."`, `arrival_city_".$lang."`, `cancellation_policy_".$lang."` FROM `ct_packages` WHERE `id` = $package_id";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->row_array();
		}
		return false;

	}


	public function credentials_exists( $package_id ) 
	{
		$sql = "SELECT `id`, `crm_id`, `seo_url`, `title_eng` FROM `ct_packages` WHERE `id` = $package_id";
		$model = $this->db->query($sql);		
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->row_array();
        	}
        return false;
	}

	public function distinct_credentials( $credentials ) 
	{
		$sql = "SELECT DISTINCT(`".$credentials."`) FROM `ct_packages`";
		$model = $this->db->query($sql);
		if(!empty($model) && $model->num_rows() > 0){
        		return $model->result_array();
        	}
        return false;
	}



	public function getDeletedPackages()
	{
		$sql = "SELECT packages.*, files.`id` as file_id, files.`file_name`, files.`file_path`, files.`flag` FROM `ct_packages` packages, `ct_files` files WHERE packages.id = files.fileable_id AND files.fileable_type = 'Package' AND packages.deleted_at IS NOT NULL";

		$model = $this->db->query($sql);
		if (!empty($model) && $model->num_rows() > 0) {
			return $model->result_array();
		}
		return false;
	}




} // end of model class



?>