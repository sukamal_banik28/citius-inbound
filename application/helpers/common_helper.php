<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function checkSession()
{
    $CI = &get_instance();
    $user_id = $CI->session->userdata("user_id");
    $CI->load->model('users');
    $user = $CI->users->getUserByID($user_id); 
    if (empty($user)) { 
        $CI->session->sess_destroy();
        redirect('/');
    } else {
        return $user;
    }
}


function checkRole()
{
    $CI = &get_instance();
    $user_id = $CI->session->userdata("user_id");
    $CI->load->model('users');
    $user = $CI->users->getUserByID($user_id);

    if ($user['role_id'] == 1) {
        return $user;
    } else {
        $CI->session->sess_destroy();
        redirect('/');
    }
}



function getDetailsBySEO($seo_url, $model, $table = 'ct_packages')
{
    $CI = &get_instance();
    $CI->load->model($model);
    $details = $CI->$model->getDetailsBySEO( $seo_url, $table );

    if ( !empty($details['id'] )) {
        return $details;
    } else {
        redirect('404');
    }
}


function getPackages( $model = 'package' )
{
    $CI = &get_instance();
    $CI->load->model( $model );
    $temp = array();
    $packages = $CI->$model->getPackages();

    if(!empty($packages)) {
        foreach ($packages as $key => $value) {
            if($value['flag'] == 'package_logo') {
                $temp[$key] = $value;
            }
        }

        return $temp;
    }
}



function localEmailConfig()
{
    $CI = &get_instance();
    $CI->load->library('email');
    $config = array(
        'protocol'  => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'sukamalbanik11@gmail.com',
        'smtp_pass' => 'cdhpb8171r',
        'mailtype'  => 'html',
        'charset'   => 'utf-8'
    );
    $CI->email->initialize($config);
    $CI->email->set_mailtype("html");
    $CI->email->set_newline("\r\n");
}


    
function file_upload($file_data, $fileable_id, $fileable_type, $file_path, $flag = 'package_logo', $file_name = null) 
{
    $CI = &get_instance();
    $filesCount = count($file_data['name']);
    $files = $file_data;
    $data = array(); 

    for($i = 0; $i < $filesCount; $i++) {

        $_FILES['files']['name']     = $files['name'][$i];
        $_FILES['files']['type']     = $files['type'][$i];
        $_FILES['files']['tmp_name']    = $files['tmp_name'][$i];
        $_FILES['files']['error']     = $files['error'][$i];
        $_FILES['files']['size']     = $files['size'][$i];

        $extension = pathinfo($_FILES['files']['name'], PATHINFO_EXTENSION);
        $newFileName = microtime(true) * 10000 . '.'. $extension;

        $config['upload_path'] = $file_path;
        $config['allowed_types'] = '*';
        $config['overwrite'] = TRUE;

        $config['file_name'] = empty($file_name) ? $newFileName : $file_name;

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config); 

        if(!$CI->upload->do_upload('files')) {
            echo $CI->upload->display_errors(); 
            exit();
        } else {
            $config['source_image'] = $CI->upload->upload_path.$CI->upload->file_name;
            $config['maintain_ratio'] = FALSE;  
            

            $data = array(
                // 'file_name' => $CI->upload->data('file_name'),

                'fileable_id' => $fileable_id,
                'fileable_type' => $fileable_type,
                'file_name' => empty($file_name) ? $CI->upload->data('file_name') : $file_name,
                'file_type' => $CI->upload->data('file_type'),
                'file_path' => $file_path,
                'file_ext' => $CI->upload->data('file_ext'),
                'file_size' => $CI->upload->data('file_size'),
                'original_name' => $CI->upload->data('client_name'),
                'flag' => $flag,
            );
                
            $CI->files->store($data);

        }
    } // end loop
} // end file upload function


function getFileableImage( $fileable_id, $fileable_type = 'User', $flag = 'profile_photo' )
{
    $CI = &get_instance();
    $fileable_image = $CI->files->getFilesByFileableId( $fileable_id, $fileable_type, $flag );
    return $fileable_image;
}



function sendInboundEmailNotification ( $to, $subject, $mesg, $attachment = null )
{
    require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');     
    $mail = new PHPMailer();
    
    // $mail->isSMTP();                                      // Set mailer to use SMTP
    // $mail->Host = 'mail.citius.in';              // Specify main and backup SMTP servers
    // $mail->Username = 'inbound@citius.in';                 // SMTP username
    // $mail->Password = 'Inbound@1234';                           // SMTP password
    
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Host = 'smtp.rediffmailpro.com';
    $mail->Username = 'inbound@citiusinfo.com';
    $mail->Password = 'Inbound@123';
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;
    $mail->AddCustomHeader("X-MSMail-Priority: High");

    $mail->setFrom('inbound@citiusinfo.com', 'CITIUS Inbound', 0);
    $mail->addAddress( $to );
    $mail->Subject  = $subject;
    $mail->Body = $mesg;
    $mail->msgHTML($mesg);

    if(!empty($attachment)) {
        $mail->addAttachment($attachment);
    }

    $mail->send();

    // if(!$mail->send()) {
    //   echo 'Message was not sent.';
    //   echo 'Mailer error: ' . $mail->ErrorInfo;
    // } else {
    //   echo 'Message has been sent.';
    // }
}




function sendEmailToClient( $to, $subject, $mesg )
{
    $CI = &get_instance();
    // localEmailConfig();

    $CI->email->from('sukamal.banik@speakbinary.in', 'Citius-Inbound');
    $CI->email->to($to);
    $CI->email->subject($subject);
    $CI->email->message($mesg);
    $CI->email->set_mailtype("html");
    $CI->email->send();
}


function sendEmailToAdmin( $from, $subject, $mesg )
{
    $CI = &get_instance();
    // localEmailConfig();

    $CI->email->from( $from, 'Citius-Inbound' );
    $CI->email->to('sukamal.banik@speakbinary.in');
    $CI->email->subject($subject);
    $CI->email->message($mesg);
    $CI->email->set_mailtype("html");
    $CI->email->send();
}

function getCategories( $flag = 'package' )
{
    $CI = &get_instance();
    $temp = array();
    $categories = $CI->category->getCategoriesByFlag( $flag );       

    if(!empty($categories)){
        foreach ($categories as $key => $value) {
            if($value['parent_id'] == 0) {
                $temp[$key] = $value;
            }
        }
        return $temp;
    }
}


function getUserProfilePhotoIfLoggedIn( $user_id )
{
    $CI = &get_instance();
    $fileable_id = $user_id;
    $temp['user'] = checkSession();
    $temp['profile_photo'] = getFileableImage( $fileable_id, 'User', 'profile_photo' );
    return $temp;
}


function getCommonHeaderAttributes( $pagetitle = 'Citius-Inbound' )
{
    $CI = &get_instance();
    $temp['pagetitle'] = $pagetitle;
    $temp['lang'] = isset($_GET['lang']) ? $_GET['lang'] : 'eng';
    $temp['parent_categories'] = $CI->parent_categories;
    return $temp;
}


function getCategoriesByFlag( $flag = 'Package' )
{
    $CI = &get_instance();
    $temp = array();
    $categories = $CI->category->getCategoriesByFlag( $flag );

    if(!empty($categories)) {
        foreach ($categories as $key => $value) {
            if($value['parent_id'] == 0) {
                $temp[$key] = $value;
            }
        }
    }
    return $temp;
}



function storeHits( $hit_id, $hit_type, $model = 'ct_packages' )
{
    $CI = &get_instance();
    $date = date('Y-m-d H:i:s');
    $sql = "UPDATE ".$model." SET `viewd` = `viewd` + 1, `updated_at`= '". $date ."' WHERE id = ".$hit_id."";
    $model = $CI->db->query($sql);
}


function removeAccents($str) 
{
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
  return str_replace($a, $b, $str);
}



function getBanners( $position = 'header')
{
    $CI = &get_instance();
    $append = " AND banner.`status` = 'Active' AND banner.`position` = '".$position."' ORDER BY banner.`order_no` ASC";
    $banners = $CI->banner->getBanners( $append );
    return $banners;
}



function getCurrencyByID($id)
{
    $CI = &get_instance();
    $append = " WHERE id =".$id;
    $data = $CI->currency->getCurrencies($append);
    return $data;
}

function getTravelPax($id)
{
    $CI = &get_instance();
    $append = " WHERE id =".$id;
    $data = $CI->currency->getTravelPax($append);
    return $data;
}



function deleteCurrencyObjectByID($id = null)
{
    $CI = &get_instance();
    $CI->currency->deleteExistingObjectCurrencyByID($id);
    return true;
}


function getHideaways( $file_type = 'hideaway' )
{
    $CI = &get_instance();
    $append = " AND hideaways.file_type = '".$file_type."' AND hideaways.is_active = 1 ORDER BY hideaways.order_by ASC";
    $data = $CI->hideaways->getHideaways($append);
    return $data;
}


function getCompanyDetails()
{
    $CI = &get_instance();
    $company = $CI->company->getDetails( 'eng' );
    return $company;
}



function getValueForProductCurrency( $product_id, $productable_type , $addon_id, $addonable_type , $cur_id )
{
    
    $CI = &get_instance();
    $details = $CI->currency->getProductValuesForCurrency( $product_id, $productable_type , $addon_id, $addonable_type , $cur_id );
    return $details;
}



function getPackageMinValueForCurrency( $currency_id, $object_id, $objectable_type = 'package' )
{
    $CI = &get_instance();
    $details = $CI->currency->getPackageMinValueForCurrency($currency_id, $object_id, $objectable_type = 'package');
    return $details;
}


function getTravelPaxDefByID( $travel_pax_id )
{
    $CI = &get_instance();
    $details = $CI->home->getTravelPaxDef($travel_pax_id);
    return $details;
}


function getAddOnDefByID( $addon_id ) 
{
    $CI = &get_instance();
    $details = $CI->home->getAddOnDefByID( $addon_id );
    return $details;
}


function getAllPackages($append)
{
    $CI = &get_instance();
    $details = $CI->package->getPackages( $append );
    return $details;
}



function packagePagination( $search_string, $page, $packages )
{
    $CI = &get_instance();
    $CI->load->library('pagination');
    $config['base_url'] = base_url().'manage-tour/'.$search_string.'/page/';
    $config['total_rows'] = count($packages);
    $config['per_page'] = 2;

    $CI->pagination->initialize($config);

    return $CI->pagination->create_links();
}


function travelPaxUsingPackageCat($append)
{
    $CI = &get_instance();
    $data = $CI->currency->getTravelPax($append);
    return $data;
}



function store_booking_details($booking_id, $requests, $currency_id)
{
    $CI = &get_instance();

    foreach ($requests as $p_key => $p_value) 
    {
        foreach ($p_value as $key => $value) 
        {

        $data = array(

            'booking_id' => $booking_id,
            'currency_id' => $currency_id,
            'objectable_id' => $p_key,
            'objectable_type' => 'product',
            'sub_objectable_id' => $key,
            'sub_objectable_type' => 'addon',
            'value' => $value,
            );

            $CI->booking->store_booking_details($data);
        }
    }
}


function getProductByID( $product_id, $lang )
{
    $CI = &get_instance();
    $data = $CI->product->getProductDetailsByID( $product_id, $lang );
    return $data;
}


function getPackageByID( $product_id, $lang )
{
    $CI = &get_instance();
    $data = $CI->package->getPackageDetailsByID( $product_id, $lang );
    return $data;
}



function getLocationByObject_id( $object_id )
{
    $CI = &get_instance();
    $data = $CI->city->getCitiDetailsByObjectID( $object_id )[0];
    return $data;
}


function unlinkFile( $file )
{
    $CI = &get_instance();
    unlink(FCPATH.$file['file_path'].$file['file_name']);
}

    


function getStateDefByID( $object_id )
{
    $CI = &get_instance();
    $append = "WHERE `id` = ".$object_id;
    $data = $CI->city->getStates( $append );
    return $data;
}

function common_elements($pagetitle = 'Inbound')
{
    $data['user'] = checkRole();
    $data['pagetitle'] = $pagetitle;
    return $data;
}



function getAmenityDefById( $amenity_id ) 
{
    $CI = &get_instance();
    $append = " WHERE `id` = ".$amenity_id;
    $details = $CI->product->getAmenities($append)[0];
    return $details;
}

function getAboutUsPageContent()
{
    $CI = &get_instance();
    $data = $CI->page->getPageByKeyWord('about');
    return $data;
}


function getSocialLinks( $append )
{
    $CI = &get_instance();
    $data = $CI->home->getSocialLinks( $append );
    return $data;
}


function getPaginationConfig()
{
    $config = array();
    $config['per_page'] = 10;
    $config['reuse_query_string'] = TRUE;
    $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['query_string_segment'] = 'page';
    
    $config['full_tag_open']= '<nav aria-label="..."><ul class="pagination pagination-sm">';
    $config['full_tag_close']= '</ul></nav>';
    
    // first link
    $config['first_tag_open']= '<li class="first page-item">';
    $config['first_tag_close']= '</li>';
    
    // last link
    $config['last_tag_open']= '<li class="last page-item">';
    $config['last_tag_close']= '</li>';
    
    // current active pagination
    $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
    $config['cur_tag_close'] = '</a></li>';
    
    // number link
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    
    // next (>) link
    // $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';
    
    // prev (<) link
    // $config['prev_link'] = 'prev';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';

    $config['attributes'] = array('class' => 'page-link');

    return $config;
}




function getPartners()
{
    $CI = &get_instance();
    $append = " AND partner.`status` = 'Active' ORDER BY order_no ASC";
    $partners = $CI->partner->getPartners($append);
    return $partners;
}


function getProvinces( $parent_id )
{ 
    $CI = &get_instance();
    $append = " AND province.parent_id = ".$parent_id." AND province.is_active = 1 ORDER BY province.name_eng ASC";
    $provinces = $CI->province->getProvinces($append);
    return $provinces;
}



function getCurrencies()
{
    $CI = &get_instance();
    $append = ' WHERE `is_active` = 1';
    $currencies = $CI->currency->getCurrencies($append);
    return $currencies;
}


?>
