<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>
	

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Addons
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">

             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-addon'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>

              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Flag</th>
                  <th>Status(1 = Active, 0 = Deactive)</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                <?php if(!empty($addons)) {

                	foreach ($addons as $key => $value) { ?>
                		
                <tr>
                  <td><?php echo $value['id']; ?></td>
                  <td><?php echo $value['type_name_eng']; ?></td>
                  <td><?php echo ucwords($value['flag']); ?></td>
                  <td><span class="label <?php echo $value['is_active'] == 1 ? 'text-success' : 'deactive'; ?>"><?php echo $value['is_active']; ?></span></td>

                  <td style="width: 200px;">
	                   
	                   <a href="<?php echo base_url().'inbound-admin/manage-addon/addon-id/'.$value['id'].'/update?lang=eng' ?>"><span class="label edit-text"><i class="fa fa-pencil" aria-hidden="true"></i>
	                   Edit</span></a> 

                </tr>

                <?php }
                
                } ?>


                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
  	$(document).ready(function() {
    	$('#example1').DataTable();
	});
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>