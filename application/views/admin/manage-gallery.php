<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Gallery
      </h1>

      

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-gallery'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Image</th>
                  <th>Order</th>
                  <th>Status(1 = Active, 0 = Deactive)</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php 
                  if(!empty($gallery)) {
                  foreach ($gallery as $key => $value) { ?>
                      <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['title']; ?></td>
                        <td><img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" width="100" height="100" ></td>
                        <td><?php echo $value['order_by']; ?></td>
                        <td><span class="label <?php echo $value['is_active'] == 1 ? 'text-success' : 'deactive'; ?>"><?php echo $value['is_active']; ?></span></td>
                        <td>

                        <a href="<?php echo base_url().'inbound-admin/manage-gallery/gallery-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        Update</span></a>

                        <!-- href="<?php //cho base_url().'inbound-admin/manage-gallery/gallery-id/'.$value['id'].'/delete'; ?>" -->

                        <a class="delete" data-gallery="<?=$value['id']; ?>" ><span class="label delete-item"><i class="fa fa-trash" aria-hidden="true"></i>
                        Delete</span></a>

                        
                      </td> 
                      </tr>
                  <?php } } ?>
                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script type="text/javascript">
	$(document).ready(function() {

     var table = $('#example1').DataTable();
      $('#example1').on('click', '.delete', function () {

          var result = confirm('Do you want to delete this image from gallery permanently? Press OK to confirm!');

          if(result) {

            $id = $(this).data('gallery');
            var href= '<?php echo base_url().'inbound-admin/manage-gallery/gallery-id/'; ?>'+$id+'<?php echo '/delete'; ?>';
            window.location.href = href;

          }

      });

	});
</script>

<?php $this->load->view("admin/common/footer.php"); ?>
