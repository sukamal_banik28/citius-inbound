<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Packages
      </h1>

      

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-package'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Citius Touch</th>
                  <th>Image</th>
                  <th>From Date</th>
                  <th>To Date</th>
                  <th>Viewed(Times)</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php 
                  if(!empty($packages)) {
                  foreach ($packages as $key => $value) { ?>
                      <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['title_eng']; ?></td>
                        <td><?php echo $value['citius_touch']; ?></td>
                        <td><img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" width="100" height="100" ></td>
                        <td><?php echo date('d-M-Y', strtotime($value['validity_date_from'])); ?></td> 
                        <td><?php echo date('d-M-Y', strtotime($value['validity_date_to'])); ?></td>
                        <td><?php echo $value['viewd']; ?></td>
                        <td>

                          <a href="<?php echo base_url().'inbound-admin/manage-packages/package-id/'.$value['id'].'/manage-itinerary?lang=eng'; ?>"><span class="label edit-text"><i class="fa fa-file-text-o" aria-hidden="true"></i>
                         Manage Itinerary</span></a>

                         <a href="<?php echo base_url().'inbound-admin/manage-packages/package-id/'.$value['id'].'/update?lang=eng'; ?>"><span class="label edit-text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        Update</span></a>

                        <select class="form-control lang" data-packageid="<?php echo $value['id']; ?>" style="width: 150px; border: solid 1px #ccc; padding: 3px;">
                          <option value="eng" selected>English</option>
                          <option value="spanish">Spanish</option>
                          <option value="french">French</option>
                         </select>

                      </td> 
                      </tr>
                  <?php } } ?>
                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
	$(document).ready(function() {
    	$('#example1').DataTable();
	});

  $('.lang').change(function(event) {
      var lang = $(this).val();
      var packageid = $(this).data('packageid');
      
      var updateUrl = $(this).prev();
      var newUpdateUrl = "<?php echo base_url().'inbound-admin/manage-packages/package-id/'; ?>"+packageid+'/update?lang='+lang; 
      updateUrl.attr("href", newUpdateUrl);

      // var itineraryUrl = $(this).prev().prev();
      // var newItineraryUrl = "<?php //echo base_url().'inbound-admin/manage-packages/package-id/'; ?>"+packageid+'/itinerary?lang='+lang;
      // itineraryUrl.attr("href", newItineraryUrl);
  });


</script>

<?php $this->load->view("admin/common/footer.php"); ?>
