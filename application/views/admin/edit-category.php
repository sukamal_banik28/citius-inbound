<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Edit Category: <b><?php echo $category['name_eng']; ?></b> <br>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

	       <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Category Name(English)<span class="required"> * </span></label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" value="<?php echo $category['name_eng']; ?>" name="name" class="form-control" required >
					</div>
				</div>


        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Name(Spanish)<span class="required"> * </span></label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" value="<?php echo $category['name_spanish']; ?>" name="name_spanish" class="form-control" required>
          </div>
        </div>


        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Name(French)</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" value="<?php echo $category['name_french']; ?>" name="name_french" class="form-control">
          </div>
        </div>



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Short Desc(English) </label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" value="<?php echo $category['short_desc_eng']; ?>" name="short_desc_eng" class="form-control" >
          </div>
        </div>


        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Short Desc(Spanish)</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" value="<?php echo $category['short_desc_spanish']; ?>" name="short_desc_spanish" class="form-control">
          </div>
        </div>



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12"><span data-toggle="tooltip" title="" data-original-title="Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.">SEO URL</span><span class="required">*</span></label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" value="<?php echo $category['seo_url']; ?>" name="seo_url" class="form-control" placeholder="Seo Urls must be unique" >
            <span style="color: red; float: left;"><?php echo form_error('seo_url'); ?></span>
          </div>
        </div>




         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Position</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="number" min="1" value="<?php echo $category['order_by']; ?>" name="order_by" class="form-control" >
          </div>
        </div>





                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-categories'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>