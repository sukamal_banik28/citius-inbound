<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA53RbrL34Vo9SA8EsuZHMsUKyXbXeebAg&libraries=places"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add User<br>
        <small>user basic information</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" enctype='multipart/form-data'>

             <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="name" class="form-control" placeholder="Name" required="" value="<?php echo set_value('name'); ?>">
              </div>
            </div>

                <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Address <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="email" name="email" id="email" class="form-control" placeholder="Email Address" required="" value="<?php echo set_value('email'); ?>">
                <span style="color: red; float: left;"><?php echo form_error('email'); ?></span>
              </div>
            </div>

                

                <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact No. <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="phone" class="form-control" placeholder="Contact No." required="" value="<?php echo set_value('phone'); ?>">
                <span style="color: red; float: left;"><?php echo form_error('phone'); ?></span>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Address <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input class="form-control" type="text" id="locationTextField" name="address" value="<?php echo set_value('address'); ?>">
              </div>
            </div>



            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="password" name="confirm_password" class="form-control" placeholder="Password" required="">
                <span style="color: red; float: left;"><?php echo form_error('confirm_password'); ?></span>
              </div>
            </div>


             
              <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Profile Photo </label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="file" name="profile_photo[]"  class="form-control" placeholder="Profile Photo" accept="image/*">
              </div>
            </div>

                <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">User Type</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" tabindex="-1" name="role_id" id="user_type">
                  <option value="1">Administrator</option>
                  <option value="2" selected="">Customer</option>
                </select>
              </div>
            </div>

                <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">User Status</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" tabindex="-1" name="status" id="user_status">
                  <option value="1">Active</option>
                  <option value="0">InActive</option>
                </select>
              </div>
            </div>

                <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href="<?php echo base_url().'inbound-admin/manage-users'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
                <button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
              </div>
            </div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
        function init() {
        var input = document.getElementById('locationTextField');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }   
    google.maps.event.addDomListener(window, 'load', init);
    </script>

<?php $this->load->view("admin/common/footer.php"); ?>