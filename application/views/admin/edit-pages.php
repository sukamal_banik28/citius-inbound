<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Page<br>
          <small>content basic information</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">

            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

             <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Page Title<span class="required"> * </span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="title" id="cms_title"  class="form-control" placeholder="Page Title" required="" value="<?php echo $page['title']; ?>">
							</div>
						</div>
                
             

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"> Description(English)</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="long_desc" name="long_desc" rows="10" cols="80"><?php echo $page['description']; ?></textarea>
            </div>
							</div>
						</div>




            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12"> Description(Spanish)</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="description_spanish" name="description_spanish" rows="10" cols="80"><?php echo $page['description_spanish']; ?></textarea>
            </div>
              </div>
            </div>






                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="select2_single form-control" tabindex="-1" name="status" id="cms_status">
									<option value="1" <?php echo $page['is_active'] == 1 ? 'selected' : ''; ?> >Active</option>
                  <option value="0" <?php echo $page['is_active'] == 0 ? 'selected' : ''; ?> >Deactive</option>
								</select>
							</div>
						</div>


                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

								<a href="<?php echo base_url().'inbound-admin/manage-pages'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
                <button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>

							</div>
						</div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function() {

      CKEDITOR.replace('long_desc', {
          filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
      });

      CKEDITOR.replace('description_spanish', {
          filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
      });

    });
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>