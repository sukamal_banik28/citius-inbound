<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add New
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Type Name *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="type_name" class="form-control" placeholder="Title" required="" value="<?php echo $addons['type_name_eng']; ?>" >
          </div>
          </div>


            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Type Category</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control"  name="flag" >
                  <option value="hotel" <?php echo $addons['flag'] == 'hotel' ? 'selected' : ''; ?> >Hotel</option>
                  <option value="meal" <?php echo $addons['flag'] == 'meal' ? 'selected' : ''; ?> >Meal</option>
                  <option value="extension" <?php echo $addons['flag'] == 'extension' ? 'selected' : ''; ?> >Extension</option>
                </select>
              </div>
            </div>



            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control"  name="is_active" >
                  <option value="1" <?php echo $addons['is_active'] == 1 ? 'selected' : ''; ?> >Active</option>
                  <option value="0" <?php echo $addons['is_active'] == 0 ? 'selected' : ''; ?> >Deactive</option>
                </select>
              </div>
            </div>



              <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-addon'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>