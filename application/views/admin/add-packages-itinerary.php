<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Package Itinerary<br>
        <small><?php echo $package['title_eng']; ?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

             	<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Name</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" value="<?php echo $package['title_eng']; ?>" data-required="1" class="form-control" readonly="">
					</div>
				</div>

                <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Day Wise Itinerary</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="number" name="day_wise_plan" id="day" class="form-control" min="1">
					</div>
				</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Itinerary Short Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="short_desc" name="short_desc" rows="10" cols="80"></textarea>
            </div>
								
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Itinerary Long Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="long_desc" name="long_desc" rows="10" cols="80"></textarea>
            </div>
								
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Itinerary Image</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input name="itinerary_image[]" type="file" class="form-control" accept="image/*">
							
							</div>
						</div>

                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-packages/package-id/'.$package['id'].'/manage-itinerary'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
  	$(document).ready(function() {

  		CKEDITOR.replace('short_desc', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

	    CKEDITOR.replace('long_desc', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

  	});
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>