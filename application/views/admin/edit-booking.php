<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        View Booking Details
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Name</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="client_name" class="form-control" placeholder="" required="" value="<?php  echo $booking_info['client_name']; ?>" readonly >
          </div>
          </div>




          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Telephone</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="client_telephone" class="form-control" placeholder="" required="" value="<?php  echo $booking_info['client_telephone']; ?>" readonly >
          </div>
          </div>



          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Email</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="client_email" class="form-control" placeholder="" required="" value="<?php  echo $booking_info['client_email']; ?>" readonly >
          </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Address</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="client_address" class="form-control" placeholder="" required="" value="<?php  echo $booking_info['client_address']; ?>" readonly >
          </div>
          </div>



          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Message(if any)</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <textarea name="client_message" class="form-control" readonly rows="5" ><?php  echo $booking_info['client_message']; ?></textarea>
          </div>
          </div>





              <?php if(!empty($booking_info['fixed_pax_range_id'])) { 
                $travel_pax = getTravelPax($booking_info['fixed_pax_range_id']);
              ?>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">No of Pax Group.</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="fixed_pax_range" class="form-control" placeholder="" required="" value="<?=$travel_pax[0]['pax_range']; ?>" readonly >
              </div>
              </div>
            <?php } ?>



            <?php if(!empty($booking_info['fit_adult'])) { 
              ?>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">No of Adult.</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="fit_adult" class="form-control" placeholder="" required="" value="<?=$booking_info['fit_adult']; ?>" readonly >
              </div>
              </div>
            <?php } ?>



            <?php if(!empty($booking_info['fit_child'])) { 
              ?>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">No of Child.</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="fit_child" class="form-control" placeholder="" required="" value="<?=$booking_info['fit_child']; ?>" readonly >
              </div>
              </div>
            <?php } ?>





            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Addons and Extension Details</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <table class="table">

                  <thead>
                    <tr>
                      <th>Product Name</th>
                      <th>Location</th>
                      <th>Type</th>
                      <th>Price</th>
                    </tr>
                  </thead>


                  <tbody>

            <?php if( !empty($booking_details)) { 

                foreach ($booking_details as $key => $value) {

                  $lang = isset($_GET['lang']) ? $_GET['lang'] : 'eng';

                  if( $value['objectable_type'] == 'product' ) {
                    $product = getProductByID($value['objectable_id'], $lang);
                  } else {
                    $product = getPackageByID($value['objectable_id'], $lang);
                  }

                  $addonType = getAddOnDefByID($value['sub_objectable_id']);
                  $location = getLocationByObject_id($value['objectable_id']);

                  $currency = getCurrencyByID($value['currency_id']);

              ?>
                  <tr>
                    <td><?=$product['title_'.$lang]?></td>
                    <td><?=$location['city']?></td>

                    <?php if(!empty($addonType['type_name_eng'])) { ?>
                    <td><?=$addonType['type_name_eng']; ?></td>
                    <td><?=$currency[0]['cur_name'] ?>.  <?=$value['value']; ?></td>
                    <?php } else { ?>

                    <td colspan="2">Added Package</td>
                    <?php } ?>

                  </tr>
                  <?php }  } ?>
                </tbody>
                  
                  
                </table>
              </div>
              </div>




          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Total Amount</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="total" class="form-control" placeholder="" required="" value="<?php  echo $booking_info['total']; ?>" readonly >
          </div>
          </div>





            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="select2_single form-control"  name="confirmed" >
                <option value="1" <?php echo $booking_info['confirmed'] == 1 ? 'selected' : '' ?> >Confirmed</option>
                <option value="0" <?php echo $booking_info['confirmed'] == 0 ? 'selected' : '' ?> >Pending</option>
                <option value="0" <?php echo $booking_info['confirmed'] == 2 ? 'selected' : '' ?> >Cancelled</option>
              </select>
            </div>
          </div>




            <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/booking-history'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<!-- <button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button> -->
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>