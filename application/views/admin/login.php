<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login - Citius</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/front-end/'; ?>img/favicon.ico" type="image/x-icon">
    
    <!-- BASE CSS -->
    <link href="<?php echo base_url().'assets/front-end/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/front-end/'; ?>css/style.css" rel="stylesheet">
	<link href="<?php echo base_url().'assets/front-end/'; ?>css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url().'assets/front-end/'; ?>css/custom.css" rel="stylesheet">

</head>

<body id="login_bg">
	
	<nav id="menu" class="fake_menu"></nav>
	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->
	
	<div id="login">
		<aside>
			<figure>
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url().'assets/front-end/img/citius-logo.png'; ?>" width="150" height="36" data-retina="true" alt="" class="logo_sticky"></a>
			</figure>
			
				<?php if(isset($errorMsg)) { echo '<span style="color: red;">'.$errorMsg."</spna>"; } ?>

			  <form method="post" action="">
				<div class="form-group">
					<label>Email OR Phone OR Registration</label>
					<input type="text" class="form-control" name="loginID" placeholder="Email or Phone or Registration" value="<?php echo set_value('loginID'); ?>">
					<i class="icon_mail_alt"></i>
					<span style="color: red; float: left;"><?php echo form_error('loginID'); ?></span>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password" id="password" value="">
					<i class="icon_lock_alt"></i>
					<span style="color: red; float: left;"><?php echo form_error('password'); ?></span>
				</div>
				<div class="clearfix add_bottom_30">
					<div class="checkboxes float-left">
						<label class="container_check">Remember me
						  <input type="checkbox">
						  <span class="checkmark"></span>
						</label>
					</div>
					<div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
				</div>
				<input type="submit" name="submit" value="Login to Citius" class="btn_1 rounded full-width">
			</form>
			<div class="copy">© <?php echo date('Y'); ?> Citius</div>
		</aside>
	</div>
	<!-- /login -->
		
	<!-- COMMON SCRIPTS -->
    <script src="<?php echo base_url().'assets/front-end/'; ?>js/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url().'assets/front-end/'; ?>js/common_scripts.js"></script>
    <script src="<?php echo base_url().'assets/front-end/'; ?>js/main.js"></script>
  
</body>
</html>