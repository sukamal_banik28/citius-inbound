<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Payments
      </h1>

      

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-offline-payment'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Sl.No.</th>
                  <th>Invoice No.</th>
                  <th>Invoice Amount</th>
                  <th>Client Name</th>
                  <th>Client Email</th>
                  <th>Client Address</th>
                  <th>Is Active(1 = Active, 0 = Deactive)</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php 
                  if(!empty($payments)) {
                  foreach ($payments as $key => $value) { 

                    $currency = getCurrencyByID($value['currency_id'])[0];

                    ?>
                      <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['invoice_no']; ?></td>
                        <td><?php echo $value['invoice_amount'].' '.$currency['cur_name']; ?></td>
                        <td><?php echo $value['client_name']; ?></td>
                        <td><?php echo $value['client_email']; ?></td>
                        <td><?php echo $value['client_address']; ?></td>
                        <td><span class="label <?php echo $value['is_active'] == 1 ? 'text-success' : 'deactive'; ?>"><?=$value['is_active']; ?></td>
                        <td><?php echo ucwords($value['status']); ?></span></td>
                        
                        <td>
                        <a href="<?php echo base_url().'inbound-admin/manage-offline-payments/payment-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        Update</span></a>
                        
                      </td> 
                      </tr>
                  <?php } } ?>
                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
	$(document).ready(function() {
    	$('#example1').DataTable();
	});
</script>

<?php $this->load->view("admin/common/footer.php"); ?>
