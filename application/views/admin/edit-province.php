<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Province<br>
          <small>Province basic information</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>





	         <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Province Name(English) *</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" name="name_eng" class="form-control" placeholder="Province Name in English" required="" value="<?=$province['name_eng'];?>" >
					</div>
				</div>


        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Province Name(Spanish) *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="name_spanish" class="form-control" placeholder="Province Name in Spanish" required="" value="<?=$province['name_spanish'];?>" >
          </div>
        </div>



        <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Province Description(English)</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea name="description_eng" id="description_eng" class="form-control" rows="3" placeholder="Province Description"><?=$province['description_eng'];?></textarea>
              </div>
          </div>



          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Province Description(Spanish)</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea name="description_spanish" id="description_spanish" class="form-control" rows="3" placeholder="Province Description"><?=$province['description_spanish'];?></textarea>
              </div>
            </div>



            <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Province Font Icon for Country (Refers: <a href="http://flag-icon-css.lip.is/" target="_blank" >Click here</a> )</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="font_icon_tag" class="form-control" value="<?=$province['font_icon_tag']; ?>" >
          </div>
        </div>




	      <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Province Image</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="file" name="province_image[]" class="form-control" >
					</div>
				</div>



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Province Order</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="number" min="1" name="order_by" class="form-control" placeholder="Province Order" value="<?=$province['order_by'];?>" >
          </div>
        </div>



        <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Is Active</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="select2_single form-control"  name="is_active" >
							<option value="1" <?php echo $province['is_active'] == 1 ? 'selected' : ''; ?> >Active</option>
              <option value="0" <?php echo $province['is_active'] == 0 ? 'selected' : ''; ?> >Deactive</option>
						</select>
					</div>
				</div>

            <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-province'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>