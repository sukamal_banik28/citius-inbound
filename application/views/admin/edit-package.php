<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>


	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Package: <b><?php echo $package['title_'.$lang]; ?></b>
      </h1>
    </section>


    <?php 
	    $data = array('package_id' => $package['id']);
	    $this->load->view("admin/common/package-currency-model", $data); 
	?>



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">

            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>


             <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12 control-label">Package Id CRM <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="crm_id" id="Package_id_crm" class="form-control" placeholder="Package Id CRM" required value="<?php echo $package['crm_id']; ?>">
								<span style="color: red; float: left;"><?php echo form_error('crm_id'); ?></span>
								
							</div>
						</div>



				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Channel Type <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="channels[]" required multiple>
									<?php if(!empty($channels)) { 
										foreach ($channels as $key => $value) {
										?>
									<option value="<?php echo $value['id'] ?>" 
										<?php echo !empty($value['selected']) ? 'selected' : ''; ?> >Citius-<?php echo ucfirst($value['name']); ?></option>
									<?php } }?>
								</select>								
							</div>
						</div>




                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Title <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="title" id="Package_title" class="form-control" placeholder="Package Title" required value="<?php echo $package['title_'.$lang]; ?>">
								<span style="color: red; float: left;"><?php echo form_error('title'); ?></span>
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Short Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="short_desc" id="Package_short_desc" class="form-control" rows="3" placeholder="Package Short Description"><?php echo $package['short_desc_'.$lang]; ?></textarea>
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Long Description <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="long_description" name="long_description" rows="10" cols="80"><?php echo $package['long_desc_'.$lang]; ?></textarea>
            </div>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Category <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">

                                		<select name="categories[]" id="category" class="form-control" multiple required >
                                			<?php 
                                				foreach ($categories as $key => $value) { 
                                					?>
                                			
                                				<option value="<?php echo $value['id']; ?>" 
                                					<?php echo !empty($value['selected']) ? 'selected' : ''; ?>
                                					><?php echo $value['name_eng']; ?></option>

                                			<?php }  ?>
                                		</select>
								
							</div>
						</div>
                <div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Sub Category</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<select name="sub_categories[]" id="sub_category" class="form-control" multiple>
											<?php 
                                				foreach ($sub_categories as $key => $value) { 
                                					?>
                                			
                                				<option value="<?php echo $value['id']; ?>" 
                                					<?php echo !empty($value['selected']) ? 'selected' : ''; ?>
                                					><?php echo $value['name_eng']; ?></option>

                                			<?php }  ?> 
										</select>
									</div>
							</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Seat <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="seat" id="seat" pattern="[0-9]" title="Please insert numeric value" class="form-control" placeholder="seat" required value="<?php echo $package['seat']; ?>" >
								
							</div>
						</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Total number of Days<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" min="1" name="days" id="days" class="form-control" placeholder="Total number of Days" required value="<?php echo $package['days']; ?>" >
								
							</div>
						</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Total number of Nights<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" min="1" name="nights" id="nights" class="form-control" placeholder="Total number of Nights" required value="<?php echo $package['nights']; ?>" >
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Inventory <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" min="1" name="inventory" id="inventory" class="form-control" placeholder="Inventory" required value="<?php echo $package['inventory']; ?>" >
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">City of Departure <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="city_depart" class="form-control" placeholder="City of Departure" required value="<?php echo $package['depart_city_'.$lang]; ?>">
								
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">City of Arrival <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="city_arrival" id="myInput" class="form-control" placeholder="City of Arrival" required value="<?php echo $package['arrival_city_'.$lang]; ?>" >
								<div id="city1-suggesstion-box"></div>
						
							</div>
						</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Validity Date From <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control" id="Validity_date_from" name="validity_date_from" type="text">
                </div>
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Validity Date To <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control" id="Validity_date_to" name="validity_date_to" type="text">
                </div>
								
							</div>
						</div>
                
                
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Cancellation Policy<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="policy" name="cpolicy" rows="10" cols="80" required><?php echo $package['cancellation_policy_'.$lang]; ?></textarea>
            </div>
								
							</div>
						</div>

                <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Deletion Flag <span class="required">*</span></label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="form-control select2me" data-required="1" name="dflag" id="dflag">
									<option value="No" <?php echo $package['deletion_flag'] == 'No' ? 'selected' : ''; ?> >No</option>
									<option value="Yes" <?php echo $package['deletion_flag'] == 'Yes' ? 'selected' : ''; ?> >Yes</option>
                        </select>
					
					</div>
				</div>



				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Citius Touch ( Note. Please select Fixed if the package is in Fixed Departures Category )<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" data-required="1" name="citius_touch" id="citius_touch">
									<option value="Fixed" <?php echo $package['citius_touch'] == 'Fixed' ? 'selected' : ''; ?> >Fixed</option>
									<option value="Customised" <?php echo $package['citius_touch'] == 'Customised' ? 'selected' : ''; ?> >Customised</option>
                                </select>
							
							</div>
						</div>



				<!-- This feature enables only when product type is in Hotel -->
				<div class="form-group" >
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Prices ( Depending upon which type of Citius Touch you select. i.e. Fixed or Customised )<span class="required">*</span></label>
					<div class="col-md-9 col-sm-9 col-xs-12">

							<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#currencyModal">Click here to Add Prices</button>
						
							<!-- <select class="form-control select2me" name="product_addons" id="product_addons" multiple > -->
							
							<?php //if($currencies) { 
									//foreach ($currencies as $key => $value) {
								?>

							<!-- <label>Rupees is in <?php //echo $value['cur_name']; ?></label> -->
							<!-- <input type="number" min="1" name="price[<?php //echo $value['id']; ?>]" class="form-control" placeholder="<?php //echo $value['cur_name']; ?>" value="<?php //echo !empty($value['package']) ? $value['package']['value'] : ''; ?>" > -->

							<!-- <option value="<?php //$value['id'] ?>"><?php //echo $value['type_name_eng'] ?></option> -->

							<?php //} } ?>
							
						<!-- </select>								 -->
					</div>
				</div>


                <!-- <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Regular Price <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" min="1" name="rprice" id="rprice" placeholder="Regular Price" class="form-control" required value="<?php //echo $package['regular_price']; ?>" >
					
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Sales Price </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" min="1" name="sprice" id="sprice" placeholder="Sales Price" class="form-control" value="<?php //echo $package['sales_price']; ?>" >
					
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">GST in % <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" min="1" name="gst" id="gst" data-required="1" placeholder="GST" class="form-control" required value="<?php //echo $package['gst']; ?>" >
							
							</div>
						</div> -->


                
                

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Image</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="package_image[]" class="form-control" accept="image/*" >
							
							</div>
						</div>


				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Banner</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="package_banner[]" class="form-control" accept="image/*" >
							
							</div>
						</div>


				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Gallery</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="package_gallery[]" class="form-control" accept="image/*" multiple>
							
							</div>
						</div>




                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Add Tags</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input name='tags' data-role="tagsinput" class='form-control' placeholder='write some tags' autofocus value="<?php echo $package['tags']; ?>">
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><span data-toggle="tooltip" title="" data-original-title="Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.">SEO URL</span><span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="seo_url" id="seo_url" placeholder="Seo Url" class="form-control" required value="<?php echo $package['seo_url']; ?>" >
								<span style="color: red; float: left;"><?php echo form_error('seo_url'); ?></span>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Key </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								
							<textarea name="meta_key" id="meta_key" class="form-control" rows="3" placeholder="Meta Description"><?php echo $package['meta_key']; ?></textarea>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="meta_desc" id="meta_desc" class="form-control" rows="3" placeholder="Meta Description"><?php echo $package['meta_description']; ?></textarea>
							</div>
						</div>


				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" data-required="1" name="status">
											<option value="publish" <?php echo empty($package['deleted_at']) ? 'selected' : ''; ?> >Publish</option>
											<option value="unpublish" <?php echo !empty($package['deleted_at']) ? 'selected' : ''; ?> >Unpublish</option>
									
                                </select>
							
							</div>
						</div>



				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Show as New <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" data-required="1" name="is_new">
											<option value="1" <?php echo $package['is_new'] == 1 ? 'selected' : ''; ?> >Is New</option>
											<option value="0" <?php echo $package['is_new'] == 0 ? 'selected' : ''; ?> >Is Not New</option>
									
                                </select>
							
							</div>
						</div>




                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-packages'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">

  	$(document).ready(function() {
  		
  	
	    CKEDITOR.replace('long_description', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

	    CKEDITOR.replace('policy', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });


	    var startDate = '<?php echo date('Y-m-d', strtotime($package['validity_date_from'])); ?>';
	    $('#Validity_date_from').daterangepicker({
	    	startDate : startDate ,
	    	singleDatePicker: true,
	    	showDropdowns: true,
	        locale: {
	                format: 'YYYY-MM-DD',
	            }, 
	    });


	    var endDate = '<?php echo date('Y-m-d', strtotime($package['validity_date_to'])); ?>';
	    $('#Validity_date_to').daterangepicker({
	    	startDate : endDate,
	    	singleDatePicker: true,
	    	showDropdowns: true,
	        locale: {
	                format: 'YYYY-MM-DD',
	            },
	    });


	    $('#category').change(function(event) {
	    	var category_ids = $(this).val();

	    	console.log(category_ids)

	    	$.ajax({
	    		url: '<?php echo base_url().'CategoryController/getSubCategoriesByAjax' ?>',
	    		type: 'POST',
	    		data: {
	    			category_ids : category_ids 
	    		},
	    	})
	    	.done(function(response) {
	    		$('#sub_category').html(response);
	    	})
	    	.fail(function() {
	    		console.log("error");
	    	})
	    	.always(function() {
	    	});
	    	
	    });



    }); // ready function end

</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<?php $this->load->view("admin/common/footer.php"); ?>