<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Blog
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data' >

             <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Blog Title <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="title" id="blog_title" class="form-control" placeholder="Blog Title" required="" value="<?php echo $blog['title_'.$lang]; ?>">
								<span style="color: red; float: left;"><?php echo form_error('title'); ?></span>
							</div>
						</div>

			<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Channel Type <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="channels[]" required multiple>
									<?php if(!empty($channels)) { 
										foreach ($channels as $key => $value) {
										?>
									<option value="<?php echo $value['id'] ?>" 
										<?php echo !empty($value['selected']) ? 'selected' : ''; ?> >Citius-<?php echo ucfirst($value['name']); ?></option>
									<?php } }?>
								</select>								
							</div>
						</div>



					<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Blog Categories <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">

                                		<select name="categories[]" id="category" class="form-control"  multiple required >
                                			<?php foreach ($categories as $key => $value) { ?>
                                				<option value="<?php echo $value['id']; ?>" 
                                					<?php echo !empty($value['selected']) ? 'selected' : ''; ?>
                                					><?php echo $value['name_eng']; ?></option>
                                			<?php } ?>
                                		</select>
								
							</div>
						</div>
                



                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="description" name="description" rows="10" cols="80"><?php echo $blog['desc_'.$lang]; ?></textarea>
            </div>
								
							</div>
						</div>



				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Author <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="author" id="author" class="form-control" placeholder="Blog Author" required="" value="<?php echo $blog['author']; ?>">
							</div>
						</div>


				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Add Tags</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input name='tags' data-role="tagsinput" class='form-control' placeholder='write some tags' autofocus value="<?php echo $blog['tags']; ?>">
							</div>
						</div>



				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><span data-toggle="tooltip" title="" data-original-title="Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.">SEO URL</span><span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="seo_url" id="seo_url" placeholder="Seo Url" class="form-control" required value="<?php echo $blog['seo_url']; ?>">
								<span style="color: red; float: left;"><?php echo form_error('seo_url'); ?></span>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Key </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								
							<textarea name="meta_key" id="meta_key" class="form-control" rows="3" placeholder="Meta Description"><?php echo $blog['meta_key']; ?></textarea>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="meta_desc" id="meta_desc" class="form-control" rows="3" placeholder="Meta Description"><?php echo $blog['meta_description']; ?></textarea>
							</div>
						</div>




                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" data-required="1" name="status">
											<option value="publish" <?php echo $blog['status'] == 'publish' ? 'selected' : ''; ?> >Publish</option>
											<option value="unpublish" <?php echo $blog['status'] == 'unpublish' ? 'selected' : ''; ?>>Unpublish</option>
									
                                </select>
							
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Image To Upload</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input name="blog_image[]" type="file" class="form-control">
							</div>
						</div>


                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-blogs'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>

                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
  	
  	$(document).ready(function() {
  		CKEDITOR.replace('description', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });
  	});

  </script>


	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
	<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        
<?php $this->load->view("admin/common/footer.php"); ?>