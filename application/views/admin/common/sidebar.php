<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url().'assets/'; ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user['name']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu tree" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url().'inbound-admin/dashboard'; ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>


        <li>
          <a href="<?php echo base_url().'inbound-admin/manage-enquire'; ?>">
            <i class="fa fa-envelope-o" aria-hidden="true"></i> <span> Manage Enquiries</span>
          </a>
        </li>



        <li class="treeview">
          <a href="#">
            <i class="fa fa-database" aria-hidden="true"></i> <span> Master Data Management </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-users'; ?>">
                <i class="fa fa-user-circle-o"></i> Manage Users</a>
              </li>

              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-blogs'; ?>">
                <i class="fa fa-rss-square" aria-hidden="true"></i> <span> Manage Blogs</span>
              </a>
              </li>

              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-reviews'; ?>">
              <i class="fa fa-comments-o" aria-hidden="true"></i> <span> Reviews/Comments</span>
              </a>
              </li>

              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-banners'; ?>">
              <i class="fa fa-picture-o" aria-hidden="true"></i> <span> Banner Management</span>
              </a>
              </li>

              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-testimonials'; ?>">
              <i class="fa fa-quote-left" aria-hidden="true"></i> <span> Testimonial Management</span>
              </a>
              </li>

              <li>
              <a href="<?php echo base_url().'inbound-admin/company-details'; ?>">
              <i class="fa fa-building"></i> <span> Company Details</span>
              </a>
              </li>

              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-pages'; ?>">
              <i class="fa fa-info-circle"></i> <span> Content Management</span>
              </a>
              </li>

              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-hideaways'; ?>">
              <i class="fa fa-indent"></i> <span> Unique Hideaways / Videos Management</span>
              </a>
              </li>


              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-gallery'; ?>">
              <i class="fa fa-picture-o"></i> <span> Gallery Management</span>
              </a>
              </li>


              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-ebrochure'; ?>">
              <i class="fa fa-book"></i> <span> Ebrochures Management</span>
              </a>
              </li>


              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-addon'; ?>">
              <i class="fa fa-puzzle-piece"></i> <span> Addon Management</span>
              </a>
              </li>


              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-amenities'; ?>">
              <i class="fa fa-object-group"></i> <span> Amenities Management</span>
              </a>
              </li>


              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-cities'; ?>">
              <i class="fa fa-globe"></i> <span> City Management</span>
              </a>
              </li>


              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-sociallinks'; ?>">
              <i class="fa fa-link"></i> <span> Social Link Management</span>
              </a>
              </li>


              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-partners'; ?>">
              <i class="fa fa-link" aria-hidden="true"></i> <span> Associated Partners</span>
              </a>
              </li>


              <li>
              <a href="<?php echo base_url().'inbound-admin/manage-province'; ?>">
              <i class="fa fa-globe"></i> <span> Provices Management</span>
              </a>
              </li>


            
          </ul>
        </li>


        

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-user-circle-o" aria-hidden="true"></i> <span> User / Customer <br> Management </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php //echo base_url().'inbound-admin/manage-users'; ?>"><i class="fa fa-circle-o"></i> Manage Users / Customers</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Customer List</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Customer Online</a></li>
          </ul>
        </li> -->


        <li class="treeview">
          <a href="#">
            <i class="fa fa-product-hunt" aria-hidden="true"></i> <span> Package Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'inbound-admin/manage-categories'; ?>"><i class="fa fa-circle-o"></i> Manage Categories</a></li>
            <li><a href="<?php echo base_url().'inbound-admin/manage-packages'; ?>"><i class="fa fa-circle-o"></i> Manage Packages</a></li>
            <li><a href="<?php echo base_url().'inbound-admin/manage-products'; ?>"><i class="fa fa-circle-o"></i> Manage Products</a></li>
            <li><a href="<?php echo base_url().'inbound-admin/manage-deleted-packages'; ?>"><i class="fa fa-circle-o"></i> Deleted Packages</a></li>
          </ul>
        </li>

        


        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>  Booking Management </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'inbound-admin/booking-history'; ?>"><i class="fa fa-circle-o"></i> Booking History</a></li>
            <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Add Order</a></li> -->
            <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Booking Refund/Cancellation</a></li> -->
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dollar" aria-hidden="true"></i> <span> Payment Management </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'inbound-admin/manage-offline-payments'; ?>"><i class="fa fa-circle-o"></i> Payment History</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>