<ol class="breadcrumb">        
    <div class="form-group">
		<?php echo ( $lang == 'spanish' ) ? $this->lang->line('spanish_Language_Selection') : ( $lang == 'french' ? $this->lang->line('french_Language_Selection') : 'Language Selection' ) ; ?> <span class="required">*</span>&nbsp;
			<select class="" data-required="1" name="language" id="language" required="" style="width: 150px; border: solid 1px #ccc; padding: 3px;">
				<?php foreach ($language as $key => $value) { ?>
					<option value="<?php echo $key; ?>" <?php echo $key == $lang ? 'selected' : ''; ?> ><?php echo $value; ?></option>
				<?php } ?>
			</select>
	</div>
</ol>