	
	<!-- Currency Modal -->
	<div id="currencyModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Set Price for Package </h4>
			</div>
			<div class="modal-body">

				<input type="hidden" id="product_id" value="<?php echo $product['id']; ?>">

				<div class="form-group">
					<label>Select Currency Type</label>
					<select class="form-control" name="" id="currency_type">
					
					<?php if($currencies) { 
							foreach ($currencies as $key => $value) {
					?>
					<option value="<?php echo $value['id']; ?>"><?php echo $value['cur_name']; ?></option>
					<?php } } ?>

					
					</select>
				</div>


				<?php if( $product['type'] == 7 ) { ?>
				<div class="form-group">
					<label>Select Room Type (Default 2 Bed) </label>
					<select class="form-control" name="" id="room_types">
					
					<?php if($addons) { 
							foreach ($addons as $key => $value) {
					?>
					<option value="<?php echo $value['id']; ?>"><?php echo $value['type_name_eng']; ?></option>
					<?php } } ?>

					</select>
				</div>
				<?php } ?>



				<div class="form-group">
					<label>Price</label>
					<input class="form-control" type="number" min="1" id="price">
				</div>

				<div class="checkbox">
					<label><input type="checkbox" value="true" id="isDefault" checked >Is default currency?</label>
				</div>

				<!-- <hr> -->

				<table class="table table-striped">
					<thead>
					<tr>
					<th>Currency Type</th>
					<th>Room Type</th>
					<th>Price</th>
					<th>Is Default Currency?</th>
					<th>Edit</th>
					</tr>
					</thead>

					<tbody id="tbody">
						<?php if(!empty($records)) {
								foreach ($records as $key => $value) {
									$currency = getCurrencyByID($value['currency_id']);
									$pax = getTravelPax($value['travel_pax_id']);
						 ?>
					<tr id="rec<?=$value['id'];?>">
					<td><?php echo $currency[0]['cur_name']; ?></td>
					<td><?php echo $pax[0]['pax_range']; ?></td>
					<td><?php echo $value['value']; ?></td>
					<td><?php echo $value['is_default']; ?></td>
					<td><input type="button" value="edit" class="edit" data-currency_id="<?=$value['currency_id'];?>"   data-travel_pax_id="<?php echo $value['travel_pax_id']; ?>" data-value="<?php echo $value['value']; ?>" data-is_default="<?php echo $value['is_default']; ?>"  ></td>
					
					<td><input type="button" class="delete" value="delete" data-record_id="<?=$value['id']; ?>" ></td>

					</tr>

						<?php } } ?>
					
					</tbody>
				</table>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="save">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>

		</div>
	</div>



	<script type="text/javascript">
		$(document).ready(function() {

			$('#save').click(function(event) {
				
				var product_id = $('#product_id').val();
				var currency_type = $('#currency_type').val();
				var room_types = $('#room_types').val();
				var price = $('#price').val();
				var checked = $('#isDefault').is(":checked");

				var isDefault = (checked == true) ? true : false;
				


				console.log(product_id)
				console.log(currency_type)
				console.log(room_types)
				console.log(price)
				console.log(isDefault)

				// if(price != '') 
				// {
				// 	var result = confirm('Do you want to save? Press OK to confirm.');

				// 	if(result) {

				// 		$.ajax({
				// 		url: '<?php // echo base_url().'PackageController/storePackagePrices' ?>',
				// 		type: 'POST',
				// 		data: {  

				// 			package_id : package_id,
				// 			currency_type : currency_type,
				// 			travel_pax : travel_pax,
				// 			price : price,
				// 			isDefault : isDefault

				// 			},
				// 		})
				// 		.done(function(response) {

				// 			if(response) {
				// 				$('#tbody').html(response);
				// 			} else {
				// 				alert('');
				// 			}
				// 		})
				// 		.fail(function() {
				// 			console.log("error");
				// 		})
				// 		.always(function() {
				// 		});
				// 		/* Ajax End */

				// 	} // if confirm
				
				// } else {
				// 	alert('Please fill all the fields.');
				// }

			}); // click end


			$('#tbody').on('click', 'input.edit', function(event) 
			{
				var currency_type = $(this).data("currency_id");
				var travel_pax = $(this).data("travel_pax_id");
				var price = $(this).data("value");
				var isDefault = $(this).data("is_default");

				// console.log(currency_type)
				// console.log(travel_pax)
				// console.log(price)
				// console.log(isDefault)

				$("#currency_type").val(currency_type).change();
				$("#travel_pax").val(travel_pax).change();
				$('#price').val(price);

				isDefault == true ? $('#isDefault').prop('checked', true) : $('#isDefault').prop('checked', false);
			});



			$('#tbody').on('click', 'input.delete', function(event) 
			{
				var record_id = $(this).data("record_id");

				var result = confirm("Do you want to delete this record? Press OK to continue.");

				if(result) {
					$.ajax({
						url: '<?php echo base_url().'PackageController/deleteCurrencyRecord' ?>',
						type: 'POST',
						data: { record_id : record_id },
					})
					.done(function(response) {
						
						if(response == true) {							
							$('#rec'+record_id).remove();
							alert('Record deleted successfully.');
						} else {
							alert('There is something wrong.');
						}
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
					});
					
				}

			});
			
			
		}); // end document ready
	</script>