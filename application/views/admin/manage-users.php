<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Customers
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-users'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Reg.no.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Date Added</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                	<?php if(!empty($customers)) { 
                			foreach ($customers as $key => $value) {
        		 	?>
                <tr>
					<td><b><?php echo $value['registration_number']; ?></b></td>
					<td><?php echo $value['name']; ?></td>
					<td><?php echo $value['email']; ?></td>
					<td><?php echo $value['phone']; ?></td> 
					<td><?php echo date('d-M-Y', strtotime($value['created_at'])); ?></td>                 
					<td><span class="label <?php echo $value['is_active'] == 1 ? 'text-success' : 'edit-text bg-red' ?>"> <?php echo $value['is_active'] == 1 ? 'Active' : 'InActive' ?></span></td>
					<td style="width: 200px;">
					<a href="<?php echo base_url().'inbound-admin/manage-users/user-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil" aria-hidden="true"></i>
					Edit</span></a>
					</td> 
                </tr>

            	<?php } } ?>
                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
  	$(document).ready(function() {
    	$('#example1').DataTable({
        "order": [[ 4, 'desc' ]]
      });
  	});
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>