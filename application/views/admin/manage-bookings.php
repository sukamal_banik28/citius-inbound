<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Bookings
      </h1>

      

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             
             <!-- <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php //echo base_url().'inbound-admin/add-hideaways'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div> -->
             
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Telephone</th>
                  <th>Address</th>
                  <th>Message(if any)</th>
                  <th>Confirmed(1 = Confirmed, 0 = Pending, 2 = Cancelled)</th>
                  <th>Date Created</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php 
                  if(!empty($bookings)) {
                  foreach ($bookings as $key => $value) { ?>
                      <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['client_name']; ?></td>
                        <td><?php echo $value['client_telephone']; ?></td>
                        <td><?php echo $value['client_address']; ?></td>
                        <td><?php if(!empty($value['client_message'])) { echo substr($value['client_message'], 0, 70).'...'; } ?></td>

                        <td><span class="label <?php echo $value['confirmed'] == 1 ? 'text-success' : 'deactive'; ?>"><?php echo $value['confirmed']; ?></span></td>

                        <td><?php echo date('Y-m-d', strtotime($value['created_at'])); ?></td>
                        
                        <td>
                        <a href="<?php echo base_url().'inbound-admin/booking-history/booking-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        View Details</span></a>
                        
                      </td> 
                      </tr>
                  <?php } } ?>
                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
	$(document).ready(function() {
    	$('#example1').DataTable();
	});

  $('.lang').change(function(event) {
      var lang = $(this).val();
      var packageid = $(this).data('packageid');
      
      var updateUrl = $(this).prev();
      var newUpdateUrl = "<?php echo base_url().'inbound-admin/manage-packages/package-id/'; ?>"+packageid+'/update?lang='+lang; 
      updateUrl.attr("href", newUpdateUrl);

      // var itineraryUrl = $(this).prev().prev();
      // var newItineraryUrl = "<?php //echo base_url().'inbound-admin/manage-packages/package-id/'; ?>"+packageid+'/itinerary?lang='+lang;
      // itineraryUrl.attr("href", newItineraryUrl);
  });


</script>

<?php $this->load->view("admin/common/footer.php"); ?>
