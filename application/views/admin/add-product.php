<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>



	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Product
      </h1>
    </section>

    <!-- <div class="input-group">
	  <span class="input-group-addon">Label Name</span>
	  <input type="text" class="form-control" placeholder="Username">
	  <input type="text" class="form-control" placeholder="Username">
	</div> -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">

            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>
            

            <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12 control-label">Product Id CRM <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="crm_id" id="product_id_crm" class="form-control" placeholder="Product Id CRM" required="">
								<span style="color: red; float: left;"><?php echo form_error('crm_id'); ?></span>
								
							</div>
						</div>



			<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Channel Type <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="channels[]" required multiple>
									<?php if(!empty($channels)) { 
										foreach ($channels as $key => $value) {
										?>
									<option value="<?php echo $value['id'] ?>">Citius-<?php echo ucfirst($value['name']); ?></option>
									<?php } }?>
								</select>								
							</div>
						</div>






                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Title <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="title" id="product_title" class="form-control" placeholder="Product Title" required="">
								<span style="color: red; float: left;"><?php echo form_error('title'); ?></span>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Name<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="packages[]" id="packages" multiple required>
									<?php if(!empty(($packages))) {
											foreach ($packages as $key => $value) {
									?>
									<option value="<?php echo $value['id']; ?>"><?php echo $value['title_eng']; ?></option> 
									<?php } } ?>                                    
								</select>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Short Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="short_description" name="short_desc" rows="10" cols="80">
                                            This is my textarea to be replaced with CKEditor.
                    </textarea>
            </div>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Long Description </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="long_description" name="long_description" rows="10" cols="80">
                                            This is my textarea to be replaced with CKEditor.
                    </textarea>
            </div>
								
							</div>
						</div>




				<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Location <span class="required">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select class="form-control select2me" name="location[]" required >
								<?php if(!empty($locations)) { 
										foreach ($locations as $key => $value) {
									?>
									<option value="<?php echo $value['id'] ?>"><?php echo $value['city'] ?></option>
								<?php } } ?>
							</select>
						</div>
					</div>






				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Type <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="product_type" required id="product_type">
										<option disabled="" selected="" >Select product type</option>
									<?php if(!empty($amenities)) { 
											foreach ($amenities as $key => $value) {
										?>
										<option value="<?php echo $value['id'] ?>" data-name="<?php echo $value['name_eng']; ?>" ><?php echo ucwords($value['name_eng']); ?></option>
									<?php } } ?>
								</select>
							</div>
						</div>




				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Amenities</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="amenities[]" id="amenities" multiple >
									
									<?php //if(!empty(($amenities))) {
											//foreach ($amenities as $key => $value) {
									?>
									<!-- <option value="<?php //echo $value['id']; ?>"><?php //echo $value['name_eng']; ?></option>  -->
									<?php //} } ?> 

								</select>
								
							</div>
						</div>

                



				<!--  This feature enables only when product type is in Hotel -->
				<div class="form-group" style="display: none" id="hotel_rating">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Hotel Rating <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="product_rating" >
									<option value="5">5 Star</option>
									<option value="4">4 Star</option>
									<option value="3">3 Star</option>
									<option value="2">2 Star</option>
									<option value="1">1 Star</option>
									<option value="Heritage">Heritage</option>
									<option value="Resort">Resort</option>
								</select>								
							</div>
						</div>



				<!-- This feature enables only when product type is in Hotel -->
				<!-- <div class="form-group" style="display: none" id="addon_type">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Hotel Types(Default - Standerd) <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12"> -->
								<!-- <select class="form-control select2me" name="product_addons" id="product_addons" multiple > -->
									
									<?php //if($addons) { 
											//foreach ($addons as $key => $value) {
										?>

									<!-- <label><?php //echo $value['type_name_eng']; ?></label>
									<input type="number" min="0" name="rates[<?php //echo $value['id']; ?>]" class="form-control" placeholder="<?php //echo $value['type_name_eng']; ?>"> -->

									<!-- <option value="<?php //$value['id'] ?>"><?php //echo $value['type_name_eng'] ?></option> -->

									<?php // } } ?>
									
								<!-- </select>								 -->
							<!-- </div>
						</div> -->





				<div class="form-group" id="addon_type">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Variation prices according to product type </label>
							<div class="col-md-9 col-sm-9 col-xs-12 variation">

									<b>Please select a product type first</b>
								
									<!-- <label><?php //echo $value['type_name_eng']; ?></label> -->
									<!-- <input type="number" min="0" name="rates[<?php //echo $value['id']; ?>]" class="form-control" placeholder="<?php //echo $value['type_name_eng']; ?>"> -->

							</div>
						</div>

				



                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Validity Date From <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control" id="Validity_date_from" name="validity_date_from" type="text">
                </div>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Validity Date To <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control" id="Validity_date_to" name="validity_date_to" type="text">
                </div>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Deletion Flag <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="dflag" id="dflag">
											<option value="No">No</option>
											<option value="Yes">Yes</option>
                                </select>
							
							</div>
						</div>

                <!-- <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Regular Price </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="rprice" id="rprice" placeholder="Regular Price" class="form-control" min="1" >
					
							</div>
						</div> -->

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Image</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input name="product_image[]" id="product_image" type="file" class="form-control" accept="image/*">
							
							</div>
						</div>


				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Banner</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="product_banner[]" class="form-control" accept="image/*" >
							
							</div>
						</div>



				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Product Gallery</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="product_gallery[]" class="form-control" accept="image/*" multiple>
							
							</div>
						</div>



				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><span data-toggle="tooltip" title="" data-original-title="Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.">SEO URL</span><span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="seo_url" id="seo_url" value="" placeholder="Seo Url" class="form-control" required>
								<span style="color: red; float: left;"><?php echo form_error('seo_url'); ?></span>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Key </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								
							<textarea name="meta_key" id="meta_key" class="form-control" rows="3" placeholder="Meta Description"></textarea>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="meta_desc" id="meta_desc" class="form-control" rows="3" placeholder="Meta Description"></textarea>
							</div>
						</div>



                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-products'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>

                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
  	$(document).ready(function() {
  		
  		CKEDITOR.replace('long_description', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

	    CKEDITOR.replace('short_description', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

	    $('#Validity_date_from').daterangepicker({
	    	singleDatePicker: true,
	    	showDropdowns: true,
	        locale: {
	                format: 'YYYY-MM-DD',
	            }, 
	    });

	    $('#Validity_date_to').daterangepicker({
	    	singleDatePicker: true,
	    	showDropdowns: true,
	        locale: {
	                format: 'YYYY-MM-DD',
	            },
	    });


	    $('#product_type').change(function(event) {
	    	var value = $(this).val();

	    	var type_name = $(this).find(':selected').data('name')
	    	
	    	if( value == 7 ) {
	    		$('#hotel_rating').prop('style', 'display:block');
	    	} else {
	    		$('#hotel_rating').prop('style', 'display:none');
	    	}


	    	$.ajax({
	    		url: '<?php echo base_url().'ProductController/getProductSubTypes' ?>',
	    		type: 'POST',
	    		data: { 
	    			parent_id : value },
	    	})
	    	.done(function(response) {
	    		$('#amenities').html(response);
	    	})
	    	.fail(function() {
	    		console.log("error");
	    	})
	    	.always(function() {
	    	});



	    	$.ajax({
	    		url: '<?php echo base_url().'ProductController/getProductVariation' ?>',
	    		type: 'POST',
	    		data: { flag : type_name },
	    	})
	    	.done(function(response) {

	    		$('.variation').html(response);
	    		console.log( response );
	    	})
	    	.fail(function() {
	    		console.log("error");
	    	})
	    	.always(function() {
	    		console.log("complete");
	    	});
	    	
	    	
	    }); // end product type change





  	});
  </script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<?php $this->load->view("admin/common/footer.php"); ?>