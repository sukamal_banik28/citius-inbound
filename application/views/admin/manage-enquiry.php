<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Enquiry List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">

            <form method="post">
              <div class="form-group">
                Status
                  <select class="" data-required="1" name="status" style="width: 150px; border: solid 1px #ccc; padding: 3px;" onchange="this.form.submit()">
                      <option value="All" <?php echo 'All'==$status ? 'selected' : ''; ?> >All Enquiry</option>
                      <option value="Requested" <?php echo 'Requested'==$status ? 'selected' : ''; ?> >Requested</option>
                      <option value="Review" <?php echo 'Review'==$status ? 'selected' : ''; ?> >Review</option>
                      <option value="Resolved" <?php echo 'Resolved'==$status ? 'selected' : ''; ?> >Resolved</option>
                      <option value="Closed" <?php echo 'Closed'==$status ? 'selected' : ''; ?> >Closed</option>
                   </select>
              </div>
            </form>
      

              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Enquiry Ticket</th>
                  <th>Customer Name</th>
                  <th>Mail id</th>
                  <th>Mobile Number</th>
                  <th>Date added</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($enquiries)) {
                		foreach ($enquiries as $key => $value) { 
                	?>
                <tr>
                  <td><?php echo $value['ticket_no']; ?></td>
                  <td><?php echo $value['name']; ?></td>
                  <td><?php echo $value['email']; ?></td>
                  <td><?php echo $value['phone']; ?></td>
                  <td><?php echo date('d-M-Y', strtotime($value['created_at'])); ?></td>
                  <td><span class="label text-success"> <?php echo $value['current_status']; ?></span></td>
                  <td><a href="<?php echo base_url().'inbound-admin/manage-enquire/enquire-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span></a></td>
                </tr>

            	<?php } } ?>

                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
  	$(document).ready(function() {
    	$('#example1').DataTable({
        "order": [[ 4, 'desc' ]]
      });
	});
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>