<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Category Name</th>
                  <th>Flag</th>
                  <th>Seo Urls</th>
                  <th>Order By</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php if(!empty($package_seourls)) { 
                      foreach ($package_seourls as $key => $value) { ?>
                        
                  <tr>
                    <td><?php echo $value['id']; ?></td>

                    <td><?php echo $value['name_eng']; ?></td>

                    <td><?php echo $value['flag']; ?></td>

                    <td><?php echo $value['seo_url']; ?></td>

                    <td><?php echo $value['order_by']; ?></td>

                    <td style="width: 200px;"><a href="<?php echo base_url().'inbound-admin/manage-category/category-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil" aria-hidden="true"></i>
                     Edit</span></a>
                   </td> 

                  </tr>

                  <?php } 
                    }
                  ?>


                  <?php if(!empty($blog_seourls)) { 
                      foreach ($blog_seourls as $key => $value) { ?>
                        
                  <tr>
                    <td><?php echo $value['id']; ?></td>

                    <td><?php echo $value['name_eng']; ?></td>

                    <td><?php echo $value['flag']; ?></td>

                    <td><?php echo $value['seo_url']; ?></td>

                    <td><?php echo $value['order_by']; ?></td>

                    <td style="width: 200px;"><a href="<?php echo base_url().'inbound-admin/manage-category/category-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil" aria-hidden="true"></i>
                     Edit</span></a>
                   </td> 

                  </tr>

                  <?php } 
                    }
                  ?>


                

                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
  	$(document).ready(function() {
    	$('#example1').DataTable();
	  });
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>
