<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Social Link
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="name" class="form-control" placeholder="Name" required="">
          </div>
          </div>





        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Web Link *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="link" class="form-control" placeholder="Web Link" required="">
          </div>
        </div>



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Copy and Paste a Font Icon for display in website (Ref. <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">Click Here to get an Icon</a>)  </label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="fa_icon" class="form-control" placeholder="Select a Font Icon to add" >
          </div>
        </div>




        <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="select2_single form-control"  name="is_active" >
							<option value="1">Active</option>
              <option value="0">Deactive</option>
						</select>
					</div>
				</div>





                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-sociallinks'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>