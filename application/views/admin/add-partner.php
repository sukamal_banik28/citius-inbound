<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Partner
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

          
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Partner Image *</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="file" name="banner_image[]" class="form-control" required >
                </div>
              </div>



            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Partner Order</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="number" min="1" name="order" class="form-control" placeholder="Partner Order" >
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Position</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control"  name="position" >
                  <option value="header">Header</option>
                </select>
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control"  name="status" >
                  <option value="Active">Active</option>
                  <option value="Deactive">Deactive</option>
                </select>
              </div>
            </div>


                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-partners'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>