<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
			Company Details<br>
			<small>Company basic information</small>
		</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="">


            	<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Language<span class="required">*</span></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<select class="form-control select2me" name="language">
								<option value="eng" selected>English</option>
								<option value="spanish">Spanish</option>
								<option value="french">French</option>
							</select>
						</div>
				</div>



                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="name" id="banner_title" class="form-control" placeholder="Company Name" required="" value="<?php echo $company['name_'.$lang]; ?>">
								
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Address </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="address" id="banner_desc" class="form-control" rows="3" placeholder="Address"><?php echo $company['address_'.$lang]; ?></textarea>
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Company Telephone <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="telephone" id="phone" value="<?php echo $company['telephone']; ?>" class="form-control" placeholder="Company Telephone" required="">
								
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Company Email <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="email" name="email" id="email" value="<?php echo $company['email']; ?>" class="form-control" placeholder="Company Email" required="">
								
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Key</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="meta_key" id="meta_key" class="form-control" rows="3" placeholder="Meta Key"><?php echo $company['meta_key']; ?></textarea>
								
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta desc</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="meta_desc" id="meta_desc" class="form-control" rows="3" placeholder="Meta Description"><?php echo $company['meta_description']; ?></textarea>
							</div>
						</div>


                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>