<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Product Amenities
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>





          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Eng Name *</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="name_eng" class="form-control" value="<?=$amenities['name_eng']?>" placeholder="Eng Name" required="">
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Eng Spanish(if any) </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="name_spanish" class="form-control"  value="<?=$amenities['name_spanish']?>"  placeholder="Eng Spanish" >
            </div>
          </div>



          <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Amenities Group</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <select class="select2_single form-control"  name="parent_id" >
              <?php if(!empty($parent_amenities)) { 

                foreach ($parent_amenities as $key => $value) {
                  
                  ?>

              <option value="<?=$value['id'];?>"  <?php echo $amenities['parent_id'] == $value['id'] ? 'selected' : ''; ?>  ><?=strtoupper($value['name_eng']);?></option>

              <?php } } ?>
            </select>
          </div>
        </div>




        



                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-amenities'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>