<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>
	
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Product
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-product'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Product Title</th>
                  <th>Image</th>
                  <th>Product Short Description</th>
                  <th>Product Long Description</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php if(!empty($products)) {
                        foreach ($products as $key => $value) {                         
                   ?>
                <tr>
                  <td><?php echo $value['id']; ?></td>
                  <td><?php echo $value['title_eng']; ?></td>
                  <td><img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" width="100" height="100"></td>
                  <td><?php echo $value['short_desc_eng']; ?></td> 
                  <td><?php echo $value['long_desc_eng']; ?></td>
                  <td>
                      <a href="<?php echo base_url().'inbound-admin/manage-products/product-id/'.$value['id'].'/update?lang=eng'; ?>"><span class="label edit-text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    edit</span></a>

                        <select class="form-control lang" data-productid="<?php echo $value['id']; ?>" style="width: 100px; border: solid 1px #ccc; padding: 3px;">
                          <option value="eng" selected>English</option>
                          <option value="spanish">Spanish</option>
                          <option value="french">French</option>
                        </select>


                  </td> 
                </tr>

                <?php } } ?>
                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function() {

      $('#example1').DataTable();

      $('#example1').on('change', '.lang', function () {
          var lang = $(this).val();
          var productid = $(this).data('productid');
          var updateUrl = $(this).prev();
          var newUpdateUrl = "<?php echo base_url().'inbound-admin/manage-products/product-id/'; ?>"+productid+'/update?lang='+lang; 
          updateUrl.attr("href", newUpdateUrl);
      }); // end

    });
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>