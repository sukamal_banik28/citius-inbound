<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Itinerary<br>
        <small><?php echo $package['title_eng']; ?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/manage-packages/package-id/'.$package['id'].'/add-itinerary'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Day Wise Itinerary</th>
                  <th>Short Description</th>
                  <th>Long Description</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php 
                  if(!empty($itinerary)) {
                  foreach ($itinerary as $key => $value) { ?>
                    <tr>
                      <td><?php echo $value['id']; ?></td>
                      <td><?php echo 'Day '.$value['day_wise_plan']; ?></td>
                      <td><?php echo $value['short_desc_eng']; ?></td> 
                      <td><?php echo $value['long_desc_eng']; ?></td>                 
                      <td><img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" width="50" height=""></td>
                      <td style="width: 200px;">
                        
                        <a href="<?php echo base_url().'inbound-admin/manage-packages/itinerary-id/'.$value['id'].'/update?lang=eng'; ?>"><span class="label edit-text"><i class="fa fa-pencil" aria-hidden="true"></i>
                        Edit</span></a> 

                        <a data-itineraryid="<?php echo $value['id']; ?>" class="delete"><span class="label edit-text bg-red"><i class="fa fa-times" aria-hidden="true"></i>
                        Delete</span></a>

                        <select class="form-control lang" data-itineraryid="<?php echo $value['id']; ?>" style="width: 100px; border: solid 1px #ccc; padding: 3px;">
                          <option value="eng" selected>English</option>
                          <option value="spanish">Spanish</option>
                          <option value="french">French</option>
                        </select>

                      </td> 
                    </tr>
                  <?php } } ?>

                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function() {

      $('#example1').DataTable();

      $('.lang').change(function(event) {
        var lang = $(this).val();
        var itineraryid = $(this).data('itineraryid');
        
        var updateUrl = $(this).prev().prev();
        var newUpdateUrl = "<?php echo base_url().'inbound-admin/manage-packages/itinerary-id/'; ?>"+itineraryid+'/update?lang='+lang; 
        updateUrl.attr("href", newUpdateUrl);
      });

      $("#example1").on("click", ".lang", function change(argument) {
        var lang = $(this).val();
        var itineraryid = $(this).data('itineraryid');
        
        var updateUrl = $(this).prev().prev();
        var newUpdateUrl = "<?php echo base_url().'inbound-admin/manage-packages/itinerary-id/'; ?>"+itineraryid+'/update?lang='+lang; 
        updateUrl.attr("href", newUpdateUrl);
      });


      $('.delete').click(function(event) {
        var itinerary_id = $(this).data('itineraryid');

        var result = confirm('Do you want to delete? Press ok to confirm.');
        
        if(result) {

            $.ajax({
              url: '<?php echo base_url().'PackageController/deleteItinerary'; ?>',
              type: 'POST',
              data: {
                itinerary_id: itinerary_id },
            })
            .done(function(response) {
              $('.delete').parent().parent().remove()
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
            });
            
        }

      });

  });
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>