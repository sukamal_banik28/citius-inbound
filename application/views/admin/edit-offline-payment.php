<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Update Offline Payment Info
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>



              <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Currency in which bill will be paid for</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <select class="select2_single form-control"  name="currency_id" >
              <?php if(!empty($currencies)) { 
                  foreach ($currencies as $key => $value) {
              ?>
              <option value="<?=$value['id'];?>"   <?php echo $payment['currency_id'] == $value['id'] ? 'selected' : ''; ?>   ><?=$value['cur_name'];?></option>
              <?php } } ?>
            </select>
          </div>
        </div>



          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Invoice Number *</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="invoice_no" class="form-control" placeholder="Enter Invoice Number" value="<?=$payment['invoice_no']; ?>" required="">
            </div>
          </div>






          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Invoice Amount *</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="number" min="1" name="invoice_amount" class="form-control" placeholder="Enter Invoice Amount" value="<?=$payment['invoice_amount']; ?>" required="">
            </div>
          </div>





          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Name *</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="client_name" class="form-control" placeholder="Client Name" value="<?=$payment['client_name']; ?>" required="">
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Phone *</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="client_phone" class="form-control" placeholder="Client Phone" value="<?=$payment['client_phone']; ?>" required="">
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Email *</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="client_email" class="form-control" placeholder="Client Email"  value="<?=$payment['client_email']; ?>"  required="">
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Address *</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="client_address" id="locationTextField" class="form-control"  value="<?=$payment['client_address']; ?>"  required="">
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="select2_single form-control"  name="status" >
                <option value="pending" <?php echo $payment['status'] == 'pending' ? 'selected' : '' ?> >Pending</option>
                <option value="paid" <?php echo $payment['status'] == 'paid' ? 'selected' : '' ?> >Paid</option>
                <option value="partially paid" <?php echo $payment['status'] == 'partially paid' ? 'selected' : '' ?> >Partially Paid</option>
              </select>
            </div>
          </div>


          <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Is Active</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <select class="select2_single form-control"  name="is_active" >
              <option value="1" <?php echo $payment['is_active'] == 1 ? 'selected' : '' ?> >Active</option>
              <option value="0" <?php echo $payment['is_active'] == 0 ? 'selected' : '' ?> >Deactive</option>
            </select>
          </div>
        </div>

        

                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-offline-payments'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
        function init() {
        var input = document.getElementById('locationTextField');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }   
    google.maps.event.addDomListener(window, 'load', init);
    </script>

<?php $this->load->view("admin/common/footer.php"); ?>