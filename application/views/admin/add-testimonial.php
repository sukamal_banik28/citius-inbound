<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Testimonial
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

            <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Testimonial Title<span class="required"> * </span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="title" id="testm_title" class="form-control" placeholder="Testimonial Title" required="">
								<span style="color: red; float: left;"><?php echo form_error('title'); ?></span>
							</div>
			</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Create By<span class="required"> * </span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="created_by" id="testm_created_by" class="form-control" required="">
								<span style="color: red; float: left;"><?php echo form_error('created_by'); ?></span>
							</div>
						</div>
                


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"> Short Description</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<div class="">
								<textarea name="short_desc" id="short_description" rows="10" cols="80"></textarea>
							</div>
						</div>
					</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Long Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="long_description" name="long_description" rows="10" cols="80"></textarea>
            </div>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Testimonial Image </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="testimonial_image[]" class="form-control" placeholder="Testimonial Image">
								<span class="fa fa-picture-o form-control-feedback right" aria-hidden="true"></span>
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Testimonial Status</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="select2_single form-control" tabindex="-1" name="status" id="testm_status">
									<option value="Active">Active</option>
                                    <option value="Deactive">Deactive</option>
								</select>
							</div>
						</div>
                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-testimonials'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
  	$(document).ready(function() {
  		
  		CKEDITOR.replace('short_description', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

	    CKEDITOR.replace('long_description', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

  	});
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>