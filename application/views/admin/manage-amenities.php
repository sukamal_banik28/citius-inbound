<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Product Amenities
      </h1>

      

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-amenities'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Sl.No.</th>
                  <th>Eng Name</th>
                  <th>Spanish Name(if any)</th>
                  <th>Parent Group</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php 
                  if(!empty($child_amenities)) {
                  foreach ($child_amenities as $key => $value) { 

                    $amenity = getAmenityDefById($value['parent_id']);

                    ?>
                      <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['name_eng']; ?></td>
                        <td><?php echo $value['name_spanish']; ?></td>
                        <td><?php echo ucwords($amenity['name_eng']); ?></td>
                        
                        <td>
                        <a href="<?php echo base_url().'inbound-admin/manage-amenities/amenities-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        Update</span></a>
                        
                      </td> 
                      </tr>
                  <?php } } ?>
                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
	$(document).ready(function() {
    	$('#example1').DataTable();
	});
</script>

<?php $this->load->view("admin/common/footer.php"); ?>
