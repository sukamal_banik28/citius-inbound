<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Enquiry Ticket No. <b><?php echo $enquiry['ticket_no']; ?></b><br>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post">


              <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Package Code(if any)</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" name="package_code" id="package_code" class="form-control" rows="3" value="<?php echo $package['code']; ?>" placeholder="Package" readonly="readonly">
                                    </div>
                                </div>


             	<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Package Name(if any)</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" name="package" id="package" class="form-control" rows="3" value="<?php echo $package['title_eng']; ?>" placeholder="Package" readonly="readonly">
                                    </div>
                                </div>

                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Customer Name </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" name="name" id="name" class="form-control" rows="3" value="<?php echo $enquiry['name']; ?>" placeholder="Name" readonly="readonly">
                                    </div>
                                </div>
                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Number </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" name="phone" id="phone_no" class="form-control" rows="3" value="<?php echo $enquiry['phone']; ?>" placeholder="Phone Number" readonly="readonly">
                                    </div>
                                </div>

                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">User Message</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <textarea class="form-control" rows="10" readonly="readonly"><?php echo $enquiry['message']; ?></textarea>
                                    </div>
                                </div>


                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Id(if any) </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" name="email" id="email" class="form-control" rows="3" value="<?php echo $enquiry['email']; ?>" placeholder="email">
                                    </div>
                                </div>
                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select class="select2_single form-control" tabindex="-1" name="status" id="status">
                                      	  <option value="Requested" <?php echo 'Requested'==$enquiry['current_status'] ? 'selected' : ''; ?> >Requested</option>
                                          <option value="Review" <?php echo 'Review'==$enquiry['current_status'] ? 'selected' : ''; ?> >Review</option>
                                          <option value="Resolved" <?php echo 'Resolved'==$enquiry['current_status'] ? 'selected' : ''; ?> >Resolved</option>
                                          <option value="Closed" <?php echo 'Closed'==$enquiry['current_status'] ? 'selected' : ''; ?> >Closed</option>
                                        </select>
                                    </div>
                                </div>

                

                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Feedback*</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <textarea class="form-control" rows="10" name="feedback" id="feedback" minlength="140" placeholder="Min 140, Max 500 characters" required></textarea>
                                        <p>
                                          <span>characters remaining: <span id="rem_feedback" title="500"></span></span>
                                        </p>
                                    </div>
                                </div>


                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-enquire'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <script type="text/javascript">
      $(document).ready(function() {
          $("#feedback").keyup(function () { 
            var cmax = $("#rem_" + $(this).attr("id")).attr("title"); 

            if ($(this).val().length >= cmax) {
                $(this).val($(this).val().substr(0, cmax));
            }

            $("#rem_" + $(this).attr("id")).text(cmax - $(this).val().length);
            });
      });       
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>