<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Ebrochures
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Title *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="title" class="form-control" placeholder="Title" required="">
          </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Author Name *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="author" class="form-control" placeholder="Author Name" required="">
          </div>
          </div>



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Display Image *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="file" name="brochure_image[]" class="form-control" required="">
          </div>
        </div>



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Uploaded File *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="file" name="brochure_file[]" class="form-control" required="">
          </div>
        </div>



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Order *</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="number" min="1" name="order_by" required="" class="form-control" placeholder="Brochure Order" >
          </div>
        </div>




                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-ebrochure'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>