<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Banner<br>
          <small>banner basic information</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

	         <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Banner Slogan(English)</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" name="slogan" class="form-control" placeholder="Banner Slogan" >
					</div>
				</div>


        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner Slogan(Spanish)</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="slogan_spanish" class="form-control" placeholder="Banner Spanish Slogan" >
          </div>
        </div>


        <!-- <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner Slogan(French)</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" name="slogan_french" class="form-control" placeholder="Banner French Slogan" >
          </div>
        </div> -->



	      <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Banner Image</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="file" name="banner_image[]" id="banner_image" class="form-control" accept="image/*" >
					</div>
				</div>


        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner Video</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="file" name="banner_video[]" id="banner_video" class="form-control" accept="video/mp4,video/x-m4v,video/*" >
          </div>
        </div>



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Banner Order</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="number" min="1" name="order" class="form-control" placeholder="Banner Order" >
          </div>
        </div>


        



        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Position</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <select class="select2_single form-control"  name="position" >
              <option value="header">Header Slider</option>
              <option value="footer">Footer Slider</option>
            </select>
          </div>
        </div>


        <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="select2_single form-control"  name="status" >
							<option value="Active">Active</option>
                                <option value="Deactive">Deactive</option>
						</select>
					</div>
				</div>

                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-banners'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
						</div>


                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $('#banner_image').click(function(event) {
      var result = confirm('You can upload BANNER IMAGE or BANNER VIDEO once at a time. Can not use both.');

      if(result) {
        $('#banner_video').prop('disabled', true)
        $('#banner_image').prop('required', 'required')
      } else {
        return false;
      }
    });

    $('#banner_video').click(function(event) {
      var result = confirm('You can upload BANNER IMAGE or BANNER VIDEO once at a time. Can not use both.');

      if(result) {
        $('#banner_image').prop('disabled', true)
        $('#banner_video').prop('required', 'required')
      } else {
        return false;
      }

    });

  </script>

<?php $this->load->view("admin/common/footer.php"); ?>