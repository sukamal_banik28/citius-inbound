<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Banners
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-banner'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>ID.No</th>
                  <th>Banner Image</th>
                  <th>Order</th>
                  <th>Position</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php if(!empty($banners)) { 
                      foreach ($banners as $key => $value) { ?>
                        

                  <tr>
                    <td><?php echo $value['id']; ?></td>

                    <td>

                      <?php if($value['flag'] == 'banner_image') { ?>
                      <img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" style="width:100px; height: 80px;">
                      <?php } else { ?>

                        <iframe width="100px" height="80px"
                        src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>">
                        </iframe>

                      <?php } ?>

                    </td>

                    <td><?php echo $value['order_no']; ?></td>

                    <td><?php echo $value['position']; ?></td>

                    <td><span class="label <?php echo $value['status'] == 'Active' ? 'text-success' : 'deactive'; ?>"><?php echo $value['status']; ?></span></td>


                    <td style="width: 200px;"><a href="<?php echo base_url().'inbound-admin/manage-banners/banner-id/'.$value['id'].'/update'; ?>"><span class="label edit-text"><i class="fa fa-pencil" aria-hidden="true"></i>
                     Edit</span></a>
                   </td> 

                  </tr>


                  <?php } 
                    }
                  ?>
                

                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
  	$(document).ready(function() {
    	$('#example1').DataTable();
	});
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>