<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>
	
	
	<!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
        Dashboard
      </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue1"><img src="<?php echo base_url(); ?>assets/dist/img/icon-package.png"></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Packages</span>
                                <span class="info-box-number"><?php if(!empty($packages)) { echo count($packages); } else { echo 'No Data'; } ?></span>
                                <span class="info-box-link"><a href="<?php echo base_url().'inbound-admin/manage-packages'; ?>">All Packages <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->

                   <!--  <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-orange"><img src="dist/img/order-icon.png"></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Orders</span>
                                <span class="info-box-number">0</span>
                                <span class="info-box-link"><a href="booking.html">All Order <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</span>
                            </div>
                            
                        </div>
                        
                    </div> -->
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>

                    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><img src="dist/img/icon-amount.png"></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Paid Amount</span>
                                <span class="info-box-number">Rs 0</span>
                                <span class="info-box-link"><a href="booking.html">All Order <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</span>
                            </div>
                           
                        </div>
                       
                    </div> -->
                    <!-- /.col -->


                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue1"><img src="<?php echo base_url(); ?>assets/dist/img/icon-comments.png"></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Reviews</span>
                                <span class="info-box-number"><?php if(!empty($reviews)) { echo count($reviews); } else { echo 'No Data'; } ?></span>
                                <span class="info-box-link"><a href="<?php echo base_url().'inbound-admin/manage-reviews'; ?>">All Reviews <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</span>
                            </div>                         
                        </div>
                    </div>


                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-orange"><img src="<?php echo base_url(); ?>assets/dist/img/icon-customer.png"></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Customer</span>
                                <span class="info-box-number"><?php if(!empty($customers)) { echo count($customers); } else { echo 'No Data'; } ?></span>
                                <span class="info-box-link"><a href="<?php echo base_url().'inbound-admin/manage-users'; ?>">All Customers <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</span>
                            </div>                         
                        </div>
                    </div>


                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><img src="<?php echo base_url(); ?>assets/dist/img/icon-review.png"></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Enquiries</span>
                                <span class="info-box-number"><?php if(!empty($enquiries)) { echo count($enquiries); } else { echo 'No Data'; } ?></span>
                                <span class="info-box-link"><a href="<?php echo base_url().'inbound-admin/manage-enquire'; ?>">All Enquiries <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</span>
                            </div>                           
                        </div>                        
                    </div>

                    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue1"><img src="dist/img/icon-customer.png"></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Customers Online</span>
                                <span class="info-box-number">36</span>
                                <span class="info-box-link"><a href="customer_online.html">All Customers Online <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
							</span>
                            </div>
                        </div>
                    </div> -->


                </div>
                <!-- /.row -->


                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-primary" style="margin-top: 15px;">
                            <div class="box-header">
                                <i class="ion ion-clipboard"></i>
                                <h3 class="box-title">To Do List</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                                <div style="height: 285px; overflow: auto;">

                                    <ul class="todo-list">

                                    	<?php if(!empty($enquiries)) { 
                                    		foreach ($enquiries as $key => $value) {
                                    		?>

										<li>


										<span class="text"><b>Enquiry Ticket# <?php echo $value['ticket_no']; ?></b> &nbsp; &nbsp; <a href="<?php echo base_url().'inbound-admin/manage-enquire/enquire-id/'.$value['id'].'/update'; ?>">Click here to view details.</a>
										</span>
										<small class="text-muted pull-right"><i class="fa fa-clock-o"></i><?php echo date('d-M-Y', strtotime($value['created_at'])); ?></small>
										</li>

                                    	<?php } } ?>

                                        
                                    </ul>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-primary" style="margin-top: 15px;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Recent Activity</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <ul class="products-list product-list-in-box">
                                    <li class="item"><a href="#">Ajay  Singh</a> Logged in.
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 28/08/2018 03:26:05 pm</small></li>
                                    <!-- <li class="item"><a href="#">Ajay  Singh</a> Logged in.
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 28/08/2018 03:26:05 pm</small></li>
                                    <li class="item"><a href="#">Ajay  Singh</a> Logged in.
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 28/08/2018 03:26:05 pm</small></li>
                                    <li class="item"><a href="#">Ajay  Singh</a> Logged in.
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 28/08/2018 03:26:05 pm</small></li>
                                    <li class="item"><a href="#">Ajay  Singh</a> Logged in.
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 28/08/2018 03:26:05 pm</small></li>
                                    <li class="item"><a href="#">Ajay  Singh</a> Logged in.
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 28/08/2018 03:26:05 pm</small></li>
                                    <li class="item"><a href="#">Ajay  Singh</a> Logged in.
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 28/08/2018 03:26:05 pm</small></li> -->
                                </ul>
                            </div>
                            <!-- /.box-body -->
                            <!-- /.box-footer -->
                        </div>
                    </div>
                </div>

                <!-- /.row -->

                <!-- Main row -->

                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

			
<?php $this->load->view("admin/common/footer.php"); ?>