<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blogs
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
             <div style="width: 100%; float: left; margin-bottom: 20px;"><a class="btn bg-orange pull-right" href="<?php echo base_url().'inbound-admin/add-blogs'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Blog Title</th>
                  <th>Blog Author</th>
                  <th>Description</th>
                  <th>Publish Date</th>
                  <th>Blog Image</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php if(!empty($blogs)) { 
                      foreach ($blogs as $key => $value) {
                        
                    ?>
                <tr>

                  <td><?php echo $value['title_eng']; ?></td>
                  <td><?php echo $value['author']; ?></td>
                  <td><?php echo $value['desc_eng']; ?></td>
                  <td><?php echo date('d-M-Y', strtotime($value['created_at'])); ?></td>

                  <td><img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" width="100" height="100"></td>

                  <td><span class="label text-success"> <?php echo ucfirst($value['status']); ?></span></td>

                  <td style="width: 200px;">
                    <a href="<?php echo base_url().'inbound-admin/manage-blogs/blogs-id/'.$value['id'].'/update?lang=eng'; ?>"><span class="label edit-text"><i class="fa fa-pencil" aria-hidden="true"></i>
                    Edit</span></a>

                    <select class="form-control lang" data-blogid="<?php echo $value['id']; ?>" style="width: 100px; border: solid 1px #ccc; padding: 3px;">
                          <option value="eng" selected>English</option>
                          <option value="spanish">Spanish</option>
                          <option value="french">French</option>
                      </select>

                  </td>

                </tr>

                  <?php } } ?>

                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <script type="text/javascript">
  	$(document).ready(function() {
    	$('#example1').DataTable({
        "order": [[ 3, "desc" ]]
      });

      $('.lang').change(function(event) {
        var lang = $(this).val();
        var blogid = $(this).data('blogid');
        
        var updateUrl = $(this).prev();
        var newUpdateUrl = "<?php echo base_url().'inbound-admin/manage-blogs/blogs-id/'; ?>"+blogid+'/update?lang='+lang; 
        updateUrl.attr("href", newUpdateUrl);
      });


    });
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>