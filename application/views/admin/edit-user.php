<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit User<br>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post">

             <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo $user['name']; ?>" required="">
              </div>
            </div>

                <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Address <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="email" name="email" id="email" class="form-control" placeholder="Email Address" value="<?php echo isset($user['email']) ? $user['email'] : ''; ?>">
              </div>
            </div>

               

                <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact No. <span class="required">*</span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="phone" id="mobile" class="form-control" placeholder="Contact No." value="<?php echo isset($user['phone']) ? $user['phone'] : ''; ?>" required="">
              </div>
            </div>


              <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">User Type</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" tabindex="-1" name="role_id" id="user_type">
                  <option value="1">Administrator</option>
                  <option value="2" selected="">Customer</option>
                </select>
              </div>
            </div>

                <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">User Status</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="select2_single form-control" tabindex="-1" name="status" id="user_status">
                  <option value="1" <?php echo $user['is_active'] == 1 ? 'selected' : ''; ?> >Active</option>
                  <option value="0" <?php echo $user['is_active'] == 0 ? 'selected' : ''; ?> >InActive</option>
                </select>
              </div>
            </div>
                <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href="<?php echo base_url().'inbound-admin/manage-users'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
                <button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
              </div>
            </div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/common/footer.php"); ?>