<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Reviews/Comments
      </h1>

      

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding-top: 10px;">                         
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped" width="100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>UserName</th>
                  <th>UserType</th>                  
                  <th>Object Name</th>
                  <th>Contents</th>
                  <th>Status</th>
                  <th>Date Added</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                  <?php 
                  if(!empty($reviews)) {
                  foreach ($reviews as $key => $value) { ?>
                      <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['name']; ?></td>
                        <td><?php echo !empty($value['user']) ? 'Registered' : 'Guest'; ?></td>
                        <td><?php echo $value['package']['title_eng'].'('.$value['reviewable_type'].')'; ?></td>
                        <td><?php echo substr($value['review'], 0, 20); ?></td>
                        <td><span class="label <?php echo $value['status'] == 'published' ? 'text-success' : 'deactive'; ?>"><?php echo $value['status']; ?></span></td>
                        <td><?php echo date('d-M-Y h:i:sa', strtotime($value['created_at'])); ?></td> 
                        
                        <td>

                         <a href="<?php echo base_url().'inbound-admin/manage-reviews/review-id/'.$value['id'].'/update'; ?>">
                          <span class="label edit-text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update Review Status</span>       
                        </a>

                      </td> 
                      </tr>
                  <?php } } ?>
                
                </tbody>
              </table>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
  $(document).ready(function() {
      $('#example1').DataTable({
        "order": [0, 'desc']
      });
  });
</script>

<?php $this->load->view("admin/common/footer.php"); ?>
