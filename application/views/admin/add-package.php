<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>


	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Package
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">

            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>


            	<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12 control-label">Package Id CRM <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="crm_id" id="Package_id_crm" class="form-control" placeholder="Package Id CRM" required value="<?php echo set_value('crm_id'); ?>">
								<span style="color: red; float: left;"><?php echo form_error('crm_id'); ?></span>
							</div>
						</div>



            	<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Channel Type <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" name="channels[]" required multiple>
									<?php if(!empty($channels)) { 
										foreach ($channels as $key => $value) {
										?>
									<option value="<?php echo $value['id'] ?>">Citius-<?php echo ucfirst($value['name']); ?></option>
									<?php } }?>
								</select>								
							</div>
						</div>


            	


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Title <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="title" id="Package_title" class="form-control" placeholder="Package Title" required value="<?php echo set_value('title'); ?>">
								<span style="color: red; float: left;"><?php echo form_error('title'); ?></span>
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Short Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="short_desc" id="Package_short_desc" class="form-control" rows="3" placeholder="Package Short Description"></textarea>
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Long Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="long_description" name="long_description" rows="10" cols="80">
                                            Package Long Description
                    </textarea>
            </div>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Category <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">

                                		<select name="categories[]" id="category" class="form-control" multiple required >
                                			<?php foreach ($categories as $key => $value) { ?>
                                				<option value="<?php echo $value['id']; ?>"><?php echo $value['name_eng']; ?></option>
                                			<?php } ?>
                                		</select>
								
							</div>
						</div>
						
                <div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Sub Category</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<select name="sub_categories[]" id="sub_category" class="form-control" multiple></select>
									</div>
							</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Seat <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="seat" id="seat" pattern="[0-9]" title="Please insert numeric value" class="form-control" placeholder="seat" required >
								
							</div>
						</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Total number of Days<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="days" id="days" class="form-control" placeholder="Total number of Days" required >
								
							</div>
						</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Total number of Nights<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="nights" id="nights" class="form-control" placeholder="Total number of Nights" required >
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Inventory <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="inventory" id="inventory" class="form-control" placeholder="Inventory" required>
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">City of Departure <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="city_depart" class="form-control" placeholder="City of Departure" required>
								
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">City of Arrival <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="city_arrival" id="myInput" class="form-control" placeholder="City of Arrival" required>
								<div id="city1-suggesstion-box"></div>
						
							</div>
						</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Validity Date From <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control" id="Validity_date_from" name="validity_date_from" type="text">
                </div>
								
							</div>
						</div>
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Validity Date To <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control" id="Validity_date_to" name="validity_date_to" type="text">
                </div>
								
							</div>
						</div>
                
                
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Cancellation Policy</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="policy" name="cpolicy" rows="10" cols="80">
                                            Package Cancellation Policy
                    </textarea>
            </div>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Deletion Flag <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" data-required="1" name="dflag" id="dflag">
											<option value="No">No</option>
											<option value="Yes">Yes</option>
											
									
                                </select>
							
							</div>
						</div>




				<!-- This feature enables only when product type is in Hotel -->
				<!-- <div class="form-group" >
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Prices <span class="required">*</span></label>
					<div class="col-md-9 col-sm-9 col-xs-12"> -->

						<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#currencyModal">Click here to Add Prices</button> -->

							<!-- <select class="form-control select2me" name="product_addons" id="product_addons" multiple > -->
							
							<?php //if($currencies) { 
									//foreach ($currencies as $key => $value) {
								?>

							<!-- <label>Rupees is in <?php //echo $value['cur_name']; ?></label> -->
							<!-- <input type="number" min="0" name="price[<?php //echo $value['id']; ?>]" class="form-control" placeholder="<?php //echo $value['cur_name']; ?>"> -->

							<!-- <option value="<?php //$value['id'] ?>"><?php //echo $value['type_name_eng'] ?></option> -->

							<?php //} } ?>
							
						<!-- </select> -->

					<!-- </div>
				</div> -->

						

                <!-- <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Regular Price <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="rprice" id="rprice" placeholder="Regular Price" class="form-control" required>
					
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Sales Price </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="sprice" id="sprice" placeholder="Sales Price" class="form-control">
					
							</div>
						</div> -->


                <!-- <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">GST in % <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="number" name="gst" id="gst" data-required="1" placeholder="GST" class="form-control" required>
							
							</div>
						</div> -->


                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Citius Touch ( Note. Please select Fixed if the package is in Fixed Departures Category )<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" data-required="1" name="citius_touch" id="citius_touch" required="">
									<option value="Customised">Customised</option>									
									<option value="Fixed">Fixed</option>
                                </select>
							
							</div>
						</div>




				<div class="form-group" id="addon_type">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Variation prices according to Package Category </label>
					<div class="col-md-9 col-sm-9 col-xs-12 variation">

							<b>Please select a package flag first</b>

					</div>
				</div>



                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Image</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="package_image[]" class="form-control" accept="image/*" >
							
							</div>
						</div>


				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Banner</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="package_banner[]" class="form-control" accept="image/*" >
							
							</div>
						</div>
						


				<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Package Gallery</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="file" name="package_gallery[]" class="form-control" accept="image/*" multiple>
							
							</div>
						</div>



                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Add Tags</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input name='tags' data-role="tagsinput" class='form-control' placeholder='write some tags' autofocus>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><span data-toggle="tooltip" title="" data-original-title="Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.">SEO URL</span><span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="seo_url" id="seo_url" value="<?php echo set_value('seo_url'); ?>" placeholder="Seo Url" class="form-control" required>
								<span style="color: red; float: left;"><?php echo form_error('seo_url'); ?></span>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Key </label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								
							<textarea name="meta_key" id="meta_key" class="form-control" rows="3" placeholder="Meta Description"><?php echo set_value('meta_key'); ?></textarea>
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="meta_desc" id="meta_desc" class="form-control" rows="3" placeholder="Meta Description"><?php echo set_value('meta_desc'); ?></textarea>
							</div>
						</div>

                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-packages'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">

  	$(document).ready(function() {
  		
  	
	    CKEDITOR.replace('long_description', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

	    CKEDITOR.replace('policy', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });

	    $('#Validity_date_from').daterangepicker({
	    	singleDatePicker: true,
	    	showDropdowns: true,
	        locale: {
	                format: 'YYYY-MM-DD',
	            }, 
	    });

	    $('#Validity_date_to').daterangepicker({
	    	singleDatePicker: true,
	    	showDropdowns: true,
	        locale: {
	                format: 'YYYY-MM-DD',
	            },
	    });


	    $('#category').change(function(event) {
	    	var category_ids = $(this).val();

	    	console.log(category_ids)

	    	$.ajax({
	    		url: '<?php echo base_url().'CategoryController/getSubCategoriesByAjax' ?>',
	    		type: 'POST',
	    		data: {
	    			category_ids : category_ids 
	    		},
	    	})
	    	.done(function(response) {
	    		$('#sub_category').html(response);
	    	})
	    	.fail(function() {
	    		console.log("error");
	    	})
	    	.always(function() {
	    	});
	    	
	    });



    }); // ready function end

</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<script type="text/javascript">
	
	$('#citius_touch').change(function(event) {
		var value = $(this).val();
		console.log(value)

		$.ajax({
			url: '<?php echo base_url().'PackageController/getPackageVariations' ?>',
			type: 'POST',
			data: { flag: value },
		})
		.done(function(response) {
			$('.variation').html(response);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
		});
		
	});

</script>

<?php $this->load->view("admin/common/footer.php"); ?>