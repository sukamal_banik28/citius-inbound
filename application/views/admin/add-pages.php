<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add New Page<br>
          <small>content basic information</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">

            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data'>

             <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Page Title<span class="required"> * </span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="title" id="cms_title"  class="form-control" placeholder="Page Title" required="" value="<?php echo set_value('title'); ?>">
							</div>
						</div>
                
                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"> Short Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="short_desc" name="short_desc" rows="10" cols="80"><?php echo set_value('short_desc'); ?></textarea>
            </div>
								
							</div>
						</div>

                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"> Long Description</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
            <!-- /.box-header -->
            <div class="">
                    <textarea id="long_desc" name="long_desc" rows="10" cols="80"><?php echo set_value('long_desc'); ?></textarea>
            </div>
							</div>
						</div>


            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12"><span data-toggle="tooltip" title="" data-original-title="Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.">Seo Url<span class="required"> * </span></label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="seo_url"  class="form-control" placeholder="Seo Url" required="" value="<?php echo set_value('seo_url'); ?>">
                <span style="color: red; float: left;"><?php echo form_error('seo_url'); ?></span>
              </div>
            </div>



                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Key</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="meta_key" name="meta_key" id="meta_key" value="" class="form-control" placeholder="Meta Key" value="<?php echo set_value('meta_key'); ?>">
								
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Meta desc</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="meta_desc" name="meta_desc" id="meta_desc" value="" class="form-control" placeholder="Meta Description" value="<?php echo set_value('meta_desc'); ?>">
								
							</div>
						</div>


                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="select2_single form-control" tabindex="-1" name="status" id="cms_status">
									<option value="Active">Active</option>
                                    <option value="Deactive">Deactive</option>
								</select>
							</div>
						</div>


                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

								<a href="<?php echo base_url().'inbound-admin/manage-pages'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
                <button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>

							</div>
						</div>
                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function() {
      
      CKEDITOR.replace('short_desc', {
          filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
      });

      CKEDITOR.replace('long_desc', {
          filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
      });

    });
  </script>

<?php $this->load->view("admin/common/footer.php"); ?>