<?php $this->load->view("admin/common/header.php"); ?>
<?php $this->load->view("admin/common/sidebar.php"); ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 345px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Review Status
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="padding:  30px; padding-left: 0px;">                         
            <div class="box-body">
            <form class="form-horizontal" method="post" action="" enctype='multipart/form-data' >



             	<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Review Contents</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea class="form-control" rows="10" readonly="readonly" style="resize: none;"><?php echo $review['review']; ?></textarea>
							</div>
						</div>



                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control select2me" data-required="1" name="status">
									<option value="published" <?php echo $review['status'] == 'published' ? 'selected' : ''; ?> >Published</option>
									<option value="unpublished" <?php echo $review['status'] == 'unpublished' ? 'selected' : ''; ?>>Unpublished</option>									
                                </select>							
							</div>
						</div>


                <div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<a href="<?php echo base_url().'inbound-admin/manage-reviews'; ?>"><button type="button" class="btn btn-primary bg-orange">Cancel</button></a>
								<button type="submit" name="submit" value="submit" class="btn btn-primary bg-green">Submit</button>
							</div>
						</div>

                </form>
            </div>
              
              
            <!-- /.box-body -->
           </div>
            
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
  	
  	$(document).ready(function() {
  		CKEDITOR.replace('description', {
	        filebrowserUploadUrl: "<?php echo base_url(); ?>ckeditor/upload"
	    });
  	});

  </script>


	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
	<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        
<?php $this->load->view("admin/common/footer.php"); ?>