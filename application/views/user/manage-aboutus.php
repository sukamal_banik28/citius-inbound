<?php $this->load->view("user/common/header.php"); ?>

	<main>
		
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>About Citius</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		
		<?php echo $lang == 'eng' ? $content['description'] : $content['description_spanish']; ?>

		
	</main>
	<!--/main-->

<?php $this->load->view("user/common/footer.php"); ?>