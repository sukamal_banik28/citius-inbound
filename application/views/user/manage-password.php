<?php $this->load->view("user/common/header.php"); ?>

	<main>
		
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Edit Profile</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_80_55">
			<div class="row profile">
				
				<?php $this->load->view('user/common/left-panel-profile'); ?>

				<div class="col-md-9">
					<div class="profile-content">
						<form method="post" action=""  autocomplete="off" enctype='multipart/form-data'>
							
							
							<div class="row">
								<div class="col-md-12 mt-2"><h6>Change Password</h6></div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Old Password</label>
										<input class="form-control" type="password" name="old_password">
										<span style="color: red; float: left;"><?php echo form_error('old_password'); ?></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>New Password</label>
										<input class="form-control" type="password" name="password">
										<span style="color: red; float: left;"><?php echo form_error('password'); ?></span>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Confirm Password</label>
										<input class="form-control" type="password" name="confirm_password">
										<span style="color: red; float: left;"><?php echo form_error('confirm_password'); ?></span>
									</div>
								</div>
							</div>
							<!-- /row -->

							<?php if(isset($errorMsg)) {
										echo $errorMsg;
									}
							?>
							
							<p class="add_top_30"><input type="submit" value="Change Password" name="submit" class="btn_1"></p>
						</form>
					</div>
				</div>
			</div>
			<!--/row-->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->

<?php $this->load->view("user/common/footer.php"); ?>