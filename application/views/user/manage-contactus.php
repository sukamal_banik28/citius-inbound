<?php $this->load->view("user/common/header.php"); ?>


	<main>
		<section class="hero_in contacts">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span><?php echo $lang == 'eng' ? 'Contact Us' : $this->lang->line('spanish_Contact_Us'); ?></h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="contact_info">
			<div class="container">
				<ul class="clearfix">
					<li>
						<i class="pe-7s-map-marker"></i>
						<h4><?php echo $lang == 'eng' ? 'Address' : $this->lang->line('spanish_Address'); ?></h4>
						<span><?php echo $company['address_'.$lang]; ?></span>
					</li>
					<li>
						<i class="pe-7s-mail-open-file"></i>
						<h4><?php echo $lang == 'eng' ? 'Email address' : $this->lang->line('spanish_Email_address'); ?></h4>
						<span><?php echo $company['email']; ?><br><!-- <small>Monday to Friday 9am - 7pm</small> --></span>

					</li>
					<li>
						<i class="pe-7s-phone"></i>
						<h4><?php echo $lang == 'eng' ? 'Contacts info' : $this->lang->line('spanish_Contacts_info'); ?></h4>
						<span><?php echo $company['telephone']; ?><br><!-- <small>Monday to Friday 9am - 7pm</small> --></span>
					</li>
				</ul>
			</div>
		</div>
		<!--/contact_info-->

		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="row justify-content-between">
					<div class="col-lg-5">
						
						<div id="gmap"></div>

						<style>
							/* Set the size of the div element that contains the map */
							#gmap {
								height: 500px;  /* The height is 500 pixels */
								width: 100%;  /* The width is the width of the web page */
							}
						</style>


						<!-- /map -->
					</div>
					<div class="col-lg-6">
						<h4><?php echo $lang == 'eng' ? 'Send a message' : $this->lang->line('spanish_Send_a_message'); ?></h4>
						<div id="message-contact"></div>
						<form method="post" action="" id="contactform" autocomplete="off">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label><?php echo $lang == 'eng' ? 'Name' : $this->lang->line('spanish_Name'); ?>*</label>
										<input class="form-control" type="text" name="name" value="<?php echo isset($user['name']) ? $user['name'] : ''; ?>" required >
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><?php echo $lang == 'eng' ? 'Email' : $this->lang->line('spanish_Email'); ?>*</label>
										<input class="form-control" type="email" id="email_contact" name="email" value="<?php echo isset($user['email']) ? $user['email'] : ''; ?>" required >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label><?php echo $lang == 'eng' ? 'Telephone' : $this->lang->line('spanish_Telephone'); ?>*</label>
										<input class="form-control" type="number" min="1" id="phone_contact" name="phone" value="<?php echo isset($user['phone']) ? $user['phone'] : ''; ?>" required>
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="form-group">
								<label><?php echo $lang == 'eng' ? 'Your Message(Min 140, Max 500 characters)' : $this->lang->line('spanish_Your_Message'); ?>*</label>
								<textarea class="form-control" id="message" name="message" style="height:150px;" minlength="140" required ></textarea>
								<p>
                                   <span><?php echo $lang == 'eng' ? 'characters remaining' : $this->lang->line('spanish_characters_remaining'); ?>: <span id="rem_message" title="500"></span></span>
                                </p>
							</div>
							<p class="add_top_30"><input type="submit" value="<?php echo $lang == 'eng' ? 'Enquire Now' : $this->lang->line('spanish_enquire'); ?>" class="btn_1 rounded" name="submit"></p>
						</form>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->

	<script type="text/javascript">
      $(document).ready(function() {
          $("#message").keyup(function () {
            var cmax = $("#rem_" + $(this).attr("id")).attr("title");

            if ($(this).val().length >= cmax) {
                $(this).val($(this).val().substr(0, cmax));
            }

            $("#rem_" + $(this).attr("id")).text(cmax - $(this).val().length);
            });
      });       
  	</script>

	<script type="text/javascript">

	
	  	function initMap() { 
	  	  var name = 'Citius Holidays';

	      var latitude = 22.519180; // YOUR LATITUDE VALUE
          var longitude = 88.364190; // YOUR LONGITUDE VALUE
		  // The location of myLatLng
		  var myLatLng = {lat: latitude, lng: longitude};
		  // The map, centered at myLatLng
		  var map = new google.maps.Map(
		      document.getElementById('gmap'), {zoom: 12, center: myLatLng});
		  // The marker, positioned at myLatLng
		  var marker = new google.maps.Marker({position: myLatLng, map: map, title: name});
		}
	

	</script>

	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA53RbrL34Vo9SA8EsuZHMsUKyXbXeebAg&callback=initMap">
    </script>

<?php $this->load->view("user/common/footer.php"); ?>