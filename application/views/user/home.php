<?php $this->load->view("user/common/header.php"); ?>

<style type="text/css">
	@media only screen and (min-width: 1600px)	{
		.container {
		    max-width: 1500px;
		}
	}
</style>


	<main>



		<!-- Slider -->
		<!-- <div id="full-slider-wrapper"> -->
			<!-- <div id="layerslider" class="home-slider" style="height: 660px; width: 100%;"> -->
				<!-- first slide -->

				<?php // $banners = getBanners('header'); 
				//if(!empty($banners)) { 
					//foreach ($banners as $key => $value) {
					?>

				<!-- <div class="ls-slide" data-ls="slidedelay: 3000;">
					<img src="<?php //echo base_url().$value['file_path'].$value['file_name']; ?>" class="ls-bg" style="width: 100%;margin-top: 0px;" alt="<?php //echo $value['original_name']; ?>">
					<h3 class="ls-l slide_typo" style="top: 40%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;"> <strong><?php //echo $value['slogan_'.$lang]; ?></strong></h3>
				</div> -->
				<?php //} } ?>

				
				<!-- Slider Form -->
				<!-- <div class="slider-form">
					<form method="post" action="">
						<div id="custom-search-input">
							<div class="input-group">
								<input type="text" name="search_string" class="search-query" 
								placeholder="<?php //echo $lang == 'eng' ? 'Ex. North India, Wildlife...' : $this->lang->line('spanish_placeholder'); ?>">
								<input type="submit" name="search" class="btn_search" 
								value="<?php //echo $lang == 'eng' ? 'Search' : $this->lang->line('spanish_Search'); ?>">
							</div>
						</div>
					</form>
				</div> --><!--/Slider Form -->
			<!-- </div>
		</div> -->
		<!-- End layerslider -->




		<!-- Slider -->
		<div class="slider-wrapper">
			<div class="main-slider">

				<?php 
					$banners = getBanners('header'); 
						if(!empty($banners)) { 
							foreach ($banners as $key => $value) {
								if($value['flag'] == 'banner_image') {
				?>
				

			  <div class="item image">
				<figure>
				  <div class="slide-image slide-media" style="background-image:url('<?php echo base_url().$value['file_path'].$value['file_name']; ?>');">
					<img data-lazy="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" class="image-entity" alt="<?php echo $value['original_name']; ?>" />
				  </div>
				  <figcaption class="caption"><?php echo $value['slogan_'.$lang]; ?></figcaption>
				</figure>
			  </div>

			<?php } else { ?>

			  <div class="item video">
				<video class="slide-video slide-media" loop muted autoplay="autoplay" preload="metadata" poster="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSXZCakVGZWhOV00" plays-inline controls>
					<source src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" type="video/mp4" />
				</video>
				<p class="caption"><?php echo $value['slogan_'.$lang]; ?></p>
			  </div>

			<?php } } } ?>


			</div>
			
			<!-- Slider Form -->
			<div class="slider-form">
				<form method="post" action="">
					<div id="custom-search-input">
						<div class="input-group">
							<input type="text" name="search_string" class="search-query" 
								placeholder="<?php echo $lang == 'eng' ? 'Ex. North India, Wildlife...' : $this->lang->line('spanish_placeholder'); ?>">
							<input type="submit" name="search" class="btn_search" 
								value="<?php echo $lang == 'eng' ? 'Search' : $this->lang->line('spanish_Search'); ?>">
						</div>
					</div>
				</form>
			</div><!--/Slider Form -->
		</div>
		<!--/ Slider -->





		<?php 
			$i = 0;
			foreach ($categories as $key => $value) { 
				$i++;
			?>

			<!-- Cultural Heritage Slider -->
		<div class="container"> <!-- margin_80_0 -->
			<div class="main_title_2">
				<span><em></em></span>
				<h2><?php echo $value['name_'.$lang]; ?></h2>
				<p><?php echo $value['short_desc_'.$lang]; ?></p>
			</div>

			<?php //$div_id = ($i == 1) ? 'culturalHeritageSlider' : ($i == 2 ? 'natureWildlifeSlider' : ($i == 3 ? 'colorsOfIndiaSlider' : 'fixedDepartureSlider') ); 

				$div_id = ($i == 1) ? 'culturalHeritageSlider' : ($i == 2 ? 'natureWildlifeSlider' : ($i == 3 ? 'colorsOfIndiaSlider' : ($i == 4 ? 'fixedDepartureSlider' : 'neighbouringSlider') ) );

			?>

			<div id="<?php echo $div_id; ?>" class="owl-carousel owl-theme">

				<?php 
				if(!empty($value['packages'])) {
					foreach ($value['packages'] as $pkey => $pvalue) { 
						if( !empty($pvalue) ) { ?>
				<div class="item">
					<div class="box_grid">

						<?php if($pvalue['is_new'] == 1) { ?>
						<!-- Ribbon -->
						<div class="ribbon-wrapper">
							<div class="ribbon">NEW</div>
						  </div> <!-- /ribbon -->
						<?php } ?>



						<figure>
							<a href="<?php echo base_url().'packages/'.$pvalue['seo_url'].'?lang='.$lang; ?>">
								<img src="<?php echo base_url().$pvalue['file_path'].$pvalue['file_name']; ?>" class="img-fluid" alt="" width="800" height="533">
								<div class="read_more">
									<span><?php echo $pvalue['title_'.$lang]; ?></span>
								</div>
							</a>
						</figure>
						<div class="wrapper">
							<h3><a href="<?php echo base_url().'packages/'.$pvalue['seo_url'].'?lang='.$lang; ?>"><?php echo $pvalue['title_'.$lang]; ?></a></h3>
							<p><?php echo substr( trim(strip_tags($pvalue['short_desc_'.$lang])) , 0, 100)
								?> <a href="<?php echo base_url().'packages/'.$pvalue['seo_url'].'?lang='.$lang; ?>" >Read more</a>
							</p>
						</div>
						<ul class="mobile-meta">
							<li class="first"><i class="icon_clock_alt"></i> <?php echo $pvalue['nights']; ?>N <?php echo $pvalue['days']; ?>D </li>

							<?php 

							// ID 2 measn USD and 1 means EUR
							$currency_id = $lang == 'eng' ? 2 : 1;
							$cur_symbol = $lang == 'eng' ? '$' : '€';

							$price_details = getPackageMinValueForCurrency($currency_id, $pvalue['id'], 'package' );

							// echo '<pre>';
							// print_r($price_details);

							if(!empty($price_details)) {
								$travel_pax = getTravelPaxDefByID($price_details['travel_pax_id']);
							} else {
								$travel_pax = '';
							}

							?>

							<li class="second"><div class="score"><span>Package<em> Price for <?php echo is_array($travel_pax) ? $travel_pax['pax_range'].'pax' : $travel_pax; ?> </em></span><strong><?php //echo 'Rs. '.$pvalue['sales_price']; ?>
								<?php echo is_array($price_details) ? $cur_symbol.' '.$price_details['value'] : '<a style="color:#fff" href='.base_url('contact-us').'>Contact for prices</a>'; ?>
							</strong></div></li>
						</ul>
					</div>
				</div>
							<?php } // end if !empty package values
						} // end loop
					} // end if !empty packages
				?>

			</div>
			<!-- /carousel -->
			<div class="container">
				<p class="text-center">
					<a href="<?php echo base_url().'manage-tour/'.$value['seo_url'].'?lang='.$lang; ?>" class="btn_1 rounded"><?php echo $lang == 'eng' ? 'View all tours' : $this->lang->line('spanish_View_all_tours'); ?></a>
				</p>
			</div>
			<!-- /container -->
			<!--<hr class="large">-->
		</div>
		<!-- /container -->

		<?php } ?>
		






		<!-- Our Unique Hideaways -->
		<div class="bg_color_1" style="padding-top: 15px;">
			<div class="container">
				<div class="main_title_2">
					<span><em></em></span>
					<h3><?php echo $lang == 'eng' ? 'Our Unique Hideaways' : $this->lang->line('spanish_Our_Unique_Hideaways'); ?></h3>
				</div>
				<div id="carousel-partners" class="owl-carousel owl-theme">

					<?php $hideaways = getHideaways('hideaway'); 
					if(!empty($hideaways)) {
						foreach ($hideaways as $key => $value) {
							
					?>
					<div class="item">
						<a target="_blank" href="<?php echo $value['web_link']; ?>" class="box_feat">
							<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" alt="<?php echo $value['title']; ?>">
						</a>
					</div>
					<?php } } ?>

				</div>
			</div>
		</div>
		<!-- Our Unique Hideaways -->






		<!--  VIdeo Slider -->
		<div class="bg_color_1">
			<div class="container margin_60_35">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>Here some videos...</h2>
				</div>
				<div class="grid">
					<div id="carousel-video-sliders">
					<ul class="magnific-gallery">

					<?php $hideaways = getHideaways('video'); 
						if(!empty($hideaways)) {
							foreach ($hideaways as $key => $value) {	
					?>

						<li class="item">
							<figure>
								<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" alt="">
								<figcaption>
									<div class="caption-content">
										<a href="<?php echo $value['web_link'].'?autoplay=1&loop=1&autopause=0'; ?>" class="video" title="<?php echo $value['title']; ?>">
										<i class="pe-7s-film"></i>
										<p><?php echo $value['title']; ?></p>
									</a>
									</div>
								</figcaption>
							</figure>
						</li>

						<?php } } ?>


					</ul>
					</div>
				</div>
				<!-- /grid -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->







		<!-- Place somewhere in the <body> of your page -->
		<div class="flexslider">
			<ul class="slides">

				<?php $banners = getBanners('footer'); 
				if(!empty($banners)) { 
					foreach ($banners as $key => $value) {
					?>
				<li>
					<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" />
				</li>

				<?php } } ?>

			</ul>
			<div class="sticky-cab clearfix">
				<div class="col-lg-5 col-md-6 float-right wow" data-wow-offset="250">
					<div class="block-reveal">
						<div class="block-vertical"></div>
						<div class="box_1">
							<h3>About Us</h3>

							<?php $content = getAboutUsPageContent(); 
								echo '<p>'.substr( trim(strip_tags($content['description'])) , 0, 150).'</p>' ;
							?>

							<a href="<?php echo base_url().'about-us?lang='.$lang; ?>" class="btn_1 rounded">Read more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- footer banner -->




		<div class="bg_color_1" style="padding-top: 15px;">
			<div class="container">
				<div class="main_title_2">
					<span><em></em></span>
					<h3><?php echo $lang == 'eng' ? 'Accreditations' : $this->lang->line('spanish_Accreditations'); ?></h3>
				</div>
				<div id="carousel-partners2" class="owl-carousel owl-theme">

					<?php $partners = getPartners(); 
					if(!empty($partners)) {
						foreach ($partners as $key => $value) {
							
					?>
					<div class="item">
						<a href="#" class="box_feat">
							<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" alt="">
						</a>
					</div>
					<?php } } ?>

				</div>
			</div>
		</div>


	</main>
	<!-- /main -->


	<!-- SPECIFIC SCRIPTS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="<?php echo base_url().'assets/front-end/'; ?>ivslider/js/init.js"></script>
	<!-- Hero slider Init -->
	
	<script>
		// HeaderVideo.init({
		// 	container: $('.header-video'),
		// 	header: $('.header-video--media'),
		// 	videoTrigger: $("#video-trigger"),
		// 	autoPlayVideo: true
		// });

		// Can also be used with $(document).ready()
		$(window).load(function() {
			$('.flexslider').flexslider({
				animation: "slide",
				controlNav: true,  
				directionNav: false
			});
		});

		

	</script>
	<!-- SPECIFIC SCRIPTS -->



	<!-- <script type="text/javascript">
		// $(document).ready(function() {


		// // 	HeaderVideo.init({
		// // 	container: $('.header-video'),
		// // 	header: $('.header-video--media'),
		// // 	videoTrigger: $("#video-trigger"),
		// // 	autoPlayVideo: true
		// // });

		// // Can also be used with $(document).ready()
		// 	// $(window).load(function() {
		// 	// 	$('.flexslider').flexslider({
		// 	// 		animation: "slide",
		// 	// 		controlNav: true,  
		// 	// 		directionNav: false
		// 	// 	});
		// 	// });


		// 	// 'use strict';
	 //  //       $('#layerslider').layerSlider({
	 //  //           autoStart: true,
	 //  //           navButtons: false,
	 //  //           navStartStop: false,
	 //  //           showCircleTimer: true,
	 //  //           responsive: true,
	 //  //           responsiveUnder: 1280,
	 //  //           layersContainer: 1200,
	 //  //           skinsPath: '<?php //echo base_url()."assets/front-end/"; ?>layerslider/skins/'
	 //  //               // Please make sure that you didn't forget to add a comma to the line endings
	 //  //               // except the last line!
	 //  //       });


		// }); // end document ready
		
	</script> -->

<?php $this->load->view("user/common/footer.php"); ?>