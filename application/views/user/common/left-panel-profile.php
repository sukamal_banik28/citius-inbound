<div class="col-md-3">
<div class="profile-sidebar">
	<!-- SIDEBAR USERPIC -->
	<div class="profile-userpic text-center">
		<img src="<?php echo base_url().$profile_photo['file_path'].$profile_photo['file_name']; ?>" class="img-responsive" alt="">
	</div>
	<!-- END SIDEBAR USERPIC -->
	<!-- SIDEBAR USER TITLE -->
	<div class="profile-usertitle">
		<div class="profile-usertitle-name">
			<?php echo $user['name']; ?>
		</div>
		<div class="profile-usertitle-job">
			<?php echo $user['email']; ?>
		</div>
	</div>
	<!-- END SIDEBAR USER TITLE -->
	<!-- SIDEBAR BUTTONS -->
	<div class="profile-userbuttons">
		<a href="<?php echo base_url().'user/edit-profile/id/'.$user['id'].'?lang='.$lang; ?>" class="btn btn-success btn-sm">Edit Profile</a>
		<a href="<?php echo base_url().'inbound-admin/logout'; ?>"><button type="button" class="btn btn-danger btn-sm">Log Out</button></a>
	</div>
	<!-- END SIDEBAR BUTTONS -->
	<!-- SIDEBAR MENU -->
	<div class="profile-usermenu">
		<ul class="nav">
			<li class=" <?php echo $pagetitle == 'Profile' ? 'active' : ''; ?> ">
				<a href="<?php echo base_url().'user/manage-profile?lang='.$lang; ?>">Overview </a>
			</li>
			<li class=" <?php echo $pagetitle == 'Update' ? 'active' : ''; ?> ">
				<a href="<?php echo base_url().'user/edit-profile/id/'.$user['id'].'?lang='.$lang; ?>">Edit Profile </a>
			</li>
			<li class=" <?php echo $pagetitle == 'Enquire' ? 'active' : ''; ?> ">
				<a href="<?php echo base_url().'user/manage-enquire?lang='.$lang; ?>">Track Enquiries </a>
			</li>
			<li class=" <?php echo $pagetitle == 'Change' ? 'active' : ''; ?> ">
				<a href="<?php echo base_url().'user/manage-password?lang='.$lang; ?>">Change Password </a>
			</li>
		</ul>
	</div>
	<!-- END MENU -->
</div>
</div>