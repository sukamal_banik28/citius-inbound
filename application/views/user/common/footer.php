
	<footer>

		<?php $company = getCompanyDetails(); ?>
		<div class="container margin_60_35">
			<div class="row">
				<div class="col-lg-5 col-md-12 p-r-5 white-font">
					<p><img src="<?php echo base_url().'assets/front-end/'; ?>img/citius-logo.png" data-retina="true" alt=""></p>

					<?php $content = getAboutUsPageContent(); 
						echo substr( trim(strip_tags($content['description'])) , 0, 150) ;
					?>
					<a href="<?php echo base_url().'about-us?lang='.$lang; ?>" >...Read more</a>

					<div class="follow_us">
						<ul>
							<li>Follow us</li>

							<?php 
							$append = " WHERE `is_active` = 1";
							$links = getSocialLinks($append); 

								if(!empty($links)) {
									foreach ($links as $key => $value) {

							?>
							<li><a href="<?=$value['link'];?>">
								<?=$value['fa_icon'];?>
								
							</a></li>

							<?php } } ?>

							<!-- <i class="ti-facebook"></i> -->
							<!-- <li><a href="#0"><i class="ti-twitter-alt"></i></a></li> -->
							<!-- <li><a href="#0"><i class="ti-google"></i></a></li> -->
							<!-- <li><a href="#0"><i class="ti-pinterest"></i></a></li> -->
							<!-- <li><a href="#0"><i class="ti-instagram"></i></a></li> -->
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 ml-lg-auto">
					<h5>Useful links</h5>
					<ul class="links" id="menu">
						<li><a href="<?php echo base_url().'about-us?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'About' : $this->lang->line('spanish_About'); ?></a></li>
						<li><a href="<?php echo base_url().'blogs?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'Blogs' : $this->lang->line('spanish_Blog'); ?></a></li>
						<li><a href="<?php echo base_url().'contact-us?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'Contact Us' : $this->lang->line('spanish_Contact_Us'); ?></a></li>
						<li><a href="<?php echo base_url().'inbound-admin/login?lang='.$lang; ?>">Sign In</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-6">
					<h5>Contact with Us</h5>
					<ul class="contacts" id="menu">
						<li><a href="tel://+919830007617"><i class="ti-mobile"></i> <?php echo $company['telephone']; ?></a></li>
						<li><a href="mailto:info@citius.com"><i class="ti-email"></i> <?php echo $company['email']; ?></a></li>
					</ul>

					<!-- <div id="newsletter">
					<h6>Newsletter</h6>
					<div id="message-newsletter"></div>
					<form method="post" action="#" name="newsletter_form" id="newsletter_form">
						<div class="form-group">
							<input type="email" name="email_newsletter" id="email_newsletter" class="form-control" placeholder="Your email">
							<input type="submit" value="Submit" id="submit-newsletter">
						</div>
					</form>
					</div> -->
					
				</div>
			</div>
			<!--/row-->
			<hr>
			<div class="row">
				<div class="col-lg-6">
					<ul id="footer-selector">
						<li><a href="#0">Link</a></li>
						<li><img src="<?php echo base_url().'assets/front-end/'; ?>img/cards_all.svg" alt=""></li>
					</ul>
				</div>
				<div class="col-lg-6">
					<ul id="additional_links">
						<li><a href="#0">Terms and conditions</a></li>
						<li><a href="#0">Privacy</a></li>
						<li><span>© <?php echo date('Y'); ?> Citius</span></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!--/footer-->
	</div>
	<!-- page -->
	
	<div id="toTop"></div><!-- Back to top button -->

	<!-- Brochure Download action for mobile -->
	<script>
		jQuery(document).ready(function( $ ){
			$(window).scroll(function() {    
				var scroll = $(window).scrollTop();
				if (scroll >= 100) {
					$("#brochurediv").addClass("mobile-brochurediv");
				}
				else {
				 $("#brochurediv").removeClass("mobile-brochurediv"); 
				}
			}); 
		});
	</script>
	
	<!-- COMMON SCRIPTS -->
    <script src="<?php echo base_url().'assets/front-end/'; ?>js/common_scripts.js"></script>
	<script src="<?php echo base_url().'assets/front-end/'; ?>js/main.js"></script>

	<script src="<?php echo base_url().'assets/front-end/'; ?>js/jquery.flexslider.js"></script>
	
	<!-- SPECIFIC SCRIPTS -->
	<script src="<?php echo base_url().'assets/front-end/'; ?>js/video_header.js"></script>

	<!-- SPECIFIC SCRIPTS -->
    <script src="<?php echo base_url().'assets/front-end/'; ?>layerslider/js/greensock.js"></script>
    <script src="<?php echo base_url().'assets/front-end/'; ?>layerslider/js/layerslider.transitions.js"></script>
    <script src="<?php echo base_url().'assets/front-end/'; ?>layerslider/js/layerslider.kreaturamedia.jquery.js"></script>

    <script type="text/javascript" src="https://shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
	
</body>

</html>