	<aside class="col-lg-4" id="sidebar">
		<div class="box_detail booking">
			<div class="price">
				<!--<span>$5,000 <small>person</small></span>-->
				<span style="font-size:25px;"><?php echo $lang == 'eng' ? 'Quick Enquiry' : $this->lang->line('spanish_Quick_Enquiry'); ?></span>
			</div>

			<h5 class="text-center"></h5>

			<div class="form-group">
				<input class="form-control name" type="text" placeholder="<?php echo $lang == 'eng' ? 'Name' : $this->lang->line('spanish_Name'); ?>" value="<?php echo isset($user['name']) ? $user['name'] : ''; ?>"  >
				<i class="icon_info"></i>
			</div>

			<div class="form-group">
				<input class="form-control phone" type="text" placeholder="<?php echo $lang == 'eng' ? 'Telephone' : $this->lang->line('spanish_Telephone'); ?>" value="<?php echo isset($user['phone']) ? $user['phone'] : ''; ?>">
				<i class="icon_info"></i>
			</div>

			<div class="form-group">
				<input class="form-control email" type="text" placeholder="<?php echo $lang == 'eng' ? 'Email' : $this->lang->line('spanish_Email'); ?>" value="<?php echo isset($user['email']) ? $user['email'] : ''; ?>">
				<i class="icon_info"></i>
			</div>

			<div class="form-group">
				<select class="form-control package">
					<?php if(!empty($package_list)) {
						foreach ($package_list as $key => $value) {
					?>
						<option value="<?php echo $value['id']; ?>"  <?php echo $package['id'] == $value['id'] ? 'selected' : ''; ?>  ><?php echo $value['title_'.$lang].' ('.$value['code'].')'; ?></option>
					<?php } } ?>
				</select>
			</div>

			<!-- <div class="form-group">
				<select class="form-control adults">
					<option>2 adults</option>
					<option>3 adults</option>
					<option>4 adults</option>
					<option>5+ adults</option>
				</select>
			</div> -->

			<div class="form-group">
				<textarea class="form-control message" style="height:100px;" placeholder="<?php echo $lang == 'eng' ? 'Your Message(Min 140, Max 500 characters)' : $this->lang->line('spanish_Your_Message'); ?>" minlength="140"></textarea>
				<i class="icon_info"></i>
			</div>

			<button class="btn_1 full-width purchase save" data-type="quick_enquire"><?php echo $lang == 'eng' ? 'Enquire Now' : $this->lang->line('spanish_enquire'); ?></button>
			<!-- <div class="text-center"><small>Use this for quick enquiry</small></div> -->
		</div>
		<ul class="share-buttons">
			<li>

				<a class="fb-share" href="http://www.facebook.com/sharer.php?u=<?php echo $facebook_share_link; ?>" target="_blank"><i class="social_facebook"></i> Share</a>

			</li>
			<li><a class="twitter-share" href="#" target="_blank"><i class="social_twitter"></i> Tweet</a></li>
			<li><a class="gplus-share" href="#0"><i class="social_googleplus"></i> Share</a></li>
		</ul>
	</aside>


	<script type="text/javascript">
      $(document).ready(function() {
          $("#message").keyup(function () {
            var cmax = $("#rem_" + $(this).attr("id")).attr("title");

            if ($(this).val().length >= cmax) {
                $(this).val($(this).val().substr(0, cmax));
            }

            $("#rem_" + $(this).attr("id")).text(cmax - $(this).val().length);
            });
      });       
  </script>



	<script type="text/javascript">
		$(document).ready(function() {
			
			$('.save').click(function(event) { 

				var type = $(this).data('type');
				
				var name = (type == 'quick_enquire') ? $('.name').val() : $('#name').val();
				var phone = (type == 'quick_enquire') ? $('.phone').val() : $('#phone').val();
				var email = (type == 'quick_enquire') ? $('.email').val() : $('#email').val();
				var message = (type == 'quick_enquire') ? $('.message').val() : $('#message').val();
				var package_id = (type == 'quick_enquire') ? $('.package').val() : $('#package').val();
				
				// var adults = $('.adults').val();

				if(message.length < 140) {
					alert('Message length should be atleast 140 characters.');
				} else {

				if( name == '' || phone == '' || message == '' || email == '' ) {
					alert('Please fill all the fileds');
				
				} else {

					var result = confirm('Do you want submit? Press OK to confirm the request.');
					if(result) {

						$.ajax({
						url: '<?php echo base_url()."HomeController/saveEnquiryByAjax"; ?>',
						type: 'POST',
						dataType: 'json',
						data: {
							name: name,
							phone: phone,
							email: email,
							package_id: package_id,
							// adults: adults,
							message: message,
						},
						})
						.done(function(response) {
							alert('Your Reference No.' + response['ticket_no'] + '. Please note that for future reference. Expected FeedBack Time 2 Days.');
						})
						.fail(function() {
							console.log("error");
						})
						.always(function() {
							
							// if(type == 'quick_enquire') {
								
							// 	$('.name').val('');
							// 	$('.phone').val('');
							// 	$('.email').val('');
							// 	$('.message').val('');
								
							// 	} else {

							// 	$('#name').val('');
							// 	$('#phone').val('');
							// 	$('#email').val('');
							// 	$('#message').val('');
							// }


						});


						} // end if
					} // end else

				}// end else
				
			});

		});
	</script>