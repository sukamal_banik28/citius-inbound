<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta charset="charset=iso8859-15"> -->
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=iso8859-15" /> -->
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php if( !empty($package['meta_description'])) { echo $package['meta_description']; } ?>">
    <meta name="keywords" content="<?php if( !empty($package['meta_key'])) { echo $package['meta_key']; } ?>">
    <meta name="author" content="">
    <meta name="google" content="notranslate">
    <title><?php echo $pagetitle; ?> - Home</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/front-end/'; ?>img/favicon.ico" type="image/x-icon">

    <!-- BASE CSS -->
    <link href="<?php echo base_url().'assets/front-end/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/front-end/'; ?>css/style.css" rel="stylesheet">
	<link href="<?php echo base_url().'assets/front-end/'; ?>css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
	<link href="<?php echo base_url().'assets/front-end/'; ?>css/custom.css" rel="stylesheet">
	<link href="<?php echo base_url().'assets/front-end/'; ?>css/flexslider.css" rel="stylesheet">
	
	<!-- Modernizr -->
	<script src="<?php echo base_url().'assets/front-end/'; ?>js/modernizr.js"></script>

	<script src="<?php echo base_url().'assets/front-end/'; ?>js/jquery-2.2.4.min.js"></script>

	<!-- SPECIFIC CSS -->
    <link href="<?php echo base_url().'assets/front-end/css/blog.css'; ?>" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="<?php echo base_url().'assets/front-end/layerslider/css/layerslider.css'; ?>" rel="stylesheet">

    <!-- SPECIFIC CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">
	<link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick-theme.css">
	<link rel="stylesheet" href="<?php echo base_url().'assets/front-end/ivslider/css/main-style.css'; ?>">
	<!-- SPECIFIC CSS -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

   	<!-- Tree View structure css -->
    <link id="themecss" rel="stylesheet" type="text/css" href="https://shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />

    <!-- Form validation using Jquery -->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js" type="text/javascript"></script>
	<script src="https://ajax.microsoft.com/ajax/jquery.validate/1.11.1/additional-methods.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139975968-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-139975968-2');
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->

</head>

<body>
	
	<div id="page">
	
	<header class="header menu_fixed">
		<div id="preloader"><div data-loader="circle-side"></div></div><!-- /Preload -->
		<div id="logo">
			<a href="<?php echo base_url(); ?>" >
				<img src="<?php echo base_url().'assets/front-end/'; ?>img/citius-logo.png" width="150" height="36" data-retina="true" alt="" class="logo_normal">
				<img src="<?php echo base_url().'assets/front-end/'; ?>img/citius-logo.png" width="150" height="36" data-retina="true" alt="" class="logo_sticky">
			</a>
		</div>
		<ul id="top_menu">
			<li>
				<div class="dropdown dropdown-user">
					<a href="#0" class="logged" data-toggle="dropdown" title="Login">

						<?php 

							if( !empty($this->session->userdata('user_id')) ) {

								$user_id = $this->session->userdata('user_id');
								$temp = getUserProfilePhotoIfLoggedIn( $user_id );
								extract($temp);
								$src = base_url().$profile_photo['file_path'].$profile_photo['file_name']; 
							} else {
								$src = base_url().'assets/front-end/img/avatar.jpg';
							}
						?>

						<img src="<?php echo $src; ?>" alt="Avatar">

					</a>
					<div class="dropdown-menu">
						<ul>
							
							<?php if( $this->session->userdata('user_id') !='' ) { ?>
								<li><a href="<?php echo base_url().'inbound-admin/logout'; ?>">Sign Out</a></li>
							<?php } else { ?>
								<li><a href="<?php echo base_url().'user-signup?lang='.$lang; ?>">Sign Up</a></li>
								<li><a href="<?php echo base_url().'inbound-admin/login?lang='.$lang; ?>">Sign In</a></li>
							<?php } ?>

						</ul>
					</div>
				</div>
			</li>

			<li class="nav-item dropdown lang-drop">
				<a class="nav-link dropdown-toggle" href="<?php echo parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH) . '?lang=eng'; ?>" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #5d5d5d;">
					<?php //$icon = ($lang == 'eng') ? 'gb' : ($lang == 'spanish' ? 'es' : 'fr'); 
						  $icon = ($lang == 'eng') ? 'gb' : 'es'; 
						  $selectedCountry = ($lang == 'eng') ? 'English' : ($lang == 'spanish' ? 'Spanish' : 'Frence');
					?>
					<span class="flag-icon flag-icon-<?php echo $icon; ?>"> </span> 
					<!-- <span class="lang-name-default"><?php //echo $selectedCountry; ?></span> -->
				</a>

				<div class="dropdown-menu" aria-labelledby="dropdown09">

					<a class="dropdown-item" href="<?php echo parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH) . '?lang=eng'; ?>"><span class="flag-icon flag-icon-gb"> </span>  <span class="lang-name">English</span></a>
					<a class="dropdown-item" href="<?php echo parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH) . '?lang=spanish'; ?>"><span class="flag-icon flag-icon-es"> </span>  <span class="lang-name">Spanish</span></a>

				</div>

			</li>
		</ul>
		<!-- /top_menu -->
		<a href="#menu" class="btn_mobile">
			<div class="hamburger hamburger--spin" id="hamburger">
				<div class="hamburger-box">
					<div class="hamburger-inner"></div>
				</div>
			</div>
		</a>
		<nav id="menu" class="main-menu">
			<ul>
				<!-- <li class="active"><span><a href="<?php //echo base_url().'?lang='.$lang; ?>"><?php //echo $lang == 'eng' ? 'Home' : $this->lang->line('spanish_home'); ?></a></span></li> -->
				
				<li class="active"><span><a href="<?php echo base_url().'about-us?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'About' : $this->lang->line('spanish_About'); ?></a></span></li>

				<li><a href="<?php echo base_url().'citius-gallery?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'Gallery' : $this->lang->line('spanish_gallery'); ?></a></li>
				
				<li><a> <?php echo $lang == 'eng' ? 'Tour packages' : $this->lang->line('spanish_tours_packages'); ?></a>
					<ul>

						<?php if(!empty($parent_categories)) { 

							array_multisort(array_column($parent_categories, 'order_by'), SORT_ASC, $parent_categories);			
							foreach ($parent_categories as $key => $value) {
						?>

							<li><a style="color: #ea1910" href="<?php echo base_url().'manage-tour/'.$value['seo_url'].'?lang='.$lang; ?>"><?php echo $value['name_'.$lang]; ?></a></li>

						<?php } } ?>

					</ul>
				</li>


				<li><a href="<?php echo base_url().'blogs?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'Blogs' : $this->lang->line('spanish_Blog'); ?></a></li>

				<li><a href="<?php echo base_url().'citius-destinations?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'Destinations' : $this->lang->line('spanish_Destinations'); ?></a></li>


				<li class="active"><span><a href="<?php echo base_url().'citius-quickpayment?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'Payment' : 'Payment'; ?></a></span></li>


				<li><a href="<?php echo base_url().'contact-us?lang='.$lang; ?>"><?php echo $lang == 'eng' ? 'Contact Us' : $this->lang->line('spanish_Contact_Us'); ?></a></li>
			</ul>
		</nav>

	</header>
	<!-- /header -->

	<!-- Download Brochure Button -->
	<div id="brochurediv" >
        <a href="<?php echo base_url().'citius-ebrochure?lang='.$lang; ?>">
            <img src="<?php echo base_url(); ?>assets/front-end/img/brochure-download-icon.png" class="img-responsive">
        </a>
    </div>



    <!-- Code to be added for Google Ads -->
	<script> (function(i,s,o,g,r,a,m){ i['GoogleAnalyticsObject']=r; i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)}, i[r].l=1*new Date(); a=s.createElement(o),m=s.getElementsByTagName(o)[0]; a.async=1; a.src=g; m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-139975968-2', 'auto'); ga('send', 'pageview'); </script>
	<!-- Code to be added for Google Ads -->