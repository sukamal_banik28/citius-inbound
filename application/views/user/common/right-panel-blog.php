<aside class="col-lg-3">
	<div class="widget">
		<form>
			<div class="form-group" style="margin-bottom: 1rem;">
				<input type="text" name="search" id="search" class="form-control" placeholder="<?php echo $lang == 'eng' ? 'Search' : $this->lang->line('spanish_Search'); ?>">
			</div>
			<button type="submit" id="submit" class="btn_1 rounded"> <?php echo $lang == 'eng' ? 'Search' : $this->lang->line('spanish_Search'); ?></button>
		</form>
	</div>
	<!-- /widget -->
	<div class="widget">
		<div class="widget-title">
			<h4><?php echo $lang == 'eng' ? 'Recent Posts' : $this->lang->line('Recent_Posts'); ?></h4>
		</div>
		<ul class="comments-list">

			<?php if(!empty($blogs)) { 
				$i = 0;
				foreach ($blogs as $key => $value) {
					$i++;
					if($i == 4) { break; }						
	
					if(!empty($value)) {
				?>
			
			<li>
				<div class="alignleft">
					<a href="<?php echo base_url().'blogs/'.$value['seo_url'].'?lang='.$lang; ?>">

						<?php if(isset($flag)) { ?>
							<img src="<?php echo base_url().$value['file']['file_path'].$value['file']['file_name']; ?>" alt="">
						<?php } else { ?>
							<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" alt="">
						<?php } ?>

						
				</div>
				<small><?php echo date( 'd M.Y', strtotime($value['created_at']) ); ?></small>
				<h3><a href="#" title=""><?php echo $value['title_'.$lang]; ?></a></h3>
			</li>

			<?php } } } ?>
			
		</ul>
	</div>
	<!-- /widget -->
	<div class="widget">
		<div class="widget-title">
			<h4><?php echo $lang == 'eng' ? 'Blog Categories' : $this->lang->line('blog_category'); ?></h4>
		</div>
		<ul class="cats">

			<?php if(!empty($categories)) {
				foreach ($categories as $key => $value) {
			?>
			<li>
				<a href="<?php echo base_url().'blogs/category/'.$value['seo_url'].'?lang='.$lang; ?>">
					<?php echo $value['name_'.$lang]; ?> <span> <?php echo !empty($value['blogs']) ? count($value['blogs']) : '0'; ?> </span></a></li>
			<?php } } ?>
			
		</ul>
	</div>
	<!-- /widget -->
	<div class="widget">
		<div class="widget-title">
			<h4><?php echo $lang == 'eng' ? 'Popular Tags' : $this->lang->line('spanish_Popular_Tags'); ?></h4>
		</div>
		<div class="tags">

			<?php if(!empty($tags)) {
				foreach ($tags as $key => $value) {
			?>

			<a href="<?php echo base_url().'blogs/tags/'.$value.'?lang='.$lang; ?>"><?php echo ucfirst($value); ?></a>

			<?php } } ?>
			
		</div>
	</div>
	<!-- /widget -->
</aside>
<!-- /aside -->