<?php $this->load->view("user/common/header.php"); ?>

	<main>
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Citius blog</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_60_35">
			<div class="row">
				<div class="col-lg-9">

					<?php if(!empty($blogs)) { 
						foreach ($blogs as $key => $value) {
							if(!empty($value)) {							
						?>

					<article class="blog wow fadeIn">
						<div class="row no-gutters">
							<div class="col-lg-7">
								<figure>
									<a href="<?php echo base_url().'blogs/'.$value['seo_url'].'?lang='.$lang; ?>">

										<?php if(isset($flag)) { ?>
											<img src="<?php echo base_url().$value['file']['file_path'].$value['file']['file_name']; ?>" alt="">
										<?php } else { ?>
											<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" alt="">
										<?php } ?>

										<div class="preview"><span>Read more</span></div>
									</a>
								</figure>
							</div>
							<div class="col-lg-5">
								<div class="post_info">
									<small><?php echo date( 'd M.Y', strtotime($value['created_at']) ); ?></small>
									<h3><a href="<?php echo base_url().'blogs/'.$value['seo_url'].'?lang='.$lang; ?>"><?php echo $value['title_'.$lang]; ?></a></h3>
									<p><?php echo $value['desc_'.$lang]; ?></p>
									<ul>
										<li>
											<div class="thumb"><img src="<?php echo base_url().'assets/uploads/users/user-avatar.png'; ?>" alt=""></div> <?php echo $value['author']; ?>
										</li>
										<li><i class="icon_comment_alt"></i> <?php if(!empty($value['comments'])) { echo count($value['comments']); } ?></li>
									</ul>
								</div>
							</div>
						</div>
					</article>

					<?php } } } else echo 'No blogs found.'; ?>

					
				

					<!-- <nav aria-label="...">
						<ul class="pagination pagination-sm">
							<li class="page-item disabled">
								<a class="page-link" href="#" tabindex="-1">Previous</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item">
								<a class="page-link" href="#">Next</a>
							</li>
						</ul>
					</nav> -->
					<!-- /pagination -->
				</div>
				<!-- /col -->

				
				<?php $this->load->view('user/common/right-panel-blog.php'); ?>


			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->
	
<?php $this->load->view("user/common/footer.php"); ?>