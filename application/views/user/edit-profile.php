<?php $this->load->view("user/common/header.php"); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA53RbrL34Vo9SA8EsuZHMsUKyXbXeebAg&libraries=places"></script>
	
	<main>
		
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Edit Profile</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_80_55">
			<div class="row profile">
				
				<?php $this->load->view('user/common/left-panel-profile'); ?>

				<div class="col-md-9">
					<div class="profile-content">
						<form method="post" action=""  autocomplete="off" enctype='multipart/form-data'>
							<div class="row">
								<div class="col-md-12 mt-2"><h6>Personal Details</h6></div>

								<!-- -->
								
								<div class="col-md-6">
									<div class="form-group">
										<label>Name*</label>
										<input class="form-control" required type="text"  name="name" value="<?php echo $user['name']; ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Phone*</label>
										<input class="form-control" required type="number" min="1"  name="phone" value="<?php echo $user['phone']; ?>">
										<span style="color: red; float: left;"><?php echo form_error('phone'); ?></span>
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Email*</label>
										<input class="form-control" required type="email"  name="email" value="<?php echo $user['email']; ?>">
										<span style="color: red; float: left;"><?php echo form_error('email'); ?></span>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Address</label>
										<input class="form-control" type="text" id="locationTextField" name="address" value="<?php echo $user['address']; ?>">
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="form-group">
								<label>Profile Picture</label>
								<input class="form-control" type="file"  name="profile_photo[]" accept="image/*">
							</div>
							
							<p class="add_top_30"><input type="submit" value="Edit Profile" name="submit" class="btn_1"></p>
						</form>
					</div>
				</div>
			</div>
			<!--/row-->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->

	<script type="text/javascript">
        function init() {
        var input = document.getElementById('locationTextField');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }   
    google.maps.event.addDomListener(window, 'load', init);
    </script>

<?php $this->load->view("user/common/footer.php"); ?>