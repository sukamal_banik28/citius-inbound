<?php $this->load->view("user/common/header.php"); 
	
	$user_id = $this->session->userdata('user_id');

?>

	<main>
		
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>My Enquiries</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_80_55">
			<div class="row profile">
				
				<?php $this->load->view('user/common/left-panel-profile'); ?>

				<div class="col-md-9">
					<div class="profile-content">
						<table class="table" id="example1">
						<thead>
						  <tr>
							<th>Ticket No.</th>
							<th>Original Message</th>
							<th>Status</th>
							<th>Date added</th>
						  </tr>
						</thead>
						<tbody>
							<?php if(!empty($enquiries)) {
									foreach ($enquiries as $key => $value) {
								?>
						  <tr>
							<td><?php echo $value['ticket_no']; ?></td>
							<td><?php echo $value['message']; ?></td>
							<td><span class="badge badge-<?php echo $value['current_status'] == 'Resolved' ? 'primary' : 'success'; ?>"><?php echo $value['current_status']; ?></span></td>
							<td><?php echo date('d-M-Y', strtotime($value['created_at'])); ?></td>
						  </tr>

						  <?php
						  	}	}
						  ?>
						  
						</tbody>
					  </table>
					</div>
				</div>

			</div>
			<!--/row-->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->

	<script type="text/javascript">
  	$(document).ready(function() {
    	$('#example1').DataTable();
	});
  </script>

<?php $this->load->view("user/common/footer.php"); ?>