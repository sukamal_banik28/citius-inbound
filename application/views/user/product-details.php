\<?php $this->load->view("user/common/header.php"); ?>
<style type="text/css">
	.hero_in.tours_detail:before {
	  	background: url(../<?php echo $banner['file_path'].$banner['file_name']; ?>) center center no-repeat;
	}
</style>

<main>
		<section class="hero_in tours_detail">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span><?php echo $product['title_'.$lang]; ?></h1>
				</div>
				<span class="magnific-gallery">
					<?php if(!empty($gallery)) {
						foreach ($gallery as $key => $value) { ?>
							<a href="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" class="btn_photos" title="<?php echo $value['original_name']; ?>" data-effect="mfp-zoom-in">Gallery</a>
					<?php
						}
					}
					?>
				</span>
				
				<!-- <div class="hotel-star">
					<a class="btn_photos">4 ⭐ Hotel</a>
				</div> -->
				
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<nav class="secondary_nav sticky_horizontal">
				<div class="container">
					<ul class="clearfix">
						<li><a href="#description" class="active">Description</a></li>
						<?php 
						if( !( $product['type'] ==14 || $product['type'] ==13) ) { ?>
						<li><a href="#amenities">Amenities</a></li>
						<?php } ?>
					</ul>
				</div>
			</nav>
			<div class="container margin_60_35">
				<div class="row">
					<div class="col-lg-8">

						<!-- Description -->
						<section id="description">
							<?php echo $product['long_desc_'.$lang]; ?>
						</section>
						<!-- /section -->

						<?php 
						if( !( $product['type'] ==14 || $product['type'] ==13) ) { ?>
						<!-- itinerary -->
						<section id="amenities">
							<h2>Amenities</h2>
							<ul class="hotel_facilities">
								<?php if(!empty($amenities)) { 
									foreach ($amenities as $key => $value) {
									?>
								<li><img src="<?php echo base_url().'assets/front-end/img/'.$value['font_icon']; ?>" alt=""><?php echo $value['name_'.$lang]; ?></li>
								<?php }
								} ?>
							</ul>
						</section>
						<?php } ?>

					</div>
					<!-- /col -->
					
					<?php $this->load->view( "user/common/quick-enquiry" ); ?>


				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->



<?php $this->load->view("user/common/footer.php"); ?>