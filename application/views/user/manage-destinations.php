<?php $this->load->view("user/common/header.php"); ?>


<main>
		
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Destinations</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_80_55">
			<div class="row profile">
				<div class="col-md-4">
					<div class="profile-sidebar" style="padding-top: 0;">
						<div class="panel">
							<ul id="treeviewIconCss">

								<?php if(!empty($countries)) { 
									foreach ($countries as $key => $value) {
									?>

								<li data-icon-cls="flag-icon flag-icon-<?=$value['font_icon_tag'];?>" data-expanded="false"><strong><?=$value['name_'.$lang];?></strong>
									
									<?php $states = getProvinces($value['id']); 
										if(!empty($states)) { 			
									?>
									<ul>

										<?php foreach ($states as $s_key => $s_value) { ?>
										<li><?=$s_value['name_'.$lang];?>

											<?php $cities = getProvinces($s_value['id']); 
												if(!empty($cities)) { 			
											?>
											
											<ul>

											<?php foreach ($cities as $key => $value) {	?>
											<li><span class="city_details" data-cityid="<?=$value['id']?>" ><?=$value['name_'.$lang];?></span></li>
											<?php } ?>

											</ul>

											<?php } ?>

										</li>
										<?php } ?>
										
									</ul>
									<?php } ?>



								</li>

								<?php } } ?>

							</ul>
						</div>
					</div>
				</div>
				
				<div class="col-md-8">
					<div class="profile-content" style="display: none;">
						<div class="city-img">
							<img class="picture" src="https://mgts.co.in/img/backgrounds/Kolkata.jpg">
						</div>
						<div class="city-writeup">
							<p>Kolkata (formerly Calcutta) is the capital of India's West Bengal state. Founded as an East India Company trading post, it was India's capital under the British Raj from 1773–1911. Today it’s known for its grand colonial architecture, art galleries and cultural festivals. It’s also home to Mother House, headquarters of the Missionaries of Charity, founded by Mother Teresa, whose tomb is on site.</p>
						</div>
					</div>
				</div>
			</div>
			<!--/row-->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->

	
	
	<script type="text/javascript">

    jQuery(function ($) {
        $("#treeviewIconCss").shieldTreeView();
    });

    $(document).ready(function() {

    	$(document).on('click', '.city_details', function() {
		    
		    var cityid = $(this).data('cityid');
		    console.log(cityid)

		    $.ajax({
		    	url: '<?php echo base_url().'HomeController/getProvinceDetails'; ?>',
		    	type: 'POST',
		    	dataType: 'json',
		    	data: { cityid : cityid },
		    })
		    .done(function(response) {
		    	//console.log(response);

		    	$(".profile-content").show();
		    	var src = '<?php echo base_url(); ?>'+response.file_path+response.file_name;
		    	//console.log(src)
		    	$(".picture").attr("src", src);
		    	$(".city-writeup").html('<p>'+response.description_eng+'</p>');
		    })
		    .fail(function() {
		    	console.log("error");
		    })
		    .always(function() {
		    });
		    
		});

    }); // document ready


	</script>

<?php $this->load->view("user/common/footer.php"); ?>