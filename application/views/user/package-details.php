<?php $this->load->view("user/common/header.php"); ?>



<style type="text/css">
	.hero_in.tours_detail:before {
	  	background: url(<?php echo base_url().$banner['file_path'].$banner['file_name']; ?>) center center no-repeat;
	}
</style>
	<main>
		<section class="hero_in tours_detail">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span><?php echo $package['title_'.$lang]; ?></h1>
				</div>
				<span class="magnific-gallery">
					<?php if(!empty($gallery)) {
						foreach ($gallery as $key => $value) { ?>
							<a href="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" class="btn_photos" title="<?php echo $value['original_name']; ?>" data-effect="mfp-zoom-in">Gallery</a>
					<?php
						}
					}
					?>
				</span>

				<div class="hotel-star">
					<a class="btn_photos"><?php echo $lang == 'eng' ? 'Package Code' : $this->lang->line('spanish_package_code'); ?> <?php echo ' - '.$package['code']; ?></a>
				</div>

			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<nav class="secondary_nav sticky_horizontal">
				<div class="container">
					<ul class="clearfix">

						<li><a href="#description" class="active"><?php echo $lang == 'eng' ? 'Description' : $this->lang->line('spanish_Description'); ?></a></li>

						<li><a href="#itinerary"><?php echo $lang == 'eng' ? 'Itinerary' : $this->lang->line('spanish_Itinerary'); ?></a></li>
						
						<li><a href="#accommodation"><?php echo $lang == 'eng' ? 'Hotels' : $this->lang->line('spanish_package_code'); ?></a></li>

						<li><a href="#add-on"
							><?php echo $lang == 'eng' ? 'Add-ons' : $this->lang->line('spanish_addons'); ?></a></li>

						<?php if(!empty($onlinePackage)) { ?>
						
						<li><a href="#extensions">
							<?php echo $lang == 'eng' ? 'Extensions' : $this->lang->line('spanish_extensions'); ?></a></li>
						<li><a href="#booking">
							<?php echo $lang == 'eng' ? 'Payment' : $this->lang->line('spanish_Payments'); ?></a></li>

						<?php } ?>

						<?php if( !empty($reviews) ) { ?>
							<li><a href="#reviews"><?php echo $lang == 'eng' ? 'Reviews' : $this->lang->line('spanish_Reviews'); ?></a></li>
						<?php } ?>

					</ul>
				</div>
			</nav>
			<div class="container margin_60_35">
				<div class="row">
					<div class="col-lg-8">

						<!-- Description -->
						<section id="description">
							<h2><?php //echo $lang == 'eng' ? 'Description' : $this->lang->line('spanish_Description'); ?></h2>
							<?php echo $package['long_desc_'.$lang]; ?>
						</section>
						<!-- /section -->

						<!-- itinerary -->
						<section id="itinerary">
							<h2><?php echo $lang == 'eng' ? 'Itinerary' : $this->lang->line('spanish_Itinerary'); ?></h2>
							<ul class="cbp_tmtimeline">

								<?php if(!empty($packageItinerary)) { 
									foreach ($packageItinerary as $key => $value) {
										
									?>

								<li>
									<time class="cbp_tmtime" datetime="09:30">
										<span><?php echo $lang == 'eng' ? 'Day' : $this->lang->line('spanish_Day'); ?></span>
									</time>
									<div class="cbp_tmicon">
										<?php echo $value['day_wise_plan']; ?>
									</div>
									<div class="cbp_tmlabel">
										<div class="hidden-xs">
											<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" alt="" class="rounded-circle thumb_visit">
										</div>
										<h4><?php echo $value['short_desc_'.$lang]; ?></h4>
										<!-- <p> -->
											<?php echo $value['long_desc_'.$lang]; ?>
										<!-- </p> -->
									</div>
								</li>

								<?php } } ?>
								
							</ul>
						</section>



						
						<!-- Accommodation -->
						<section id="accommodation">
							<h2>Hotels</h2>
							<?php 
								if(!empty($products['7'])) {
									foreach ($products['7'] as $key => $value) {
									 	if(!empty($value)) {
								?>
							<div class="room_type first">
								<div class="row">
									<div class="col-md-4">
										<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" class="img-fluid" alt="">
									</div>
									<div class="col-md-8">
										<h4>
											<a href="<?php echo base_url().'products/'.$value['seo_url'].'?lang='.$lang; ?>" target="_blank" ><?php echo $value['title_'.$lang]; ?></a>

											<span class="star-rate badge badge-success"><?=$value['rating']; ?>
												<?php if(is_numeric($value['rating'])) { ?><i class="icon-star" ></i><?php } ?>
											</span>

										</h4>
											<?php echo substr( $value['short_desc_'.$lang], 0, 100 ); ?><a href="<?php echo base_url().'products/'.$value['seo_url'].'?lang='.$lang; ?>" target="_blank" >Readmore...</a>
									</div>
								</div>
							</div>
						<?php 		} // end if !empty value 
								} // end loop
							} // end if !empty products
						?>
						</section>





						<!-- Accommodation -->
						<section id="add-on">
							<h2>Add-Ons</h2>


							<div class="types">Special Interests</div>
							<?php 
								if(!empty($products['14'])) {
									foreach ($products['14'] as $key => $value) {
									 	if(!empty($value)) {
								?>

							<div class="room_type first">
								<div class="row">
									<div class="col-md-4">
										<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" class="img-fluid" alt="">
									</div>
									<div class="col-md-8">
										<h4>
											<a href="<?php echo base_url().'products/'.$value['seo_url'].'?lang='.$lang; ?>" target="_blank" ><?php echo $value['title_'.$lang]; ?></a>

											<!-- <span class="star-rate badge badge-success"><?php //echo $value['rating']; ?><i class="icon-star" ></i></span> -->

										</h4>
											<?php echo substr( $value['short_desc_'.$lang], 0, 200 ); ?><a href="<?php echo base_url().'products/'.$value['seo_url'].'?lang='.$lang; ?>" target="_blank" >Readmore...</a>
									</div>
								</div>
							</div>
						<?php 		} // end if !empty value 
								} // end loop
							} // end if !empty products
							else { echo '<h5>No Data Found</h5>'; }
						?>




						<div class="types">Extensions</div>
							<?php 
								if(!empty($products['13'])) {
									foreach ($products['13'] as $key => $value) {
									 	if(!empty($value)) {
								?>

							<div class="room_type first">
								<div class="row">
									<div class="col-md-4">
										<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" class="img-fluid" alt="">
									</div>
									<div class="col-md-8">
										<h4>
											<a href="<?php echo base_url().'products/'.$value['seo_url'].'?lang='.$lang; ?>" target="_blank" ><?php echo $value['title_'.$lang]; ?></a>

											<!-- <span class="star-rate badge badge-success"><?php //echo $value['rating']; ?><i class="icon-star" ></i></span> -->

										</h4>
											<?php echo substr( $value['short_desc_'.$lang], 0, 100 ); ?><a href="<?php echo base_url().'products/'.$value['seo_url'].'?lang='.$lang; ?>" target="_blank" >Readmore...</a>
									</div>
								</div>
							</div>
						<?php 		} // end if !empty value 
								} // end loop
							} // end if !empty products
							else { echo '<h5>No Data Found</h5>'; }
						?>


						</section>




						<?php if(!empty($onlinePackage)) { ?>

						<form method="post" action="<?php //echo base_url().'savePrimaryBookingInfo'; ?>" enctype='multipart/form-data'>

						<!-- Add - On -->
						<section id="add-on">
							<h2>Add-Ons</h2>


							<div class="types">Hotels</div>
							<div class="card custom-tabs">							

								<!-- Tab panes -->
								<div class="tab-content">

									
									<div id="" class="container tab-pane active">
										<!-- <h3>Hotels</h3> -->
										<table class="table">
											<thead>
												<tr style="background-color: #fc5b62; color: white;">
													<th>Select product</th>
													<th>Hotel Name</th>
													<th>Location</th>
													<th>Hotel Type</th>
													<th>Hotel Price(INR)</th>
												</tr>
											</thead>
											<tbody>

											<?php 

												// ID 2 measn USD and 1 means EUR
												$currency_id = $lang == 'eng' ? 2 : 1;
												$cur_symbol = $lang == 'eng' ? '$' : '€';
												
												if(!empty($products)) { 

												if(!empty($products['7'])) { 												

												/* 7 for Hotel */
												foreach ($products['7'] as $p_key => $p_value) {
													
												if(!empty($p_value['addon'])) {
												foreach ($p_value['addon'] as $key => $value) {


												$addonType = getAddOnDefByID($value['sub_object_id']);

											?>

												<tr>

													<input type="hidden" name="currency_id" value="<?=$currency_id; ?>">

													<td>


														<input type="checkbox" class="addon_price" 
														name="hotels[<?=$p_value['id']; ?>][<?=$addonType['id']; ?>]" 
														data-addonid="<?php echo $addonType['id']; ?>"  
														value="<?php echo $value['value']; ?>"  
														data-object_id="<?=$p_value['id'];?>"
														
														<?php if($package['citius_touch'] == 'Fixed') { echo !empty($value['is_default_addon']) ? 'checked' : ''; } ?>  >


													</td>
													<td><a href="<?php echo base_url().'products/'.$p_value['seo_url'].'?lang='.$lang; ?>" target="_blank" >
													<?php echo $p_value['title_'.$lang]; ?></a></td>
													<td><?php echo $p_value['location']['city']; ?></td>
													<td><?php echo $addonType['type_name_eng']; ?></td>
													<td><?php echo $cur_symbol.' '.$value['value']; ?></td>
												</tr>

												<?php } } } } } ?>
												
											</tbody>
										</table>
									</div>								


									
								</div>
							</div>

							<hr>

							<div class="types">Meals</div>
							<div class="card custom-tabs">
								<!-- Nav tabs -->							

								<!-- Tab panes -->
								<div class="tab-content">
									<div id="" class="container tab-pane active">
										<!-- <h3>Breakfast</h3> -->
										<table class="table" >
											<thead>
												<tr style="background-color: #fc5b62; color: white;">
													<th>Select product</th>
													<th>Meal Name</th>
													<th>Location</th>
													<th>Meal Type</th>
													<th>Price</th>
												</tr>
											</thead>
											<tbody>

											<?php if(!empty($products['8'])) { 

												/* 8 for Meal */
												foreach ($products['8'] as $key => $value) {

													if(!empty($value['addon'])) {
													foreach ($value['addon'] as $addon_key => $addon_value) {

													$addonType = getAddOnDefByID($addon_value['sub_object_id']);
													
											?>

												<tr>
													<td>

														<input type="checkbox" class="addon_price" 
														name="meals[<?=$value['id']; ?>][<?=$addonType['id']?>]" 
														data-addonid="<?php echo $addonType['id']; ?>" 
														value="<?php echo $addon_value['value']; ?>"   
														data-object_id="<?=$value['id'];?>"

														<?php  if($package['citius_touch'] == 'Fixed') { echo !empty($addon_value['is_default_addon']) ? 'checked' : ''; } ?> >

													</td>
													<td><a href="<?php echo base_url().'products/'.$p_value['seo_url'].'?lang='.$lang; ?>" target="_blank" ><?php echo $value['title_'.$lang]; ?></a></td>
													<td><?php echo $p_value['location']['city']; ?></td>
													<td><?php echo $addonType['type_name_eng']; ?></td>
													<td><?php echo $cur_symbol.' '.$addon_value['value']; ?></td>
												</tr>

											<?php } } } } ?>

											</tbody>
										</table>
									</div>
								</div><!-- End tab content -->
							</div><!-- card custom-tabs -->

							<hr>

							<div class="types">Other Packages</div>
							<div class="card custom-tabs">
								<!-- Nav tabs -->
								<!-- Tab panes -->
								<div class="tab-content">
									<div id="" class="container tab-pane active">

										<div class="form-group">
											<select class="form-control other_package" style="padding: 7px;" name="other_package" >

												<option value="">Please select a package if you want to add</option>
												<?php 

												$current_package_id = $package['id'];
												$append = "  AND files.flag = 'package_logo' AND packages.id NOT IN (".$current_package_id.")";
												$packages = getAllPackages( $append );

												$currency_id = $lang == 'eng' ? 2 : 1;
												$cur_symbol = $lang == 'eng' ? '$' : '€';

												if(!empty($packages)) {
													foreach ($packages as $key => $value) {


														$price_details = getPackageMinValueForCurrency($currency_id, $value['id'], 'package' );

														if(!empty($price_details)) {
															$travel_pax = getTravelPaxDefByID($price_details['travel_pax_id']);
														} else {
															$travel_pax = '';
														}



												?>
													<option value="<?php echo $value['id']; ?>" data-other_price="<?php echo is_array($price_details) ? $price_details['value'] : 0; ?>" >


													<?php echo $value['title_'.$lang].' ('.$value['code'].')'; ?> - 
													Package Price for <?php echo is_array($travel_pax) ? $travel_pax['pax_range'].'pax' : $travel_pax; ?> - <?php echo is_array($price_details) ? $cur_symbol.' '.$price_details['value'] : 'Not Available'; ?>

														
													</option>
												<?php } } ?>
											</select>
										</div>


									</div>
								</div><!-- End tab content -->
							</div><!-- card custom-tabs -->
						</section>



						<!-- Add - On -->
						<section id="extensions">
							<h2>Extensions</h2>

							<!-- <div class="types">Hotels</div> -->
							<div class="card custom-tabs">							

								<!-- Tab panes -->
								<div class="tab-content">

									
									<div id="" class="container tab-pane active">
										<!-- <h3>Hotels</h3> -->
										<table class="table">
											<thead>
												<tr style="background-color: #fc5b62; color: white;">
													<th>Select product</th>
													<th>Extension Name</th>
													<th>Location</th>
													<th>Extension Price(INR)</th>
												</tr>
											</thead>
											<tbody>

											<?php 

												// ID 2 measn USD and 1 means EUR
												$currency_id = $lang == 'eng' ? 2 : 1;
												$cur_symbol = $lang == 'eng' ? '$' : '€';
												
												if(!empty($products)) { 												

												if(!empty($products['13'])) { 

												/* 13 for Extensions */
												foreach ($products['13'] as $p_key => $p_value) {
													
												if(!empty($p_value['addon'])) {
												foreach ($p_value['addon'] as $key => $value) {


												$addonType = getAddOnDefByID($value['sub_object_id']);

											?>

												<tr>
													<td>

														<input type="checkbox" 
														name="extensions[<?=$p_value['id']; ?>][<?=$addonType['id']?>]" 
														class="addon_price"  
														data-addonid="<?php echo $addonType['id']; ?>"  
														data-object_id="<?=$p_value['id'];?>"
														value="<?php echo $value['value']; ?>">


													</td>
													
													<td><a href="<?php echo base_url().'products/'.$p_value['seo_url'].'?lang='.$lang; ?>" target="_blank" >
													<?php echo $p_value['title_'.$lang]; ?></a></td>
													<td><?php echo $p_value['location']['city']; ?></td>
													<td><?php echo $cur_symbol.' '.$value['value']; ?></td>
												</tr>

												<?php } } } } } ?>
												
											</tbody>
										</table>
									</div>								
									
								</div>
							</div>
						</section>






						<!-- Booking -->
						<section id="booking">
							<h2><?php echo $lang == 'eng' ? 'Book Now' : $this->lang->line('spanish_Book_Now'); 

								if($package['citius_touch'] == 'Fixed') {
									$append = ' WHERE `type` = "others" '; 
								}

								$packageValue = travelPaxUsingPackageCat($append);

								$currency_id = $lang == 'eng' ? 2 : 1;
								$cur_symbol = $lang == 'eng' ? '$' : '€';
								$price_details = getPackageMinValueForCurrency($currency_id, $package['id'], 'package' );

							?></h2>
							<form>
								<div class="row">

									<div class="form-group col-md-6">
										<label><?php echo $lang == 'eng' ? 'Name' : $this->lang->line('spanish_Name'); ?> *</label>
										<input type="text"  placeholder="" class="form-control" id="name" name="client_name" value="<?php echo isset($user['name']) ? $user['name'] : ''; ?>">
									</div>

									<div class="form-group col-md-6">
										<label><?php echo $lang == 'eng' ? 'Telephone' : $this->lang->line('spanish_Telephone'); ?> *</label>
										<input type="number" class="form-control" id="phone" name="client_telephone" value="<?php echo isset($user['phone']) ? $user['phone'] : ''; ?>">
									</div>

									<div class="form-group col-md-6">
										<label><?php echo $lang == 'eng' ? 'Email' : $this->lang->line('spanish_Email'); ?> *</label>
										<input type="email" class="form-control"  name="client_email" value="<?php echo isset($user['email']) ? $user['email'] : ''; ?>">
									</div>


									<div class="form-group col-md-6">
										<label><?php echo $lang == 'eng' ? 'Address' : $this->lang->line('spanish_Address'); ?> *</label>
										<input type="text" class="form-control"  name="client_address" value="<?php echo isset($user['address']) ? $user['address'] : ''; ?>">
									</div>


									<div class="form-group col-md-6 clearfix">

										<?php if($package['citius_touch'] == 'Fixed') { ?>
										<label>How many people</label>

										<select class="form-control fixed" >
											<?php if(!empty($packageValue)) { 
												foreach ($packageValue as $key => $value) {
													?>
											<option value="<?=$value['id']; ?>"  > <?=$value['pax_range']; ?></option>
											<?php } } ?>
										</select>

										<input type="hidden" id="fixed_pax_range" name="fixed_pax_range" value="1">

										<?php } else { ?>

										<div class="row">

										<div class="form-group col-md-6">
											<label>How many adult</label>

											<select class="form-control col-md-12 adult" name="fit_adult" >
												<?php  for ($i=1; $i <= 10 ; $i++) { ?>
													<option value="<?=$i?>" ><?=$i?></option>
												<?php } ?>
											</select>

										</div>

										<div class="form-group col-md-6">
											<label>How many child</label>

											<select class="form-control col-md-12 child" disabled="" name="fit_child" >
												<?php  for ($i=0; $i <= 10 ; $i++) { ?>
													<option value="<?=$i?>" ><?=$i?></option>
												<?php } ?>
											</select>

											</div>

										</div><!-- end row -->

										<?php } ?>

									</div>


									<div class="form-group col-md-6 clearfix">
										<label>
										<?php echo $lang == 'eng' ? 'Package Price for' : $this->lang->line('spanish_package_price'); ?>
										<?php echo ($package['citius_touch'] == 'Fixed') ? 'Group' : '1 Adult'; ?> (<?php echo $cur_symbol ?>)
										</label>


										<input type="text" class="form-control package_price" name="package_price" value="<?php echo is_array($price_details) ? $price_details['value'] : 'NA'; ?>" readonly>

										<input type="hidden" id="org_price" value="<?php echo is_array($price_details) ? $price_details['value'] : 0; ?>">

									</div>

									
									<div class="form-group col-md-12 clearfix">
										<label><?php echo $lang == 'eng' ? 'Package Code' : $this->lang->line('spanish_package_code'); ?></label>
										<input type="hidden" id="package" name="package_id" value="<?php echo $package['id']; ?>">
										<input type="text" class="form-control" value="<?php echo $package['title_'.$lang].' ('.$package['code'].')' ; ?>" readonly>

									</div>



								



									<div class="form-group col-md-12">
										<label><?php echo $lang == 'eng' ? 'Your Message(Min 140, Max 500 characters)' : $this->lang->line('spanish_Your_Message'); ?></label>
										<textarea class="form-control" id="message" name="client_message" style="height:130px;"  minlength="140"></textarea>
										<p>
                                          <span><?php echo $lang == 'eng' ? 'characters remaining' : $this->lang->line('spanish_characters_remaining'); ?>: <span id="rem_message" title="500"></span></span>
                                        </p>
									</div>
									<div class="form-group col-md-12 add_top_20">
										<input type="button" value="<?php echo $lang == 'eng' ? 'Book Now' : $this->lang->line('spanish_enquire'); ?>" class="btn_1" data-type="booking_enquire">
									</div>
								</div>
							</form>
						</section>

					</form>

					<?php } ?>





						<?php if( !empty($reviews) ) { ?>
						
						<!-- Review -->
						<section id="reviews">
							<h2><?php echo $lang == 'eng' ? 'Reviews' : $this->lang->line('spanish_Reviews'); ?></h2>
							<div class="reviews-container">
								<div class="row">
									<div class="col-lg-3">
										<div id="review_summary">
											<small>Count</small>
											<strong><?php echo count($reviews); ?></strong>
											<em><?php echo $lang == 'eng' ? 'Reviews' : $this->lang->line('spanish_Reviews'); ?></em>
											<!--<small>Based on 4 reviews</small>-->
										</div>
									</div>
								</div>
								<!-- /row -->
							</div>

							<hr>

							<div class="reviews-container">

								<?php foreach ($reviews as $key => $value) { ?>								
								
								<div class="review-box clearfix">
									<figure class="rev-thumb"><img src="<?php echo base_url().'assets/front-end/img/personal_default_avatar.png'; ?>" alt="">
									</figure>
									<div class="rev-content">
										<div class="rev-info">
											<?php echo $value['name']; ?> – <?php echo date('M d, Y', strtotime($value['created_at'])); ?>:
										</div>
										<div class="rev-text">
											<p>
												<?php echo $value['review']; ?>
											</p>
										</div>
									</div>
								</div>
								<!-- /review-box -->
								<?php } ?>

							</div>
							<!-- /review-container -->
						</section>
						<!-- /section -->

						<?php } ?>

						<div class="add-review">
							<h5>Leave a <?php echo $lang == 'eng' ? 'Review' : $this->lang->line('spanish_Reviews'); ?></h5>
							<form>
								<div class="row">
									<div class="form-group col-md-6">
										<label><?php echo $lang == 'eng' ? 'Name' : $this->lang->line('spanish_Name'); ?> *</label>
										<input type="text" name="name_review" id="name_review" placeholder="" class="form-control" value="<?php echo isset($user['name']) ? $user['name'] : ''; ?>">
									</div>
									<div class="form-group col-md-6">
										<label><?php echo $lang == 'eng' ? 'Email' : $this->lang->line('spanish_Email'); ?> *</label>
										<input type="email" name="email_review" id="email_review" class="form-control" value="<?php echo isset($user['name']) ? $user['name'] : ''; ?>">
									</div>
									<div class="form-group col-md-12">
										<label><?php echo $lang == 'eng' ? 'Review' : $this->lang->line('spanish_Feedback'); ?></label>
										<textarea name="review_text" id="review_text" class="form-control" style="height:130px;"></textarea>
									</div>
									<div class="form-group col-md-12 add_top_20">
										<input type="button" value="Submit" class="btn_1" id="submit-review">
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /col -->
					
					<?php $this->load->view( "user/common/quick-enquiry" ); ?>


				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->


	<script type="text/javascript">
		$(document).ready(function() {
			$('#submit-review').click(function(event) {

				var result = confirm('Do you want to submit this review? Press OK to confirm.');
				if(result) 
				{
				
					var name = $('#name_review').val();
					var email = $('#email_review').val();
					var review = $('#review_text').val();

					if( name == '' || email == '' || review == '' )
					{
						alert('Please fill all the fileds');
					} else {

						$.ajax({
							url: '<?php echo base_url().'HomeController/saveReviewByAjax' ?>',
							type: 'POST',
							data: {
								name: name,
								email: email,
								review: review,
								reviewable_id: <?php echo $package['id']; ?>,
								reviewable_type: 'Package'
							},
						})
						.done(function() {
							alert('Thank you for your valuable feedback. To view your feedback in the site, it will take 72 hours.');
						})
						.fail(function() {
							console.log("error");
						})
						.always(function() {
							$('#name_review').val('');
							$('#email_review').val('');
							$('#review_text').val('');
						});

					} // end else
				} // end result
			});
			


			$('.addon_price').click(function(event) {
				
				/* Act on the event */
				var original_package_price = parseInt($('.package_price').val(), 10);

				var sum = 0;
				var checked = $(this).is(':checked');
				if(checked) {

					$('.fixed').prop('disabled', true);
					var object_id = $(this).data('object_id');
					var addonid = $(this).data('addonid');
					console.log(object_id);

					$.ajax({
						url: '<?php echo base_url().'HomeController/AddDifferentiationAmount' ?>',
						type: 'POST',
						dataType: 'json',
						data: {
							object_id : object_id,
							addonid : addonid,
							lang : '<?php echo $lang; ?>',
							citius_touch : '<?php echo $package['citius_touch']; ?>',
						},
					})
					.done(function(response) {
						var sum = original_package_price + response;
						console.log(sum)
						$('.package_price').val(sum);
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
					});
					
					// $('.fixed').prop('disabled', true);
					// var value = parseInt($(this).val(), 10)
					// var sum = original_package_price + value;
					// $('.package_price').val(sum);
					// console.log(sum)

				} else {
					

					//var value = parseInt($(this).val(), 10)
					//var sum = original_package_price - value;
					//$('.package_price').val(sum);
					//console.log(sum)
				}
				
			});


			$('.other_package').change(function(event) {
				var original_package_price = parseInt($('.package_price').val(), 10);
				var additional_package_price = $(this).find(':selected').data('other_price');

				var sum = original_package_price + additional_package_price;
				$('.package_price').val(sum);

			});



			$('.fixed').change(function(event) {
				var id = $(this).val();

				$.ajax({
					url: '<?php echo base_url().'HomeController/getFixedPricesForPackage' ?>',
					type: 'POST',
					dataType: 'json',
					data: {
						pax_id : id,
						package_id : '<?php echo $package['id']; ?>',
						lang : '<?php echo $lang; ?>',
					},
				})
				.done(function(response) {
					$('.package_price').val(response.value);
					$('#fixed_pax_range').val(id);
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
				});
				
			});



			$('.adult').change(function(event) {
				var value = parseInt($(this).val());
				var original_package_price = parseInt($('.package_price').val(), 10);
				var static_package_price = parseInt($('#org_price').val(), 10);

				if(value == 10) {

					alert('Please contact citius for more discount offer');
					$('.adult').val(1);
					$('.package_price').val(static_package_price);
					return false;

				} else {
					
					$.ajax({
						url: '<?php echo base_url().'HomeController/getFITPricesForPackage' ?>',
						type: 'POST',
						dataType: 'json',
						data: {
							pax : value,
							package_id : '<?php echo $package['id']; ?>',
							lang : '<?php echo $lang; ?>',
							flag : 'adult',
						},
					})
					.done(function(response) {

						var adult_pax_value = (value-1) * response.value;

						console.log(original_package_price)
						console.log(adult_pax_value)
						var sum = original_package_price + adult_pax_value;
						$('.package_price').val(sum)

					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						$('.child').prop('disabled', false)
					});
					

				}

				
			});

			
			$('.child').change(function(event) {
				var value = parseInt($(this).val());
				var original_package_price = parseInt($('.package_price').val(), 10);

				$.ajax({
					url: '<?php echo base_url().'HomeController/getFITPricesForPackage' ?>',
					type: 'POST',
					dataType: 'json',
					data: {
						pax : value,
						package_id : '<?php echo $package['id']; ?>',
						lang : '<?php echo $lang; ?>',
						flag : 'child',
					},
				})
				.done(function(response) {

					var child_pax_value = value * response.value;
					var sum = child_pax_value + original_package_price;
					$('.package_price').val(sum)
					console.log(child_pax_value)
					console.log(sum);

				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
				});
				
			});

		});
	</script>

<?php $this->load->view("user/common/footer.php"); ?>