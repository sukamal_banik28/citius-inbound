<?php $this->load->view("user/common/header.php"); ?>

<style type="text/css">
	.error {
		color: #0006ff;
	}
</style>
			
	
	<main>
		<section class="hero_in contacts">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Quick Payment</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="row justify-content-between">
					<div class="col-lg-8 mx-auto">
						<p>Please fill out this form and make payment. (Fields Marked with <span style="color: red;">*</span> are mandatory) </p>

						<?php if(!empty($msg)) { 
							echo '<p class="error">'.$msg.'</p>';
						} ?>

						<div id="message-contact"></div>
						<form method="post" action="" id="form1" autocomplete="off">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Full Name <span style="color: red;">*</span></label>
										<input class="form-control" type="text" id="name" name="name">
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Email ID <span style="color: red;">*</span></label>
										<input class="form-control" type="email" id="email" name="email">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Contact Number <span style="color: red;">*</span></label>
										<input class="form-control" type="text" id="phone" name="phone">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group">
										<label>Invoice Number <span style="color: red;">*</span></label>
										<input class="form-control" type="text" id="invoice_number" name="invoice_number">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group">
										<label>Payment Regarding <span style="color: red;">*</span></label>
										<select name="payment_regarding" id="payment_regarding" class="form-control">
			                                <option value="">Select Payment Regarding</option>
			                                <option value="Tour Packages">Tour Packages</option>
			                                <option value="Hotels">Hotels</option>
			                                <option value="Flights">Flights</option>
			                                <option value="Others">Others</option>
			                            </select>
									</div>
								</div>
                                
                             <div class="col-md-12">
							<div class="form-group">
								<label>Remark</label>
								<textarea class="form-control" id="remark" name="remark" style="height:100px;"></textarea>
							</div>
                            </div>
                                
                            <div class="col-md-6">
									<div class="form-group">
										<label>Currency <span style="color: red;">*</span></label>
										<select name="currency" class="form-control">
										<?php $currency = getCurrencies(); 
											if(!empty($currency)) {
												foreach ($currency as $key => $value) {		
										?>
                                        <option value="<?=$value['id'];?>"><?=$value['cur_name'];?></option>
                                        <?php } } ?>
                                        
                                    </select>
									</div>
								</div>
                                
                            <div class="col-md-6">
									<div class="form-group">
										<label>Amount <span style="color: red;">*</span></label>
										<input class="form-control" type="text" id="amount_payment" name="amount_payment">
									</div>
								</div>
                                <div class="col-md-12">
                                
                                <div class="quickpaymentcheckbox">
                            <label>
                                <input type="checkbox" name="agreed" id="agreed" value="true" required=""> I agree to the terms and conditions of Destinos India<span style="color: red;">*</span>
                            </label>
                        </div>
                                </div>


                                <!-- <div class="col-md-12">
                                    <div class="form-group" style="margin-top: 20px;">                                    
                                    <p>Captcha</p>
                                    </div>
                                </div> -->


							</div>
							<!-- /row -->
							<p class="add_top_30"><input type="submit"  name="submit" value="Pay Now" class="btn_1 rounded" id="submit-contact"></p>
						</form>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->



	<script type="text/javascript">
		
		$(document).ready(function() {
			
		jQuery(function ($) {

          var result = $('#form1').validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 2,
                        maxlength: 20,
                        lettersonly: true
                    },
                   
                    phone: {
                        required: true,
                        minlength: 10,
                        maxlength: 13,
                        digits: true
                    },
                   
                    email: {
                        required: true,
                        minlength: 6,
                        email: true
                    },

                    invoice_number: {
                        required: true,
                    },

                    payment_regarding: {
                    	required: true,
                    },

                    amount_payment: {
                    	required: true,
                    },

                },
                messages: {
                    name: {
                        required: "Please enter your name",
                        minlength: "Name should be more than 2 characters",
                        maxlength: "Name should be less than 20 characters",
                        lettersonly: "Name should contain only letters"
                    },
                    
                    phone: {
                        required: "Please enter your mobile number",
                        minlength: "Mobile number should be more than 10 characters",
                        maxlength: "Mobile number should be less than 13 characters",
                        digits: "Mobile number should contain only digits"
                    },
                    
                    email: {
                        required: "Please enter your email address",
                        minlength: "Password should be more than 6 characters",
                        email: "Please enter a valid email address"
                    },

                    invoice_number: {
                        required: "Please enter invoice number",                    
                    },

                    payment_regarding: {
                        required: "Please select payment type",                    
                    },

                    amount_payment: {
                    	required: "Please enter amount",
                    },

                },
            });

        });

		});
	</script>

<?php $this->load->view("user/common/footer.php"); ?>