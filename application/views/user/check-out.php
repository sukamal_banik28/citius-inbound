<?php $this->load->view("user/common/header.php"); ?>

	<main>
		
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Order Summary</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_80_55">
			<div class="main_title_2">
				<span><em></em></span>
				<p>Preview your order and proceed to place order.</p>
			</div>
			<div class="row">
				<div class="col-md-6 max-height-300">
				   <h5>Customer Details</h5>
				   <ul class="list-group">
					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 Name
						<span class="badge badge-info badge-pill"><?=$booking_info['client_name']; ?></span>
					  </li>
					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 Address
						<span class="badge badge-info badge-pill"><?=$booking_info['client_address']; ?></span>
					  </li>
					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 Telephone
						<span class="badge badge-info badge-pill"><?=$booking_info['client_telephone']; ?></span>
					  </li>

					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 Email
						<span class="badge badge-info badge-pill"><?=$booking_info['client_email']; ?></span>
					  </li>
					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 Message(if any)
						<span class="badge badge-info badge-pill"><?=$booking_info['client_message']; ?></span>
					  </li>

					  <?php if(!empty($booking_info['fixed_pax_range_id'])) { 
					  		$travel_pax = getTravelPax($booking_info['fixed_pax_range_id']);
					  	?>
					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 No of Pax.
						<span class="badge badge-info badge-pill"><?=$travel_pax[0]['pax_range']; ?></span>
					  </li>
					  <?php } ?>



					  <?php if(!empty($booking_info['fit_adult'])) { ?>
					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 No of Adult.
						<span class="badge badge-info badge-pill"><?=$booking_info['fit_adult']; ?></span>
					  </li>
					  <?php } ?>




					  <?php if(!empty($booking_info['fit_child'])) { ?>
					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 No of Child.
						<span class="badge badge-info badge-pill"><?=$booking_info['fit_child']; ?></span>
					  </li>
					  <?php } ?>





					</ul>
				</div>
				
				<div class="col-md-6 max-height-300">
				   <h5>Addons and Extension Details</h5>

				   <table class="table">

				   	<thead>
						<tr style="background-color: #fc5b62; color: white;">
							<th>Product Name</th>
							<th>Location</th>
							<th>Type</th>
							<th>Price</th>
						</tr>
					</thead>

					<tbody>

						<?php if( !empty($booking_details)) { 

								foreach ($booking_details as $key => $value) {

									$lang = isset($_GET['lang']) ? $_GET['lang'] : 'eng';

									if( $value['objectable_type'] == 'product' ) {
										$product = getProductByID($value['objectable_id'], $lang);
									} else {
										$product = getPackageByID($value['objectable_id'], $lang);
									}

									$addonType = getAddOnDefByID($value['sub_objectable_id']);
									$location = getLocationByObject_id($value['objectable_id']);

							?>
						<tr>
							<td><?=$product['title_'.$lang]?></td>
							<td><?=$location['city']?></td>

							<?php if(!empty($addonType['type_name_eng'])) { ?>
							<td><?=$addonType['type_name_eng']; ?></td>
							<td><?=$value['value']; ?></td>
							<?php } else { ?>

							<td colspan="2">Added Package</td>
							<?php } ?>

						</tr>
						<?php }  } ?>
					</tbody>
				   	

				   </table>
				</div>
				
				<style>
					.more-plus {
						font-size: 16px;
					}
				</style>
				
				<div class="col-md-12 mt-2 mb-2 clearfix">
					<ul class="list-group">
					  <li class="list-group-item d-flex justify-content-between align-items-center">
						 Total Amount
						<span class="badge badge-info badge-pill more-plus">Rs. <?=$booking_info['total']; ?></span>
					  </li>
					 </ul>
				</div>
				
				<div class="col-md-6">
					<button class="btn_1 rounded">Cancel</button>
				</div>
				<div class="col-md-6 text-right">
					<button class="btn_1 rounded">Make Payment</button>
				</div>
				
                     
			</div>
			<!--/row-->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->

<?php $this->load->view("user/common/footer.php"); ?>