<?php $this->load->view("user/common/header.php"); ?>

	<main>
		
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Media Gallery</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_60_35">
			<!-- <div class="main_title_2">
				<h2>Here some pictures ...</h2>
			</div> -->
			<div class="grid">
				<ul class="magnific-gallery">

					<?php if(!empty($gallery)) { 

							foreach ($gallery as $key => $value) {

						?>
					<li>
						<figure>
							<img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" alt="">
							<figcaption>
								<div class="caption-content">
									<a href="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" title="Photo title" data-effect="mfp-zoom-in">
										<i class="pe-7s-albums"></i>
										<p><?php echo $value['description']; ?></p>
									</a>
								</div>
							</figcaption>
						</figure>
					</li>
					<?php } } ?>

				</ul>
			</div>
			<!-- /grid gallery -->

				<?php echo $links; ?>
				
			<!-- <nav aria-label="...">
				<ul class="pagination pagination-sm">
					<li class="page-item disabled">
						<a class="page-link" href="#" tabindex="-1">Previous</a>
					</li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item">
						<a class="page-link" href="#">Next</a>
					</li>
				</ul>
			</nav> -->
			<!-- /pagination -->


		</div>
		<!-- /container -->

	</main>
	<!--/main-->

<?php $this->load->view("user/common/footer.php"); ?>