<?php $this->load->view("user/common/header.php"); ?>

<main>
    
    <section class="hero_in general">
      <div class="wrapper">
        <div class="container">
          <h1 class="fadeInUp"><span></span>e-Brochures</h1>
        </div>
      </div>
    </section>
    <!--/hero_in-->

    <div class="container margin_80_55">
      <div class="main_title_2">
        <span><em></em></span>
        <h2>e-Brochures</h2>
      </div>
      <div class="row">

      <?php if(!empty($ebrochure)) { 
          foreach ($ebrochure as $key => $value) {
            $file = getFileableImage( $value['id'], 'Brochure', 'brochure_file');
      ?>

        <div class="col-xl-4 col-lg-4 col-md-6">
          <a  class="grid_item">
            <figure title="Travel to the Jungle">
              <img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" class="img-fluid" alt="<?php echo $value['title']; ?>">
              <div class="info">
                <h3 class="many-wrap"><?php echo $value['title']; ?></h3>
              </div>
              <div class="read_more">
                <span class="download" data-download="<?php echo base_url().$file['file_path'].$file['file_name']; ?>" >Download</span>
                <span class="view" data-download="<?php echo base_url().$file['file_path'].$file['file_name']; ?>" >View Online</span>
              </div>
            </figure>
          </a>
        </div>

        <?php } } ?>
        
        
      </div>
      <!--/row-->
    </div>
    <!-- /container -->
  </main>
  <!--/main-->


  <script type="text/javascript">
    
    $('.download, .view').click(function(event) {
        var download = $(this).data('download');
        window.location.href = download
    });


  </script>

<?php $this->load->view("user/common/footer.php"); ?>