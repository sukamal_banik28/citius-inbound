<?php $this->load->view("user/common/header.php"); ?>

	<main>
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Citius <?php echo $lang == 'eng' ? 'Blogs' : $this->lang->line('spanish_Blog'); ?></h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_60_35">
			<div class="row">
				<div class="col-lg-9">
					<div class="bloglist singlepost">
						<p><img alt="" class="img-fluid" src="<?php echo base_url().$blog_logo['file_path'].$blog_logo['file_name']; ?>"></p>
						<h1><?php echo $blog['title_'.$lang]; ?></h1>
						<div class="postmeta">
							<ul>
								<!-- <li><a href="#"><i class="icon_folder-alt"></i> Collections</a></li> -->
								<li><i class="icon_clock_alt"></i> <?php echo date('d-M-Y', strtotime($blog['created_at'])); ?></li>
								<li><i class="icon_pencil-edit"></i> <?php echo $blog['author']; ?></li>
								<li><i class="icon_comment_alt"></i> (<?php if(!empty($comments)) { echo count($comments); } ?>) Comments</li>
							</ul>
						</div>
						<!-- /post meta -->
						<div class="post-content">
							<div class="dropcaps">
								<p><?php echo $blog['desc_'.$lang]; ?></p>
							</div>
						</div>
						<!-- /post -->
					</div>
					<!-- /single-post -->

					<div id="comments">
						<h5>Comments</h5>
						<ul>
							<!-- <li>
								<div class="avatar">
									<a href="#"><img src="img/avatar1.jpg" alt="">
									</a>
								</div>
								<div class="comment_right clearfix">
									<div class="comment_info">
										By <a href="#">Anna Smith</a><span>|</span>25/10/2019<span>|</span><a href="#">Reply</a>
									</div>
									<p>
										Nam cursus tellus quis magna porta adipiscing. Donec et eros leo, non pellentesque arcu. Curabitur vitae mi enim, at vestibulum magna. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sit amet sem a urna rutrumeger fringilla. Nam vel enim ipsum, et congue ante.
									</p>
								</div>
								<ul class="replied-to">
									<li>
										<div class="avatar">
											<a href="#"><img src="img/avatar2.jpg" alt="">
											</a>
										</div>
										<div class="comment_right clearfix">
											<div class="comment_info">
												By <a href="#">Anna Smith</a><span>|</span>25/10/2019<span>|</span><a href="#">Reply</a>
											</div>
											<p>
												Nam cursus tellus quis magna porta adipiscing. Donec et eros leo, non pellentesque arcu. Curabitur vitae mi enim, at vestibulum magna. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sit amet sem a urna rutrumeger fringilla. Nam vel enim ipsum, et congue ante.
											</p>
											<p>
												Aenean iaculis sodales dui, non hendrerit lorem rhoncus ut. Pellentesque ullamcorper venenatis elit idaipiscingi Duis tellus neque, tincidunt eget pulvinar sit amet, rutrum nec urna. Suspendisse pretium laoreet elit vel ultricies. Maecenas ullamcorper ultricies rhoncus. Aliquam erat volutpat.
											</p>
										</div>
										<ul class="replied-to">
											<li>
												<div class="avatar">
													<a href="#"><img src="img/avatar2.jpg" alt="">
													</a>
												</div>
												<div class="comment_right clearfix">
													<div class="comment_info">
														By <a href="#">Anna Smith</a><span>|</span>25/10/2019<span>|</span><a href="#">Reply</a>
													</div>
													<p>
														Nam cursus tellus quis magna porta adipiscing. Donec et eros leo, non pellentesque arcu. Curabitur vitae mi enim, at vestibulum magna. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sit amet sem a urna rutrumeger fringilla. Nam vel enim ipsum, et congue ante.
													</p>
													<p>
														Aenean iaculis sodales dui, non hendrerit lorem rhoncus ut. Pellentesque ullamcorper venenatis elit idaipiscingi Duis tellus neque, tincidunt eget pulvinar sit amet, rutrum nec urna. Suspendisse pretium laoreet elit vel ultricies. Maecenas ullamcorper ultricies rhoncus. Aliquam erat volutpat.
													</p>
												</div>
											</li>
										</ul>
									</li>
								</ul>
							</li> -->

							<?php if(!empty($comments)) { 
								foreach ($comments as $key => $value) {
									
								?>

							<li>
								<div class="avatar">
									<a href="#"><img src="<?php echo base_url().'assets/front-end/img/personal_default_avatar.png'; ?>" alt="">
									</a>
								</div>

								<div class="comment_right clearfix">
									<div class="comment_info">
										By <a href="#"><?php echo $value['name']; ?></a><span>|</span><?php echo date('d/m/Y', strtotime($value['created_at'])) ?>
									</div>
									<p>
										<?php echo $value['review']; ?>
									</p>
								</div>
							</li>

							<?php } } ?>


						</ul>
					</div>

					<hr>

					<h5><?php echo $lang == 'eng' ? 'Leave a Comment' : $this->lang->line('leave_a_comment'); ?></h5>
					<form>

						<div class="form-group">
							<input type="text" name="name" id="name_review" class="form-control" placeholder="Name">
						</div>

						<div class="form-group">
							<input type="text" name="email" id="email_review" class="form-control" placeholder="Email">
						</div>
						
						<div class="form-group">
							<textarea class="form-control" name="comments" id="review_text" rows="6" placeholder="Message Below"></textarea>
						</div>
						<div class="form-group">
							<button type="button" id="submit-review" class="btn_1 rounded add_bottom_30"><?php echo $lang == 'eng' ? 'Submit' : $this->lang->line('spanish_Submit'); ?></button>
						</div>
					</form>
				</div>
				<!-- /col -->

				<?php $this->load->view('user/common/right-panel-blog.php'); ?>

			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->

	<script type="text/javascript">
		$(document).ready(function() {
			$('#submit-review').click(function(event) {

				var result = confirm('Do you want to submit this review? Press OK to confirm.');
				if(result) 
				{
				
					var name = $('#name_review').val();
					var email = $('#email_review').val();
					var review = $('#review_text').val();

					if( name == '' || email == '' || review == '' )
					{
						alert('Please fill all the fileds');
					} else {

						$.ajax({
							url: '<?php echo base_url().'HomeController/saveReviewByAjax' ?>',
							type: 'POST',
							data: {
								name: name,
								email: email,
								review: review,
								reviewable_id: <?php echo $blog['id']; ?>,
								reviewable_type: 'Blog'
							},
						})
						.done(function() {
							alert('Thank you for your valuable feedback. To view your feedback in the site, it will take 72 hours.');
						})
						.fail(function() {
							console.log("error");
						})
						.always(function() {
							$('#name_review').val('');
							$('#email_review').val('');
							$('#review_text').val('');
						});

					} // end else

				} // end result

			});
		});
	</script>

<?php $this->load->view("user/common/footer.php"); ?>