<?php $this->load->view("user/common/header.php"); 
	
	$user_id = $this->session->userdata('user_id');

?>

	<main>
		
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>My Profile</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="container margin_80_55">
			<div class="row profile">
				
				<?php $this->load->view('user/common/left-panel-profile'); ?>

				<div class="col-md-9">
					<div class="profile-content">
						<ul class="bullets">
							<li><strong>Name</strong> <?php echo $user['name']; ?></li>
							<li><strong>Email</strong> <?php echo $user['email']; ?></li>
							<li><strong>Mobile</strong> <?php echo $user['phone']; ?></li>
						</ul>
					</div>
				</div>
			</div>
			<!--/row-->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->

<?php $this->load->view("user/common/footer.php"); ?>