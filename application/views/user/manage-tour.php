<?php $this->load->view("user/common/header.php"); ?>

	<main>
		
		<section class="hero_in tours">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span><?php echo $lang == 'eng' ? 'Tour Listing' : $this->lang->line('Tour_Listing'); ?></h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->
		
		<div class="filters_listing sticky_horizontal">
			<div class="container">
				<ul class="clearfix">
					<li>
						<div class="switch-field">

							<input type="radio" data-filter="*" <?php echo $viewd_flag == 'all' ? 'checked' : '';  ?> >
							<label for="all" class="flag" data-value="all" ><?php echo $lang == 'eng' ? 'Tour Listing' : $this->lang->line('all'); ?></label>
							<input type="radio" data-filter=".popular" <?php echo $viewd_flag == 'popular' ? 'checked' : '';  ?> >
							<label for="popular" class="flag" data-value="popular"><?php echo $lang == 'eng' ? 'Popular' : $this->lang->line('popular'); ?></label>
							<input type="radio" data-filter=".latest" <?php echo $viewd_flag == 'latest' ? 'checked' : '';  ?> >
							<label for="latest" class="flag" data-value="latest"><?php echo $lang == 'eng' ? 'Latest' : $this->lang->line('latest'); ?></label>

						</div>
					</li>
				</ul>
			</div>
			<!-- /container -->
		</div>
		<!-- /filters -->

		<div class="container margin_60_35">
			<div class="row">
				<aside class="col-lg-3" id="sidebar">
					<form method="post" action="<?php echo base_url().'home'; ?>">
						<div class="custom-search-input-2 inner-2">
							<div class="form-group">
								<input class="form-control" type="text" name="search_string" placeholder="<?php echo $lang == 'eng' ? 'Ex. North India, Wildlife...' : $this->lang->line('spanish_placeholder'); ?>" style="width: 100%">
								<i class="icon_search"></i>
							</div>
							<input type="submit" name="search" class="btn_search" value="<?php echo $lang == 'eng' ? 'Search' : $this->lang->line('spanish_Search'); ?>">
						</div>
					</form>
					<!-- /custom-search-input-2 -->
					<div id="filters_col">
						<a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">Filters </a>
						<div class="collapse show" id="collapseFilters">
							<div class="filter_type">
								<h6>Price Range</h6>
								<input type="text" id="range" name="range" value="">
							</div>
							<div class="filter_type">
								<h6>Rating</h6>
									<ul>
									<li>
										<label>Superb: 9+</label>
										<input type="checkbox" class="js-switch" checked>
									</li>
									<li>
										<label>Very good: 8+</label>
										<input type="checkbox" class="js-switch">
									</li>
									<li>
										<label>Good: 7+</label>
										<input type="checkbox" class="js-switch">
									</li>
									<li>
										<label>Pleasant: 6+</label>
										<input type="checkbox" class="js-switch">
									</li>
									<li>
										<label>No rating</label>
										<input type="checkbox" class="js-switch">
									</li>
								</ul>
							</div>
						</div>
						<!--/collapse -->
					</div>
					<!--/filters col-->
				</aside>
				<!-- /aside -->

				<div class="col-lg-9">
					<div class="isotope-wrapper">
						<div class="row">

							<?php if(!empty($packages)) { 
 										foreach ($packages as $key => $value) {
 											
								?>

							<div class="col-md-6 isotope-item popular">
								<div class="box_grid">

									<?php if($value['is_new'] == 1) { ?>
									<!-- Ribbon -->
									<div class="ribbon-wrapper">
										<div class="ribbon">NEW</div>
									  </div> <!-- /ribbon -->
									<?php } ?>

									<figure>
										<a href="<?php echo base_url().'packages/'.$value['seo_url'].'?lang='.$lang; ?>"><img src="<?php echo base_url().$value['file_path'].$value['file_name']; ?>" class="img-fluid" alt="" width="800" height="533">
										<div class="read_more">
											<span><?php echo $value['title_'.$lang] ?></span>
										</div>
									</a>
									</figure>
									<div class="wrapper">
										<h3><a href="<?php echo base_url().'packages/'.$value['seo_url'].'?lang='.$lang; ?>"><?php echo $value['title_'.$lang] ?></a></h3>
										<p><?php echo substr($value['short_desc_'.$lang], 0, 50); ?></p>
										<a href="<?php echo base_url().'packages/'.$value['seo_url'].'?lang='.$lang; ?>">ReadMore</a>
									</div>


									<?php

										// ID 2 measn USD and 1 means EUR
										$currency_id = $lang == 'eng' ? 2 : 1;
										$cur_symbol = $lang == 'eng' ? '$' : '€';

										$price_details = getPackageMinValueForCurrency($currency_id, $value['id'], 'package' );

										// echo '<pre>';
										// print_r($price_details);

										if(!empty($price_details)) {
											$travel_pax = getTravelPaxDefByID($price_details['travel_pax_id']);
										} else {
											$travel_pax = '';
										}

									?>

									<ul>
										<li><div class="score"><i class="icon_clock_alt"></i> <?php echo $value['nights']; ?>N <?php echo $value['days']; ?>D </div></li>
										<li><div class="score"><span>Package<em> Price for <?php echo is_array($travel_pax) ? $travel_pax['pax_range'].'pax' : $travel_pax; ?></em></span><strong><?php echo is_array($price_details) ? $cur_symbol.' '.$price_details['value'] : '<a style="color:#fff" href='.base_url('contact-us').'>Contact for prices</a>'; ?></strong></div></li>
									</ul>
								</div>
							</div>

							<?php  } } else { echo '<p>No Package Found.</p>'; }?>

						</div>
						<!-- /row -->
				</div>
				<!-- /isotope-wrapper -->
					


				<!-- <p class="text-center"><a href="#0" class="btn_1 rounded add_top_30">Load more</a></p> -->

					<?php echo $links; ?>
					
					<!-- <nav aria-label="...">
						<ul class="pagination pagination-sm">
							<li class="page-item disabled">
								<a class="page-link" href="#" tabindex="-1">Previous</a>
							</li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item">
								<a class="page-link" href="#">Next</a>
							</li>
						</ul>
					</nav> -->
					<!-- /pagination -->

					
				</div>
				<!-- /col -->
			</div>		
		</div>
		<!-- /container -->
		
		<div class="bg_color_1">
			<div class="container margin_60_35">
				<div class="row">
					<div class="col-md-4 m-auto">
						<a href="<?php echo base_url().'contact-us?lang=eng' ?>" class="boxed_list">
							<i class="pe-7s-help2"></i>
							<h4>Need Help? Contact us</h4>
						</a>
					</div>
					<!-- <div class="col-md-4">
						<a href="#" class="boxed_list">
							<i class="pe-7s-wallet"></i>
							<h4>Payments</h4>
							<p>Qui ea nemore eruditi, magna prima possit eu mei.</p>
						</a>
					</div>
					<div class="col-md-4">
						<a href="#" class="boxed_list">
							<i class="pe-7s-note2"></i>
							<h4>Cancel Policy</h4>
							<p>Hinc vituperata sed ut, pro laudem nonumes ex.</p>
						</a>
					</div> -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
		
	</main>
	<!--/main-->

	<script type="text/javascript">

		$('.flag').click(function(event) {
			flag = $(this).data("value");
			var href = "<?php echo base_url().$this->uri->uri_string(); ?>" + "<?php echo '?lang='. $lang; ?>" + "&flag=" + flag;
			window.location = href;
		});

			
	</script>

<?php $this->load->view("user/common/footer.php"); ?>