<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sign Up - Citius</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/front-end/'; ?>img/favicon.ico" type="image/x-icon">

    <!-- BASE CSS -->
    <link href="<?php echo base_url().'assets/front-end/'; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/front-end/'; ?>css/style.css" rel="stylesheet">
	<link href="<?php echo base_url().'assets/front-end/'; ?>css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url().'assets/front-end/'; ?>css/custom.css" rel="stylesheet">

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA53RbrL34Vo9SA8EsuZHMsUKyXbXeebAg&libraries=places"></script>

</head>


    <body id="register_bg">
    
    <nav id="menu" class="fake_menu"></nav>
    
    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div>
    <!-- End Preload -->
    
    <div id="login">
        <aside>
            <figure>
                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url().'assets/front-end/img/citius-logo.png'; ?>" width="150" height="36" data-retina="true" alt="" class="logo_sticky"></a>
            </figure>
            <form autocomplete="off" method="post" action="">
                <div class="form-group">
                    <label>Your Full Name*</label>
                    <input class="form-control" type="text" name="name" value="<?php echo set_value('name'); ?>">
                    <span style="color: red; float: left;"><?php echo form_error('name'); ?></span>
                    <i class="ti-user"></i>
                </div>
                <div class="form-group">
                    <label>Your Phone Number*</label>
                    <input class="form-control" type="number" min="1" name="phone" value="<?php echo set_value('phone'); ?>">
                    <span style="color: red; float: left;"><?php echo form_error('phone'); ?></span>
                    <i class="ti-mobile"></i>
                </div>
                <div class="form-group">
                    <label>Your Email*</label>
                    <input class="form-control" type="email" name="email" value="<?php echo set_value('email'); ?>">
                    <span style="color: red; float: left;"><?php echo form_error('email'); ?></span>
                    <i class="icon_mail_alt"></i>
                </div>

                <div class="form-group">
                    <label>Your Address</label>
                    <input class="form-control" type="text" name="address" id="locationTextField" value="<?php echo set_value('address'); ?>">
                    <i class="ti-location-pin"></i>
                </div>

                <div class="form-group">
                    <label>Your password*</label>
                    <input class="form-control" type="password" id="password1" name="password" >
                    <span style="color: red; float: left;"><?php echo form_error('password'); ?></span>
                    <i class="icon_lock_alt"></i>
                </div>
                <div class="form-group">
                    <label>Confirm password*</label>
                    <input class="form-control" type="password" id="password2" name="confirm_password" >
                    <span style="color: red; float: left;"><?php echo form_error('confirm_password'); ?></span>
                    <i class="icon_lock_alt"></i>
                </div>
                <div id="pass-info" class="clearfix"></div>
                <input type="submit" name="submit" value="Sign Up" class="btn_1 rounded full-width add_top_30">
                <div class="text-center add_top_10">Already have an acccount? <strong><a href="<?php echo base_url().'inbound-admin/login?lang='.$lang; ?>">Sign In</a></strong></div>
            </form>
            <div class="copy">© <?php echo date('Y'); ?> Citius</div>
        </aside>
    </div>
    <!-- /login -->

    <script type="text/javascript">
        function init() {
        var input = document.getElementById('locationTextField');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }   
    google.maps.event.addDomListener(window, 'load', init);
    </script>

<!-- COMMON SCRIPTS -->
    <script src="<?php echo base_url().'assets/front-end/'; ?>js/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url().'assets/front-end/'; ?>js/common_scripts.js"></script>
    <script src="<?php echo base_url().'assets/front-end/'; ?>js/main.js"></script>
  
</body>
</html>