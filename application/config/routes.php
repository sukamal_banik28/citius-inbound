<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


/* inbound-admin routes */
$route['inbound-admin/login'] = 'AuthController/login';
$route['inbound-admin/logout'] = 'AuthController/logout';

$route['inbound-admin/dashboard'] = 'AdminController/dashboard';
$route['inbound-admin/add-users'] = 'AdminController/addUsers';
$route['inbound-admin/manage-users'] = 'AdminController/manageUsers';
$route['inbound-admin/manage-users/user-id/(:num)/update'] = 'AdminController/updateUser/$1';


$route['inbound-admin/manage-categories'] = 'CategoryController/manageCategories';
$route['inbound-admin/manage-category/category-id/(:num)/update'] = 'CategoryController/updateCategory/$1';


$route['inbound-admin/add-package'] = 'PackageController/addNewPackage';
$route['inbound-admin/manage-packages'] = 'PackageController/managePackage';
$route['inbound-admin/manage-packages/package-id/(:num)/update'] = 'PackageController/updatePackage/$1';
$route['inbound-admin/manage-packages/package-id/(:num)/manage-itinerary'] = 'PackageController/packageItinerary/$1';
$route['inbound-admin/manage-packages/package-id/(:num)/add-itinerary'] = 'PackageController/addPackageItinerary/$1';
$route['inbound-admin/manage-packages/itinerary-id/(:num)/update'] = 'PackageController/updateItinerary/$1';
$route['inbound-admin/manage-deleted-packages'] = 'PackageController/manageDeletedPackage';


$route['inbound-admin/add-product'] = 'ProductController/addNewProduct';
$route['inbound-admin/manage-products'] = 'ProductController/manageProduct';
$route['inbound-admin/manage-products/product-id/(:num)/update'] = 'ProductController/updateProduct/$1';

$route['inbound-admin/add-testimonial'] = 'TestimonialController/addTestimonial';
$route['inbound-admin/manage-testimonials'] = 'TestimonialController/manageTestimonial';
$route['inbound-admin/manage-testimonials/testimonial-id/(:num)/update'] = 'TestimonialController/updateTestimonial/$1';

$route['inbound-admin/company-details'] = 'CompanyController/UpdateCompanyDetails';

$route['inbound-admin/add-pages'] = 'PageController/addPages';
$route['inbound-admin/manage-pages'] = 'PageController/managePages';
$route['inbound-admin/manage-pages/page-id/(:num)/update'] = 'PageController/updatePages/$1';

$route['inbound-admin/add-banner'] = 'BannerController/addNewBanners';
$route['inbound-admin/manage-banners'] = 'BannerController/manageBanners';
$route['inbound-admin/manage-banners/banner-id/(:num)/update'] = 'BannerController/updateBanners/$1';

$route['inbound-admin/manage-enquire'] = 'EnquiryController/manageEnquiry';
$route['inbound-admin/manage-enquire/enquire-id/(:num)/update'] = 'EnquiryController/updateEnquiry/$1';

$route['inbound-admin/add-blogs'] = 'BolgController/addBlogs';
$route['inbound-admin/manage-blogs'] = 'BolgController/manageBlogs';
$route['inbound-admin/manage-blogs/blogs-id/(:num)/update'] = 'BolgController/updateBlogs/$1';

$route['inbound-admin/manage-reviews'] = 'ReviewController/manageReviews';
$route['inbound-admin/manage-reviews/review-id/(:num)/update'] = 'ReviewController/updateReview/$1';


$route['inbound-admin/add-hideaways'] = 'HideawaysController/addHideaways';
$route['inbound-admin/manage-hideaways'] = 'HideawaysController/manageHideaways';
$route['inbound-admin/manage-hideaways/hideaways-id/(:num)/update'] = 'HideawaysController/updateHideaways/$1';


$route['inbound-admin/add-gallery'] = 'GalleryController/addGallery';
$route['inbound-admin/manage-gallery'] = 'GalleryController/manageGallery';
$route['inbound-admin/manage-gallery/gallery-id/(:num)/delete'] = 'GalleryController/deleteGallery/$1';
$route['inbound-admin/manage-gallery/gallery-id/(:num)/update'] = 'GalleryController/updateGallery/$1';

$route['inbound-admin/add-addon'] = 'MasterController/addAddon';
$route['inbound-admin/manage-addon'] = 'MasterController/manageAddon';
$route['inbound-admin/manage-addon/addon-id/(:num)/update'] = 'MasterController/updateAddon/$1';


$route['inbound-admin/add-ebrochure'] = 'MasterController/addEbrochure';
$route['inbound-admin/manage-ebrochure'] = 'MasterController/manageEbrochure';
$route['inbound-admin/manage-ebrochure/ebrochure-id/(:num)/update'] = 'MasterController/updateEbrochure/$1';


$route['inbound-admin/booking-history'] = 'MasterController/bookingHistory';
$route['inbound-admin/booking-history/booking-id/(:num)/update'] = 'MasterController/updateBookingHistory/$1';


$route['inbound-admin/add-cities'] = 'MasterController/addCities';
$route['inbound-admin/manage-cities'] = 'MasterController/manageCities';
$route['inbound-admin/manage-cities/city-id/(:num)/update'] = 'MasterController/updateCities/$1';


$route['inbound-admin/add-offline-payment'] = 'PaymentController/addOfflinePayment';
$route['inbound-admin/manage-offline-payments'] = 'PaymentController/manageOfflinePayments';
$route['inbound-admin/manage-offline-payments/payment-id/(:num)/update'] = 'PaymentController/updatePayment/$1';


$route['inbound-admin/add-amenities'] = 'MasterController/addAmenities';
$route['inbound-admin/manage-amenities'] = 'MasterController/manageAmenities';
$route['inbound-admin/manage-amenities/amenities-id/(:num)/update'] = 'MasterController/updateAmenities/$1';


$route['inbound-admin/add-sociallinks'] = 'MasterController/addSocialLinks';
$route['inbound-admin/manage-sociallinks'] = 'MasterController/manageSocialLinks';
$route['inbound-admin/manage-sociallinks/sociallinks-id/(:num)/update'] = 'MasterController/updateSocialLinks/$1';


$route['inbound-admin/add-province'] = 'MasterController/addProvince';
$route['inbound-admin/manage-province'] = 'MasterController/manageProvince';
$route['inbound-admin/manage-province/province-id/(:num)/update'] = 'MasterController/updateProvince/$1';


$route['inbound-admin/add-partner'] = 'PartnerController/addNewPartners';
$route['inbound-admin/manage-partners'] = 'PartnerController/managePartners';
$route['inbound-admin/manage-partners/partner-id/(:num)/update'] = 'PartnerController/updatePartners/$1';


/* inbound-user routes */
$route['home'] = 'HomeController/home';
$route['packages/(:any)'] = 'HomeController/packageDetails/$1';
$route['products/(:any)'] = 'HomeController/productDetails/$1';
$route['user-signup'] = 'HomeController/signup';
$route['manage-tour/(:any)'] = 'HomeController/manageTour/$1';
$route['manage-tour/search-result/(:any)'] = 'HomeController/searchResult/$1';

// $route['manage-tour/search-result/(:any)/page/(:num)'] = 'HomeController/searchResult/$1/$2';
// $route['manage-tour/(:any)/page/(:num)'] = 'HomeController/manageTour/$1/$2';

$route['about-us'] = 'HomeController/aboutUs';
$route['contact-us'] = 'HomeController/contactUs';
$route['citius-gallery'] = 'HomeController/gallery';
$route['citius-ebrochure'] = 'HomeController/ebrochure';
$route['citius-destinations'] = 'HomeController/destinations';
$route['citius-quickpayment'] = 'HomeController/quickPayment';
$route['citius-package-summary/booking-id/(:num)/check-out'] = 'HomeController/paymentCheckout/$1';

$route['blogs'] = 'HomeController/manageBlogs';
$route['blogs/(:any)'] = 'HomeController/blogDetails/$1';
$route['blogs/category/(:any)'] = 'HomeController/manageCategoryBlogs/$1';
$route['blogs/tags/(:any)'] = 'HomeController/manageTagsBlogs/$1';

$route['user/manage-profile'] = 'UserController/manageProfile';
$route['user/edit-profile/id/(:num)'] = 'UserController/updateProfile/$1';
$route['user/manage-enquire'] = 'UserController/manageEnquire';
$route['user/manage-password'] = 'UserController/managePassword';

$route['savePrimaryBookingInfo'] = 'HomeController/savePrimaryBookingInfo';

$route['email'] = 'HomeController/emailTest';
