<?php

// Header Menu
$lang['spanish_home'] = 'casa'; 
$lang['spanish_About'] = 'Acerca de';
$lang['spanish_Blog'] = 'Blogs';
$lang['spanish_Destinations'] = 'Destinos'; 
$lang['spanish_Contact_Us'] = 'Contáctenos';

// Home Page
$lang['spanish_Find_Unique_Experiences'] = 'Encuentra experiencias únicas';
$lang['spanish_Expolore'] = 'Expolore los mejores tours de entrada, tours de vida silvestre en todo el país';
$lang['spanish_placeholder'] = 'Ex. India del norte, vida silvestre ...';
$lang['spanish_Our_Unique_Hideaways'] = 'Nuestros escondites únicos';
$lang['spanish_Most_popular_tours'] = 'Los más populares, tours indios están aquí';

$lang['spanish_Address'] = 'Dirección';
$lang['spanish_Email_address'] = 'Dirección de correo electrónico'; 
$lang['spanish_Contacts_info'] = 'Información de contactos';
$lang['spanish_Send_a_message'] = 'Enviar un mensaje';
$lang['spanish_Cultural_Heritage'] = 'Patrimonio cultural'; 

$lang['spanish_View_all_tours'] = 'Ver todos los tours';
$lang['spanish_Nature_and_Wildlife'] = 'Naturaleza y vida silvestre'; 
$lang['spanish_Most_popular_Indian_tours_are_here'] = 'Los más populares, tours indios están aquí';
$lang['spanish_Colors_of_India'] = 'Colores de la india';

$lang['spanish_Most_popular_Indian_tours_are_here'] = 'Los más populares, tours indios están aquí'; 
$lang['spanish_Useful_links'] = 'Enlaces útiles';

$lang['Follow_us'] = 'Síguenos';
$lang['spanish_Sign_In'] = 'Registrarse'; 
$lang['spanish_Contact_with_Us'] = 'Contactar con nosotros';
$lang['Newsletter'] = 'Hoja informativa';
$lang['spanish_About_Us'] = 'Sobre nosotros'; 
$lang['Read_More'] = 'Lee mas';
$lang['spanish_Accreditations'] = 'Acreditaciones';



//about us page
$lang['spanish_Why_Choose_Citius'] = '¿Por qué elegir Citius?';
$lang['spanish_Lorem_ipsum_dolor_sit_amet_homero_erroribus_in_cum'] = 'Lorem ipsum dolor sit amet, errores homero adelante con ella.'; 
$lang['spanish_Team_of_dedicated_consultants'] = 'Equipo de consultores dedicados.';
$lang['spanish_Relationship_and_Support_through_Account_Managers'] = 'Relación y soporte a través de gestores de cuentas.';
$lang['spanish_Travel_tips'] = 'Consejos de viaje'; 
$lang['spanish_24X7_backup_Support'] = 'Soporte de respaldo 24 x 7';
$lang['spanish_Client_Satisfaction_Survey'] = 'Encuesta de satisfacción del cliente';
$lang['spanish_Error_Tracking_and_quick_resolution_to_queries'] = 'Seguimiento de errores y resolución rápida a consultas.';
$lang['spanish_Our_Origins_and_Story'] = 'Nuestros orígenes e historia';
$lang['Lorem_ipsum_dolor_sit_amet_homero_erroribus_in_cum'] = 'Lorem ipsum dolor sit amet, errores homero adelante con ella.';

//destinations
$lang['Tour_Listing'] = 'Listado del Tour';
$lang['all'] = 'Todos';
$lang['popular'] = 'Popular';
$lang['latest'] = 'Último';
$lang['spanish_Filters'] = 'Filtros';
$lang['spanish_Price_Range'] = 'Rango de precios';
$lang['spanish_Rating'] = 'Clasificación';
$lang['Superb'] = 'Magnífica';
$lang['spanish_Very_good'] = 'Muy bien';
$lang['spanish_Good'] = 'Buena';
$lang['spanish_Pleasant'] = 'Agradable';
$lang['spanish_No_rating'] = 'Sin evaluar';
$lang['spanish_Count'] = 'contar';
$lang['spanish_Reviews'] = 'Opiniones';

$lang['spanish_Need_Help_Contact_us'] = '¿Necesitas ayuda? Contáctenos';
$lang['spanish_Cum_appareat_maiestatis_interpretaris_et_et_sit'] = 'Cuando aparecen no interpretar la dignidad y la';
$lang['spanish_Payments'] = 'Pagos';

// Package Details
$lang['spanish_Description'] = 'Descripción';
$lang['spanish_Itinerary'] = "Itinerario";
$lang['spanish_Day'] = "Día";
$lang['spanish_Book_Now'] = "Reservar ahora";
$lang['spanish_Feedback'] = "Realimentación";
$lang['spanish_Quick_Enquiry'] = "Consulta rápida";
$lang['spanish_package_code'] = "Código del paquete";
$lang['spanish_enquire'] = "Preguntar ahora";
$lang['spanish_package_price'] = "Precio del paquete";
$lang['spanish_addons'] = "Add-ons";
$lang['spanish_gallery'] = "Galería";
$lang['spanish_extensions'] = "Extensions";
$lang['spanish_brochure'] = "Folletos";
$lang['spanish_tours_packages'] = "Paquetes turísticos";




//Blog
$lang['spanish_Search'] = 'Buscar';
$lang['spanish_Popular_Tags'] = 'Etiquetas Populares';
$lang['spanish_Tours'] = 'Excursiones';
$lang['leave_a_comment'] = 'Deja un comentario';
$lang['blog_category'] = 'Categoría de blog';
$lang['Recent_Posts'] = 'Mensajes recientes';


//contact us
$lang['spanish_Contact_Us'] = 'Contáctenos';
$lang['spanish_Address'] = 'Dirección';
$lang['spanish_Email_address'] = 'Dirección de correo electrónico';
$lang['spanish_Contacts_info'] = 'Información de contactos';
$lang['spanish_Send_a_message'] = 'Enviar un mensaje';
$lang['spanish_Name'] = 'Nombre';
$lang['spanish_Email'] = 'Email';
$lang['spanish_Telephone'] = 'Teléfono';
$lang['spanish_Your_Message'] = 'Tu mensaje (Min 140, Max 500 caracteres)';
$lang['spanish_characters_remaining'] = 'caracteres restantes';
$lang['spanish_Submit'] = 'Enviar';

$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";


?>