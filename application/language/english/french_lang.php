<?php

$lang['french_Language_Selection'] = "Selección de idioma";

/* Add Package */
$lang['french_Add_Package'] = "Ajouter un paquet";
$lang['french_Package_Id_CRM'] = "Id de package CRM";
$lang['french_Package_Title'] = "Titre du colis";
$lang['french_Package_Short_Description'] = "Description courte du paquet";
$lang['french_Package_Long_Description'] = "Description longue du paquet";
$lang['french_Package_Category'] = "Catégorie de colis";
$lang['french_Package_Sub_Category'] = "Paquet Sous Catégorie";
$lang['french_seat'] = "siège";
$lang['french_Total_number_of_Days'] = "Nombre total de jours";
$lang['french_Total_number_of_Nights'] = "Nombre total de nuits";
$lang['french_Inventory'] = "Inventaire";
$lang['french_City_of_Departure'] = "Ville de départ";
$lang['french_City_of_Arrival'] = "Ville d'arrivée";
$lang['french_Validity_Date_From'] = "Date de validité du";
$lang['french_Cancellation_Policy'] = "Politique d'annulation";
$lang['french_Package_Deletion_Flag'] = "Indicateur de suppression de paquet";
$lang['french_Offer_Price'] = "Prix ​​de l'offre";
$lang['french_Sales_Price'] = "Prix ​​de vente";
$lang['french_GST_in'] = "TPS en";
$lang['french_Feature_Package'] = "Forfait caractéristique";
$lang['french_Citius_Touch'] = "Citius Touch";
$lang['french_Package_Image'] = "Image du colis";
$lang['french_Add_Tags'] = "Ajouter des balises";
$lang['french_SEO_URL'] = "URL de référencement";
$lang['french_Meta_Key'] = "Clé méta";
$lang['french_Meta_Description'] = "Meta Description";
$lang['french_cancel'] = "Annuler";
$lang['french_save'] = "enregistrer";

$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";


?>